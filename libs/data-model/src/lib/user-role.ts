export enum UserRole {
    GlobalAdmin = 'GlobalAdmin',
    LocalAdmin = 'LocalAdmin',
    Teacher = 'Teacher',
    Clinician = 'Clinician',
    Child = 'Child',
    Parent = 'Parent',
}
