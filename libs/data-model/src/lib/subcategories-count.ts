export interface SubcategoriesCount {
    challenges: number;
    activities: number;
    news: number;
    questionnaires: number;
}
