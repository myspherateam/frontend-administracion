import { SubcategoriesCount } from './subcategories-count';

export interface CategoriesCount {
    count: number;
    subcategories: SubcategoriesCount;
}
