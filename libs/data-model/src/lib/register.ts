export interface Register {
    username: string;

    password: string;

    roles: string[];

    isAlreadyRegistered: boolean;
}
