import { TopUser } from "./top-user";

export interface Ranking {
    myPosition: number;
    myValue: number;
    totalUsers: number;
    topUsers: TopUser[];
}
