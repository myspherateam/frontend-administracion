import { AssignedToIds } from './assigned-to-ids';
import { ChallengeDefinition } from './challenge-definition';

export interface ChallengeProgramming extends ChallengeDefinition {

    id: string;
    definitionId: string;
    programedById: string;
    startDatetime: string;
    finishDatetime: string;
    visibleDatetime: string;
    overridesIds: string[];
    assignedToIds: AssignedToIds;
}
