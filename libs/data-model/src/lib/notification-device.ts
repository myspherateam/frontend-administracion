export interface NotificationDevice{
    userId:String,
    token:String,
}

export interface UpdateNotificationDevice extends NotificationDevice{
    oldToken:String
}
