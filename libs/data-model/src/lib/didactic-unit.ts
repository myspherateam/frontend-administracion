import { DidacticUnitChallenge } from './didactic-unit-challenge';

export interface DidacticUnit {
    id: string;
    name: string;
    duration?: number;
    challenges?: DidacticUnitChallenge[];
    badgeId?: string;
}
