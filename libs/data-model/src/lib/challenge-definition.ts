import { UntypedFormControl } from '@angular/forms';

import { Location } from './location';
import { Point } from './point';

export const enum ChallengeLockdownTypes {
    ALL = 'todos',
    COMPATIBLE = 'lockDown',
    UNCOMPATIBLE = 'noLockDown',
};
export interface ChallengeDefinition {

    id: string;
    title: string;
    description: string;
    explanation: string;
    preWarn: boolean;
    periodDays: number;
    periodAmount: number;
    category: string;
    subcategory: string;
    points: Point;

    /**
     * AssignedFor can have this values: parent | child
     */
    assignedFor: string[];
    type: string;
    minAge: number;
    maxAge: number;
    lockDown: boolean;
    didacticUnitsIds: string[];

    // Type: Physical
    steps: number;

    // Type: Authority
    childRelation: string[];
    specificIds: string[];

    // Type: GPS
    location: Location;
    radiusMeters: number;

    // Type: Questionnaire
    questionnaire?: string;
    questionnaireCorrectAnswer?: string;

    // Type: News
    news: string;

    // Intern vals for selected challenges in didactic unit creation and modification
    selected?: boolean;
    startDay: UntypedFormControl;
    finishDay: UntypedFormControl;
    startOffset?: number;
    endOffset?: number;
}
