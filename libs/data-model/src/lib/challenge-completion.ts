import { ChallengeProgramming } from './challenge-programming';
import { CompletedByParents } from './completed-by-parents';
import { Location } from './location';

export interface ChallengeCompletion extends ChallengeProgramming {

    id: string;
    definitionId: string;
    programmingId: string;
    forId: string;
    displayName: string;
    completionDatetime: string;
    completed: boolean;
    completedByParents: CompletedByParents[];
    periodStartDatetime: string;
    periodFinishDatetime: string;
    important: boolean;

    active: boolean;

    awardedPoints: number;

    // Type: Physical
    userSteps: number;

    // Type: Authority
    approvedById: string;

    // Type: GPS
    userLocation: Location;
    precision: number;

    // Type: Questionnaire
    questionnaireAnswer: string;

    // Type: News
    newsAnswer: string;
}
