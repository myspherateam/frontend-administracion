import { displayNameComponents } from './user';

describe('User entity', () => {

  describe('displayNameComponents method', () => {
    it('should parse only the number part', () => {
      const displayName = '1234';
      const parsed = displayNameComponents(displayName);
      expect(parsed).toBeDefined();
      expect(parsed).toHaveLength(3);
      expect(parsed[0]).toBeUndefined();
      expect(parsed[1]).toBeUndefined();
      expect(parsed[2]).toBe('1234');
    });

    it('should parse both text and number part', () => {
      const displayName = 'SharkBlue1234';
      const parsed = displayNameComponents(displayName);
      expect(parsed).toBeDefined();
      expect(parsed).toHaveLength(3);
      expect(parsed[0]).toBe('Shark');
      expect(parsed[1]).toBe('Blue');
      expect(parsed[2]).toBe('1234');
    });

    it('should return undefined components when empty displayname provided', () => {
      const displayName = '';
      const parsed = displayNameComponents(displayName);
      expect(parsed).toBeDefined();
      expect(parsed).toHaveLength(3);
      expect(parsed[0]).toBeUndefined();
      expect(parsed[1]).toBeUndefined();
      expect(parsed[2]).toBeUndefined();
    });
  });
});
