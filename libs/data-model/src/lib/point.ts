export interface Point {

    onlyParent?: number;
    onlyChild?: number;
    both?: number;
}
