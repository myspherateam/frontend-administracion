export interface ChallengeDefaultPoints {
    [key: string]: string | number;
    id: string;
    assignedFor: string;
    questionnaires: number;
    news: number;
    activities: number;
    challenges: number;
}
