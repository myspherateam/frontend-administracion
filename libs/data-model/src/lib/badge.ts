export interface Badge {
    id: string;

    name: string;

    description: string;

    image: any;

    code?: string;

    currentProgress?: number;

    nextMilestone?: number;
}
