export interface NumberAssignedChallenges {
    physical: number;
    authority: number;
    gps: number;
    questionnaire: number;
    news: number;
}
