export interface CompletedByParents {
    parentId: string;
    completionDatetime?: string;
}
