import { Entity } from './entity';
import { NumberAssignedChallenges } from './number-assigned-challenges';
import { NumberCompletedChallenges } from './number-completed-challenges';

export interface User {

    entities: Entity[];
    id: string;
    roles: string[];
    externalId?: string;
    displayId: number;
    displayName: string;
    entityIds: string[];
    groupIds: string[];
    caringForIds: string[];
    creator: string;
    email: string;
    userRegistrationCode: string;
    registrationCode: string;
    points: number;
    parents: string[];
    childsUsers: User[];
    parentsUsers: User[];
    registerState: string;
    steps: number;
    numAssignedChallenges: NumberAssignedChallenges;
    numCompletedChallenges: NumberCompletedChallenges;
    potentialPoints: number;
    badges: string[];

    // non persisted fields
    description: string;

    password?: string;
}

export function getUserIdentifier(user: User) {
    let result: string;

    if (user.displayName) {
        result = user.displayName;
    } else if (user.externalId) {
        result = `${user.displayId}-${user.externalId}`;
    } else {
        result = user.displayId.toString();
    }

    return result;
}

export function userIdsToString(users: User[]): string {
    return users.map((user) => user.displayId.toString()).join(', ');
}

const DisplayNameRegex = /^((?<animal>[A-Z][a-z]+)(?<color>[A-Z][a-z]+))?(?<code>\d{0,6})?$/;

export function displayNameComponents(displayName: string): string[] {
    const matched: RegExpMatchArray | null = displayName?.match(DisplayNameRegex);
    return (matched === null) ? ([] as string[]) : [matched?.groups?.['animal'], matched?.groups?.['color'], matched?.groups?.['code']] as string[];
}
