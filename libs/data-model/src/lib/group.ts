export interface Group {
    id: string;
    entityId: string;
    name: string;
    creator: string;
    usersAmount: number;
    pointsAmount: number;
}
