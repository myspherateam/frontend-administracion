export interface ApiResponse<T> {
    message: T;
    status: number;
    statusMessage: string;
    count: number;
    errorCode: string;
}
