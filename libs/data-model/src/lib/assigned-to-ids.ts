export interface AssignedToIds {
    users: string[];
    groups: string[];
    entities: string[];
}
