import { Badge } from './badge';

export interface UserBadges {
    badges: Badge[];

    futureBadges: Badge[];
}
