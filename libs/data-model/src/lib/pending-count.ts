import { CategoriesCount } from './categories-count';

export interface PendingCount {
    prepare: CategoriesCount;
    activate: CategoriesCount;
    avoid: CategoriesCount;
    enjoy: CategoriesCount;
    keep: CategoriesCount;
}
