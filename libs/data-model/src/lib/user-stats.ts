import { Ranking } from './ranking';
import { User } from './user';

export interface UserStats extends User {
    // ranking values
    ranking: Ranking;
}
