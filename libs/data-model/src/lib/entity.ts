export interface Entity {
    id: string;
    type: string;
    name: string;
    acronym: string;
    creator: string;
    country: string;
    postalCode: string;
    location: GCSPoint;
}

export class GCSPoint {
    public lon: number;

    public lat: number;

    constructor(lon: number, lat: number) {
        this.lon = lon;
        this.lat = lat;
    }
}
