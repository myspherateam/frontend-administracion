export interface DidacticUnitChallenge {
    challengeId: string;
    startOffset: number;
    endOffset: number;
}
