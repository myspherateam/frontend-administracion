import { CategoriesCount } from './categories-count';

export interface Step {
    index: number;
    name: string;
    key: string;
    width: number;
    pendingCount: CategoriesCount;
}
