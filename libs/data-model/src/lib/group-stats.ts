import { NumberAssignedChallenges } from './number-assigned-challenges';
import { NumberCompletedChallenges } from './number-completed-challenges';

export interface GroupStats {
    id: string;
    numChildren: number;
    numParents: number;
    numSteps: number;
    numAssignedChallenges: NumberAssignedChallenges;
    numCompletedChallenges: NumberCompletedChallenges;
    potentialPoints: number;
    points: number;
}
