import { NumberAssignedChallenges } from './number-assigned-challenges';
import { NumberCompletedChallenges } from './number-completed-challenges';

export interface EntityStats {
    id: string;
    numGroups: number;
    numTeachers: number;
    numClinicians: number;
    numChildren: number;
    numParents: number;
    numSteps: number;
    numProgrammingChallenges: NumberAssignedChallenges;
    numAssignedChallenges: NumberAssignedChallenges;
    numCompletedChallenges: NumberCompletedChallenges;
    potentialPoints: number;
    points: number;
}
