export enum RegisterUserState {
    RegisteredFromParent = 'registeredFromParent',
    RegisteredDirectly = 'registeredDirectly',
}
