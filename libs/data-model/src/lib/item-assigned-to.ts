import { Entity } from './entity';
import { Group } from './group';
import { User } from './user';

export interface ItemAssignedTo {
    users: User[];
    groups: Group[];
    entities: Entity[];

    canAssignGroups?: boolean;
    canAssignUsers?: boolean;
}
