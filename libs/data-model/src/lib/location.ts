export interface LocationPoint {
    type: 'Point';
    coordinates: [number, number];
}

export interface LocationPolygon {
    type: 'Polygon';
    coordinates: [number, number][][];
}

export type Location = LocationPoint | LocationPolygon;
