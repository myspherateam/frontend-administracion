import { EMPTY, Observable, of } from 'rxjs';
import { LangChangeEvent, TranslationChangeEvent } from '@ngx-translate/core';
import { EventEmitter } from '@angular/core';

export class TranslateServiceStub {

  /**
   * An EventEmitter to listen to translation change events
   * onTranslationChange.subscribe((params: TranslationChangeEvent) => {
     *     // do something
     * });
   */
  get onTranslationChange(): EventEmitter<TranslationChangeEvent> {
    return new EventEmitter();
  }

  /**
   * An EventEmitter to listen to lang change events
   * onLangChange.subscribe((params: LangChangeEvent) => {
     *     // do something
     * });
   */
  get onLangChange(): EventEmitter<LangChangeEvent> {
    return new EventEmitter();
  }

  /**
   * An EventEmitter to listen to default lang change events
   * onDefaultLangChange.subscribe((params: DefaultLangChangeEvent) => {
     *     // do something
     * });
   */
  get onDefaultLangChange() {
    return;
  }

  /**
   * The default lang to fallback when translations are missing on the current lang
   */
  get defaultLang(): string {
    return '';
  }

  set defaultLang(_defaultLang: string) {
    // empty on purpose
  }

  /**
   * The lang currently used
   */
  get currentLang(): string {
    return '';
  }

  set currentLang(_currentLang: string) {
    // empty on purpose
  }

  /**
   * an array of langs
   */
  get langs(): string[] {
    return [];
  }

  set langs(_langs: string[]) {
    // empty on purpose
  }

  /**
   * a list of translations per lang
   */
  get translations(): any {
    return {};
  }

  set translations(translations: any) {
    // empty on purpose
  }

  /**
  * Sets the default language to use as a fallback
  */
  public setDefaultLang(_lang: string): void {
    // empty on purpose
  }

  /**
   * Gets the default language used
   */
  public getDefaultLang(): string {
    return '';
  }

  /**
   * Changes the lang currently used
   */
  public use(_lang: string): Observable<any> {
    return EMPTY;
  }

  /**
   * Retrieves the given translations
   */
  private retrieveTranslations(_lang: string): Observable<any> | undefined {
    return EMPTY;
  }

  /**
   * Gets an object of translations for a given language with the current loader
   * and passes it through the compiler
   */
  public getTranslation(_lang: string): Observable<any> {
    return EMPTY;
  }

  /**
   * Manually sets an object of translations for a given language
   * after passing it through the compiler
   */
  public setTranslation(_lang: string, _translations: Record<string, unknown>, _shouldMerge = false): void {
    // empty on purpose
  }

  /**
   * Returns an array of currently available langs
   */
  public getLangs(): Array<string> {
    return [];
  }

  /**
   * Add available langs
   */
  public addLangs(langs: Array<string>): void {
    // empty on purpose
  }

  /**
   * Update the list of available langs
   */
  private updateLangs(): void {
    // empty on purpose
  }

  /**
   * Returns the parsed result of the translations
   */
  public getParsedResult(_translations: any, _key: any, _interpolateParams?: Record<string, unknown>): any {
    return '';
  }

  /**
   * Gets the translated value of a key (or an array of keys)
   * @returns the translated key, or an object of translated keys
   */
  public get(key: string | Array<string>, _interpolateParams?: Record<string, unknown>): Observable<string | any> {
    return of(key);
  }

  /**
   * Returns a stream of translated values of a key (or an array of keys) which updates
   * whenever the translation changes.
   * @returns A stream of the translated key, or an object of translated keys
   */
  public getStreamOnTranslationChange(_key: string | Array<string>, _interpolateParams?: Record<string, unknown>): Observable<string | any> {
    return EMPTY;
  }

  /**
   * Returns a stream of translated values of a key (or an array of keys) which updates
   * whenever the language changes.
   * @returns A stream of the translated key, or an object of translated keys
   */
  public stream(_key: string | Array<string>, _interpolateParams?: Record<string, unknown>): Observable<string | any> {
    return EMPTY;
  }

  /**
   * Returns a translation instantly from the internal state of loaded translation.
   * All rules regarding the current language, the preferred language of even fallback languages will be used except any promise handling.
   */
  public instant(_key: string | Array<string>, _interpolateParams?: Record<string, unknown>): string | any {
    return '';
  }

  /**
   * Sets the translated value of a key, after compiling it
   */
  public set(_key: string, _value: string, _lang: string = this.currentLang): void {
    // empty on purpose
  }

  /**
   * Changes the current lang
   */
  private changeLang(_lang: string): void {
    // empty on purpose
  }

  /**
   * Changes the default lang
   */
  private changeDefaultLang(_lang: string): void {
    // empty on purpose
  }

  /**
   * Allows to reload the lang file from the file
   */
  public reloadLang(_lang: string): Observable<any> {
    return EMPTY;
  }

  /**
   * Deletes inner translation
   */
  public resetLang(_lang: string): void {
    // empty on purpose
  }

  /**
   * Returns the language code name from the browser, e.g. "de"
   */
  public getBrowserLang(): string | undefined {
    return '';
  }

  /**
   * Returns the culture language code name from the browser, e.g. "de-DE"
   */
  public getBrowserCultureLang(): string | undefined {
    return '';
  }
}
