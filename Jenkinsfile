pipeline {
  agent {
    docker {
      label jenkinsDevEnvLabel()
      alwaysPull true
      image jenkinsDevEnvImage('web-2.0.0')
      args jenkinsDevEnvArgs()
    }
  }

  stages {
    stage('Install') {
      steps {
        // setup your build here
        echo 'Installing dependencies...'
        sh 'npm ci'
      }
    }

    stage('Build') {
      steps {
        // setup your build here
        echo 'Building...'
        sh 'npx nx run-many --target=build --all=true'
      }
    }

    stage('Test') {
      steps {
        // setup your build here
        echo 'Testing...'
        sh 'npx jest --coverage --coverageDirectory="coverage" || true'
      }
    }

    stage('Code Analysis') {
      steps {
        // params: projectKey, projectSources
        // sonarScanner('frontend-administracion', 'apps,libs')
        withSonarQubeEnv('sonarqube-mysphera') {
          script {
            versionParam = ''
            try {
              packageJson = readJSON file: 'package.json'
              echo 'Project version found in package.json = ' + packageJson.version
              versionParam = '-Dsonar.projectVersion=' + packageJson.version
            } catch (Exception e) {
              echo 'Skipping project version. Unable to read version from package.json : ' + e.toString()
            }

            if (fileExists('test-report.xml')) {
              testExecutionReportPathsParam = '-Dsonar.testExecutionReportPaths=test-report.xml -Dsonar.test.inclusions=**/*.spec.ts -Dsonar.tests=. '
            } else {
              testExecutionReportPathsParam = ''
            }

            baseCommand = "sonar-scanner -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_AUTH_TOKEN " +
                  '-Dsonar.projectKey=frontend-administracion ' +
                  '-Dsonar.sources=apps,libs ' +
                  '-Dsonar.exclusions=apps/**/*.java '

            if (env.CHANGE_ID) {
              sh baseCommand +
                  "-Dsonar.pullrequest.key=$CHANGE_ID " +
                  "-Dsonar.pullrequest.branch=$CHANGE_BRANCH " +
                  "-Dsonar.pullrequest.base=$CHANGE_TARGET " +
                  testExecutionReportPathsParam +
                  versionParam
            } else {
              sh baseCommand +
                  "-Dsonar.branch.name=$BRANCH_NAME " +
                  testExecutionReportPathsParam +
                  versionParam
            }
          }
        }
      }
    }

    stage('Staging deployment') {
      when {
          expression { env.BRANCH_NAME != 'master' }
      }
      steps {
          echo 'Building staging images'
          script {
            docker.withRegistry('https://registry.locs.es', 'registry-locs-id') {
                //sh 'gradle dockerStagingPush'
                sh 'npx nx run admin-frontend:docker-build-staging'
                sh 'npx nx run paido-mobile:docker-build-staging'
            }
          }
      }
    }
    stage('Prod deployment') {
      when {
          expression { env.BRANCH_NAME == 'master' }
      }
      steps {
          echo 'Building Production images'
          script {
            docker.withRegistry('https://registry.locs.es', 'registry-locs-id') {
                sh 'npx nx run admin-frontend:docker-build-prod'
                sh 'docker image push registry.locs.es/paido/web:latest'

                sh 'npx nx run paido-mobile:docker-build-prod'
                sh 'docker image push registry.locs.es/paido/app:latest'
            }
          }
      }
    }
  }

  post {
    success {
      // May not work, since a special user need to be added to the channel
      notifyPipelineSuccessToChannel('#proj-innov-paido')
      // notify committer
      notifyPipelineSuccess()
    }
    failure {
      notifyPipelineFailure()
    }
    always {
      echo 'Finished. Clean Up our workspace'
      deleteDir() /* clean up our workspace */
    }
  }
}
