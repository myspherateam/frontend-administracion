// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { version } from './version';

export const baseEnvironment = {
  appVersion: version,
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyC89onMWhuoe3td3HxSmXACLa-usKxVqJU',
    authDomain: 'paido-f84e4.firebaseapp.com',
    databaseURL: 'https://paido-f84e4.firebaseio.com',
    projectId: 'paido-f84e4',
    storageBucket: 'paido-f84e4.appspot.com',
    messagingSenderId: '625028522209',
    appId: '1:625028522209:web:9a35a591c40209517a57a0',
    measurementId: 'G-4LK52KYSYE',
  },
  apiConfig: {
    // baseUrl: 'https://test.escalasalut.com',
    baseUrl: 'https://test.escalasalut.mysphera-innov.es',
    privacyUrl: 'assets/privacy.pdf',
  },
  keycloak: {
    realm: 'paido',
  },
};
