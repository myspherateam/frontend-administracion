import { baseEnvironment } from './baseEnvironment';

export const environment = {
  ...baseEnvironment,
  appVersion: baseEnvironment.appVersion + '-STAGING',
  apiConfig: {
    ...baseEnvironment.apiConfig,
    baseUrl: 'https://staging.escalasalut.mysphera-innov.es',
  },
  production: false,
  keycloak: {
    realm: 'paido',
  },
};
