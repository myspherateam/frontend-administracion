import { baseEnvironment } from './baseEnvironment';

export const environment = {
  ...baseEnvironment,
  apiConfig: {
    ...baseEnvironment.apiConfig,
    baseUrl: 'https://www.escalasalut.mysphera-innov.es',
  },
  production: true,
  keycloak: {
    realm: 'paido',
  },
};
