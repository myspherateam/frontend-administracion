import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { NEVER, Observable, firstValueFrom, from } from 'rxjs';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';

import { ApiService } from './api-service';
import { LocalStorageService } from '../services/storage-service';

@Injectable({
    providedIn: 'root',
})
export class AuthenticatorInterceptor implements HttpInterceptor {

    constructor(
        public apiService: ApiService,
        public storageService: LocalStorageService,
        public jwtHelper: JwtHelperService,
        public router: Router,
    ) {
        this.jwtHelper = new JwtHelperService();
    }

    intercept(request: HttpRequest<void>, next: HttpHandler): Observable<HttpEvent<void>> {
        const token: string = this.storageService.getFromLocalStorage<string>('access_token');

        const promise: Promise<Observable<HttpEvent<void>>> = new Promise((resolve) => {
            if (!request.url.includes('token')) {
                if (token && this.jwtHelper.isTokenExpired(token)) {
                    // if token expires
                    firstValueFrom(this.apiService.refreshToken())
                        .then((result) => {
                            if (result) {
                                this.storageService.storeOnLocalStorage('access_token', result.access_token);
                                this.storageService.storeOnLocalStorage('refresh_token', result.refresh_token);

                                request = request.clone({
                                    setHeaders: { Authorization: `Bearer ${result.access_token}` },
                                });
                            } else {
                                // reset token and send to login
                                this.storageService.logOut();
                                this.router.navigateByUrl('/login');
                                resolve(NEVER);
                            }
                        })
                        .catch((error: HttpErrorResponse) => {
                            console.error(error);
                            this.storageService.logOut();
                            this.router.navigateByUrl('/login');
                            resolve(NEVER);
                        });

                } else if (token) {
                    // if token is available
                    request = request.clone({
                        setHeaders: { Authorization: `Bearer ${token}` },
                    });
                }
            }
            resolve(next.handle(request));
        });

        return from(promise).pipe(
            mergeMap(it => it),
        );
    }
}
