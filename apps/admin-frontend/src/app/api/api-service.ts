import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { fromEvent, map, switchMap } from 'rxjs';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

import {
    ApiResponse, AssignedToIds, Badge, ChallengeCompletion, ChallengeDefaultPoints, ChallengeDefinition,
    ChallengeProgramming, DidacticUnit, Entity, EntityStats, Group, GroupStats, Login, PendingCount, User, UserStats,
} from '@paido/data-model';

import { LocalStorageService } from '../services/storage-service';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class ApiService {

    public accessToken: string = '';

    baseUrl: string = 'https://www.escalasalut.com';

    constructor(private http: HttpClient, private localStorage: LocalStorageService) { }

    // API CALLS
    isUserAuthorized(): boolean {
        return this.localStorage.containsKey('access_token');
    }

    getUserRoles() {
        if (this.localStorage.containsKey('user')) {
            const user: User = this.localStorage.getFromLocalStorage('user');
            return user.roles;
        }
        return null;
    }

    // AUTH
    login(username: string, password: string): Observable<Login> {
        const httpParams = new HttpParams()
            .set('username', username)
            // .set('password', encodeURIComponent(password))
            .set('password', password)
            .set('grant_type', 'password')
            .set('client_id', 'frontend');
        return this.http.post<Login>(
            this.baseUrl + `/auth/realms/${environment.keycloak.realm}/protocol/openid-connect/token`,
            httpParams,
        );
    }

    refreshToken(): Observable<Login> {
        const refreshToken = this.localStorage.getFromLocalStorage<string>('refresh_token');
        const httpParams = new HttpParams()
            .set('grant_type', 'refresh_token')
            .set('client_id', 'frontend')
            .set('refresh_token', refreshToken);
        return this.http.post<Login>(
            this.baseUrl + `/auth/realms/${environment.keycloak.realm}/protocol/openid-connect/token`,
            httpParams,
        );
    }

    getAuthHeader(): HttpHeaders {
        this.accessToken = this.localStorage.getFromLocalStorage('access_token');
        return new HttpHeaders({ Authorization: 'Bearer ' + this.accessToken });
    }

    // CHALLENGES
    getFrontProgrammingChallenges(
        entities: string,
        groups: string,
        users: string,
        category?: string,
        subcategory?: string,
        type?: string,
        finished?: boolean,
    ): Observable<ApiResponse<ChallengeProgramming[]>> {
        let queryParams = new HttpParams();
        if (entities) { queryParams = queryParams.set('entities', entities); }
        if (groups) { queryParams = queryParams.set('groups', groups); }
        if (users) { queryParams = queryParams.set('users', users); }
        if (finished) { queryParams = queryParams.set('finished', String(finished)); }
        if (category) { queryParams = queryParams.set('category', category); }
        if (subcategory) { queryParams = queryParams.set('subcategory', subcategory); }
        if (type) { queryParams = queryParams.set('type', type); }

        return this.http
            .get<ApiResponse<ChallengeProgramming[]>>(this.baseUrl + '/challenges/challengeProgramming', { params: queryParams });
    }

    cancelChallengeProgrammingForUsers(programmingId: string, usersIds: string[]) {
        const body = {} as AssignedToIds;
        body.users = usersIds;
        return this.http
            .post<ApiResponse<ChallengeCompletion>>(this.baseUrl + `/challenges/challengeProgramming/${programmingId}/cancel`, body);
    }

    getProgrammingChallengeDetail(programmingId: string): Observable<ApiResponse<ChallengeProgramming>> {
        return this.http.get<ApiResponse<ChallengeProgramming>>(this.baseUrl + '/challenges/challengeProgramming/' + programmingId);
    }

    createChallengeProgramming(body: ChallengeProgramming): Observable<ApiResponse<ChallengeProgramming>> {
        return this.http.post<ApiResponse<ChallengeProgramming>>(this.baseUrl + '/challenges/challengeProgramming', body);
    }

    markAChallengeAsComplete(body: ChallengeCompletion): Observable<ApiResponse<ChallengeCompletion>> {
        return this.http.post<ApiResponse<ChallengeCompletion>>(this.baseUrl + '/challenges/challengeCompletion', body);
    }

    getPendingChallengeProgramming(): Observable<ApiResponse<PendingCount>> {
        return this.http.get<ApiResponse<PendingCount>>(this.baseUrl + '/challenges/challengeProgramming/count');
    }

    createChallengeDefinition(body: ChallengeDefinition): Observable<ApiResponse<ChallengeDefinition>> {
        return this.http.post<ApiResponse<ChallengeDefinition>>(this.baseUrl + '/challenges/challengeDefinitions', body);
    }

    deleteChallengeDefinition(body: ChallengeDefinition): Observable<ApiResponse<ChallengeDefinition>> {
        return this.http.post<ApiResponse<ChallengeDefinition>>(this.baseUrl + '/challenges/challengeDefinitions/delete', body);
    }

    getChallengeDefinitions(): Observable<ApiResponse<ChallengeDefinition[]>> {
        return this.http.get<ApiResponse<ChallengeDefinition[]>>(this.baseUrl + '/challenges/challengeDefinitions');
    }

    getUserChallengeCompletion(programmingId: string): Observable<ApiResponse<ChallengeCompletion[]>> {
        return this.http
            .get<ApiResponse<ChallengeCompletion[]>>(this.baseUrl + `/challenges/challengeProgramming/${programmingId}/users`);
    }

    getUserChallenges(userId: string): Observable<ApiResponse<ChallengeCompletion[]>> {
        return this.http.get<ApiResponse<ChallengeCompletion[]>>(this.baseUrl + `/challenges/challengeCompletion/forId/${userId}`);
    }

    getDefaultPoints(): Observable<ApiResponse<ChallengeDefaultPoints[]>> {
        return this.http.get<ApiResponse<ChallengeDefaultPoints[]>>(this.baseUrl + '/challenges/challengeDefinitions/defaultPoints');
    }

    // Didactic Unit

    createOrUpdateDidacticUnit(body: DidacticUnit): Observable<ApiResponse<DidacticUnit>> {
        return this.http.post<ApiResponse<DidacticUnit>>(this.baseUrl + '/challenges/didacticUnit', body);
    }

    getDidacticUnits(ids?: Array<string>): Observable<ApiResponse<DidacticUnit[]>> {
        let queryParams: HttpParams = new HttpParams();
        if (ids && ids.length > 0) { queryParams = queryParams.set('ids', ids.join(',')); }
        return this.http.get<ApiResponse<DidacticUnit[]>>(this.baseUrl + '/challenges/didacticUnit', { params: queryParams });
    }

    getDidacticUnitsDetail(id: string): Observable<ApiResponse<DidacticUnit>> {
        return this.http.get<ApiResponse<DidacticUnit>>(this.baseUrl + `/challenges/didacticUnit/${id}`);
    }

    deleteDidacticUnit(body: DidacticUnit): Observable<ApiResponse<DidacticUnit>> {
        return this.http.post<ApiResponse<DidacticUnit>>(this.baseUrl + '/challenges/didacticUnit/delete', body);
    }

    // Badges
    getBadges(): Observable<ApiResponse<Badge>> {
        const queryParams = new HttpParams().set('includePictures', 'true');
        return this.http.get<ApiResponse<Badge>>(this.baseUrl + '/challenges/badge', { params: queryParams });
    }

    getBadge(id: string): Observable<ApiResponse<Badge>> {
        return this.http.get<ApiResponse<Badge>>(this.baseUrl + `/challenges/badge/${id}`);
    }

    getBadgeImage(id: string): Observable<string | ArrayBuffer> {
        return this.http.get(this.baseUrl + `/challenges/badge/${id}/image`, {
            headers: this.getAuthHeader(),
            observe: 'response',
            responseType: 'blob' as 'json',
        }).pipe(
            map((res: HttpResponse<Blob>) => res.body),
            switchMap((imageBlob: Blob) => {
                const reader = new FileReader();
                reader.readAsDataURL(imageBlob);
                return fromEvent(reader, 'loadend').pipe(map((ev: ProgressEvent<FileReader>) => ev.target.result));
            }),
        );
    }

    createBadge(badge: Badge): Observable<ApiResponse<Badge>> {
        let queryParams = new HttpParams();
        if (badge.name) { queryParams = queryParams.set('name', badge.name); }
        if (badge.description) { queryParams = queryParams.set('description', badge.description); }
        return this.http.post<ApiResponse<Badge>>(this.baseUrl + '/challenges/badge', badge.image, { params: queryParams });
    }

    modifyBadge(badge: Badge): Observable<ApiResponse<Badge>> {
        let queryParams = new HttpParams();
        queryParams = queryParams.set('id', badge.id);
        queryParams = queryParams.set('name', badge.name);
        queryParams = queryParams.set('description', badge.description);
        return this.http.post<ApiResponse<Badge>>(this.baseUrl + '/challenges/badge', badge.image, { params: queryParams });
    }

    assignDidacticUnit(didacticUnitId: string, startDatetime: string, assignedToIds: AssignedToIds) {
        const body = {
            startDatetime,
            assignedToIds,
        };
        return this.http.post<ApiResponse<DidacticUnit>>(this.baseUrl + `/challenges/didacticUnit/${didacticUnitId}/program`, body);
    }

    // ADMINISTRATION
    getNewRegistrationCodeFor(user: User): Observable<ApiResponse<User>> {
        return this.http.get<ApiResponse<User>>(this.baseUrl + `/administration/users/${user.id}/registrationCode`);
    }

    getEntities(): Observable<ApiResponse<Entity[]>> {
        return this.http.get<ApiResponse<Entity[]>>(this.baseUrl + '/administration/entities');
    }

    getGroups(entityIds: string[]): Observable<ApiResponse<Group[]>> {
        const queryParams: HttpParams = new HttpParams().set('entityIds', entityIds.join(','));
        return this.http.get<ApiResponse<Group[]>>(this.baseUrl + '/administration/groups', { params: queryParams });
    }

    getUsers(entityIds: string[], groupIds?: string[], usersIds?: string[], roles?: string[]): Observable<ApiResponse<User[]>> {
        let queryParams: HttpParams = new HttpParams();
        if (entityIds && entityIds.length > 0) { queryParams = queryParams.set('entityIds', entityIds.join(',')); }
        if (groupIds) { queryParams = queryParams.set('groupIds', groupIds.join(',')); }
        if (usersIds) { queryParams = queryParams.set('ids', usersIds.join(',')); }
        if (roles) { queryParams = queryParams.set('roles', roles.join(',')); }
        return this.http.get<ApiResponse<User[]>>(this.baseUrl + '/administration/users', { params: queryParams });
    }

    getMe(): Observable<ApiResponse<User>> {
        return this.http.get<ApiResponse<User>>(this.baseUrl + '/administration/me');
    }

    createEntity(body: Entity): Observable<ApiResponse<Entity>> {
        return this.http.post<ApiResponse<Entity>>(this.baseUrl + '/administration/entities', body);
    }

    createGroup(body: Group): Observable<ApiResponse<Group>> {
        return this.http.post<ApiResponse<Group>>(this.baseUrl + '/administration/groups', body);
    }

    createUser(body: User): Observable<ApiResponse<User>> {
        return this.http.post<ApiResponse<User>>(this.baseUrl + '/administration/users', body);
    }

    resendEmail(id: string): Observable<ApiResponse<boolean>> {
        return this.http.post<ApiResponse<boolean>>(this.baseUrl + `/administration/users/${id}/resendEmail`, null);
    }

    lostPasswordEmail(body: User): Observable<ApiResponse<boolean>> {
        return this.http.post<ApiResponse<boolean>>(this.baseUrl + '/administration/users/lostPassword', body);
    }

    getUserDetail(id: string): Observable<ApiResponse<User>> {
        return this.http.get<ApiResponse<User>>(this.baseUrl + '/administration/users/' + id);
    }

    modifyGroup(group: Group): Observable<ApiResponse<Group>> {
        return this.http.patch<ApiResponse<Group>>(`/administration/groups/${group.id}`, group);
    }

    removeGroup(groupId: string): Observable<ApiResponse<void>> {
        return this.http.delete<ApiResponse<void>>(`/administration/groups/${groupId}`);
    }

    // Stats
    getGroupStats(id: string): Observable<ApiResponse<GroupStats>> {
        return this.http.get<ApiResponse<GroupStats>>(this.baseUrl + `/administration/groups/${id}/stats`);
    }

    getEntityStats(id: string): Observable<ApiResponse<EntityStats>> {
        return this.http.get<ApiResponse<EntityStats>>(this.baseUrl + `/administration/entities/${id}/stats`);
    }

    getUserStats(id: string, field: string, rankingType: string): Observable<ApiResponse<UserStats>> {
        let queryParams = new HttpParams();
        if (field) { queryParams = queryParams.set('field', field); }
        if (rankingType) { queryParams = queryParams.set('rankingType', rankingType); }
        return this.http.get<ApiResponse<UserStats>>(this.baseUrl + `/administration/users/${id}/stats`, { params: queryParams });
    }

    // EXTERNAL
    requestAddressData(limit: number, address: string): Observable<OSMAddressData[]> {
        return this.http
            .get<OSMAddressData[]>(`https://nominatim.openstreetmap.org/search?format=json&limit=${limit}&q=${address}&countrycodes=es`);
    }

    getMockDidacticUnit(): DidacticUnit {
        const didacticUnit: DidacticUnit = {
            id: '5f71fe7217b42a68e8f24a8e',
            name: 'UD2: “La Huerta valenciana y las verduras de temporada”',
            challenges: [
                { challengeId: '5e80eddc85b6db109c545575', startOffset: 0, endOffset: 5 },
                { challengeId: '5f915a5a0929995ae76fa1fd', startOffset: 1, endOffset: 7 },
            ],
            duration: 7,
        };
        return didacticUnit;
    }

    getCountries(): string[] {
        return [
            'Spain',
            'Austria',
            'Belgium',
            'Bulgaria',
            'Croatia',
            'Cyprus',
            'Czech Republic',
            'Denmark',
            'Estonia',
            'Finland',
            'France',
            'Germany',
            'Greece',
            'Hungary',
            'Ireland',
            'Italy',
            'Latvia',
            'Lithuania',
            'Luxemburg',
            'Malta',
            'Netherlands',
            'Poland',
            'Portugal',
            'Romania',
            'Slovakia',
            'Slovenia',
            'Sweden',
        ];
    }
}

export interface OSMAddressData {
    boundingbox?: string[];
    class: string;
    display_name: string;
    icon: string;
    importance: number;
    lat: string;
    licence: string;
    lon: string;
    osm_id: number;
    osm_type: string;
    place_id: number;
    type: string;
}
