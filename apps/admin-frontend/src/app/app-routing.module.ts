import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from './services/guards/auth.guard';
import { EntityGuard } from './services/guards/entity.guard';
import { RoleGuard } from './services/guards/role.guard';
import { environment } from '../environments/environment';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'entities',
    pathMatch: 'full',
  },
  {
    path: 'entities',
    loadChildren: () => import('./entities/entities.module').then(m => m.EntitiesPageModule),
    canActivate: [AuthGuard, RoleGuard, EntityGuard],
    canLoad: [AuthGuard],
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule),
  },
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then(m => m.MenuPageModule),
    canActivate: [
      AuthGuard,  // a user must be logged
      EntityGuard, // an entity must be selected
    ],
    canLoad: [AuthGuard],

  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    enableTracing: !environment.production,
    onSameUrlNavigation: 'reload',
}),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
