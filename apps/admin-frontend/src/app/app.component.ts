import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { Entity } from '@paido/data-model';

import { LocalStorageService } from './services/storage-service';
import { PresentatorService } from './services/common/presentator.service';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  privacyUrl = environment.apiConfig.privacyUrl;

  version = environment.appVersion;

  userLang: string;

  // navigation history
  private history: string[] = [];

  constructor(
    private localStorage: LocalStorageService,
    private presentator: PresentatorService,
    private router: Router,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit(): void {
    const localStorageLanguage = this.localStorage.getFromLocalStorage('lang');

    if (localStorageLanguage) {
      this.userLang = localStorageLanguage as string;
    } else {
      this.userLang = this.translateService.getBrowserLang();
      console.log(`${this.translateService.getBrowserLang()}-${this.translateService.getBrowserCultureLang()}`);
    }

    this.translateService.setDefaultLang(this.userLang);

    this.router.events.subscribe({
      next: (event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this.history.push(event.urlAfterRedirects);
        }
      },
    });
  }

  /**
   * Method that provides a history back navigation
   */
  navigateBack() {
    this.history.pop(); // current page
    if (this.history.length) {
      const targetUrl = this.history[this.history.length - 1];
      if (targetUrl.includes('login')) {
        this.navigateBack();
      } else {
        this.router.navigateByUrl(this.history.pop());
      }
    } else {
      this.router.navigate(['/']);
    }
  }

  changeLanguage(language: string) {
    this.translateService.use(language).subscribe({
      next: () => {
        this.localStorage.storeOnLocalStorage('lang', language);
      },
    });
  }

  logout() {
    this.presentator.createYesNoAlert('LOGIN.LOGOUT', 'LOGIN.LOGOUT_EXTENDED').then(
      (value: boolean) => {
        if (value) {
          this.localStorage.logOut();
          this.history = [];
          this.router.navigate(['/login']);
        }
      },
    );
  }

  get user(): boolean {
    return this.localStorage.containsKey('access_token');
  }

  showBackButton(): boolean {
    return this.history.length &&  // at least there is a back navigation available
      this.user && // the user object is available
      this.router.url !== '/entities' // we are not at the init page
      ;
  }

  showSubHeader(): boolean {
    return this.router.url !== '/login';
  }

  getSubHeaderTitle(): string {
    let title = this.translateService.instant('MENU.ENTITIES').toString();

    if (
      this.localStorage.containsKey('entitySelected') &&
      this.router.url !== '/entities' &&
      this.router.url !== '/entities/add-entity'
    ) {
      const entity = this.localStorage.getFromLocalStorage<Entity>('entitySelected');
      title = entity.name;
    }

    return title;
  }

}
