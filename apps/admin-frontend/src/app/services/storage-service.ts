import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable()
export class LocalStorageService {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

    storeOnLocalStorage<T>(key: string, value: T): void {
        this.storage.set(key, value);
    }

    getFromLocalStorage<T>(key: string): T | undefined {
        return this.storage.get(key);
    }

    remove(key: string): void {
        return this.storage.remove(key);
    }

    containsKey(key: string): boolean {
        return this.storage.has(key);
    }

    logOut() {
        this.signOut();
    }

    isSignIn() {
        return this.storage.has('signIn') && this.storage.get('signIn');
    }

    private signOut() {
        // We dont want to erase privacy setting, so before clear all, we save in temp variable
        const privacyAccepted = this.getFromLocalStorage('privacyAccepted');
        this.storage.clear();
        this.storeOnLocalStorage('privacyAccepted', privacyAccepted);
    }
}
