import { ActivatedRouteSnapshot, CanDeactivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
  providedIn: 'root',
})
export class CanDeactivateGuard<T extends CanComponentDeactivate> implements CanDeactivate<T> {
  constructor(
    private readonly location: Location,
    private readonly router: Router,
  ) { }

  canDeactivate(component: T, _currentRoute: ActivatedRouteSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {
    if (component.canDeactivate()) {
      return true;
    } else {
      const currentUrlTree = this.router.createUrlTree([this.router.getCurrentNavigation()?.extractedUrl.toString() || '/']);
      this.location.go(currentUrlTree.toString());
      return false;
    }
  }
}
