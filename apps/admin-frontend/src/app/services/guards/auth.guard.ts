import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable, firstValueFrom } from 'rxjs';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

import { Login } from '@paido/data-model';

import { ApiService } from '../../api/api-service';
import { LocalStorageService } from '../storage-service';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

    constructor(
        private localStorage: LocalStorageService,
        private router: Router,
        private jwtHelper: JwtHelperService,
        private apiService: ApiService,
    ) { }

    canLoad(_route: Route, _segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        if (!this.localStorage.containsKey('access_token')) {
            this.router.navigate(['/login'], { queryParams: { returnUrl: '/entities' } });
            return false;
        }
    }

    async canActivate(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Promise<boolean> {
        const token: string | null = this.localStorage.getFromLocalStorage<string>('access_token');

        if (token) {
            const expired = this.checkAccessTokenExpired(token);
            if (expired) {
                if (this.localStorage.containsKey('refresh_token')) {
                    // refresh token
                    try {
                        const response: Login = await firstValueFrom(this.apiService.refreshToken());
                        if (response) {
                            this.localStorage.storeOnLocalStorage('access_token', response.access_token);
                            this.localStorage.storeOnLocalStorage('refresh_token', response.refresh_token);
                            return true;
                        }
                    } catch (error) {
                        // cannot do anything, so let's redirect to login
                        this.localStorage.remove('access_token');
                        this.localStorage.remove('refresh_token');
                    }
                }
            } else {
                return true;
            }
        }
        this.router.navigate(['/login'], { queryParams: { returnUrl: _state.url } });
        return false;
    }

    private checkAccessTokenExpired(token: string): boolean {
        return this.jwtHelper.isTokenExpired(token);
    }
}
