import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

import { User, UserRole } from '@paido/data-model';

import { LocalStorageService } from '../storage-service';

@Injectable()
export class RoleGuard implements CanActivate {

    constructor(
        private localStorage: LocalStorageService,
        private router: Router,
    ) {
    }

    canActivate(_route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

        if (this.localStorage.containsKey('user')) {
            const user: User = this.localStorage.getFromLocalStorage<User>('user');

            if (user.roles.includes(UserRole.GlobalAdmin)) {
                if (!state.url.includes('stats') && (!state.url.includes('manage-users'))) {
                    this.router.navigateByUrl('menu');
                    return false;
                }
            } else if (user.roles.includes(UserRole.LocalAdmin)) {
                if (!state.url.includes('manage-users') && !state.url.includes('entities')) {
                    this.router.navigateByUrl('menu');
                    return false;
                }
            } else if (user.roles.includes(UserRole.Teacher) || user.roles.includes(UserRole.Clinician)) {
                if (state.url.includes('stats') || state.url.includes('manage-users')) {
                    this.router.navigateByUrl('menu');
                    return false;
                }
            }
        } else {
            this.router.navigateByUrl('login');
            return false;
        }

        return true;
    }

}
