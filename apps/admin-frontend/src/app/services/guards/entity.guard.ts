import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment } from '@angular/router';
import { Injectable } from '@angular/core';

import { Entity } from '@paido/data-model';

import { LocalStorageService } from '../storage-service';

@Injectable({
  providedIn: 'root',
})
export class EntityGuard implements CanActivate, CanLoad {

  constructor(
    private localStorage: LocalStorageService,
    private router: Router,
  ) {
  }

  async canLoad(_route: Route, _segments: UrlSegment[]): Promise<boolean> {
    if (!this.localStorage.containsKey('entitySelected') || this.localStorage.getFromLocalStorage<Entity>('entitySelected') === null) {
      await this.router.navigate(['/entities']);
      return false;
    }
    return true;
  }

  async canActivate(
    _route: ActivatedRouteSnapshot,
    _state: RouterStateSnapshot): Promise<boolean> {
    if (_state.url.includes('/entities')) {
      this.localStorage.remove('entitySelected');
      return true;
    } else if (!this.localStorage.containsKey('entitySelected') ||
      this.localStorage.getFromLocalStorage<Entity>('entitySelected') === null) {
      await this.router.navigate(['/entities']);
      return false;
    }
    return true;
  }
}
