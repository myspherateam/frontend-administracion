import { IonicModule } from '@ionic/angular';
import { TestBed } from '@angular/core/testing';
import { TranslateService } from '@ngx-translate/core';

import { TranslateServiceStub } from '@paido/test-resources';

import { PresentatorService } from './presentator.service';

describe('PresentatorService', () => {
  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [
        //  TranslateTestingModule,
        IonicModule,
      ],
      providers: [
        PresentatorService,
        { provide: TranslateService, useClass: TranslateServiceStub },
      ],
    });
  });

  it('should be created', () => {
    const service: PresentatorService = TestBed.inject(PresentatorService);
    expect(service).toBeTruthy();
  });
});
