import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginPageComponent } from './login.page';

const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent,
  },
  {
    path: 'lost-password-modal',
    loadChildren: () => import('./lost-password-modal/lost-password-modal.module').then(m => m.LostPasswordModalPageModule),
  },
  {
    path: 'privacy-modal',
    loadChildren: () => import('./privacy-modal/privacy-modal.module').then(m => m.PrivacyModalPageModule),
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginPageRoutingModule { }
