import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { LoginPageComponent } from './login.page';
import { LoginPageRoutingModule } from './login-routing.module';
import { LostPasswordModalPageModule } from './lost-password-modal/lost-password-modal.module';
import { PrivacyModalPageModule } from './privacy-modal/privacy-modal.module';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule.forChild(),
        ReactiveFormsModule,
        IonicModule,
        LoginPageRoutingModule,
        LostPasswordModalPageModule,
        PrivacyModalPageModule,
    ],
    declarations: [LoginPageComponent],
})
export class LoginPageModule { }
