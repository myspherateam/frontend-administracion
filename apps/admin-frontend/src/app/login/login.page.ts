import { ActivatedRoute, Router } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalController } from '@ionic/angular';

import { ApiResponse, Login, User } from '@paido/data-model';

import { ApiService } from '../api/api-service';
import { LocalStorageService } from '../services/storage-service';
import { LostPasswordModalPageComponent } from './lost-password-modal/lost-password-modal.page';
import { PresentatorService } from '../services/common/presentator.service';
import { PrivacyModalPageComponent } from './privacy-modal/privacy-modal.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPageComponent {
  isPrivacyAccepted = false;

  login: UntypedFormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: UntypedFormBuilder,
    private apiService: ApiService,
    private modalController: ModalController,
    private presentatorService: PresentatorService,
    private analytics: AngularFireAnalytics,
    private localStorage: LocalStorageService,
  ) {
    this.login = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.checkAndShowPrivacyModal();
  }

  checkAndShowPrivacyModal() {
    const accepted = this.localStorage.getFromLocalStorage<boolean>('privacyAccepted');
    if (!accepted) {
      this.showPrivacyModal();
    } else {
      this.isPrivacyAccepted = true;
    }
  }

  ionViewWillEnter() {
    this.login.reset();
  }

  doLogin() {
    const loginData = this.login.value;
    const username: string = loginData.username;
    const password: string = loginData.password;

    this.apiService.login(username, password).subscribe({
      next: (result: Login) => {
        if (result) {
          this.localStorage.storeOnLocalStorage<string>('access_token', result.access_token);
          this.localStorage.storeOnLocalStorage<string>('refresh_token', result.refresh_token);
          this.getMe();
        } else {
          this.presentatorService.showInfoAlert('LOGIN.BAD_CREDENTIALS', 'ERRORS.PLEASE_TRY_AGAIN');
          this.login.get('username')?.setValue(null);
          this.login.get('password')?.setValue(null);
        }
      },
      error: (error: unknown) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          this.presentatorService.showInfoAlert('LOGIN.BAD_CREDENTIALS', 'ERRORS.PLEASE_TRY_AGAIN');
          this.login.get('username')?.setValue(null);
          this.login.get('password')?.setValue(null);
        } else {
          this.presentatorService.showInfoAlert('ERRORS.ERROR', 'ERRORS.CALL_ADMINS');
        }
      },
    });

  }

  getMe() {
    this.apiService.getMe().subscribe({
      next: (result: ApiResponse<User>) => {
        if (result.status === 200) {
          const user = result.message;
          this.localStorage.storeOnLocalStorage<User>('user', user);
          this.analytics.logEvent('login', { method: 'email' });
          this.analytics.setUserId(user.email);
          const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/entities';
          this.router.navigateByUrl(returnUrl);
        } else {
          this.presentatorService.showInfoAlert('ERRORS.USER_NOT_FOUND', 'ERRORS.PLEASE_TRY_AGAIN');
        }
      },
      error: () => {
        this.presentatorService.showInfoAlert('ERROR', 'ERRORS.CALL_ADMINS');
      },
    });
  }

  async showPrivacyModal() {
    const modal = await this.modalController.create({
      component: PrivacyModalPageComponent,
      componentProps: {

      },
      backdropDismiss: false,
    });

    modal.onDidDismiss().then((value) => {
      if (value.data === true) {
        this.localStorage.storeOnLocalStorage<boolean>('privacyAccepted', true);
        this.isPrivacyAccepted = true;
      }
    });

    return modal.present();
  }

  async showLostPasswordModal() {
    const modal = await this.modalController.create({
      component: LostPasswordModalPageComponent,
      componentProps: {

      },
      backdropDismiss: false,
    });

    return modal.present();
  }
}
