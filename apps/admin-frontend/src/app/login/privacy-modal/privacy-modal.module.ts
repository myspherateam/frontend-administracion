import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

import { PrivacyModalPageComponent } from './privacy-modal.page';
import { PrivacyModalPageRoutingModule } from './privacy-modal-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrivacyModalPageRoutingModule,
  ],
  declarations: [PrivacyModalPageComponent],
})
export class PrivacyModalPageModule { }
