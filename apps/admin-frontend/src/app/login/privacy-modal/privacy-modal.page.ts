import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-privacy-modal',
  templateUrl: './privacy-modal.page.html',
  styleUrls: ['./privacy-modal.page.scss'],
})
export class PrivacyModalPageComponent {
  privacyUrl: string = environment.apiConfig.privacyUrl;

  isAccepted = false;

  constructor(private modalController: ModalController) { }

  closeModal() {
    this.modalController.dismiss(this.isAccepted);
  }
}
