import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PrivacyModalPageComponent } from './privacy-modal.page';

const routes: Routes = [
  {
    path: '',
    component: PrivacyModalPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivacyModalPageRoutingModule { }
