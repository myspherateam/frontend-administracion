import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { LostPasswordModalPageComponent } from './lost-password-modal.page';
import { LostPasswordModalPageRoutingModule } from './lost-password-modal-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule.forChild(),
    LostPasswordModalPageRoutingModule,
  ],
  declarations: [LostPasswordModalPageComponent],
})
export class LostPasswordModalPageModule { }
