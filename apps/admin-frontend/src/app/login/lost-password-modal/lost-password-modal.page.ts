import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { User } from '@paido/data-model';

import { ApiService } from '../../api/api-service';
import { PresentatorService } from '../../services/common/presentator.service';
import { Toast } from '../../services/common/dto/toast';
import { finalize } from 'rxjs';

@Component({
  selector: 'app-lost-password-modal',
  templateUrl: './lost-password-modal.page.html',
  styleUrls: ['./lost-password-modal.page.scss'],
})
export class LostPasswordModalPageComponent {
  public lostPassword: FormGroup;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private presentatorService: PresentatorService,
    private apiService: ApiService,
  ) {
    this.lostPassword = this.formBuilder.group({
      email: new FormControl<string>(null, [Validators.required, Validators.email]),
    });
  }

  public get email() {
    return this.lostPassword.get<string>('email');
  }

  getLostPasswordEmail() {
    const email: string = this.email.value.trim();

    this.apiService.lostPasswordEmail({ email } as User).pipe(
      finalize(async () => {
        await this.presentatorService.showToast(Toast.message('EMAIL_SENT_SUCCESS', undefined, 'success'));
        this.closeModal();
      }),
    ).subscribe({
      next: () => {
        // do nothing. We do not inform the user if the operation was success or not to defend against brute force attacks
      },
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }
}
