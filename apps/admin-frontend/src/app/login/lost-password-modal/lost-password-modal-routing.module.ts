import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { LostPasswordModalPageComponent } from './lost-password-modal.page';

const routes: Routes = [
  {
    path: '',
    component: LostPasswordModalPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LostPasswordModalPageRoutingModule { }
