import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiResponse, Entity, UserRole } from '@paido/data-model';

import { ApiService } from '../api/api-service';
import { LocalStorageService } from '../services/storage-service';

@Component({
  templateUrl: './entities.page.html',
  styleUrls: ['./entities.page.scss'],
})
export class EntitiesPageComponent implements OnInit {

  public entities: Entity[] = [];

  public isAddVisible!: boolean;

  constructor(
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const userRoles = this.apiService.getUserRoles();
    if (!userRoles?.includes(UserRole.Teacher) && !userRoles?.includes(UserRole.Clinician)) {
      this.isAddVisible = true;
    }
    this.localStorage.remove('entitySelected');
    this.getEntities();
  }

  getEntities() {
    this.apiService.getEntities().subscribe({
      next: (result: ApiResponse<Entity[]>) => {
        if (result.status === 200) {
          this.entities = result.message;
        }
      },
      error: error => console.error(error),
    });
  }

  onEntitySelected(entity: Entity) {
    this.localStorage.storeOnLocalStorage<Entity>('entitySelected', entity);
    this.router.navigate(['menu']);
  }

  addEntity() {
    this.router.navigateByUrl('entities/add-entity');
  }
}
