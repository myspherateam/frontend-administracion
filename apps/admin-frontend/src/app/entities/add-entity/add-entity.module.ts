import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AddEntityPageComponent } from './add-entity.page';
import { AddEntityPageRoutingModule } from './add-entity-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    AddEntityPageRoutingModule,
    ReactiveFormsModule,
    LeafletModule,
    IonicSelectableModule,
  ],
  declarations: [AddEntityPageComponent],
})
export class AddEntityPageModule { }
