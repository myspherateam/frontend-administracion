import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AddEntityPageComponent } from './add-entity.page';

const routes: Routes = [
  {
    path: '',
    component: AddEntityPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddEntityPageRoutingModule { }
