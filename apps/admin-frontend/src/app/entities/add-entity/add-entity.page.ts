/* eslint-disable max-len */
import * as L from 'leaflet';

import { Component, NgZone, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, Entity } from '@paido/data-model';

import { ApiService, OSMAddressData } from '../../api/api-service';
import { PresentatorService } from '../../services/common/presentator.service';
import { Toast } from '../../services/common/dto/toast';

@Component({
  templateUrl: './add-entity.page.html',
  styleUrls: ['./add-entity.page.scss'],
})
export class AddEntityPageComponent implements OnInit {

  public addEntityForm: UntypedFormGroup;

  public map!: L.Map;

  public leafletLayers: L.TileLayer[];

  private defaultMapLocation: L.LatLngExpression = [39.4078969, -0.431551];

  private defaultMapZoom = 10;

  public leafletOptions: L.MapOptions;

  public currentAddressList: OSMAddressData[] = [];

  private addressResultLimit = 5;

  public markerPosition: L.Marker = null;

  public countries!: string[];

  public isSubmitted: boolean = false;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private apiService: ApiService,
    private analytics: AngularFireAnalytics,
    private presentatorService: PresentatorService,
    private translateService: TranslateService,
    private router: Router,
    private zone: NgZone,
  ) {
    this.leafletLayers = [
      L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        {
          attribution: '&copy; OpenStreetMap contributors',
        },
      ),
    ];

    this.leafletOptions = {
      zoom: this.defaultMapZoom,
      center: this.defaultMapLocation,
    };

    this.addEntityForm = this.formBuilder.group({
      name: [null, Validators.required],
      acronym: [null, Validators.required],
      entityType: [null, Validators.required],
      postalCode: [null, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(5),
        Validators.pattern('^([0-9]{5})$'),
      ]],
      country: [null, Validators.required],
    });
  }

  ngOnInit() {
    this.countries = this.apiService.getCountries();
  }

  get name() {
    return this.addEntityForm.get('name');
  }

  get acronym() {
    return this.addEntityForm.get('acronym');
  }

  get entityType() {
    return this.addEntityForm.get('entityType');
  }

  get postalCode() {
    return this.addEntityForm.get('postalCode');
  }

  get country() {
    return this.addEntityForm.get('country');
  }

  async addEntity() {
    this.isSubmitted = true;
    if (this.addEntityForm.valid && this.markerPosition) {
      const proceed = await this.presentatorService.createYesNoAlert(
        this.translateService.instant('ENTITIES.ADD_ENTITY'),
        this.translateService.instant('ENTITIES.ADD_ENTITY_QUESTION'),
      );

      if (proceed) {
        const values = this.addEntityForm.value;
        const entity = {} as Entity;
        entity.name = values.name as string;
        entity.acronym = values.acronym as string;
        entity.type = values.entityType as string;
        entity.postalCode = values.postalCode as string;
        entity.country = values.country as string;
        entity.location = {
          lon: this.markerPosition.getLatLng().lng,
          lat: this.markerPosition.getLatLng().lat,
        };

        this.apiService.createEntity(entity).subscribe({
          next: (result: ApiResponse<Entity>) => {
            if (result.status === 200) {
              this.analytics.logEvent('new_entity', { id: result.message.id });
              // show alert entity created
              this.presentatorService.showInfoAlert('ENTITIES.ADD_ENTITY_SUCCESS', 'ENTITIES.ADD_ENTITY_SUCCESS_EXTENDED')
                .then((_result: unknown) => {
                  if (result) {
                    this.isSubmitted = false;
                    this.addEntityForm.reset();
                    this.router.navigateByUrl('/entities');
                  }
                });
            }
          },
          error: error => console.error(error),
        });
      }
    } else {
      this.presentatorService.showToast(Toast.message(this.translateService.instant('ERRORS.FIELDS_ARE_REQUIRED')));
    }
  }

  onMapReady(map: L.Map) {
    this.map = map;
    // set the view by default to prevent that map loses tile layers
    setTimeout(() => {
      this.map.invalidateSize();
      this.map.setView(this.defaultMapLocation, this.defaultMapZoom);
    }, 1000);

    this.map.on('click', (coord: L.LeafletMouseEvent) => {
      const lat = coord.latlng.lat;
      const lon = coord.latlng.lng;

      if (this.markerPosition) {

        this.markerPosition.setLatLng(coord.latlng);
        this.map.panTo(coord.latlng);
      } else {
        this.zone.run(() => {
          const myIcon = L.icon({
            iconUrl: '/assets/location.png',
            iconSize: [40,40],
            // ...
         });
          this.markerPosition = L.marker([lat, lon],  { draggable: true, icon: myIcon });

          this.markerPosition.on('dragend', (event: L.DragEndEvent) => {
            const marker = event.target;
            const position = marker.getLatLng();
            this.markerPosition.setLatLng(new L.LatLng(position.lat, position.lng));
            this.map.panTo(new L.LatLng(position.lat, position.lng));
          });

          // remove marker when double-clicking
          this.markerPosition.on('dblclick', () => {
            this.map.removeLayer(this.markerPosition);
            this.markerPosition.clearAllEventListeners();
            this.zone.run(() => this.markerPosition = null);
            //this.markerPosition = null;
          });

          // necessary to stop evnet proagation to the map
          this.markerPosition.on('click', (ev: L.LeafletMouseEvent) => {
            L.DomEvent.stopPropagation(ev);
          });

          this.map.addLayer(this.markerPosition);
        });
      }
    });
  }

  onSelectAddress(address: OSMAddressData) {
    const lat = parseFloat(address.lat);
    const lon = parseFloat(address.lon);
    this.map.setView([lat, lon], 50);
  }

  onAddressClear() {
    this.currentAddressList = [];
  }

  onAddressChange(event: Event) {
    const address = (event as CustomEvent).detail.value;
    const addressFiltered = address.trim();

    if (addressFiltered !== '') {
      this.apiService.requestAddressData(this.addressResultLimit, addressFiltered).subscribe({
        next: (response: OSMAddressData[]) => {
          this.currentAddressList = response;
        },
        error: (error) => {
          // If any error occurs, we log it and set the map on center of Valencia
          console.error(error);
          this.map.setView(this.defaultMapLocation, this.defaultMapZoom);
        },
      });
    } else {
      this.currentAddressList = [];
    }
  }
}
