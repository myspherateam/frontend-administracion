import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EntitiesPageComponent } from './entities.page';

const routes: Routes = [
  {
    path: '',
    component: EntitiesPageComponent,
  },
  {
    path: 'add-entity',
    loadChildren: () => import('./add-entity/add-entity.module').then(m => m.AddEntityPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntitiesPageRoutingModule { }
