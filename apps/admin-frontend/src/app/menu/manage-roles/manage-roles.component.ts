/* eslint-disable @angular-eslint/no-empty-lifecycle-method */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiResponse, User, UserRole } from '@paido/data-model';
import { ApiService } from '../../api/api-service';

@Component({
  selector: 'app-manage-roles',
  templateUrl: './manage-roles.component.html',
  styleUrls: ['./manage-roles.component.scss'],
})
export class ManageRolesComponent  implements OnInit {

  users: User[];

  user: User;

  constructor(private modalController: ModalController,
    private apiService: ApiService) { }

  ngOnInit() {}

  closeModal() {
    this.modalController.dismiss();
  }

  //TODO
  getRole(): string {
    let role = '';

    this.user.roles.forEach((value: string) => {
      switch (value) {
        case UserRole.LocalAdmin:
          role = 'Administrador local';
          break;
        case UserRole.Parent:
          role = 'Padre/madre/tutor';
          break;
        case UserRole.Teacher:
          role = 'Profesor/a';
          break;
        case UserRole.Clinician:
          role = 'Médico/a';
          break;
        case UserRole.Child:
          role = ' Niño/a';
          break;
      }
    });

    return role;
  }

  saveUser() {
    
    this.apiService.createUser(this.user).subscribe({
      next: (result: ApiResponse<User>) => {
        if (result.status === 200) {
          this.closeModal(); 
        }
      },
    });
  }

}
