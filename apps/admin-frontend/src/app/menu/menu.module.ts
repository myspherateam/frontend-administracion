import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

import { MenuPageComponent } from './menu.page';
import { MenuPageRoutingModule } from './menu-routing.module';
import { ManageRolesComponent } from './manage-roles/manage-roles.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPageRoutingModule,
    TranslateModule.forChild(),
  ],
  declarations: [MenuPageComponent, ManageRolesComponent],
})
export class MenuPageModule { }
