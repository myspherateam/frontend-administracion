import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

import { Entity } from '@paido/data-model';

import { LocalStorageService } from '../services/storage-service';

@Injectable({
  providedIn: 'root',
})
/**
 * Resolver que inyecta en la ruta activada el objeto Entity guardado localstorage
 */
export class EntityResolver implements Resolve<Entity> {
  constructor(
    private storage: LocalStorageService,
  ) {

  }

  resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Observable<Entity | undefined> {
    return of(this.storage.getFromLocalStorage<Entity>('entitySelected'));
  }
}
