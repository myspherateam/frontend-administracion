import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import {
  ApiResponse, ChallengeLockdownTypes, ChallengeProgramming, DidacticUnit, Entity, Group, ItemAssignedTo, User,
} from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { ChallengePreviewModalComponent } from '../common/challenge-preview-modal/challenge-preview-modal.component';

@Component({
  selector: 'app-programming-list',
  templateUrl: './programming-list.page.html',
  styleUrls: ['./programming-list.page.scss'],
})
export class ProgrammingListPageComponent implements OnInit {

  public challengeProgrammings: ChallengeProgramming[] = [];

  public assingChallengeUrl = '/menu/challenges-menu/assign-challenge';

  public assingDidacticUnitUrl = '/menu/challenges-menu/assign-didactic-unit';

  public allProgrammings: ChallengeProgramming[] = [];

  public entitiesQuery!: string;

  public groupsQuery!: string;

  public usersQuery!: string;

  public allDidacticUnits: DidacticUnit[] = [];

  public selectedDidacticUnits: DidacticUnit[] = [];

  constructor(
    public apiService: ApiService,
    public modalController: ModalController,
  ) { }

  ngOnInit(): void {
    this.getChallengeDefinitions();
    this.loadDidacticUnits();
  }

  loadDidacticUnits() {
    this.apiService.getDidacticUnits().subscribe({
      next:
        (result) => {
          if (result.status === 200) {
            this.allDidacticUnits = result.message;
          }
        },
      error: error => console.error(error),
    });
  }

  formatDidacticUnits(units: DidacticUnit[]) {
    return units.map(unit => unit.name).join(', ');
  }

  onSelectDidacticUnits() { 

    // Extracting all ids from selectedDidacticUnits
    const selectedDidacticUnitsIds = this.selectedDidacticUnits.flatMap(unit => unit.id);
    
    this.challengeProgrammings = this.allProgrammings.filter(def => {
        if (this.selectedDidacticUnits.length === 0) { return true; } // No filter if no didactic unit is selected
        if (!def.didacticUnitsIds) { return false; } // Filter out challenges without didactic unit

        // Check if any of the didacticUnitsIds matches the selectedDidacticUnitsIds
        return def.didacticUnitsIds.some(unitId => selectedDidacticUnitsIds.includes(unitId));
    }); 
}

  getChallengeDefinitions() {
    this.apiService.getFrontProgrammingChallenges(
      this.entitiesQuery,
      this.groupsQuery,
      this.usersQuery).subscribe({
        next: (result: ApiResponse<ChallengeProgramming[]>) => {
          if (result.status === 200) {
            this.challengeProgrammings = [];
            this.allProgrammings = [];
            this.challengeProgrammings.push(...result.message);
            this.allProgrammings.push(...this.challengeProgrammings);
          }
        },
        error: error => console.error(error),
      });
  }

  onChallengeSelected(challenge: ChallengeProgramming) {
    this.presentModal(challenge);
  }

  async presentModal(challenge: ChallengeProgramming) {
    const modal = await this.modalController.create({
      component: ChallengePreviewModalComponent,
      componentProps: {
        challenge,
      },
      cssClass: 'modal-fullscreen',
    });
    return modal.present();
  }

  onSearch(event: Event) {
    const value = (event as CustomEvent).detail.value;
    if (value) {
      if (value.trim !== '') {
        this.challengeProgrammings = this.allProgrammings.filter(term => {
          return term.title.toLowerCase().indexOf(value.trim().toLowerCase()) > -1;
        });
      }
    } else {
      this.challengeProgrammings = this.allProgrammings;
    }
  }

  onLockDownChanged(event: CustomEvent) {
    const lockDownMode = event.detail.value;

    this.challengeProgrammings = this.allProgrammings.filter(challenge => {
      return lockDownMode === ChallengeLockdownTypes.ALL ? true :
        (lockDownMode === ChallengeLockdownTypes.COMPATIBLE ? challenge.lockDown === true : challenge.lockDown === false);
    });
  }

  /**
   * Callback function to detect changes in the app-assign-to component data
   * @param assignments 
   */
  assignToComponentChange(assignments: ItemAssignedTo[]) {
    // send this filters to challengeProgramming call
    const entities: string[] = [];
    const groups: string[] = [];
    const users: string[] = [];

    assignments.forEach((assignment: ItemAssignedTo, _index: number) => {
      assignment.users.forEach((user: User) => users.push(user.id));
      assignment.entities.forEach((entity: Entity) => entities.push(entity.id));
      assignment.groups.forEach((group: Group) => groups.push(group.id));
    });

    this.entitiesQuery = entities.join(',');
    this.groupsQuery = groups.join(',');
    this.usersQuery = users.join(',');
    this.getChallengeDefinitions();
  }
}
