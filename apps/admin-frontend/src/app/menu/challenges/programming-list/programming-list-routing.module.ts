import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ProgrammingListPageComponent } from './programming-list.page';

const routes: Routes = [
  {
    path: '',
    component: ProgrammingListPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProgrammingListPageRoutingModule { }
