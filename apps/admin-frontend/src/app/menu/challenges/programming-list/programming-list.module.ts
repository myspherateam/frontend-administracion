import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { CommonsModule } from '../common/commons.module';
import { ProgrammingListPageComponent } from './programming-list.page';
import { ProgrammingListPageRoutingModule } from './programming-list-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    ProgrammingListPageRoutingModule,
    CommonsModule,
    IonicSelectableModule,
  ],
  declarations: [ProgrammingListPageComponent],
})
export class ProgrammingListPageModule { }
