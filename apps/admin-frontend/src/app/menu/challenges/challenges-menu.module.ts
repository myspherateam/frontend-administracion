import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { ChallengesMenuPageComponent } from './challenges-menu.page';
import { ChallengesMenuPageRoutingModule } from './challenges-menu-routing.module';
import { CommonsModule } from './common/commons.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    ChallengesMenuPageRoutingModule,
    CommonsModule,
  ],
  declarations: [ChallengesMenuPageComponent],
})
export class ChallengesMenuPageModule { }
