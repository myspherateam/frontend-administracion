import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { startWith } from 'rxjs';

@Component({
  selector: 'app-challenges-menu',
  templateUrl: './challenges-menu.page.html',
  styleUrls: ['./challenges-menu.page.scss'],
})
export class ChallengesMenuPageComponent implements OnInit {

  options: Array<Record<string, string>>;

  constructor(private translate: TranslateService) { }

  ngOnInit(): void {
    this.translate.onLangChange.pipe(startWith(this.translate.currentLang)).subscribe({
      next: () => {
        this.options = [
          {
            name: this.translate.instant('MENU.CHALLENGES.CHALLENGE_DEFINITIONS'),
            url: 'definition-list',
            icon: 'add-circle',
          },
          {
            name: this.translate.instant('DIDACTIC_UNIT.TITLE'),
            url: 'didactic-units',
            icon: 'easel',
          },
          {
            name: this.translate.instant('MENU.CHALLENGES.CHALLENGE_PROGRAMMINGS'),
            url: 'programming-list',
            icon: 'person-add',
          },
        ];
      },
    });
  }
}
