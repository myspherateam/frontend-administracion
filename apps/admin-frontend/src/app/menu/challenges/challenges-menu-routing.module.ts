import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ChallengesMenuPageComponent } from './challenges-menu.page';

const routes: Routes = [
  {
    path: '',
    component: ChallengesMenuPageComponent,
  },
  {
    path: 'definition-list',
    loadChildren: () => import('./definition-list/definition-list.module').then(m => m.DefinitionListPageModule),
  },
  {
    path: 'programming-list',
    loadChildren: () => import('./programming-list/programming-list.module').then(m => m.ProgrammingListPageModule),
  },
  {
    path: 'add-challenge',
    loadChildren: () => import('./common/add-challenge/add-challenge.module').then(m => m.AddChallengePageModule),
  },
  {
    path: 'assign-challenge',
    loadChildren: () => import('./common/assign-challenge/assign-challenge.module').then(m => m.AssignChallengePageModule),
  },
  {
    path: 'users-assigned-to-challenge',
    loadChildren: () => import('./common/users-assigned-to-challenge/users-assigned-to-challenge.module')
      .then(m => m.UsersAssignedToChallengePageModule),
  },
  {
    path: 'edit-challenge',
    loadChildren: () => import('./common/edit-challenge/edit-challenge.module').then(m => m.EditChallengePageModule),
  },
  {
    path: 'didactic-units',
    loadChildren: () => import('./didactic-units/didactic-units.module').then(m => m.DidacticUnitsPageModule),
  },
  {
    path: 'assign-didactic-unit',
    loadChildren: () => import('./common/assign-didactic-unit/assign-didactic-unit.module').then(m => m.AssignDidacticUnitPageModule),
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChallengesMenuPageRoutingModule { }
