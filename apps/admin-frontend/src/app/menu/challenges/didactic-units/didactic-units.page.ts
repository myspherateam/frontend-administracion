import { AlertController, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, ChallengeDefinition, DidacticUnit, DidacticUnitChallenge } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { ChallengePreviewModalComponent } from '../common/challenge-preview-modal/challenge-preview-modal.component';
import { PresentatorService } from '../../../services/common/presentator.service';
import { Toast } from '../../../services/common/dto/toast';

type Callback = () => void;

@Component({
  selector: 'app-didactic-units',
  templateUrl: './didactic-units.page.html',
  styleUrls: ['./didactic-units.page.scss'],
})
export class DidacticUnitsPageComponent implements OnInit {

  public allDidacticUnits: DidacticUnit[] = [];

  public selectedDidacticUnit?: DidacticUnit;

  public allDefinitions: Record<string, ChallengeDefinition> = {};

  public challengeDefinitions: ChallengeDefinition[] = [];

  public createDidacticUnitUrl = '/menu/challenges-menu/didactic-units/add-didactic-unit';

  constructor(
    private apiService: ApiService,
    private modalController: ModalController,
    private alertController: AlertController,
    private traslateService: TranslateService,
    private presentatorService: PresentatorService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.loadDidacticUnits();
  }

  /**
   * Method that retrieves all didactic units from the server
   */
  private loadDidacticUnits() {
    this.apiService.getDidacticUnits().subscribe({
      next: (result) => {
        if (result.status === 200) {
          this.allDidacticUnits = result.message;
          this.selectedDidacticUnit = this.allDidacticUnits[0];
          this.getChallengeDefinitions();
        }
      },
      error: error => console.error(error),
    });
  }

  getChallengeDefinitions() {
    this.apiService.getChallengeDefinitions().subscribe({
      next: (result: ApiResponse<ChallengeDefinition[]>) => {
        if (result.status === 200) {
          this.challengeDefinitions = [];
          this.allDefinitions = {};
          result.message.forEach((def) => {
            this.allDefinitions[def.id] = def;
          });
          if (this.selectedDidacticUnit) {
            this.getDefinitionsByUnit();
          }
        }
      },
      error: error => console.error(error),
    });
  }

  getDefinitionsByUnit() {
    if(this.selectedDidacticUnit.challenges){
      this.challengeDefinitions = [...this.selectedDidacticUnit.challenges.map((challenge: DidacticUnitChallenge) => {
        return this.allDefinitions[challenge.challengeId];
      }).filter((value) => !!value)];
    }
    
  }

  /**
   * Callback method for the unit selection control
   */
  didacticUnitSelected() {
    this.getDefinitionsByUnit();
  }

  /**
   * Callback method used to show a modal with challenda information
   * @param challenge 
   */
  async onChallengeClicked(challenge: ChallengeDefinition) {
    const modal = await this.modalController.create({
      component: ChallengePreviewModalComponent,
      componentProps: {
        challenge,
      },
      cssClass: 'modal-fullscreen',
    });
    return modal.present();
  }

  createDidacticUnit() {
    this.router.navigateByUrl(this.createDidacticUnitUrl, {
      state: {},
    });
  }

  editDidacticUnit() {
    this.router.navigateByUrl(this.createDidacticUnitUrl, {
      state: {
        selectedDidacticUnit: this.selectedDidacticUnit,
        challenges: Object.values(this.allDefinitions),
      },
    });
  }

  /**
   * Callback method for the delete unit button
   */
  deleteDidacticUnit() {
    this.presentRemoveAlert(() => {
      this.apiService.deleteDidacticUnit(this.selectedDidacticUnit as DidacticUnit).subscribe({
        next: (result: ApiResponse<DidacticUnit>) => {
          if (result.status === 200) {
            const index = this.allDidacticUnits.indexOf(this.selectedDidacticUnit as DidacticUnit);
            this.allDidacticUnits.splice(index, 1);
            this.selectedDidacticUnit = this.allDidacticUnits[0];
            this.getChallengeDefinitions();
            this.presentatorService.showToast(Toast.message(this.traslateService.instant('DIDACTIC_UNIT.DELETE_DIDACTIC_UNIT_SUCCESS')));
          }
        },
      });
    });
  }

  async presentRemoveAlert(callback: Callback) {
    const deleteAlert = await this.alertController.create({
      header: this.traslateService.instant('DIDACTIC_UNIT.DELETE_DIDACTIC_UNIT'),
      subHeader: this.traslateService.instant('DIDACTIC_UNIT.DELETE_DIDACTIC_UNIT_EXTENDED', { val: this.selectedDidacticUnit?.name }),
      buttons: [
        {
          text: this.traslateService.instant('ACTION.CANCEL'),
        },
        {
          text: this.traslateService.instant('ACTION.DELETE'),
          handler: () => {
            callback();
          },
        },
      ],
    });
    return await deleteAlert.present();
  }

}
