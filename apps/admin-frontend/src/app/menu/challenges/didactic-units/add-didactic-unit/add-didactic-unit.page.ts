import {
  AbstractControl, FormArray, FormControl, FormGroup, UntypedFormBuilder, UntypedFormGroup, ValidatorFn, Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Observable, firstValueFrom, map } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, Badge, ChallengeDefinition, ChallengeLockdownTypes, DidacticUnit, DidacticUnitChallenge } from '@paido/data-model';

import { ApiService } from '../../../../api/api-service';
import { CanComponentDeactivate } from '../../../../services/guards/can-deactivate-guard';
import { NavController } from '@ionic/angular';
import { PresentatorService } from '../../../../services/common/presentator.service';
import { Toast } from '../../../../services/common/dto/toast';

@Component({
  selector: 'app-add-didactic-unit',
  templateUrl: './add-didactic-unit.page.html',
  styleUrls: ['./add-didactic-unit.page.scss'],
})
export class AddDidacticUnitPageComponent implements OnInit, CanComponentDeactivate {

  // form reference
  public addUnitForm: UntypedFormGroup;

  // all found challenges
  private allChallenges: ChallengeDefinition[];

  // filtered challenges to show on the template
  public challenges: ChallengeDefinition[];

  public showChallenges: boolean;

  public isEditMode: boolean = false;

  public badgeUrlImage: string | ArrayBuffer;

  constructor(
    private apiService: ApiService,
    private presentatorService: PresentatorService,
    private translateService: TranslateService,
    private router: Router,
    private navController: NavController,
  ) {
    this.showChallenges = true;
    this.challenges = [];
    this.allChallenges = [];
  }

  atLeastOneInitDateZeroValidator(): ValidatorFn {
    return (formArray: FormArray) => {
      const hasOneInitDateZero = formArray.controls.some(control => control.get('initDate').value === 0);
      return hasOneInitDateZero ? null : { noInitDateZero: true };
    };
  }

  async ngOnInit() {
    this.addUnitForm = new UntypedFormBuilder().group({
      id: new FormControl<string | null>({ value: null, disabled: true }),
      name: new FormControl<string>('', Validators.required),
      duration: new FormControl<number | null>(null, [Validators.required, Validators.min(1)]),
      selectedChallenges: new FormArray<FormControl<ChallengeDefinition>>([], 
        [Validators.required, this.atLeastOneInitDateZeroValidator()]),
      addBadge: new FormControl<boolean>({ value: true, disabled: false }),
      badge: this.generateBadgeFormGroup(),
    });
    this.addUnitForm.markAllAsTouched();
    this.addBadge.valueChanges.subscribe({
      next: (value: boolean) => {
        if (value) {
          this.addUnitForm.addControl('badge', this.generateBadgeFormGroup());
        } else {
          this.addUnitForm.removeControl('badge');
        }
        /*  this.addBadge.markAsUntouched();
         this.addBadge.markAsPristine(); */
        this.addUnitForm.updateValueAndValidity();
      },
    });

    this.addUnitForm.valueChanges.subscribe();

    try {
      const selectedUnit = this.router.getCurrentNavigation()?.extras?.state?.selectedDidacticUnit;
      const challenges = this.router.getCurrentNavigation()?.extras?.state?.challenges;
      if (challenges) {
        this.challenges = [...challenges];
      } else {
        this.challenges = await firstValueFrom(this.apiService.getChallengeDefinitions()
          .pipe(
            map((response: ApiResponse<ChallengeDefinition[]>) => response.message)));
      }
      this.allChallenges = [...this.challenges];
      if (selectedUnit) {
        this.isEditMode = true;
        this.initializeUnitForm(selectedUnit);
      }

    } catch (error) {
      console.error(error);
    }
  }

  /**
   * Hook to verify the user wants to leave when changes are unsaved
   * @returns
   */
  canDeactivate(): boolean | Observable<boolean> | Promise<boolean> {
    // TODO: add translation
    return this.addUnitForm.pristine || confirm('¿Seguro que deseas salir de la página? Puede que los cambios no se mantengan.');
  }

  initializeUnitForm(unit: DidacticUnit) {
    this.id.patchValue(unit.id);
    this.name.patchValue(unit.name);
    this.duration.patchValue(unit.duration || 0);
    unit.challenges?.forEach((challenge: DidacticUnitChallenge) => {
      this.allChallenges.forEach((aux) => {
        if (aux.id === challenge.challengeId) {
          aux.selected = true;
          this.selectedChallenges.push(this.createChallengeFormGroup(aux, challenge));
        }
      });
    });
    if (unit.badgeId) { this.loadBadge(unit.badgeId); } else { this.addBadge.patchValue(false); }
  }

  // form get attributes
  get id() {
    return this.addUnitForm?.get('id');
  }

  get name() {
    return this.addUnitForm?.get('name');
  }

  get duration() {
    return this.addUnitForm?.get('duration');
  }

  get selectedChallenges() {
    return this.addUnitForm?.get('selectedChallenges') as FormArray<FormGroup>;
  }

  get addBadge() {
    return this.addUnitForm?.get('addBadge');
  }

  get badge(): FormGroup {
    return this.addUnitForm?.get('badge') as FormGroup;
  }

  get badgeName(): AbstractControl | null {
    return this.badge?.get('badgeName');
  }

  get badgeDescription() {
    return this.badge?.get('badgeDescription');
  }

  get badgeImage() {
    return this.badge?.get('badgeImage');
  }

  get badgeId() {
    return this.badge?.get('badgeId');
  }

  get badgeFile() {
    return this.badge?.get('badgeFile');
  }

  /**
   * Helper method to load the badge data into the form
   * @param badgeId
   */
  private loadBadge(badgeId: string) {
    this.apiService.getBadge(badgeId).pipe(
      map((response: ApiResponse<Badge>) => response.message),
    ).subscribe({
      next: (badge: Badge) => {
        this.addBadge.patchValue(true);
        this.addUnitForm.setControl('badge', this.generateBadgeFormGroup(badge));
      },
    });
  }

  /**
   * Callback method for the 'select challenges' button
   */
  toggleChallengesList() {
    this.showChallenges = !this.showChallenges;
  }

  /**
   * Callback method for the search challenges input
   * @param event
   */
  onSearch(event: CustomEvent) {
    const value = event.detail.value;
    if (value) {
      if (value.trim !== '') {
        this.challenges = this.allChallenges.filter(term => {
          return term.title.toLowerCase().indexOf(value.trim().toLowerCase()) > -1;
        });
      }
    } else {
      this.challenges = this.allChallenges;
    }
  }

  /**
   * Callback method for the segment control asking for "lockdown" compatible challenges
   * @param event
   */
  onLockDownChanged(event: CustomEvent) {
    const lockDownMode = event.detail.value;
    this.challenges = this.allChallenges.filter(challenge => {
      switch (lockDownMode) {
        case ChallengeLockdownTypes.ALL:
          return true;
        case ChallengeLockdownTypes.COMPATIBLE:
          return challenge.lockDown === true;
        default:
          return challenge.lockDown === false;
      }
    });
  }

  /**
   * Change selected state & save/remove selected challenge
   * @param challenge challenge selected
   */
  onChallengeSelected(challenge: ChallengeDefinition, index: number) {

    if (this.challenges[index].selected) {

      this.selectedChallenges.controls.forEach((selected: FormGroup, _index: number) => {
        if (selected.getRawValue().challenge.id === challenge.id) {
          this.selectedChallenges.removeAt(_index);
          return;
        }
      });
      challenge.selected = false;
      challenge.startOffset = undefined;
      challenge.endOffset = undefined;
    } else {
      this.selectedChallenges.push(this.createChallengeFormGroup(challenge));
      challenge.selected = true;
    }
  }

  /**
   * Helper method that adds a new challengeSetup form to the challenge array
   * @param challenge
   * @returns
   */
  private createChallengeFormGroup(challenge: ChallengeDefinition, init?: DidacticUnitChallenge): FormGroup {
    const form = new FormGroup({
      challenge: new FormControl<ChallengeDefinition>({ value: challenge, disabled: true }, Validators.required),
      initDate: new FormControl<number>(init?.startOffset, [Validators.required]),
      endDate: new FormControl<number>(init?.endOffset, [Validators.required]),
    });

    const initDate = form.get('initDate');
    initDate.addValidators([
      (control: AbstractControl) => {
        const errors: Record<string, string> = {};
        // validate initDate <= duration
        if (this.addUnitForm.get('duration').value < control.value) {
          // TODO: add translation
          errors.startDateInvalid = 'La fecha de inicio no puede ser mayor que la duración del desafío';
        }
        return errors;
      },
    ]);
    const endDate = form.get('endDate');
    endDate.addValidators([
      (control: AbstractControl) => {
        const errors: Record<string, string> = {};
        // validate endate >= initDate
        if ((initDate.value || 0) > (control.value || 0)) {
          // TODO: add translation
          errors.endDateInvalid = 'La fecha de fin no puede ser menor que la fecha de inicio';
        }
        // validate endDate <= duration
        if (this.addUnitForm.get('duration').value < control.value) {
          // TODO: add translation
          errors.endDateInvalid = 'La fecha de fin no puede ser mayor que la duración del desafío';
        }
        return errors;
      },
    ]);

    initDate.valueChanges.subscribe({ next: () => form.get('endDate').updateValueAndValidity() });
    this.duration.valueChanges.subscribe({ next: () => { initDate.updateValueAndValidity(); endDate.updateValueAndValidity(); } });

    form.markAllAsTouched(); // make ui show that dates may be filled

    return form;
  }

  private generateBadgeFormGroup(badge?: Badge) {
    const binImg = badge?.image?.data.replace('data:image/png;base64,', '').replace(/^/, 'data:image/png;base64,');
    return new FormGroup({
      badgeId: new FormControl<string>({ value: badge?.id, disabled: true }),
      badgeName: new FormControl<string>(badge?.name, [Validators.required]),
      badgeDescription: new FormControl<string>(badge?.description),
      badgeImage: new FormControl<string | ArrayBuffer>(binImg, [Validators.required]),
      badgeFile: new FormControl<File>({ value: null, disabled: true }),
    });
  }

  /**
   * Callback method for the select file button
   * @param files
   */
  loadImage(files: File[]) {
    const imageFile = files[0];
    if (imageFile) {
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent<FileReader>) => {
        this.badgeImage.patchValue(e.target.result);
        this.badgeImage.markAsDirty();
        this.badgeImage.markAsTouched();
        this.badgeFile.patchValue(imageFile);
        this.badgeFile.markAsDirty();
        this.badgeFile.markAsTouched();
      };
      reader.readAsDataURL(imageFile);
    }
  }

  dateInfoClicked(header: string) {
    const message = (header === 'DIDACTIC_UNIT.START_DAY') ? 'DIDACTIC_UNIT.HELP_START_DAY' : 'DIDACTIC_UNIT.HELP_FINISH_DAY';
    this.presentatorService
      .showInfoAlert(this.translateService.instant(header), this.translateService.instant(message));
  }

  /**
   * Create/modify button is pressed. Alert user to confirm action.
   */
  createDidacticUnit() {
    this.presentatorService.createYesNoAlert(
      this.isEditMode ? 'DIDACTIC_UNIT.MODIFY_DIDACTIC_UNIT' : 'DIDACTIC_UNIT.CREATE',
      this.isEditMode ? 'DIDACTIC_UNIT.MODIFY_QUESTION' : 'DIDACTIC_UNIT.CREATE_DIDACTIC_UNIT',
    ).then((accepted: boolean) => {
      if (accepted) {
        if (this.isEditMode) {
          this.editDidacticUnit();

        } else {
          // create badge and unit
          if (this.badge !== null) {
            this.createBadgeAndUnit();
          } else {
            this.createOrUpdateDidacticUnit();
          }
        }
      }
    });
  }

  /**
  * Call create or update didactic unit
  * @param newBadge badge instance
  */
  private createOrUpdateDidacticUnit(newBadge?: Badge) {
    this.apiService.createOrUpdateDidacticUnit(this.getDidacticUnit(newBadge)).subscribe({
      next: (responseUnit: ApiResponse<DidacticUnit>) => {
        if (responseUnit.status === 200) {
          this.addUnitForm.markAsUntouched();
          this.addUnitForm.markAsPristine();
          this.presentatorService.showToast(
            Toast.message(
              this.isEditMode ?
                this.translateService.instant('DIDACTIC_UNIT.MODIFY_DIDACTIC_UNIT_SUCCESS') :
                this.translateService.instant('DIDACTIC_UNIT.ADD_DIDACTIC_UNIT_SUCCESS'),
            ),
          );
          this.navController.back();
        }
      },
      error: (error: HttpErrorResponse) => {
        console.error('error');
        console.error(error);
        this.presentatorService.showToast(
          Toast.message(
            this.isEditMode ?
              this.translateService.instant('DIDACTIC_UNIT.MODIFY_DIDACTIC_UNIT_ERROR') +', error:'+ error.error.message:
              this.translateService.instant('DIDACTIC_UNIT.ADD_DIDACTIC_UNIT_ERROR') +', error:'+ error.error.message,
          ),
          
        );
        
      },
    });
  }

  /**
   * Edit didactic unit
   */
  private editDidacticUnit() {
    if (this.addBadge.value) {

      // if badge exists modify it, if not create one
      if (this.badgeId?.value !== undefined && this.badge.dirty) {
        const newBadgeUnit = this.getBadgeUnit();
        this.apiService.modifyBadge(newBadgeUnit).subscribe({
          next: (result: ApiResponse<Badge>) => {
            if (result.status === 200) {
              const newBadge = result.message;
              this.createOrUpdateDidacticUnit(newBadge);
            }
          },
        });
      } else {
        this.createBadgeAndUnit();
      }
    } else {
      this.createOrUpdateDidacticUnit();
    }
  }

  /**
   * Create new badge and then a new didactic unit
   */
  private createBadgeAndUnit() {
    this.apiService.createBadge(this.getBadgeUnit()).subscribe({
      next: (response: ApiResponse<Badge>) => {
        if (response.status === 200) {
          const newBadge: Badge = response.message;
          this.createOrUpdateDidacticUnit(newBadge);
        }
      },
      error: (error: HttpErrorResponse) => {
       
        console.error(error);
        this.presentatorService.showToast(
          Toast.message(
            this.isEditMode ?
              this.translateService.instant('DIDACTIC_UNIT.MODIFY_DIDACTIC_UNIT_ERROR') +', error: '+ error.error.message :
              this.translateService.instant('DIDACTIC_UNIT.ADD_DIDACTIC_UNIT_ERROR') +', error:'+ error.error.message,
          ),
        );
        
      },
    });
  }

  /**
   * Create didactic unit object with all attributes
   */
  private getDidacticUnit(badge?: Badge): DidacticUnit {

    const newDidacticUnit = {} as DidacticUnit;

    newDidacticUnit.id = this.id.value;
    newDidacticUnit.name = this.name.value;
    newDidacticUnit.duration = this.duration.value;
    newDidacticUnit.challenges = this.selectedChallenges.controls
      .map((form) => form.getRawValue())
      .map((value: Record<string, unknown>) => {
        return {
          challengeId: (value.challenge as ChallengeDefinition).id,
          startOffset: value.initDate,
          endOffset: value.endDate,
        } as DidacticUnitChallenge;
      });

    if (badge) { newDidacticUnit.badgeId = badge.id; }

    return newDidacticUnit;
  }

  /**
   * Create badge object with all attributes
   */
  private getBadgeUnit(): Badge {
    return {
      id: this.badgeId?.value,
      name: this.badgeName?.value,
      description: this.badgeDescription?.value,
      image: this.badgeFile?.value,
    } as Badge;
  }
}
