import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AddDidacticUnitPageComponent } from './add-didactic-unit.page';
import { AddDidacticUnitPageRoutingModule } from './add-didactic-unit-routing.module';
import { AuthImagePipe } from '../../../../pipes/auth-image-pipe';
import { ChallengeSetupItemComponent } from './challenge-setup-item/challenge-setup-item.component';
import { SafeHtml } from '../../../../pipes/safe-html';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddDidacticUnitPageRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AddDidacticUnitPageComponent,
    AuthImagePipe,
    SafeHtml,
    ChallengeSetupItemComponent,
  ],
  exports: [
    AuthImagePipe,
    SafeHtml,
  ],
})
export class AddDidacticUnitPageModule { }
