import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddDidacticUnitPageComponent } from './add-didactic-unit.page';

describe('AddDidacticUnitPage', () => {
  let component: AddDidacticUnitPageComponent;
  let fixture: ComponentFixture<AddDidacticUnitPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AddDidacticUnitPageComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(AddDidacticUnitPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
