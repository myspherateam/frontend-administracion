import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AddDidacticUnitPageComponent } from './add-didactic-unit.page';
import { CanDeactivateGuard } from '../../../../services/guards/can-deactivate-guard';

const routes: Routes = [
  {
    path: '',
    component: AddDidacticUnitPageComponent,
    canDeactivate: [CanDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddDidacticUnitPageRoutingModule { }
