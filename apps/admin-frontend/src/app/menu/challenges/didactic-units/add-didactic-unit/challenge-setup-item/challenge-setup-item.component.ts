import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-challenge-setup-item',
  standalone: false,
  templateUrl: './challenge-setup-item.component.html',
  styleUrls: ['./challenge-setup-item.component.scss'],
})
export class ChallengeSetupItemComponent {

  @Input() challengeForm: FormGroup;
}
