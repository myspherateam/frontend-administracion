import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { DidacticUnitsPageComponent } from './didactic-units.page';
import { DidacticUnitsPageRoutingModule } from './didactic-units-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DidacticUnitsPageRoutingModule,
    TranslateModule,
    IonicSelectableModule,
  ],
  declarations: [DidacticUnitsPageComponent],
})
export class DidacticUnitsPageModule { }
