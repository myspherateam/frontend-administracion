import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DidacticUnitsPageComponent } from './didactic-units.page';

const routes: Routes = [
  {
    path: '',
    component: DidacticUnitsPageComponent,
  },
  {
    path: 'add-didactic-unit',
    loadChildren: () => import('./add-didactic-unit/add-didactic-unit.module').then(m => m.AddDidacticUnitPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DidacticUnitsPageRoutingModule { }
