import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

import { ApiResponse, ChallengeDefinition, ChallengeLockdownTypes, DidacticUnit } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { ChallengePreviewModalComponent } from '../common/challenge-preview-modal/challenge-preview-modal.component';
import { PresentatorService } from '../../../services/common/presentator.service';
import { Toast } from '../../../services/common/dto/toast';

@Component({
  selector: 'app-definition-list',
  templateUrl: './definition-list.page.html',
  styleUrls: ['./definition-list.page.scss'],
})
export class DefinitionListPageComponent {

  public challengeDefinitions: ChallengeDefinition[] = [];

  public addChallengeUrl = '/menu/challenges-menu/add-challenge';

  public editChallengeUrl = '/menu/challenges-menu/edit-challenge';

  public allDefinitions: ChallengeDefinition[] = [];

  public allDidacticUnits: DidacticUnit[] = [];

  public selectedDidacticUnits: DidacticUnit[] = [];

  constructor(
    public apiService: ApiService,
    private modalController: ModalController,
    private presentatorService: PresentatorService,
    private router: Router) { }

  ionViewWillEnter() {
    this.getChallengeDefinitions();
    this.loadDidacticUnits();
  }

  loadDidacticUnits() {
    this.apiService.getDidacticUnits().subscribe({
      next: (result) => {
        if (result.status === 200) {
          this.allDidacticUnits = result.message;
        }
      },
      error: error => console.error(error),
    });
  }

  formatDidacticUnits(units: DidacticUnit[]) {
    return units.map(unit => unit.name).join(', ');
  }

  onSelectDidacticUnits() {
    this.challengeDefinitions = this.allDefinitions.filter(def => {
      if (this.selectedDidacticUnits.length === 0) { return true; } // on user select none (deselect) -> no filter
      if (!def.didacticUnitsIds) { return false; } // challenge without didactic unit

      return def.didacticUnitsIds.find(unitId => this.selectedDidacticUnits.map(unit => unit.id).includes(unitId)) !== null;
    });
  }

  getChallengeDefinitions() {
    this.apiService.getChallengeDefinitions().subscribe({
      next: (result: ApiResponse<ChallengeDefinition[]>) => {
        if (result.status === 200) {
          this.challengeDefinitions = [];
          this.allDefinitions = [];
          this.challengeDefinitions.push(...result.message);
          this.allDefinitions.push(...this.challengeDefinitions);
        }
      },
      error: error => console.error(error),
    });
  }

  onChallengeSelected(challenge: ChallengeDefinition) {
    this.presentModal(challenge);
  }

  async presentModal(challenge: ChallengeDefinition) {
    const modal = await this.modalController.create({
      component: ChallengePreviewModalComponent,
      componentProps: { challenge },
      cssClass: 'modal-fullscreen',
    });
    return await modal.present();
  }

  onSearch(event: CustomEvent) {
    const value = event.detail.value;
    if (value) {
      if (value.trim !== '') {
        this.challengeDefinitions = this.allDefinitions.filter(term => {
          return term.title.toLowerCase().indexOf(value.trim().toLowerCase()) > -1;
        });
      }
    } else {
      this.challengeDefinitions = this.allDefinitions;
    }
  }

  onLockDownChanged(event: CustomEvent) {
    const lockDownMode = event.detail.value;

    this.challengeDefinitions = this.allDefinitions.filter(challenge => {
      return lockDownMode === ChallengeLockdownTypes.ALL ?
        true : (lockDownMode === ChallengeLockdownTypes.COMPATIBLE ? challenge.lockDown === true : challenge.lockDown === false);
    });
  }

  editChallenge(challenge: ChallengeDefinition) {
    this.router.navigateByUrl(this.editChallengeUrl, { state: { challenge } });
  }

  deleteChallenge(challenge: ChallengeDefinition) {
    this.presentatorService.createYesNoAlert('CHALLENGES.DELETE_CHALLENGE', 'CHALLENGES.DELETE_CHALLENGE_EXTENDED').then(
      (result: boolean) => {
        if (result) {
          this.apiService.deleteChallengeDefinition(challenge).subscribe({
            next: (result2: ApiResponse<ChallengeDefinition>) => {
              if (result2.status === 200) {
                this.getChallengeDefinitions();
                this.presentatorService.showToast(Toast.message('CHALLENGES.DELETE_CHALLENGE_SUCCESS'));
              }
            },
            error: error => {
              console.error(error);
              this.presentatorService.showToast(Toast.message('ERRORS.DELETE_CHALLENGE_FAIL'));
            },
          });
        }
      },
    );
  }
}
