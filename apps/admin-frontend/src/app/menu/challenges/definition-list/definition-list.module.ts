import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { DefinitionListPageComponent } from './definition-list.page';
import { DefinitionListPageRoutingModule } from './definition-list-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    DefinitionListPageRoutingModule,
    IonicSelectableModule,
  ],
  declarations: [DefinitionListPageComponent],
})
export class DefinitionListPageModule { }
