import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DefinitionListPageComponent } from './definition-list.page';

const routes: Routes = [
  {
    path: '',
    component: DefinitionListPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DefinitionListPageRoutingModule { }
