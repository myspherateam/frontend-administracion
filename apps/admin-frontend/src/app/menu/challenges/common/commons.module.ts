import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AssignToComponent } from './assign-to/assign-to.component';
import { ChallengeFormComponent } from './challenge-form/challenge-form.component';
import { ChallengePreviewComponent } from './challenge-preview/challenge-preview.component';
import { ChallengePreviewModalComponent } from './challenge-preview-modal/challenge-preview-modal.component';
import { NewsModule } from './news/news.module';
import { SurveyModule } from './survey/survey.module';
import { UsersAssignedToChallengePageModule } from './users-assigned-to-challenge/users-assigned-to-challenge.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NewsModule,
        SurveyModule,
        TranslateModule.forChild(),
        UsersAssignedToChallengePageModule,
        ReactiveFormsModule,
        IonicSelectableModule,
        LeafletModule,
    ],
    declarations: [ChallengePreviewComponent, ChallengePreviewModalComponent, AssignToComponent, ChallengeFormComponent],
    exports: [ChallengePreviewComponent, AssignToComponent, ChallengeFormComponent],
})
export class CommonsModule { }
