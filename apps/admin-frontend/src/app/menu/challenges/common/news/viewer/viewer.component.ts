import { Component, Input } from '@angular/core';

import { Delta } from '../interfaces/delta';
// Needed to actually run the code that injects the extensions into quill
//import '../quill-extensions/load';

@Component({
  selector: 'app-news-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss'],
})
export class ViewerComponent {
  @Input() content!: Delta;

}
