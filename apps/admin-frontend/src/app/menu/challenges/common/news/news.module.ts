import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { QuillModule } from 'ngx-quill';

import { EditorComponent } from './editor/editor.component';
import { ViewerComponent } from './viewer/viewer.component';

import { ImageDrop } from 'quill-image-drop-module';
import ImageResize from 'quill-image-resize';
import Quill from 'quill';

import { ImageBlot } from './quill-extensions/ImageBlot';

// eslint-disable-next-line @typescript-eslint/no-var-requires
Quill.register('modules/imageResize', ImageResize);
// eslint-disable-next-line @typescript-eslint/no-var-requires
Quill.register('modules/imageDrop', ImageDrop);
Quill.register(ImageBlot, true);

@NgModule({
  declarations: [EditorComponent, ViewerComponent],
  imports: [
    CommonModule,
    FormsModule,
    QuillModule.forRoot(),
  ],
  exports: [
    EditorComponent,
    ViewerComponent,
  ],
})
export class NewsModule { }
