import 'leaflet';
import 'leaflet-draw';
import * as Survey from 'survey-angular';

import { Component, OnInit } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
// TODO: replace this component with native controls (PAID-450)
import { ModalController, NavController } from '@ionic/angular';
import { Ionic4DatepickerModalComponent } from '@logisticinfotech/ionic4-datepicker';
import { IonicSelectableComponent } from 'ionic-selectable';

import {
  ApiResponse, AssignedToIds, ChallengeDefinition, ChallengeLockdownTypes, ChallengeProgramming, DidacticUnit,
  Entity, Group, ItemAssignedTo, User,
} from '@paido/data-model';

import { ApiService } from '../../../../api/api-service';
import { PresentatorService } from '../../../../services/common/presentator.service';
import { Toast } from '../../../../services/common/dto/toast';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-assign-challenge',
  templateUrl: './assign-challenge.page.html',
  styleUrls: ['./assign-challenge.page.scss'],
})
export class AssignChallengePageComponent implements OnInit {

  // reference to the form in the template
  public assignForm: UntypedFormGroup;

  // support variable to get the value from the 'initialDate' form control
  public initialChallengeDate!: string;

  // support variable to get the value from the 'finishDate' form control
  public finishChallengeDate!: string;

  // support variable to get the value from the 'visibleDate' form control
  public visibleChallengeDate!: string;

  public selectedChallenge!: ChallengeProgramming;

  public challenges: ChallengeDefinition[] = [];

  private allChallenges: ChallengeDefinition[] = [];

  private minDatetime!: Date;

  private maxDatetime!: Date;

  private assignedToItems!: ItemAssignedTo[];

  public overrideDefinition!: boolean;

  private datePickerObj: Record<string, unknown>;

  private assignToLevel = 'entity';

  public selectChallengeClicked = false;

  public isSubmitted: boolean = false;

  public submitEmitter: Subject<void> = new Subject<void>();

  public clearEmitter: Subject<void> = new Subject<void>();

  public selectedChallengeEmitter: ReplaySubject<ChallengeDefinition> = new ReplaySubject<ChallengeDefinition>(1);

  public allDidacticUnits: DidacticUnit[] = [];

  constructor(
    private formBuilder: UntypedFormBuilder,
    private presentatorService: PresentatorService,
    private apiService: ApiService,
    private analytics: AngularFireAnalytics,
    private modalCtrl: ModalController,
    private translate: TranslateService,
    private navController: NavController,
  ) {
    this.assignForm = this.formBuilder.group({
      initialDate: [null, Validators.required],
      finishDate: [null, Validators.required],
      visibleDate: [null, Validators.required],
      overrideDefinition: [null],
    });
  }

  ngOnInit() {
    this.getChallengeDefinitions();
    this.loadDidacticUnits();
  }

  loadDidacticUnits() {
    this.apiService.getDidacticUnits().subscribe({
      next: (result) => {
        if (result.status === 200) {
          this.allDidacticUnits = result.message;
        }
      },
      error: error => console.error(error),
    });
  }

  formatDidacticUnits(units: DidacticUnit[]) {
    return units.map(unit => unit.name).join(', ');
  }

  onCloseDidacticUnits(event: { component: IonicSelectableComponent }) {
    const selectedDidacticUnits = event.component._selectedItems as DidacticUnit[];

    this.challenges = this.allChallenges.filter(def => {
      if (selectedDidacticUnits.length === 0) { return true; } // on user select none (deselect) -> no filter
      if (!def.didacticUnitsIds) { return false; } // challenge without didactic unit

      return def.didacticUnitsIds.every(unitId => selectedDidacticUnits.map(unit => unit.id).includes(unitId));
    });
  }

  async openDatePicker(type: string) {
    // get from-to dates to show in calendar
    this.showDatesInPicker();
    this.datePickerObj = {
      mondayFirst: true,
      titleLabel: this.translate.instant('ACTIONS.SELECT_DATE'),
      // TODO: add translation
      monthsList: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
      weeksList: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
      dateFormat: 'YYYY/MM/DD',
      closeOnSelect: true,
      showTodayButton: false,
      clearButton: true,
      fromDate: this.minDatetime,
      toDate: this.maxDatetime,
    };

    const datePickerModal = await this.modalCtrl.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: {
        objConfig: this.datePickerObj,
        selectedDate: new Date(),
      },
    });

    await datePickerModal.present();

    datePickerModal.onDidDismiss()
      .then((data) => {
        if (data?.data?.date !== 'Invalid date') {
          switch (type) {
            case 'initial':
              this.initialChallengeDate = data.data.date;
              break;
            case 'finish':
              this.finishChallengeDate = data.data.date;
              break;
            case 'visible':
              this.visibleChallengeDate = data.data.date;
              break;
          }
        }
      });
  }

  getChallengeDefinitions() {
    this.apiService.getChallengeDefinitions().subscribe({
      next: (result: ApiResponse<ChallengeDefinition[]>) => {
        if (result.status === 200) {
          const values: ChallengeDefinition[] = result.message;
          if (values.length > 0) {
            this.challenges.push(...result.message);
            this.allChallenges.push(...this.challenges);
          } else {
            this.showNoDefinitions();
          }
        }
      },
      error: error => console.error(error),
    });
  }

  private showNoDefinitions() {
    this.presentatorService.showInfoAlert(this.translate.instant('CHALLENGES.CREATE_NEW_DEFINITION'),
      this.translate.instant('CHALLENGES.NO_CHALLENGE_ASSIGNEMNT_WITHOUT_DEFS'))
      .then((result: unknown) => {
        if (result as boolean) {
          //  this.navController.back();
        }
      });
  }

  showDatesInPicker() {
    const now = new Date();
    this.minDatetime = new Date(now.getTime() - now.getTimezoneOffset() * 60000);
    // set max date to two years
    const maxDate = new Date();
    maxDate.setFullYear(maxDate.getFullYear() + 2);
    this.maxDatetime = maxDate;
  }

  get initialDate() {
    return this.assignForm.get('initialDate');
  }

  get finishDate() {
    return this.assignForm.get('finishDate');
  }

  get visibleDate() {
    return this.assignForm.get('visibleDate');
  }

  formChanged() {
    this.checkCanAssign();
  }

  onChallengeSelected(selectedIndex: number) {
    this.selectChallengeClicked = false;
    this.selectedChallenge = this.challenges[selectedIndex] as ChallengeProgramming;
    this.checkCanAssign();
    if (this.overrideDefinition) {
      this.selectedChallengeEmitter.next(this.selectedChallenge);
    }
  }

  // this method receives the selection from child component
  assignToComponentChange(event: ItemAssignedTo[]) {
    this.assignedToItems = event;
    this.checkCanAssign();
  }

  checkCanAssign(): boolean {
    this.assignForm.controls['initialDate'].setValue(this.initialChallengeDate);
    this.assignForm.controls['finishDate'].setValue(this.finishChallengeDate);
    this.assignForm.controls['visibleDate'].setValue(this.visibleChallengeDate);
    return (this.assignForm.valid &&
      this.selectedChallenge &&
      this.assignedToItems &&
      this.assignedToItems.length > 0);
  }

  onSearch(event: Event) {
    const value = (event as CustomEvent).detail.value;
    if (value) {
      if (value.trim !== '') {
        this.challenges = this.allChallenges.filter(term => {
          return term.title.toLowerCase().indexOf(value.trim().toLowerCase()) > -1;
        });
      }
    } else {
      this.challenges = this.allChallenges;
    }
  }

  onLockDownChanged(event: Event) {
    const lockDownMode = (event as CustomEvent).detail.value;

    this.challenges = this.allChallenges.filter(challenge => {
      return lockDownMode === ChallengeLockdownTypes.ALL ? true :
        (lockDownMode === ChallengeLockdownTypes.COMPATIBLE ? challenge.lockDown === true : challenge.lockDown === false);
    });
  }

  onOverridesChecked(event: Event) {
    this.overrideDefinition = (event as CustomEvent).detail.checked;
    if (this.overrideDefinition) {
      this.selectedChallengeEmitter.next(this.selectedChallenge);
    }
  }

  assignChallenge() {
    //check if assign component selection is correct   
    this.isSubmitted = true;

    if (this.checkItemAssignTo()) {
      if (this.checkCanAssign()) {
        // check if user has checked override definition.
        if (this.overrideDefinition) {
          // emit to the challenge definition form that it is submitted
          this.submitEmitter.next();
        } else {
          // send challenge programming object
          this.sendChallenge({} as ChallengeProgramming);
        }
      } else {
        this.presentatorService.showToast(Toast.message(this.translate.instant('ERRORS.FIELDS_ARE_REQUIRED')));
      }
    } else {
      this.presentatorService.showToast(Toast.message(this.translate.instant('ERRORS.ADD_ALL_SELECTED_LEVELS')));
    }
  }

  checkItemAssignTo(): boolean {
    let validSelection = true;

    if (this.assignedToItems) {
      this.assignedToItems.forEach(
        (value: ItemAssignedTo) => {
          // check if values meet the level selected

          switch (this.assignToLevel) {
            case 'entities':
              if (value.entities.length === 0) {
                validSelection = false;
                return;
              }
              break;
            case 'groups':
              if (value.groups.length === 0) {
                validSelection = false;
                return;
              }
              break;
            case 'users':
              if (value.users.length === 0) {
                validSelection = false;
                return;
              }
              break;
          }
        },
      );
    } else { validSelection = false; }

    return validSelection;
  }

  onLevelSelected(event: string) {
    this.assignToLevel = event;
  }

  getChallengeProgramming(challenge: ChallengeProgramming = {} as ChallengeProgramming): ChallengeProgramming {
    const form = this.assignForm.value;

    const initialDate = new Date(form.initialDate as string);
    initialDate.setHours(0);
    initialDate.setMinutes(0);
    initialDate.setSeconds(0);
    initialDate.setMilliseconds(0);

    const finishDate = new Date(form.finishDate as string);
    finishDate.setHours(23);
    finishDate.setMinutes(59);
    finishDate.setSeconds(59);
    finishDate.setMilliseconds(999);

    const visibleDate = new Date(form.visibleDate as string);
    visibleDate.setHours(0);
    visibleDate.setMinutes(0);
    visibleDate.setSeconds(0);
    visibleDate.setMilliseconds(0);

    const challengeProgramming: ChallengeProgramming = challenge as ChallengeProgramming;
    challengeProgramming.startDatetime = initialDate.toISOString();
    challengeProgramming.finishDatetime = finishDate.toISOString();
    challengeProgramming.visibleDatetime = visibleDate.toISOString();
    challengeProgramming.assignedToIds = this.getAssignedToIds();
    challengeProgramming.definitionId = this.selectedChallenge.id;

    // If user overrides JSON fields (questionnaire or news), we should stringify the object to string before request backend
    if (challengeProgramming.questionnaire && typeof challengeProgramming.questionnaire !== 'string') {
      // Questionnaire must be passed to Survey.Model before stringify to reduce JSON object complexity
      const surveyJson = new Survey.Model(challengeProgramming.questionnaire).toJSON();
      challengeProgramming.questionnaire = JSON.stringify(surveyJson);
    }
    if (challengeProgramming.questionnaireCorrectAnswer && typeof challengeProgramming.questionnaireCorrectAnswer !== 'string') {
      challengeProgramming.questionnaireCorrectAnswer = JSON.stringify(challengeProgramming.questionnaireCorrectAnswer);
    }
    if (challengeProgramming.news && typeof challengeProgramming.news !== 'string') {
      challengeProgramming.news = JSON.stringify(challengeProgramming.news);
    }

    return challengeProgramming;
  }

  getAssignedToIds(): AssignedToIds {
    const entities: string[] = [];
    const groups: string[] = [];
    const users: string[] = [];

    this.assignedToItems.forEach((element: ItemAssignedTo) => {
      element.users.forEach((user: User) => users.push(user.id));
      element.entities.forEach((entity: Entity) => entities.push(entity.id));
      element.groups.forEach((group: Group) => groups.push(group.id));
    });

    return {
      users,
      groups,
      entities,
    };
  }

  onSelectChallengeClick() {
    if (!this.selectChallengeClicked) {
      this.selectChallengeClicked = true;
    }
  }

  sendChallenge(event: unknown) {
    const challengeProgramming: ChallengeProgramming = this.getChallengeProgramming(event instanceof CustomEvent ? event.detail : event);
    this.presentatorService.createYesNoAlert('CHALLENGES.CREATE_NEW_PROGRAMMING', 'CHALLENGES.SCHEDULE_CHALLENGE_QUESTION').then(
      (result: boolean) => {
        if (result) {
          this.apiService.createChallengeProgramming(challengeProgramming).subscribe({
            error: () => {
              this.presentatorService.showToast(Toast.message(this.translate.instant('ERRORS.INTERNAL_SERVER_ERROR')));
            },
            next: (response: ApiResponse<ChallengeProgramming>) => {
              if (response.status === 200) {
                this.analytics.logEvent('assign_challenge', { id: response.message.id });

                this.presentatorService.showInfoAlert('CHALLENGES.COMPLETED', 'CHALLENGES.CREATE_CHALLENGE_SUCCESS').then(
                  (canProceed) => {
                    if (canProceed) {
                      this.isSubmitted = false;
                      this.assignForm.reset();
                      this.clearEmitter.next();
                      this.navController.back();
                    }
                  },
                );
              } else {
                this.presentatorService.showToast(Toast.message(this.translate.instant('ERRORS.INTERNAL_SERVER_ERROR')));
              }
            },
          });
        }
      },
    ).catch(error => console.error(error));
  }

  showChallengeError(event: string) {
    this.presentatorService.showToast(Toast.message(event));
  }
}
