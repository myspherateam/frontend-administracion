import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AssignChallengePageComponent } from './assign-challenge.page';

const routes: Routes = [
  {
    path: '',
    component: AssignChallengePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignChallengePageRoutingModule { }
