import { CommonModule } from '@angular/common';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { AssignChallengePageComponent } from './assign-challenge.page';
import { AssignChallengePageRoutingModule } from './assign-challenge-routing.module';
import { CommonsModule } from '../commons.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    AssignChallengePageRoutingModule,
    CommonsModule,
    TranslateModule.forChild(),
    Ionic4DatepickerModule,
    ReactiveFormsModule,
    IonicSelectableModule,
  ],
  declarations: [AssignChallengePageComponent],
})
export class AssignChallengePageModule { }
