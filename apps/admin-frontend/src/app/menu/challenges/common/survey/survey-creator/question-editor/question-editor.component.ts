import * as Survey from 'survey-angular';
import {
  ChangeDetectionStrategy, Component, ComponentFactoryResolver, EventEmitter, Input, OnInit,
  Output, ViewChild, ViewContainerRef,
} from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { supportedQuestionTypes } from '../question-type-modal/question-type';

@Component({
  selector: 'app-survey-creator-question-editor',
  templateUrl: './question-editor.component.html',
  styleUrls: ['./question-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionEditorComponent implements OnInit {
  @ViewChild('insertTemplate', { static: true, read: ViewContainerRef }) vcr!: ViewContainerRef;

  @Output() back: EventEmitter<Survey.Question> = new EventEmitter<Survey.Question>();

  @Output() contentChange: EventEmitter<Survey.Question> = new EventEmitter<Survey.Question>();

  @Output() deleteChange: EventEmitter<Survey.Question> = new EventEmitter<Survey.Question>();

  @Input() question!: Survey.Question;

  // TODO: add translation
  public readonly saveQuestionText: string = 'Guardar pregunta';

  // TODO: add translation
  public readonly removeQuestionText: string = 'Eliminar pregunta';

  constructor(
    private alertController: AlertController,
    private componentFactory: ComponentFactoryResolver,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadComponent();
  }

  loadComponent() {
    const componentType = supportedQuestionTypes?.find(value => {
      return value.id === this.question.getType();
    })?.component;
    if (componentType) {
      const componentFactory = this.componentFactory.resolveComponentFactory(componentType);
      this.vcr.clear();
      const componentRef = this.vcr.createComponent(componentFactory);

      if (componentRef.instance instanceof componentType) {
        componentRef.instance.question = this.question;
        componentRef.instance.questionChange.subscribe({
          next: (val: any) => {
            this.question = val;
            this.onContentChange();
          },
        });
      }
    }
  }

  onBackClick() {
    this.back.emit(this.question);
  }

  onContentChange() {
    this.contentChange.emit(this.question);
  }

  onDeleteClick() {
    this.presentAlertConfirm(() => this.deleteChange.emit(this.question));
  }

  async presentAlertConfirm(callback: Callback) {
    const alert = await this.alertController.create({
      header: this.translate.instant('CHALLENGES.ACTION.ARE_YOU_SURE_DELETE_QUESTION'),
      message: this.translate.instant('ACTION.UNREVERSAL_ACTION'),
      buttons: [
        {
          text: this.translate.instant('ACTION.CANCEL'),
          role: 'cancel',
        },
        {
          text: this.translate.instant('ACTION.CONFIRM'),
          handler: () => {
            callback();
          },
        },
      ],
    });

    await alert.present();
  }
}

type Callback = () => void;
