import * as Survey from 'survey-angular';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-input-editor-template',
  templateUrl: './input-editor-template.component.html',
  styleUrls: ['./input-editor-template.component.scss'],
})
export class InputEditorTemplateComponent {
  @Input() question!: Survey.QuestionTextModel;

  @Output() questionChange: EventEmitter<Survey.Question> = new EventEmitter<
    Survey.Question
  >();

  emitQuestionChange() {
    this.questionChange.emit(this.question);
  }

  /*
    Handler funcionts
  */

  onContentChange() {
    this.emitQuestionChange();
  }
}
