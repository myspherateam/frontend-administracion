import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { QuestionType, supportedQuestionTypes } from './question-type';

@Component({
  templateUrl: './question-type-modal.component.html',
  styleUrls: ['./question-type-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionTypeModalComponent {
  // TODO crear wrapper para lista de preguntas y hacer una por defecto en un json o ts y aceptar input
  @Input() questionTypes: QuestionType[] = supportedQuestionTypes;

  constructor(
    private modalController: ModalController,
    private translate: TranslateService,
  ) { }

  onDismissClick() {
    this.modalController.dismiss();
  }

  // TODO se devuleve la pregunta en concreto seleccionada
  onItemClick(type: QuestionType) {
    this.modalController.dismiss(type.id);
  }

  getValueTranslated(value: string) {
    return this.translate.instant(value);
  }
}
