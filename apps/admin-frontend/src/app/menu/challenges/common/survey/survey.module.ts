import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { QuestionEditorComponent } from './survey-creator/question-editor/question-editor.component';
import { QuestionListComponent } from './survey-creator/question-list/question-list.component';
import { QuestionTemplatesModule } from './survey-creator/question-editor/QuestionTemplates/question-templates.module';
import { QuestionTypeModalComponent } from './survey-creator/question-type-modal/question-type-modal.component';
import { QuestionTypeToIconPipe } from './survey-creator/question-list/icon-name.pipe';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { SurveyCreatorComponent } from './survey-creator/survey-creator.component';

@NgModule({
    declarations: [
        QuestionnaireComponent,
        SurveyCreatorComponent,
        QuestionEditorComponent,
        QuestionListComponent,
        QuestionTypeModalComponent,
        QuestionTypeToIconPipe,
    ],
    imports: [CommonModule, IonicModule, FormsModule, QuestionTemplatesModule, TranslateModule.forChild()],
    exports: [QuestionnaireComponent, SurveyCreatorComponent],
})
export class SurveyModule { }
