import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'otherText',
})
export class OtherTextPipe implements PipeTransform {

  transform(value: string): string {
    if (value === 'Other (describe)') {
      return '';
    }
    return value;
  }
}
