import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { BooleanEditorTemplateComponent } from './boolean-editor-template/boolean-editor-template.component';
import { CheckboxEditorTemplateComponent } from './checkbox-editor-template/checkbox-editor-template.component';
import { InputEditorTemplateComponent } from './input-editor-template/input-editor-template.component';
import { OtherTextPipe } from './checkbox-editor-template/other-text.pipe';

@NgModule({
    declarations: [CheckboxEditorTemplateComponent, InputEditorTemplateComponent, BooleanEditorTemplateComponent, OtherTextPipe],
    imports: [CommonModule, IonicModule, FormsModule, TranslateModule.forChild()],
    exports: [CheckboxEditorTemplateComponent, InputEditorTemplateComponent],
})
export class QuestionTemplatesModule { }
