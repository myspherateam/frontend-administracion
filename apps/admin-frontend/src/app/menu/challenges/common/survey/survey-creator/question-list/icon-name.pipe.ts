import { Pipe, PipeTransform } from '@angular/core';

import { supportedQuestionTypes } from '../question-type-modal/question-type';

@Pipe({ name: 'questionTypeToIcon' })
export class QuestionTypeToIconPipe implements PipeTransform {
  transform(value: string): string {
    return this.getIconNameFromQuestionType(value);
  }

  private getIconNameFromQuestionType(qName: string): string {
    return supportedQuestionTypes?.find(question => question.id === qName)?.icon || '';
  }
}
