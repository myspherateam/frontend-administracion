import * as Survey from 'survey-angular';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  templateUrl: './boolean-editor-template.component.html',
  styleUrls: ['./boolean-editor-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BooleanEditorTemplateComponent {
  @Input() question!: Survey.QuestionBooleanModel;

  @Output() questionChange: EventEmitter<Survey.Question> = new EventEmitter<
    Survey.Question
  >();

  emitQuestionChange() {
    this.questionChange.emit(this.question);
  }

  /*
    Handler funcionts
  */

  onContentChange(event: any) {
    if (event.detail.checked !== undefined) {
      this.question.setPropertyValue(event.target.name, event.detail.checked);
    } else {
      this.question.setPropertyValue(event.target.name, event.detail.value);
    }
    this.emitQuestionChange();
  }
}
