import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InputEditorTemplateComponent } from './input-editor-template.component';

describe('InputEditorTemplateComponent', () => {
  let component: InputEditorTemplateComponent;
  let fixture: ComponentFixture<InputEditorTemplateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InputEditorTemplateComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputEditorTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
