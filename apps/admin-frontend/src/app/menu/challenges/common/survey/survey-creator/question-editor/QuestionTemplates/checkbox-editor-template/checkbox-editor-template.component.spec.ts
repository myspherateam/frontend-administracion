import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CheckboxEditorTemplateComponent } from './checkbox-editor-template.component';

describe('CheckboxEditorTemplateComponent', () => {
  let component: CheckboxEditorTemplateComponent;
  let fixture: ComponentFixture<CheckboxEditorTemplateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxEditorTemplateComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxEditorTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
