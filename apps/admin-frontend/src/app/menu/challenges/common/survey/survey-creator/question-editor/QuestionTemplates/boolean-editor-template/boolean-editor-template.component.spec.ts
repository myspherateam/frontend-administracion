import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BooleanEditorTemplateComponent } from './boolean-editor-template.component';

describe('BooleanEditorTemplateComponent', () => {
  let component: BooleanEditorTemplateComponent;
  let fixture: ComponentFixture<BooleanEditorTemplateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [BooleanEditorTemplateComponent],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooleanEditorTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
