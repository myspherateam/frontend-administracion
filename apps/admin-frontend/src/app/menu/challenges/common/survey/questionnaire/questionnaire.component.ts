import * as Survey from 'survey-angular';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { PresentatorService } from '../../../../../services/common/presentator.service';
import theme from './theme.json';

// We are creating the theme object 'paido' here.
Survey.StylesManager.ThemeColors.paido = theme;
Survey.StylesManager.applyTheme('paido');

@Component({
  selector: 'app-survey',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionnaireComponent implements OnInit {
  @Output() results: EventEmitter<any> = new EventEmitter<any>();

  // 'edit' for editable survey. 'display' for read-only survey
  @Input() surveyMode: 'edit' | 'display' = 'edit';

  @Input() data: any;

  @Input() json: any;

  private survey!: Survey.SurveyModel;

  constructor(private presentatorService: PresentatorService, private translateService: TranslateService) { }

  ngOnInit() {
    this.initSurvey();
  }

  private initSurvey() {
    const surveyModel = new Survey.Model(this.json);

    surveyModel.onComplete.add((result: any, _options: any) => {
      this.results.emit(result.data);
      // don't remove the survey component
      result.clear(false);
      // Turn a survey into read-only mode
      result.mode = 'display';
    });

    surveyModel.mode = this.surveyMode;
    surveyModel.data = this.data;
    surveyModel.locale = this.translateService.currentLang;
    // TODO: add translation
    surveyModel.completeText = 'Guardar respuestas';
    // hide "Thank you for completing the survey!" screen message
    surveyModel.showCompletedPage = false;
    const defaultThemeStyles = Survey.StylesManager.ThemeColors.default;
    defaultThemeStyles['$body-container-background-color'] = '#ffffff';
    defaultThemeStyles['$text-color'] = '#000';
    Survey.StylesManager.applyTheme();
    Survey.SurveyNG.render('surveyElement', { model: surveyModel });
    this.survey = surveyModel;
  }

  showHelp() {
    this.presentatorService.showHelpAlert(
      // TODO: add translation
      'Selección de respuestas',
      `
        <p>A continuación, se le muestran las preguntas y respuestas que ha configurado en la parte superior.</p>
        <p>Para poder continuar, es necesario que marque las respuestas correctas.</p>
        <p>Tras marcar las respuestas, deberá guardarlas pulsando el botón <strong>Guardar respuestas</strong></p>
        <p>Una vez pulsado el botón, las respuestas se guardarán en modo lectura.
        Para editarlas, oculte la sección de respuestas correctas y pulse el botón <strong>Editar las respuestas correctas</strong>.</p>
      `,
    );
  }
}
