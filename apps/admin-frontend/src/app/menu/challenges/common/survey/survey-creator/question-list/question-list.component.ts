import * as Survey from 'survey-angular';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { QuestionTypeModalComponent } from '../question-type-modal/question-type-modal.component';

@Component({
  selector: 'app-survey-creator-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuestionListComponent {
  @Output() questionSelect: EventEmitter<Survey.Question> = new EventEmitter<Survey.Question>();

  @Output() pageChange: EventEmitter<Survey.PageModel> = new EventEmitter<Survey.PageModel>();

  @Input() surveyPage!: Survey.PageModel;

  reorderDisabled = true;

  questionArray!: Survey.Question[];

  constructor(private modalController: ModalController, private translateService: TranslateService) { }

  doReorder(event: any) {
    this.reorderQuestion(event.detail.from, event.detail.to);
    event.detail.complete();
    this.onPageChange();
  }

  toggleReorder() {
    this.reorderDisabled = !this.reorderDisabled;
  }

  async onAddQuestionClick() {
    const modal = await this.modalController.create({
      component: QuestionTypeModalComponent,
      backdropDismiss: true,
    });

    modal.onWillDismiss().then(event => {
      if (event.data as string) {
        this.addQuestion(event.data);
        this.onPageChange();
      }
    });

    return await modal.present();
  }

  addQuestion(questionType: string) {

    const question = Survey.QuestionFactory.Instance.createQuestion(questionType,
      `${this.translateService.instant('CHALLENGES.ACTION.QUESTION')} ${this.surveyPage.elements.length + 1}`,
    );
    // mark all questions as required by default
    question.isRequired = true;
    this.surveyPage.addElement(question);
  }

  onQuestionEditClick(question: Survey.IElement) {
    if (this.reorderDisabled && question instanceof Survey.Question) {
      this.questionSelect.emit(question);
    }
  }

  private reorderQuestion(from: number, to: number) {
    const element = this.surveyPage.elements[from];
    this.surveyPage.removeElement(element);
    this.surveyPage.addElement(element, to);
  }

  private onPageChange() {
    this.pageChange.emit(this.surveyPage);
  }
}
