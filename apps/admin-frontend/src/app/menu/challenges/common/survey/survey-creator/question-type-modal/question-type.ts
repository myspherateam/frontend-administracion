import { Type } from '@angular/core';

import {
  BooleanEditorTemplateComponent,
} from '../question-editor/QuestionTemplates/boolean-editor-template/boolean-editor-template.component';
import {
  CheckboxEditorTemplateComponent,
} from '../question-editor/QuestionTemplates/checkbox-editor-template/checkbox-editor-template.component';
import {
  InputEditorTemplateComponent,
} from '../question-editor/QuestionTemplates/input-editor-template/input-editor-template.component';

export interface QuestionType {
  icon: string;
  name: string;
  id: string;
  component: Type<any>;
}

export const supportedQuestionTypes: QuestionType[] = [
  {
    icon: 'checkbox-outline',
    name: 'CHALLENGES.ACTION.MULTIPLE_OPTION',
    id: 'checkbox',
    component: CheckboxEditorTemplateComponent,
  },
  {
    icon: 'text', // opts: document, paper, text
    name: 'CHALLENGES.ACTION.FREE_ANSWER',
    id: 'text',
    component: InputEditorTemplateComponent,
  },
  {
    icon: 'radio-button-on',
    name: 'CHALLENGES.ACTION.UNIQUE_ANSWER',
    id: 'radiogroup',
    component: CheckboxEditorTemplateComponent,
  },
  {
    icon: 'star-half',
    name: 'CHALLENGES.ACTION.YES_NO_ANSWER',
    id: 'boolean',
    component: BooleanEditorTemplateComponent,
  },
];
