import * as Survey from 'survey-angular';

Survey.QuestionFactory.Instance.registerQuestion('boolean', (name) => {
    const model = new Survey.QuestionBooleanModel(name);
    // TODO: add translation
    model.labelTrue = 'Sí';
    return model;
});
