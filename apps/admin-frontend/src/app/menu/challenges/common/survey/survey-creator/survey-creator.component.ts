import * as Survey from 'survey-angular';
import { ChangeDetectionStrategy, EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';
import { PageModel, SurveyModel } from 'survey-angular';

import './survey-customization';

@Component({
  selector: 'app-survey-creator',
  templateUrl: './survey-creator.component.html',
  styleUrls: ['./survey-creator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SurveyCreatorComponent implements OnInit {
  @Input() surveyJson: any;

  @Output() surveyChange: EventEmitter<any> = new EventEmitter<any>();

  private survey!: SurveyModel;

  currentPage!: PageModel;

  questionToEdit!: Survey.Question;

  questionSelected = false;

  ngOnInit() {
    if (!this.surveyJson) {
      this.surveyJson = {
        pages: [
          {
            name: 'page1',
          },
        ],
      };
    }
    this.survey = new Survey.Model(this.surveyJson);
    this.currentPage = this.survey.pages[0];
  }

  onQuestionToEditSelect(question: Survey.Question) {
    this.questionToEdit = question;
    this.questionSelected = true;
  }

  onEditFinish(_question: Survey.Question) {
    this.questionSelected = false;
  }

  onQuestionEdit(question: Survey.Question) {
    this.questionToEdit = question;

    this.surveyChange.emit(this.survey.toJSON());
  }

  getSurvey(_survey: any) {
    this.survey.toJSON();
  }

  onPageChange(page: Survey.PageModel) {
    this.survey.removePage(this.survey.getPageByName(page.name));
    this.survey.addPage(page);

    this.surveyChange.emit(this.survey.toJSON());
  }

  onDelete(_question: Survey.Question) {

    this.survey.pages[0].removeElement(this.questionToEdit);

    this.questionSelected = false;
    this.surveyChange.emit(this.survey.toJSON());
  }
}
