import * as Survey from 'survey-angular';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-checkbox-editor-template',
  templateUrl: './checkbox-editor-template.component.html',
  styleUrls: ['./checkbox-editor-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckboxEditorTemplateComponent {
  @Input() question!: Survey.QuestionCheckboxModel;

  @Output() questionChange: EventEmitter<Survey.Question> = new EventEmitter<Survey.Question>();

  deleteChoice(choice: any) {
    this.question.choices.splice(
      this.question.choices.findIndex((e: any) => e.value === choice.value), 1);
  }

  addChoice(name?: string) {
    const valueName = name
      ? name
      : 'Opción ' + (this.question.choices.length + 1);
    this.question.choices.push({
      value: valueName,
    });
  }

  emitQuestionChange() {
    this.questionChange.emit(this.question);
  }

  /*
    Handler funcionts
  */
  onContentChange(event: any) {
    if (event.detail.checked !== undefined) {
      this.question.setPropertyValue(event.target.name, event.detail.checked);
    } else {
      this.question.setPropertyValue(event.target.name, event.detail.value);
    }
    this.emitQuestionChange();
  }

  onChoiceChange(event: any, index: number) {
    this.question.choices[index] = Object.assign(this.question.choices[index], {
      value: event.detail.value,
    });
    this.emitQuestionChange();
  }

  onDeleteButtonClick(choice: any) {
    this.deleteChoice(choice);
    this.emitQuestionChange();
  }

  onAddChoice() {
    this.addChoice();
    this.emitQuestionChange();
  }

  onDeleteButtonMouseenter(params: any) {
    if (params.id) {
      this.ionFocusItemWithId(params.id);
    }
  }

  onDeleteButtonMouseleave(params: any) {
    if (params.id) {
      this.ionUnfocusItemWithId(params.id);
    }
  }

  /*
    Helper functions for styling
  */

  ionFocusItemWithId(id: string) {
    document.getElementById(id)?.classList.add('item-has-focus');
  }

  ionUnfocusItemWithId(id: string) {
    document.getElementById(id)?.classList.remove('item-has-focus');
  }
}
