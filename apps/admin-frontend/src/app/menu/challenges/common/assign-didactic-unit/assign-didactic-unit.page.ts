import { Component, OnInit } from '@angular/core';
import { FormControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Ionic4DatepickerModalComponent } from '@logisticinfotech/ionic4-datepicker';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, AssignedToIds, DidacticUnit, Entity, Group, ItemAssignedTo, User } from '@paido/data-model';

import { ApiService } from '../../../../api/api-service';
import { PresentatorService } from '../../../../services/common/presentator.service';
import { Toast } from '../../../../services/common/dto/toast';

@Component({
  selector: 'app-assign-didactic-unit',
  templateUrl: './assign-didactic-unit.page.html',
  styleUrls: ['./assign-didactic-unit.page.scss'],
})
export class AssignDidacticUnitPageComponent implements OnInit {

  public allDidacticUnits: DidacticUnit[] = [];

  public assignedToItems?: ItemAssignedTo[];

  public assignToLevel = 'entity';

  public datePickerObj: Record<string, unknown>;

  public minDatetime!: Date;

  public maxDatetime!: Date;

  public assignUnitForm: UntypedFormGroup;

  public isSubmitted: boolean = false;

  constructor(
    private apiService: ApiService,
    private modalController: ModalController,
    private presentatorService: PresentatorService,
    private translateService: TranslateService,
  ) {
    this.assignUnitForm = new UntypedFormBuilder().group({
      selectedUnit: new FormControl<DidacticUnit | null>(null, Validators.required),
      assignTo: new FormControl<string[]>([], [Validators.required, Validators.minLength(1)]),
      initialDate: new FormControl<string | null>(null, Validators.required),
    });
  }

  async ngOnInit() {
    this.apiService.getDidacticUnits().subscribe({
      next: (result) => {
        if (result.status === 200) {
          this.allDidacticUnits = result.message;
        }
      },
      error: error => console.error(error),
    });
  }

  /**
   * Accessor method to get the initialDate form control
   */
  get initialDate() {
    return this.assignUnitForm.get('initialDate');
  }

  get assignedTo() {
    return this.assignUnitForm.get('assignTo');
  }

  get selectedUnit() {
    return this.assignUnitForm.get('selectedUnit');
  }

  /**
  * Callback method for assignToComponentChange event in the app-assign-to component in the template
  * @param event 
  */
  assignToComponentChange(event: ItemAssignedTo[]) {
    this.assignedToItems = event;
  }

  /**
   * Callback method for levelSelected event in the app-assign-to component in the template
   * @param event 
   */
  onLevelSelected(event: string) {
    this.assignToLevel = event;
    this.assignedTo.patchValue(event);
  }

  /**
   * Check that the level selected has one or more items selected
   */
  private checkItemAssignTo(): boolean {
    let validSelection = true;
    if (this.assignedToItems) {
      this.assignedToItems.forEach(
        (value: ItemAssignedTo) => {
          // check if values meet the level selected
          switch (this.assignToLevel) {
            case 'entities':
              if (value.entities.length === 0) {
                validSelection = false;
                return;
              }
              break;
            case 'groups':
              if (value.groups.length === 0) {
                validSelection = false;
                return;
              }
              break;
            case 'users':
              if (value.users.length === 0) {
                validSelection = false;
                return;
              }
              break;
          }
        },
      );
    } else {
      validSelection = false;
    }

    return validSelection;
  }

  async openDatePicker(_type: string) {
    // get from-to dates to show in calendar
    this.showDatesInPicker();
    this.datePickerObj = {
      mondayFirst: true,
      titleLabel: 'Selecciona una fecha',
      monthsList: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
      weeksList: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
      dateFormat: 'YYYY/MM/DD',
      closeOnSelect: true,
      showTodayButton: false,
      clearButton: true,
      fromDate: this.minDatetime,
      toDate: this.maxDatetime,
    };

    const datePickerModal = await this.modalController.create({
      component: Ionic4DatepickerModalComponent,
      cssClass: 'li-ionic4-datePicker',
      componentProps: {
        objConfig: this.datePickerObj,
        selectedDate: new Date(),
      },
    });

    await datePickerModal.present();

    datePickerModal.onDidDismiss()
      .then((data) => {
        if (data.data.date !== 'Invalid date') {
          this.assignUnitForm.get('initialDate').patchValue(data.data.date);
        }
      });
  }

  private showDatesInPicker() {
    const now = new Date();
    this.minDatetime = new Date(now.getTime() - now.getTimezoneOffset() * 60000);
    // set max date to two years
    const maxDate = new Date();
    maxDate.setFullYear(maxDate.getFullYear() + 2);
    this.maxDatetime = maxDate;
  }

  /**
   * Handler for the ion-selectable with didacti units
   * @param unit the selected didactic unit 
   */
  onDidacticUnitSelected(unit: DidacticUnit) {
    this.selectedUnit.patchValue(unit);
  }

  private checkEnableAssignButton(): boolean {
    return this.checkItemAssignTo() && this.assignUnitForm.valid;
  }

  private getAssignedToIds(): AssignedToIds {
    const entities: string[] = [];
    const groups: string[] = [];
    const users: string[] = [];

    this.assignedToItems?.forEach((element: ItemAssignedTo) => {
      element.users.forEach((user: User) => users.push(user.id));
      element.entities.forEach((entity: Entity) => entities.push(entity.id));
      element.groups.forEach((group: Group) => groups.push(group.id));
    });

    return {
      users,
      groups,
      entities,
    };
  }

  assignDidacticUnit() {
    this.isSubmitted = true;
    if (this.checkEnableAssignButton()) {
      this.presentatorService.createYesNoAlert(
        this.translateService.instant('DIDACTIC_UNIT.PROGRAMMING_UNIT'),
        this.translateService.instant('DIDACTIC_UNIT.PROGRAMMING_QUESTION')).then(
          (result: boolean) => {
            if (result) {
              const startDatetime = new Date(this.initialDate.value);
              startDatetime.setHours(0);
              startDatetime.setMinutes(0);
              startDatetime.setSeconds(0);
              startDatetime.setMilliseconds(0);

              this.apiService.assignDidacticUnit(
                this.selectedUnit.value?.id as string,
                startDatetime.toISOString(),
                this.getAssignedToIds(),
              ).subscribe({
                next: (result2: ApiResponse<DidacticUnit>) => {
                  if (result2.status === 200) {
                    this.isSubmitted = false;
                    this.presentatorService.showToast(
                      Toast.message(this.translateService.instant('DIDACTIC_UNIT.PROGRAMMING_UNIT_SUCCESS')),
                    );
                  }
                },
                error: (error: HttpErrorResponse) => console.error(error),
              });
            }
          },
        );
    }
  }
}
