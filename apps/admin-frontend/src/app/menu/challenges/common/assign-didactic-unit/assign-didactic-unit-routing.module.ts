import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AssignDidacticUnitPageComponent } from './assign-didactic-unit.page';

const routes: Routes = [
  {
    path: '',
    component: AssignDidacticUnitPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignDidacticUnitPageRoutingModule { }
