import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AssignDidacticUnitPageComponent } from './assign-didactic-unit.page';

describe('AssignDidacticUnitPage', () => {
  let component: AssignDidacticUnitPageComponent;
  let fixture: ComponentFixture<AssignDidacticUnitPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AssignDidacticUnitPageComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(AssignDidacticUnitPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
