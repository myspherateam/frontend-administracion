import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AssignDidacticUnitPageComponent } from './assign-didactic-unit.page';
import { AssignDidacticUnitPageRoutingModule } from './assign-didactic-unit-routing.module';
import { CommonsModule } from '../commons.module';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AssignDidacticUnitPageRoutingModule,
    TranslateModule,
    IonicSelectableModule,
    CommonsModule,
    ReactiveFormsModule,
  ],
  declarations: [AssignDidacticUnitPageComponent],
})
export class AssignDidacticUnitPageModule { }
