import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

import { CommonsModule } from '../commons.module';
import { EditChallengePageComponent } from './edit-challenge.page';
import { EditChallengePageRoutingModule } from './edit-challenge-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditChallengePageRoutingModule,
    CommonsModule,
  ],
  declarations: [EditChallengePageComponent],
})
export class EditChallengePageModule { }
