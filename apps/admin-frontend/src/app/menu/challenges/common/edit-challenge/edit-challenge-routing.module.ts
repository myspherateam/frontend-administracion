import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EditChallengePageComponent } from './edit-challenge.page';

const routes: Routes = [
  {
    path: '',
    component: EditChallengePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditChallengePageRoutingModule { }
