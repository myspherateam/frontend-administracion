import { ReplaySubject, Subject } from 'rxjs';
import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { ApiResponse, ChallengeDefinition } from '@paido/data-model';

import { ApiService } from '../../../../api/api-service';
import { NavController } from '@ionic/angular';
import { PresentatorService } from '../../../../services/common/presentator.service';
import { Toast } from '../../../../services/common/dto/toast';

@Component({
  selector: 'app-edit-challenge',
  templateUrl: './edit-challenge.page.html',
  styleUrls: ['./edit-challenge.page.scss'],
})
export class EditChallengePageComponent {

  public submitEmitter: Subject<void> = new Subject<void>();

  public selectedChallengeEmitter: ReplaySubject<ChallengeDefinition> = new ReplaySubject<ChallengeDefinition>(1);

  public challenge!: ChallengeDefinition;

  constructor(
    private router: Router,
    private navController: NavController,
    private presentatorService: PresentatorService,
    private apiService: ApiService,
    private analytics: AngularFireAnalytics,
  ) {
    const challenge: ChallengeDefinition | undefined = this.router.getCurrentNavigation()?.extras.state.challenge;
    this.challenge = challenge; 
     
    if (challenge) {
      this.selectedChallengeEmitter.next(this.challenge);
    }
  }

  editChallenge() { 
    this.submitEmitter.next(); 
  }

  sendChallenge(challengeEdited: ChallengeDefinition) {

    let message = '¿Quieres editar el reto?';
    let titleKey = 'Editar reto';
    if(this.challenge.title != challengeEdited.title){
      message = `¿Quieres crear un nuevo reto con el nombre ${challengeEdited.title}?`;
      titleKey = 'Nuevo reto';
    }

    this.presentatorService.createYesNoAlert(titleKey, message)
      .then((result: boolean) => {
        if (result) {
          this.editChallengeDefinition(challengeEdited);
        }
      });
  }

  editChallengeDefinition(challengeEdited: ChallengeDefinition) {
    this.apiService.createChallengeDefinition(challengeEdited).subscribe({
      next: (result: ApiResponse<ChallengeDefinition>) => {
        this.analytics.logEvent('edit_challenge_definition', { id: result.message.id });
        // TODO: show success toast
        this.presentatorService.showToast(Toast.message('Challenge edited: ' + result.message.title));
        this.navController.back();
      },
      error: (error: HttpErrorResponse) => {
        // TODO: add translation
        this.presentatorService.showToast(Toast.message('Error: ' + error.message));
        console.error(error);
      },
    });
  }

  showChallengeError(event: string) {
    this.presentatorService.showToast(Toast.message(event));
  }

}
