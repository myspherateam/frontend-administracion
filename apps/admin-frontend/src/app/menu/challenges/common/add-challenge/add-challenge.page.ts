import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';

import { ApiResponse, ChallengeDefinition } from '@paido/data-model';

import { ApiService } from '../../../../api/api-service';
import { CanComponentDeactivate } from '../../../../services/guards/can-deactivate-guard';
import { PresentatorService } from '../../../../services/common/presentator.service';
import { Toast } from '../../../../services/common/dto/toast';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-add-challenge',
  templateUrl: './add-challenge.page.html',
  styleUrls: ['./add-challenge.page.scss'],
})
export class AddChallengePageComponent implements CanComponentDeactivate {
  public isSubmitted: boolean = false;

  // instructs the embedded form check respond whether the challenge can be submitted
  public submitEmitter: Subject<void> = new Subject<void>();

  // instructs the embedded form to clear all data
  public clearEmitter: Subject<void> = new Subject<void>();

  // boolean flag to stay on the current page and create additional challenges
  public pressContinue!: boolean;

  constructor(
    private presentatorService: PresentatorService,
    private apiService: ApiService,
    private router: Router,
    private analytics: AngularFireAnalytics,
    private translate: TranslateService,
  ) { }

  /**
   * Handler for the clean button
   */
  clean() {
    this.presentatorService.createYesNoAlert('ACTION.ARE_YOU_SURE', 'CHALLENGES.DELETE_CHALLENGE_EXTENDED').then(
      (result: boolean) => {
        if (result) {
          this.clearEmitter.next();
        }
      },
    );
  }

  /**
   * Overrides the canDeactivate guard to detect that we want to exit the page with changes without being persisted
   * @returns
   */
  canDeactivate() {

    if (this.isSubmitted || confirm(this.translate.instant('ACTION.EXIT_WHITOUT_SAVE'))) {
      this.clean();
      return true;
    } else {
      return false;
    }
  }

  /**
   * Handler for the submit buttons
   * @param canContinue
   */
  add(canContinue: boolean) {
    // check values
    this.isSubmitted = true;
    this.pressContinue = canContinue;
    this.submitEmitter.next();
  }

  sendChallenge(event: ChallengeDefinition) {
    this.presentatorService.createYesNoAlert('CHALLENGES.CREATE_CHALLENGE', 'CHALLENGES.CREATE_CHALLENGE_EXTENDED').then(
      (result: boolean) => {
        if (result) {
          this.createNewChallengeDefinition(event);
        } else {
          this.isSubmitted = false;
        }
      },
    );
  }

  private createNewChallengeDefinition(challengeDefinition: ChallengeDefinition) {
    this.apiService.createChallengeDefinition(challengeDefinition).subscribe({
      next: (result: ApiResponse<ChallengeDefinition>) => {
        this.analytics.logEvent('new_challenge_definition', { id: result.message.id });
        // reset form values
        if (this.pressContinue) {
          // user wants to continue adding challenges. Reset form.
          this.isSubmitted = false;
          this.clearEmitter.next();
        } else {
          // user wants to leave the page
          this.clearEmitter.next();
          this.router.navigate(['/menu/challenges-menu/definition-list']);
        }

        this.presentatorService.showToast(Toast.message('CHALLENGES.CREATE_CHALLENGE_SUCCESS'));
      },
      error: (error: HttpErrorResponse) => {
        this.presentatorService.showToast(Toast.message('ERRORS.ERROR_WITH_VALUE', { val: error.message }));
        console.error(error);
      },
    });
  }

  showChallengeError(messageId: string) {
    this.isSubmitted = false;
    this.presentatorService.showToast(Toast.message(messageId));
  }
}
