import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AddChallengePageComponent } from './add-challenge.page';
import { CanDeactivateGuard } from '../../../../services/guards/can-deactivate-guard';

const routes: Routes = [
  {
    path: '',
    component: AddChallengePageComponent,
    canDeactivate: [CanDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddChallengePageRoutingModule { }
