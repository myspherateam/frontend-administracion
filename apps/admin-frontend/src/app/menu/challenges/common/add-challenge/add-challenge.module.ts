import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AddChallengePageComponent } from './add-challenge.page';
import { AddChallengePageRoutingModule } from './add-challenge-routing.module';
import { CommonsModule } from '../commons.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule.forChild(),
    AddChallengePageRoutingModule,
    CommonsModule,
  ],
  declarations: [AddChallengePageComponent],
})
export class AddChallengePageModule { }
