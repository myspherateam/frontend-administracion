import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Entity, Group, ItemAssignedTo, User, UserRole, getUserIdentifier } from '@paido/data-model';
import { firstValueFrom } from 'rxjs';

import { ApiService } from '../../../../api/api-service';
import { LocalStorageService } from '../../../../services/storage-service';

@Component({
  selector: 'app-assign-to',
  templateUrl: './assign-to.component.html',
  styleUrls: ['./assign-to.component.scss'],
})
export class AssignToComponent implements OnInit {

  @Output() changed = new EventEmitter<ItemAssignedTo[]>();

  @Output() levelSelected = new EventEmitter<string>();

  @Input() canEdit: boolean = false; // if it's true means that it allows to add and delete items, if not it allows only reading

  @Input() hideEntities: boolean = false;

  @Input() hideGroups: boolean = false;

  @Input() hideUsers: boolean = false;

  @Input() passedGroupId!: string;

  @Input() clearAssignTo!: EventEmitter<boolean>;

  private users: User[] = [];

  private groups: Group[] = [];

  private entities: Entity[] = [];

  private itemsSelection: ItemAssignedTo[] = [];

  private selectedEntities: Entity[] = [];

  private selectedGroups: Group[] = [];

  private selectedUsers: User[] = [];

  public components: ItemAssignedTo[] = [];

  public haveGroups = true;

  public userOptions: unknown[] = [];

  public groupOptions: unknown[] = [];

  public entityLevel = true;

  public groupsLevel: boolean = false;

  public usersLevel: boolean = false;

  constructor(
    public apiService: ApiService,
    private localStorage: LocalStorageService) {
  }

  ngOnInit() {
    this.getData();
    // action to clear user selection
    if (this.clearAssignTo) {
      this.clearAssignTo.subscribe({
        next: () => {
          this.userOptions = [];
          this.groupOptions = [];
        },
      });
    }

    if (this.canEdit) {
      // initial mode for assign challenges
      this.hideGroups = true;
      this.hideUsers = true;
    }
  }

  resolveUser(user: User): string {
    return getUserIdentifier(user);
  }

  async getData() {
    this.entities = (await firstValueFrom(this.apiService.getEntities())).message;

    if (this.localStorage.containsKey('entitySelected') && this.passedGroupId) {
      const entityIds = [this.localStorage.getFromLocalStorage<Entity>('entitySelected').id];
      this.users = (await this.apiService.getUsers(entityIds, [this.passedGroupId], undefined, [UserRole.Child]).toPromise()).message;
    }

    this.addComponent();
  }

  addComponent() {
    // add a new row

    // is from add user caring for children
    if (this.hideEntities && this.hideGroups && !this.hideUsers) {
      this.components.push({
        users: this.users,
        groups: this.groups,
        entities: this.entities,
        canAssignUsers: true,
      });
    } else {
      this.components.push({
        users: this.users,
        groups: this.groups,
        entities: this.entities,
      });
    }

    // add new item selection. This is the array that is going to include the user selection
    this.itemsSelection.push({
      users: [],
      groups: [],
      entities: [],
    });
  }

  removeComponent(index: number) {
    this.components.splice(index, 1);
    this.itemsSelection.splice(index, 1);
    this.changed.emit(this.itemsSelection);
    this.userOptions.splice(index, 1);
    this.groupOptions.splice(index, 1);
  }

  async onEntitiesSelected(event: CustomEvent, componentIndex: number, component: ItemAssignedTo) {
    const values: number[] = event.detail.value;
    this.selectedEntities = [];

    // save entity selected
    values.forEach((index: number) => {
      const element = this.entities[index];
      this.selectedEntities.push(element);
    });
    this.itemsSelection[componentIndex].entities = this.selectedEntities;

    // if has selected entities
    if (this.selectedEntities.length > 0) {
      component.canAssignGroups = true;

      const entityIds = this.selectedEntities.map((e) => e.id);

      if (!((this.apiService.getUserRoles() || []).includes(UserRole.GlobalAdmin) ||
        (this.apiService.getUserRoles() || []).includes(UserRole.LocalAdmin))
      ) {
        this.groups = (await this.apiService.getGroups(entityIds).toPromise()).message;
        if (this.groups.length === 0) {
          this.haveGroups = false;
        } else {
          this.haveGroups = true;
          component.groups = [];
          component.groups.push(...this.groups);
        }
      }
    } else {
      // user clear all the entities selected. Clear groups and users selected
      component.canAssignGroups = false;
      this.itemsSelection[componentIndex].groups = [];
      this.groupOptions[componentIndex] = [];
      component.canAssignUsers = false;
      this.itemsSelection[componentIndex].users = [];
      this.userOptions[componentIndex] = [];
    }
    this.changed.emit(this.itemsSelection);
  }

  async onGroupsSelected(event: CustomEvent, componentIndex: number, component: ItemAssignedTo) {
    if (event.detail.value) {
      // get groups selected
      const values: number[] = event.detail.value;
      this.selectedGroups = [];
      values.forEach((index: number) => {
        const element = this.groups[index];
        this.selectedGroups.push(element);
      });
      this.itemsSelection[componentIndex].groups = this.selectedGroups;

      // only if assign users selection is active
      if (this.selectedGroups.length > 0) {
        component.canAssignUsers = true;
        const entityIds = this.selectedEntities.map((e) => e.id);
        const groupsIds = this.selectedGroups.map((g) => g.id);
        this.users = (await firstValueFrom(this.apiService.getUsers(entityIds, groupsIds, undefined, [UserRole.Child]))).message;
        component.users = [];
        component.users.push(...this.users);
      } else {
        // user clear groups selection. Clear users selection
        component.canAssignUsers = false;
        this.itemsSelection[componentIndex].users = [];
        this.userOptions[componentIndex] = [];
      }
      this.changed.emit(this.itemsSelection);
    }
  }

  onUsersSelected(event: CustomEvent, componentIndex: number) {
    if (event.detail.value) {
      const values: number[] = event.detail.value;
      this.selectedUsers = [];
      values.forEach((index: number) => {
        const element = this.users[index];
        this.selectedUsers.push(element);
      });
      this.itemsSelection[componentIndex].users = this.selectedUsers;
      this.changed.emit(this.itemsSelection);
    }
  }

  onGroupsLevelSelected() {
    // if groups level is selected to assign
    if (this.groupsLevel) {
      this.hideGroups = false;
    } else {
      this.hideGroups = true;
      this.hideUsers = true;
      // deselect users checkbox
      this.usersLevel = false;

      // remove selected options in view and in component and emit new selection
      this.removeGroups();
    }
    this.levelSelected.emit(this.getLevelSelected());
  }

  private removeGroups() {
    this.groupOptions = [];
    this.components.forEach(
      (_value: ItemAssignedTo, index: number) => {
        this.itemsSelection[index].groups = [];
      },
    );

    this.changed.emit(this.itemsSelection);
  }

  onUsersLevelSelected() {
    // if users level is selected to assign
    if (this.usersLevel) {
      this.hideUsers = false;
      this.hideGroups = false;
      this.groupsLevel = true;

    } else {
      this.hideUsers = true;

      // remove selected options in view and in component and emit new selection
      this.removeUsers();
    }
    this.levelSelected.emit(this.getLevelSelected());
  }

  private removeUsers() {
    this.userOptions = [];
    this.components.forEach(
      (value: ItemAssignedTo, index: number) => {
        if (this.itemsSelection[index].groups.length > 0) {
          value.canAssignUsers = true;
        } else {
          value.canAssignUsers = false;
        }
        this.itemsSelection[index].users = [];
      },
    );

    this.changed.emit(this.itemsSelection);
  }

  private getLevelSelected(): string {
    let level: string = '';
    if (this.entityLevel) {
      level = 'entities';
    }
    if (this.groupsLevel) {
      level = 'groups';
    }
    if (this.usersLevel) {
      level = 'users';
    }
    return level;
  }
}
