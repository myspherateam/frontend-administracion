import { ModalController, NavParams } from '@ionic/angular';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { ChallengeProgramming } from '@paido/data-model';

@Component({
  selector: 'app-challenge-preview-modal',
  templateUrl: './challenge-preview-modal.component.html',
  styleUrls: ['./challenge-preview-modal.component.scss'],
})
export class ChallengePreviewModalComponent {

  public challenge!: ChallengeProgramming;

  constructor(
    private navParams: NavParams,
    public modalControl: ModalController,
    private router: Router) {
    if (this.navParams) {
      this.challenge = this.navParams.get('challenge');
    }
  }

  closeModal() {
    this.modalControl.dismiss();
  }
}
