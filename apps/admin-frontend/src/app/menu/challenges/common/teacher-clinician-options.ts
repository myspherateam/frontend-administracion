/**
 * Contains common options for all roles
 */
export const TeacherClinicianOptions = {
    options: [
        {
            name: 'Retos',
            url: '/challenges-menu',
            icon: 'add-circle',
        },
    ],
};
