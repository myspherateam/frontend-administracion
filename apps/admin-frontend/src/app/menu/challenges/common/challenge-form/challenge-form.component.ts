import 'leaflet-draw';
import * as L from 'leaflet';
import * as Survey from 'survey-angular';

import {
  AbstractControl, FormControl, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators,
} from '@angular/forms';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import drawLocales from 'leaflet-draw-locales';

import {
  ApiResponse, CategoryNames, ChallengeDefaultPoints, ChallengeDefinition, ChallengeType, Entity, Location,
  Point, SubcategoryNames, User, UserRole,
} from '@paido/data-model';

import { ApiService } from '../../../../api/api-service';
import { Delta } from '../news/interfaces/delta';
import { LocalStorageService } from '../../../../services/storage-service';
import { PresentatorService } from '../../../../services/common/presentator.service';

@Component({
  selector: 'app-challenge-form',
  templateUrl: './challenge-form.component.html',
  styleUrls: ['./challenge-form.component.scss'],
})
export class ChallengeFormComponent implements OnInit, OnDestroy {
  private addressResultLimit = 5;

  private defaultPointsForChild!: ChallengeDefaultPoints;

  private defaultPointsForParent!: ChallengeDefaultPoints;

  public challengeForm!: UntypedFormGroup;

  public categories = CategoryNames;

  public subcategories = SubcategoryNames;

  public typeChallenges: object;

  public assignedForSelected!: string;

  public relationCliniciansSelected: boolean = false;

  public users!: User[];

  public challengeGpsLocation?: Location;

  public specificIds: string[] = [];

  public entityId?: string;

  public newsValue!: Delta;

  public newsQuestionnaireSelected: boolean = false;

  public isShowingPreview: boolean = false;

  public questionnaireJson: any;

  public surveyPreviewText?: string;

  public questionnaireCorrectAnswer: any;

  public specificIdsSelected: boolean = false;

  public isSubmitted: boolean = false;

  public selectedUsers: User[] = [];

  public challengeSelected?: ChallengeDefinition;

  @Input() public onChallengeSelected!: Subject<ChallengeDefinition>;

  @Input() public isFromAddChallenge!: boolean;

  @Input() public submitObserver!: Subject<void>;

  @Input() public clearObserver!: Subject<void>;

  @Output() public challengeCorrect: EventEmitter<ChallengeDefinition> = new EventEmitter<ChallengeDefinition>();

  @Output() public challengeError: EventEmitter<string> = new EventEmitter<string>();

  @Output() public challengeModified: EventEmitter<ChallengeDefinition> = new EventEmitter<ChallengeDefinition>();

  public map!: L.Map;

  public selectedSubscription?: Subscription;

  public currentAddressList: any[] = [];

  public leafletOptions: L.MapOptions;

  public leafletLayers: any;

  private defaultMapLocation: L.LatLngExpression = [39.4078969, -0.431551];

  private defaultMapZoom = 10;

  public questionnairePreviewJson: any;

  constructor(
    private presentatorService: PresentatorService,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private translate: TranslateService,
  ) {
    if (this.localStorage.containsKey('entitySelected')) {
      this.entityId = this.localStorage.getFromLocalStorage<Entity>('entitySelected').id;
    }
    this.loadDefaultPoints();
    this.setupForm();
    drawLocales('es');
    this.leafletLayers = [L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; OpenStreetMap contributors',
    })];
    this.leafletOptions = {
      zoom: this.defaultMapZoom,
      center: this.defaultMapLocation,
    };
  }

  ngOnInit() {
    this.typeChallenges = {
      'physical': this.translate.instant('CHALLENGES.TYPE.PHYSICAL'),
      'gps': this.translate.instant('CHALLENGES.TYPE.GPS'),
      'authority': this.translate.instant('CHALLENGES.TYPE.AUTHORITY'),
      'questionnaire': this.translate.instant('CHALLENGES.TYPE.QUESTIONNAIRE'),
      'news': this.translate.instant('CHALLENGES.TYPE.NEWS'),
    };
    this.submitObserver.subscribe({
      next: () => {
        this.isSubmitted = true;
        const validationMessage = this.checkChallengeDefinition();
        if (validationMessage === null) {
          this.challengeCorrect.emit(this.getChallengeDefinition());
        } else {
          this.challengeError.emit(validationMessage);
        }
      },
    });

    if (this.clearObserver) {
      this.clearObserver.subscribe({
        next: () => { this.challengeForm.reset(); this.localStorage.remove('challenge-form'); },
      });
    }

    if (this.onChallengeSelected) {
      this.selectedSubscription = this.onChallengeSelected.subscribe({
        next: (challenge: ChallengeDefinition) => {
          this.challengeSelected = challenge;
          this.loadChallengeDefinition();
        },
      });
    } else if (this.isFromAddChallenge) {
      const savedValue = this.localStorage.getFromLocalStorage<ChallengeDefinition>('challenge-form');
      if (savedValue) {
        this.challengeSelected = savedValue;
        this.loadChallengeDefinition();
      }
      this.challengeForm.valueChanges.subscribe({
        next: () =>
          this.onChallengeChange(),
      });
    }
  }

  onChallengeChange() {
    const val: ChallengeDefinition = this.getChallengeDefinition();
    // Avoiding bug on questionnaire without content (questionnaire editor empty or all questions removed)
    if (val.type === ChallengeType.questionnaire && !(val.questionnaire as any).pages) {
      val.questionnaire = undefined;
      val.questionnaireCorrectAnswer = undefined;
    }

    this.localStorage.storeOnLocalStorage('challenge-form', val);
  }

  ngOnDestroy() {
    if (this.selectedSubscription) {
      this.selectedSubscription.unsubscribe();
    }
    if (this.map) {
      this.map.remove();
    }
  }

  loadDefaultPoints() {
    this.apiService.getDefaultPoints().subscribe({
      next: (result: ApiResponse<ChallengeDefaultPoints[]>) => {
        if (result.status === 200) {
          result.message.forEach(def => {
            switch (def.assignedFor) {
              case 'parent':
                this.defaultPointsForParent = def;
                break;
              case 'child':
                this.defaultPointsForChild = def;
                break;
            }
          });
        }
      },
      error: error => console.error(error),
    });
  }

  setupForm() {
    this.challengeForm = new UntypedFormGroup({
      name: new FormControl<string | null>(null, Validators.required),
      description: new FormControl<string | null>(null, Validators.required),
      explanation: new FormControl<string | null>(null, null),
      periodSelected: new FormControl<boolean>(false, Validators.required),
      periodDays: new FormControl<number | null>(null),
      periodAmount: new FormControl<number | null>(null),
      categorySelected: new FormControl<string | null>(null, Validators.required),
      subcategorySelected: new FormControl<string | null>(null, Validators.required),
      typeSelected: new FormControl<string | null>(null, Validators.required),
      parentPoints: new FormControl<number | null>(null),
      childrenPoints: new FormControl<number | null>(null),
      bothPoints: new FormControl<number | null>(null),
      anyonePoints: new FormControl<number | null>(null),
      minAge: new FormControl<number | null>(null, Validators.required),
      maxAge: new FormControl<number | null>(null, Validators.required),
      lockDown: new FormControl<boolean | null>(null),
      steps: new UntypedFormControl(null),
      relationParentsSelected: new UntypedFormControl(null),
      relationTeachersSelected: new UntypedFormControl(null),
      specificIds: new UntypedFormControl(null),
      assignedFor: new UntypedFormControl(null, Validators.required),
      newsHaveQuestionnaire: new UntypedFormControl(null),
    },
      {
        validators: this.checkPointFields,
        updateOn: 'change',
      });
  }

  get name() {
    return this.challengeForm.get('name');
  }

  get description() {
    return this.challengeForm.get('description');
  }

  get minAge() {
    return this.challengeForm.get('minAge');
  }

  get maxAge() {
    return this.challengeForm.get('maxAge');
  }

  get parentPoints() {
    return this.challengeForm.get('parentPoints');
  }

  get childrenPoints() {
    return this.challengeForm.get('childrenPoints');
  }

  get anyonePoints() {
    return this.challengeForm.get('anyonePoints');
  }

  get steps() {
    return this.challengeForm.get('steps');
  }

  get relationParentsSelected() {
    return this.challengeForm.get('relationParentsSelected');
  }

  get relationTeachersSelected() {
    return this.challengeForm.get('relationTeachersSelected');
  }

  get newsHaveQuestionnaire() {
    return this.challengeForm.get('newsHaveQuestionnaire');
  }

  get assignedFor() {
    return this.challengeForm.get('assignedFor');
  }

  get periodSelected() {
    return this.challengeForm.get('periodSelected');
  }

  public checkHaveInputPoints(): boolean {
    const formData = this.challengeForm.value;
    const assignedForValue = (formData.assignedFor as string);

    if (assignedForValue) {
      switch (assignedForValue) {
        case 'parent':
          if (this.parentPoints?.value) {
            return false;
          }
          break;
        case 'child':
          if (this.childrenPoints?.value) {
            return false;
          }
          break;
        case 'both':
          if (this.parentPoints?.value && this.childrenPoints?.value) {
            return false;
          }
          break;
        default:
          if (this.anyonePoints?.value) {
            return false;
          }
          break;
      }
      return true;
    }

    return false;
  }

  public checkPointFields: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const parentPoints = control.get('parentPoints');
    const childrenPoints = control.get('childrenPoints');
    const anyonePoints = control.get('anyonePoints');
    const assignedFor = control.get('assignedFor');

    let havePoints: boolean;

    switch (assignedFor?.value) {
      case 'parent':
        havePoints = parentPoints?.value;
        break;
      case 'child':
        havePoints = childrenPoints?.value;
        break;
      case 'both':
        havePoints = parentPoints?.value && childrenPoints?.value;
        break;
      default:
        havePoints = anyonePoints?.value;
        break;
    }

    return havePoints ? null : { required: true };
  };

  onCategorySelected(_event: any) {
    // this.categorySelected = event.detail.value;
  }

  onSubcategorySelected(_event: any) {
    // this.subcategorySelected = event.detail.value;
    this.assignedFor?.setValue(null);
  }

  onNewsQuestionnaireChecked(event: any) {
    this.newsQuestionnaireSelected = event.detail.checked;
  }

  formatUsers(users: User[]) {
    const descArray = users.map(user => {
      if (user.roles.includes(UserRole.Teacher) || user.roles.includes(UserRole.Clinician)) {
        return user.email;
      } else {
        return user.displayId;
      }
    });

    return descArray.join(', ');
  }

  resolveDescriptionForUsersArray(users: User[]) {
    users.forEach(u => {
      if (u.roles.includes(UserRole.Parent)) {
        u.description = `${this.translate.instant('ROL.PARENT')}: ${u.displayId}`;
      } else if (u.roles.includes(UserRole.Teacher)) {
        u.description = `${this.translate.instant('ROL.TEACHER')}: ${u.email}`;
      } else if (u.roles.includes(UserRole.Clinician)) {
        u.description = `${this.translate.instant('ROL.CLINICIAN')}: ${u.email}`;
      } else {
        u.description = u.displayId.toString();
      }
    });
  }

  onSelectSpecificIds() {
    const formData = this.challengeForm.value;
    const specificIds: string[] = (formData.specificIds as User[]).map(user => user.id);
    if (specificIds.length > 0) {
      this.specificIdsSelected = true;
    } else {
      this.specificIdsSelected = false;
    }
  }

  onTypeSelected(_event: any) {
    switch (this.challengeForm.value.typeSelected) {
      case ChallengeType.authority:
        this.getUsersToAuthorize();
        break;
      case ChallengeType.questionnaire:
      case ChallengeType.news:
        this.questionnaireJson = undefined;
        this.questionnaireCorrectAnswer = undefined;
        this.isShowingPreview = false;
        this.surveyPreviewText = this.translate.instant('CHALLENGES.ACTION.CHOOSE_RIGHT_ANSWERS');
        break;
    }
  }

  onAssignedForSelected(event: any) {
    this.assignedForSelected = event.detail.value;

    switch (this.assignedForSelected) {
      case 'parent':
        this.parentPoints?.setValue(this.defaultPointsForParent[this.challengeForm.value.subcategorySelected]);
        break;
      case 'child':
        this.childrenPoints?.setValue(this.defaultPointsForChild[this.challengeForm.value.subcategorySelected]);
        break;
      case 'both':
        this.parentPoints?.setValue(this.defaultPointsForParent[this.challengeForm.value.subcategorySelected]);
        this.childrenPoints?.setValue(this.defaultPointsForChild[this.challengeForm.value.subcategorySelected]);
        break;
    }
  }

  getUsersToAuthorize() {
    this.getUsers().then(
      (result: ApiResponse<User[]>) => {
        if (result.status === 200 && result.message) {
          this.users = [];
          this.users.push(...result.message);
          this.resolveDescriptionForUsersArray(this.users);
        }
      },
    );
  }

  onSelectAddress(address: any) {
    const lat = parseFloat(address.lat);
    const lon = parseFloat(address.lon);
    this.map.setView([lat, lon], 50);
  }

  onAddressClear() {
    this.currentAddressList = [];
  }

  onAddressChange(event: any) {
    const address = event.detail.value;
    const addressFiltered = address.trim();

    if (addressFiltered !== '') {
      this.apiService.requestAddressData(this.addressResultLimit, addressFiltered).subscribe({
        next: (response) => {
          this.currentAddressList = response as any;
        },
        error: (error) => {
          // If any error occurs, we log it and set the map on center of Valencia
          console.error(error);
          this.map.setView(this.defaultMapLocation, this.defaultMapZoom);
        },
      });
    } else {
      this.currentAddressList = [];
    }
  }

  /**
   * leaflet on map ready event
   * @param map leaflet map
   */
  onMapReady(map: L.Map) {
    this.map = map;

    let polygon: L.Polygon = this.getPolygon();

    // set the view by default to prevent that map loses tile layers
    setTimeout(() => {
      this.map.invalidateSize();
      if (this.challengeSelected && this.challengeSelected.location) {
        // if there is a polygon draw it
        this.challengeGpsLocation = this.challengeSelected.location;
        polygon = this.getPolygon();
        this.map.fitBounds(polygon.getBounds());
      } else {
        this.map.setView(this.defaultMapLocation, this.defaultMapZoom);
      }
    }, 1000);

    const editableLayers = new L.FeatureGroup();
    this.map.addLayer(editableLayers);

    L.EditToolbar.Delete.include({
      removeAllLayers: false,
    });

    const drawAddControl = this.configAddMapToolbar(editableLayers);
    const drawEditControl = this.configEditMapToolbar(editableLayers);

    if (this.challengeSelected && this.challengeSelected.location) {
      // if there is a polygon draw it
      this.challengeGpsLocation = this.challengeSelected.location;
      polygon = this.getPolygon().addTo(editableLayers);
      this.map.fitBounds(polygon.getBounds());
      this.map.addControl(drawEditControl);
    } else {
      this.map.addControl(drawAddControl);
    }

    this.setMapListeners(this.map, drawAddControl, drawEditControl, editableLayers, this, polygon);
  }

  getPolygon(): L.Polygon {
    const latLngBounds: L.LatLngExpression[][] = [];

    this.challengeGpsLocation?.coordinates.forEach((parentArray: any) => {
      const latlngArray: L.LatLng[] = [];

      parentArray.forEach((child: [number, number]) => {
        const latlng = new L.LatLng(child[1], child[0]);
        latlngArray.push(latlng);
      });

      latLngBounds.push(latlngArray);
    });

    return L.polygon(latLngBounds, { color: '#ff4b00' });
  }

  configEditMapToolbar(editableLayers: L.FeatureGroup<any>): L.Control.Draw {
    return new L.Control.Draw({
      edit: {
        featureGroup: editableLayers,
        edit: false,
      },
      draw: {
        rectangle: false,
        polyline: false,
        circle: false,
        circlemarker: false,
        marker: false,
        polygon: false,
      },
    });
  }

  configAddMapToolbar(editableLayers: L.FeatureGroup<any>): L.Control.Draw {
    return new L.Control.Draw({
      draw: {
        rectangle: false,
        polyline: false,
        circle: false,
        circlemarker: false,
        marker: false,
        polygon: {
          allowIntersection: false,
          drawError: {
            color: '#e1e100', // the shape will turn in this color when intersects
            message: `<strong>${this.translate
              .instant('CHALLENGES.ACTION.MAP_INTERSECTIONS_NOT_ALLOWED')}<strong>`, // Message that will show when intersect
          },
          shapeOptions: {
            color: '#ff4b00',
          },
        },
      },
      edit: {
        featureGroup: editableLayers,
        edit: false,
      },
    });
  }

  setMapListeners(
    map: L.Map,
    drawAddControl: L.Control.Draw,
    drawEditControl: L.Control.Draw,
    editableLayers: L.FeatureGroup<any>,
    parent: ChallengeFormComponent,
    polygon: L.Polygon,
  ) {
    // event that triggers when a shape is created
    map.on('draw:created', (e: L.LeafletEvent) => {
      // if a polygon exists, remove it
      if (polygon) {
        polygon.remove();
      }

      const layer = e.layer;
      editableLayers.addLayer(layer);

      // avoid that user generate multiple shapes. disable draw and edit control and active only delete option.
      map.removeControl(drawAddControl);
      map.addControl(drawEditControl);

      const polygonGeoJson = layer.toGeoJSON().geometry;

      parent.challengeGpsLocation = {
        type: polygonGeoJson.type,
        coordinates: polygonGeoJson.coordinates,
      };
      this.onChallengeChange();
    });

    // event that triggers when de shape is deleted
    map.on('draw:deleted', (_e: L.LeafletEvent) => {
      // allow user all the draw and edit functions disable toolbar with only delete option
      map.removeControl(drawEditControl);
      map.addControl(drawAddControl);

      parent.challengeGpsLocation = undefined;
      this.onChallengeChange();
    });
  }

  checkAssignToTypeAuthorize(): boolean {
    // check if challenge of type authority is assigned to a child
    const formData = this.challengeForm.value;
    const assignedForValue = (formData.assignedFor as string);
    return this.challengeForm.value.typeSelected === 'authority' && assignedForValue === 'child';
  }

  getPoints(assignedFor: string, parentPoints: number, childrenPoints: number, anyonePoints: number): Point {
    let points: Point;

    switch (assignedFor) {
      case 'parent':
        points = {
          onlyParent: parentPoints,
        };
        break;
      case 'child':
        points = {
          onlyChild: childrenPoints,
        };
        break;
      case 'both':
        points = {
          onlyParent: parentPoints,
          onlyChild: childrenPoints,
          both: (parentPoints + childrenPoints),
        };
        break;
      default:
        points = {
          onlyParent: anyonePoints,
          onlyChild: anyonePoints,
          both: anyonePoints,
        };
        break;
    }
    return points;
  }

  getAssignedFor(value: string): string[] {
    const assignedFor: string[] = [];

    switch (value) {
      case 'parent':
        assignedFor.push('parent');
        break;
      case 'child':
        assignedFor.push('child');
        break;
      case 'both':
      case 'anyone':
        assignedFor.push('parent');
        assignedFor.push('child');
        break;
    }

    return assignedFor;
  }

  /**
   * returns null if all validation is ok
   * returns string if any validation failed (the reason as value)
   */
  checkChallengeDefinition(): string | null {
    const formData = this.challengeForm.value;

    if (this.challengeForm.valid && this.challengeForm.value.categorySelected &&
      this.challengeForm.value.subcategorySelected && this.challengeForm.value.typeSelected) {
      switch (this.challengeForm.value.typeSelected) {
        case ChallengeType.authority: {
          const checkParent: boolean = formData.relationParentsSelected ? formData.relationParentsSelected : false;
          const checkTeachers: boolean = formData.relationTeachersSelected ? formData.relationTeachersSelected : false;
          const specificIds = formData.specificIds as User[];

          if (checkParent || checkTeachers || (specificIds && specificIds.length > 0)) {
            return null;
          } else {
            return 'ERRORS.AUTHORITY_FIELDS_REQUIRED';
          }
        }
        case ChallengeType.gps:
          if (this.challengeGpsLocation) {
            return null;
          } else {
            return 'ERRORS.GPS_FIELDS_REQUIRED';
          }
        case ChallengeType.physical: {
          const steps = (formData.steps as number);
          if (steps && steps > 0) { return null; }
          else { return 'ERRORS.PHYSICAL_FIELDS_REQUIRED'; }
        }
        case ChallengeType.news: {
          const newsHaveQuestionnaire = (formData.newsHaveQuestionnaire as boolean);
          if (newsHaveQuestionnaire) {
            if (this.newsValue && !this.isNewsEmpty() && this.questionnaireJson) {
              return null;
            }
            else {
              return 'ERRORS.NEWS_WITH_QUESTIONNAIRE_FIELDS_REQUIRED';
            }
          } else {
            if (this.newsValue && !this.isNewsEmpty()) { return null; }
            else { return 'ERRORS.NEWS_FIELDS_REQUIRED'; }
          }
        }
        case ChallengeType.questionnaire:
          if (this.questionnaireJson) {
            return null;
          }
          else {
            return 'ERRORS.QUESTIONNAIRE_FIELDS_REQUIRED';
          }
        default:
          return null;
      }
    }

    return 'ERRORS.FIELDS_ARE_REQUIRED';
  }

  isNewsEmpty(): boolean {
    let isEmpty = false;
    if (this.newsValue.ops.length > 0) {
      if (this.newsValue.ops.length === 1 && this.newsValue.ops[0].insert === '\n') {
        isEmpty = true;
      }
    } else {
      isEmpty = true;
    }
    return isEmpty;
  }

  // survey events
  onSurveyChange(event: any) {
    if (event.pages[0]['elements']) {
      this.questionnaireJson = event;

      this.questionnairePreviewJson = Object.assign({}, this.questionnaireJson);
      this.questionnairePreviewJson.pages.forEach((page: any) => {
        page.elements.forEach((element: any) => {
          if (element.isRequired) {
            element.isRequired = false;
          }
        });
      });

    } else {
      this.questionnaireJson = undefined;
      this.questionnairePreviewJson = undefined;
    }
    this.isShowingPreview = false;

    this.surveyPreviewText = this.translate.instant('CHALLENGES.ACTION.CHOOSE_RIGHT_ANSWERS');

    this.onChallengeChange();
  }

  onPreviewSelected() {
    if (this.isShowingPreview) {
      this.isShowingPreview = false;
      if (this.questionnaireCorrectAnswer) {
        this.surveyPreviewText = this.translate.instant('CHALLENGES.ACTION.EDIT_RIGHT_ANSWERS');;
      } else {
        this.surveyPreviewText = this.translate.instant('CHALLENGES.ACTION.CHOOSE_RIGHT_ANSWERS');;
      }
    } else {
      this.isShowingPreview = true;
      this.surveyPreviewText = this.translate.instant('CHALLENGES.ACTION.HIDE_CHOOSE_RIGHT_ANSWERS_SECTION');
    }
  }

  onSurveyCompleted(event: any) {
    this.questionnaireCorrectAnswer = event;
  }

  showInfoPeriod() {
    this.presentatorService
      .showHelpAlert(this.translate.instant('CHALLENGES.PERIOD_INFO'), this.translate.instant('CHALLENGES.PERIOD_INFO_HTML'));
  }

  getChallengeDefinition(): ChallengeDefinition {

    let challengeDefinition: ChallengeDefinition = {} as ChallengeDefinition;

    if (this.isFromAddChallenge) {
      const formData = this.challengeForm.value;
      const name = (formData.name as string);
      const description = (formData.description as string);
      const explanation = (formData.explanation as string);
      // We cannot parse string to boolean, so we compare the original string to true, else false
      const lockDown = formData.lockDown === undefined ? null : (formData.lockDown === 'true');
      const hasPeriod = (formData.periodSelected as string);
      const periodDays = (formData.periodDays as number);
      const periodAmount = (formData.periodAmount as number);
      const minAge = (formData.minAge as number);
      const maxAge = (formData.maxAge as number);
      const parentPoints = (formData.parentPoints as number);
      const childrenPoints = (formData.childrenPoints as number);
      const anyonePoints = (formData.anyonePoints as number);
      const assignedForValue = (formData.assignedFor as string);
      const assignedFor: string[] = this.getAssignedFor(assignedForValue);
      const points: Point = this.getPoints(assignedForValue, parentPoints, childrenPoints, anyonePoints);

      challengeDefinition.title = name;
      challengeDefinition.description = description;
      challengeDefinition.explanation = explanation;
      if (hasPeriod === 'true') {
        if (periodDays) {
          challengeDefinition.periodDays = periodDays;
          if (periodAmount) {
            challengeDefinition.periodAmount = periodAmount;
          }
        }
      }
      challengeDefinition.points = points;
      challengeDefinition.category = this.challengeForm.value.categorySelected;
      challengeDefinition.subcategory = this.challengeForm.value.subcategorySelected;
      challengeDefinition.assignedFor = assignedFor;
      challengeDefinition.lockDown = lockDown || false;
      challengeDefinition.minAge = minAge;
      challengeDefinition.maxAge = maxAge;
      challengeDefinition.type = this.challengeForm.value.typeSelected;

      switch (this.challengeForm.value.typeSelected) {
        case ChallengeType.physical: {
          const steps = (formData.steps as number);
          challengeDefinition.steps = steps;
          break;
        }
        case ChallengeType.gps:
          challengeDefinition.location = this.challengeGpsLocation as Location;
          break;
        case ChallengeType.authority: {
          // if specific ids are selected, send only this field
          const formSpecificIds = (formData.specificIds as User[]);

          if (formSpecificIds && formSpecificIds.length > 0) {
            const specificIds: string[] = formSpecificIds.map(user => user.id);
            challengeDefinition.specificIds = specificIds;
          } else {
            // if there isn't specific ids, check checkboxes data for relation with
            const checkParent: boolean = formData.relationParentsSelected;
            const checkTeachers: boolean = formData.relationTeachersSelected;
            const relationWith: string[] = [];

            if (checkParent) { relationWith.push('parent'); }
            if (checkTeachers) {
              relationWith.push('teacher');
              relationWith.push('clinician');
            }
            challengeDefinition.childRelation = relationWith;
          }
          break;
        }
        case ChallengeType.questionnaire:
          if (typeof this.questionnaireJson === 'string') {
            challengeDefinition.questionnaire = this.questionnaireJson;
          } else {
            const surveyModel = new Survey.Model(this.questionnaireJson);
            const surveyJson = surveyModel.toJSON();
            challengeDefinition.questionnaire = JSON.stringify(surveyJson);
          }
          challengeDefinition.questionnaireCorrectAnswer = typeof this.questionnaireCorrectAnswer === 'string' ?
            this.questionnaireCorrectAnswer : JSON.stringify(this.questionnaireCorrectAnswer);
          break;
        case ChallengeType.news: {
          const jsonNews = JSON.stringify(this.newsValue);
          challengeDefinition.news = jsonNews;
          if (this.newsQuestionnaireSelected) {
            if (typeof this.questionnaireJson === 'string') {
              challengeDefinition.questionnaire = this.questionnaireJson;
            } else {
              const surveyModel = new Survey.Model(this.questionnaireJson);
              const surveyJson = surveyModel.toJSON();
              challengeDefinition.questionnaire = JSON.stringify(surveyJson);
            }
            challengeDefinition.questionnaireCorrectAnswer = typeof this.questionnaireCorrectAnswer === 'string' ?
              this.questionnaireCorrectAnswer : JSON.stringify(this.questionnaireCorrectAnswer);
          }
          break;
        }
      }
    } else {
      // is from assign challenge. Get override fields from challenge definition
      challengeDefinition = this.getOverrideDefinition(challengeDefinition);
    }

    return challengeDefinition;
  }

  getOverrideDefinition(challengeDefinition: ChallengeDefinition): ChallengeDefinition {
    const formData = this.challengeForm.value;
    const name = (formData.name as string);
    const description = (formData.description as string);
    const explanation = (formData.explanation as string);
    const hasPeriod = (formData.periodSelected as string);
    const periodDays = (formData.periodDays as number);
    const periodAmount = (formData.periodAmount as number);
    // We cannot parse string to boolean, so we compare the original string to true, else false
    const lockDown = formData.lockDown === undefined ? null : (formData.lockDown === 'true' ? true : false);
    const minAge = (formData.minAge as number);
    const maxAge = (formData.maxAge as number);
    const parentPoints = (formData.parentPoints as number);
    const childrenPoints = (formData.childrenPoints as number);
    const anyonePoints = (formData.anyonePoints as number);
    const assignedForValue = (formData.assignedFor as string);
    const assignedFor: string[] = this.getAssignedFor(assignedForValue);
    const points: Point = this.getPoints(assignedForValue, parentPoints, childrenPoints, anyonePoints);
    const steps: number = (formData.steps as number);

    // check if fields are override
    if (name !== this.challengeSelected?.title) {
      challengeDefinition.title = name;
    }
    if (description !== this.challengeSelected?.description) {
      challengeDefinition.description = description;
    }
    if (explanation !== this.challengeSelected?.explanation) {
      challengeDefinition.explanation = explanation;
    }
    if (hasPeriod === 'true') {
      if (periodDays) {
        challengeDefinition.periodDays = periodDays;
        if (periodAmount) {
          challengeDefinition.periodAmount = periodAmount;
        }
      }
    }

    if (points !== this.challengeSelected?.points) {
      challengeDefinition.points = points;
    }
    if (this.challengeForm.value.categorySelected !== this.challengeSelected?.category) {
      challengeDefinition.category = this.challengeForm.value.categorySelected;
    }
    if (this.challengeForm.value.subcategorySelected !== this.challengeSelected?.subcategory) {
      challengeDefinition.subcategory = this.challengeForm.value.subcategorySelected;
    }
    if (assignedFor.length > 0 && assignedFor !== this.challengeSelected?.assignedFor) {
      challengeDefinition.assignedFor = assignedFor;
    }
    if (lockDown !== this.challengeSelected?.lockDown) {
      challengeDefinition.lockDown = lockDown || false;
    }
    if (minAge !== this.challengeSelected?.minAge) {
      challengeDefinition.minAge = minAge;
    }
    if (maxAge !== this.challengeSelected?.maxAge) {
      challengeDefinition.maxAge = maxAge;
    }

    // dynamic challenge fields
    if (this.challengeGpsLocation && this.challengeGpsLocation !== this.challengeSelected?.location) {
      challengeDefinition.location = this.challengeGpsLocation;
    }
    if (this.challengeSelected?.news && this.newsValue !== JSON.parse(this.challengeSelected.news)) {
      challengeDefinition.news = JSON.stringify(this.newsValue);
    }
    if (this.challengeSelected?.questionnaire) {
      if (this.questionnaireJson !== this.challengeSelected.questionnaire) {
        challengeDefinition.questionnaire = this.questionnaireJson;
      }
      if (this.questionnaireCorrectAnswer !== this.challengeSelected.questionnaireCorrectAnswer) {
        challengeDefinition.questionnaireCorrectAnswer = this.questionnaireCorrectAnswer;
      }
    }

    if (steps !== this.challengeSelected?.steps) {
      challengeDefinition.steps = steps;
    }

    const formSpecificIds = (formData.specificIds as User[]);
    if (formSpecificIds && formSpecificIds.length > 0) {
      const specificIds: string[] = formSpecificIds.map(user => user.id);
      challengeDefinition.specificIds = specificIds;
    } else {
      // if there isn't specific ids, check checkboxes data for relation with
      const checkParent: boolean = formData.relationParentsSelected;
      const checkTeachers: boolean = formData.relationTeachersSelected;
      const relationWith: string[] = [];

      if (checkParent) { relationWith.push('parent'); }
      if (checkTeachers) {
        relationWith.push('teacher');
        relationWith.push('clinician');
      }

      if (relationWith !== this.challengeSelected?.childRelation) {
        challengeDefinition.childRelation = relationWith;
      }
    }

    return challengeDefinition;
  }

  // is from assign challenge load challenge data
  loadChallengeDefinition() {
    // preload form data with challenge definition
    this.challengeForm.setValue({
      name: this.challengeSelected?.title ? this.challengeSelected.title : '',
      description: this.challengeSelected?.description ? this.challengeSelected.description : '',
      explanation: this.challengeSelected?.explanation ? this.challengeSelected.explanation : '',
      periodSelected: this.challengeSelected?.periodDays ? 'true' : 'false',
      periodDays: this.challengeSelected?.periodDays ? this.challengeSelected.periodDays : '',
      periodAmount: this.challengeSelected?.periodAmount ? this.challengeSelected.periodAmount : '',
      categorySelected: this.challengeSelected?.category ? this.challengeSelected.category : '',
      subcategorySelected: this.challengeSelected?.subcategory ? this.challengeSelected.subcategory : '',
      parentPoints: this.challengeSelected?.points &&
        this.challengeSelected.points.onlyParent ? this.challengeSelected.points.onlyParent : '',
      childrenPoints: this.challengeSelected?.points &&
        this.challengeSelected.points.onlyChild ? this.challengeSelected.points.onlyChild : '',
      bothPoints: this.challengeSelected?.points && this.challengeSelected.points.both ? this.challengeSelected.points.both : '',
      anyonePoints: this.challengeSelected?.points && this.challengeSelected.points.both ? this.challengeSelected.points.both : '',
      minAge: this.challengeSelected?.minAge ? this.challengeSelected.minAge : '',
      maxAge: this.challengeSelected?.maxAge ? this.challengeSelected.maxAge : '',
      lockDown: this.challengeSelected?.lockDown === undefined ? null : (this.challengeSelected.lockDown === true ? 'true' : 'false'),
      assignedFor: '',
      steps: this.challengeSelected?.steps ? this.challengeSelected.steps : null,
      relationParentsSelected: (this.challengeSelected?.childRelation &&
        this.challengeSelected.childRelation.includes('parent')) ? true : false,
      relationTeachersSelected: (this.challengeSelected?.childRelation &&
        this.challengeSelected.childRelation.includes('teacher')) ? true : false,
      specificIds: this.challengeSelected?.specificIds ? this.challengeSelected.specificIds : null,
      newsHaveQuestionnaire: this.challengeSelected?.questionnaire ? true : false,
      typeSelected: this.challengeSelected?.type ? this.challengeSelected.type : '',
    });

    if (this.challengeSelected?.assignedFor === null || this.challengeSelected?.points === null) {
      this.assignedForSelected = '';
    } else if (this.challengeSelected?.assignedFor.length === 1) { // Could be child or parent
      this.assignedForSelected = this.challengeSelected.assignedFor[0];
    } else if (this.challengeSelected?.points.onlyChild === this.challengeSelected?.points.onlyParent &&
      this.challengeSelected?.points.onlyParent === this.challengeSelected?.points.both) {
      this.assignedForSelected = 'anyone';
    } else {
      this.assignedForSelected = 'both';
    }
    this.assignedFor?.setValue(this.assignedForSelected);

    switch (this.challengeSelected?.type) {
      case ChallengeType.authority:
        this.selectedUsers = [];
        this.getUsers().then(
          (result: ApiResponse<User[]>) => {
            if (result.status === 200 && result.message) {
              this.users = [];
              this.users.push(...result.message);
              this.resolveDescriptionForUsersArray(this.users);

              if (this.challengeSelected?.specificIds) {
                this.users.forEach(
                  (user: User) => {
                    if (this.challengeSelected?.specificIds.includes(user.id)) {
                      this.selectedUsers.push(user);
                    }
                  },
                );
              }
            }
          },
        );
        break;

      case ChallengeType.gps:
        if (this.challengeSelected?.location) {
          // if there is a polygon draw it
          this.challengeGpsLocation = this.challengeSelected.location;
        }
        break;

      case ChallengeType.news:
        try {
          this.newsValue = JSON.parse(this.challengeSelected?.news);
          if (this.challengeSelected?.questionnaire) {
            this.questionnaireJson = this.challengeSelected?.questionnaire;
            this.questionnairePreviewJson = JSON.parse(this.questionnaireJson);
            this.questionnairePreviewJson.pages.forEach((page: any) => {
              page.elements.forEach((element: any) => element.isRequired = false);
            });
            this.questionnaireCorrectAnswer = this.challengeSelected?.questionnaireCorrectAnswer ?
              JSON.parse(this.challengeSelected.questionnaireCorrectAnswer) : null;
            this.isShowingPreview = false;
            this.newsQuestionnaireSelected = true;
            this.surveyPreviewText = 'Elegir las respuestas correctas';
          }
        } catch (e) {
          console.error(e);
        }
        break;
      case ChallengeType.questionnaire:
        try {
          if (this.challengeSelected?.questionnaire) {
            this.questionnaireJson = this.challengeSelected.questionnaire;
            this.questionnairePreviewJson = JSON.parse(this.questionnaireJson);
            this.questionnairePreviewJson.pages.forEach((page: any) => {
              page.elements.forEach((element: any) => {
                if (element.isRequired) {
                  element.isRequired = false;
                }
              });
            });
            this.questionnaireCorrectAnswer = this.challengeSelected.questionnaireCorrectAnswer ?
              JSON.parse(this.challengeSelected.questionnaireCorrectAnswer) : null;
            this.isShowingPreview = false;
            this.newsQuestionnaireSelected = true;
            this.surveyPreviewText = this.translate.instant('CHALLENGES.ACTION.CHOOSE_RIGHT_ANSWERS');
          }
        } catch (e) {
          console.error(e);
        }
        break;
    }
  }

  async getUsers(): Promise<ApiResponse<User[]>> {
    let entityId: string = '';
    if (this.localStorage.containsKey('entitySelected')) {
      entityId = this.localStorage.getFromLocalStorage<Entity>('entitySelected').id;
    }
    return this.apiService.getUsers([entityId], undefined, undefined, [UserRole.Parent, UserRole.Teacher, UserRole.Clinician]).toPromise();
  }

}
