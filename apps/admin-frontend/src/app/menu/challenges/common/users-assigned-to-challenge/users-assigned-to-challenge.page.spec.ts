import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsersAssignedToChallengePageComponent } from './users-assigned-to-challenge.page';

describe('UsersAssignedToChallengePage', () => {
  let component: UsersAssignedToChallengePageComponent;
  let fixture: ComponentFixture<UsersAssignedToChallengePageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UsersAssignedToChallengePageComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(UsersAssignedToChallengePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
