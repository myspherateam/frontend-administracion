import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { UsersAssignedToChallengePageComponent } from './users-assigned-to-challenge.page';

const routes: Routes = [
  {
    path: '',
    component: UsersAssignedToChallengePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersAssignedToChallengePageRoutingModule { }
