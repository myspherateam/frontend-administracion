import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { UsersAssignedToChallengePageComponent } from './users-assigned-to-challenge.page';
import { UsersAssignedToChallengePageRoutingModule } from './users-assigned-to-challenge-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    UsersAssignedToChallengePageRoutingModule,
  ],
  declarations: [UsersAssignedToChallengePageComponent],
})
export class UsersAssignedToChallengePageModule { }
