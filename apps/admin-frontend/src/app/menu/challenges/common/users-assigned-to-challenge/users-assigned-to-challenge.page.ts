import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import moment from 'moment';

import { ApiResponse, AssignedToIds, ChallengeCompletion, ChallengeType, User } from '@paido/data-model';

import { ApiService } from '../../../../api/api-service';
import { LocalStorageService } from '../../../../services/storage-service';
import { PresentatorService } from '../../../../services/common/presentator.service';
import { Toast } from '../../../../services/common/dto/toast';

@Component({
  selector: 'app-users-assigned-to-challenge',
  templateUrl: './users-assigned-to-challenge.page.html',
  styleUrls: ['./users-assigned-to-challenge.page.scss'],
})
export class UsersAssignedToChallengePageComponent implements OnInit {

  @Input() public assignedToIds!: AssignedToIds;

  @Input() public programmingId!: string;

  public canCancelAny: boolean = false;

  public userChallengesChecks: {
    challenge: ChallengeCompletion,
    checked: boolean
  }[] = [];

  constructor(
    private modalController: ModalController,
    private apiService: ApiService,
    private presentatorService: PresentatorService,
    private localStorage: LocalStorageService,
  ) { }

  ngOnInit() {
    this.getChallengeUsers();
  }

  getChallengeUsers() {
    this.apiService.getUserChallengeCompletion(this.programmingId).subscribe({
      next: (result: ApiResponse<ChallengeCompletion[]>) => {
        if (result.status === 200) {
          this.userChallengesChecks = result.message.map(challenge => ({ challenge, checked: false }));
          this.canCancelAny = this.userChallengesChecks.find(it => this.canCancelChallenge(it.challenge)) !== null;
        }
      },
    });
  }

  canCancelChallenge(challenge: ChallengeCompletion): boolean {
    const now = moment().toDate();
    const finishDatetime = moment(challenge.periodFinishDatetime).toDate();

    const finished = now > finishDatetime;
    const completed = challenge.completed === true || challenge.completedByParents.length > 0;

    return !finished && !completed;
  }

  canCompleteChallenge(challenge: ChallengeCompletion): boolean {
    const userId: string = this.localStorage.getFromLocalStorage<User>('user').id;
    const userAuthority: string[] = challenge.specificIds ? challenge.specificIds : challenge.childRelation;
    return (
      (challenge.type === ChallengeType.authority) &&
      (challenge.completed === false || challenge.completedByParents.length === 0) &&
      (userAuthority.includes(userId) || userAuthority.includes('teacher') || userAuthority.includes('clinician'))
    );
  }

  closeModal() {
    this.modalController.dismiss();
  }

  cancelChallenge() {
    const usersToCancel = this.userChallengesChecks.filter(it => it.checked).map(it => it.challenge.forId);

    if (usersToCancel.length > 0) {
      // TODO: add translations
      this.presentatorService.createYesNoAlert('Desasignar reto',
        '¿Seguro que quieres desasignar el reto a los usuarios seleccionados?').then(
          (value: boolean) => {
            if (value) {
              this.apiService.cancelChallengeProgrammingForUsers(this.programmingId, usersToCancel).subscribe({
                next: (result: ApiResponse<ChallengeCompletion>) => {
                  if (result.status === 200) {
                    this.presentatorService.showToast(Toast.message('El reto ha sido desasignado.'));
                    this.getChallengeUsers();
                  }
                },
                error: error => console.error(error),
              });
            }
          },
        );
    } else {
      // TODO: add translations
      this.presentatorService.showInfoAlert('Desasignar reto',
        'Debes seleccionar previamente los niños/as que quieres desasignar del reto');
    }
  }

  completeChallenge(challenge: ChallengeCompletion) {
    // mark a challenge as complete
    // TODO: add translations
    this.presentatorService.createYesNoAlert('Completar reto', '¿Seguro que quieres completar el reto?').then(
      (value: boolean) => {
        if (value) {
          this.apiService.markAChallengeAsComplete(challenge).subscribe({
            next: (result: ApiResponse<ChallengeCompletion>) => {
              if (result.status === 200) {
                // TODO: add translations
                this.presentatorService.showInfoAlert('Reto completado', 'El reto ha sido completado')
                  .then(() => this.getChallengeUsers());
              }
            },
          });
        }
      },
    );
  }

}
