import * as L from 'leaflet';
import { Component, Input, OnChanges } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { ChallengeProgramming, Entity, UserRole } from '@paido/data-model';

import { ApiService } from '../../../../api/api-service';
import { Delta } from '../news/interfaces/delta';
import { LocalStorageService } from '../../../../services/storage-service';
import { UsersAssignedToChallengePageComponent } from '../users-assigned-to-challenge/users-assigned-to-challenge.page';

@Component({
  selector: 'app-challenge-preview',
  templateUrl: './challenge-preview.component.html',
  styleUrls: ['./challenge-preview.component.scss'],
})
export class ChallengePreviewComponent implements OnChanges {

  @Input() challenge!: ChallengeProgramming;

  public resolvedDidacticUnits?: string;

  public resolvedSpecificIds?: string;

  public map!: L.Map;

  public isShowingMap: boolean = false;

  public isShowingNews: boolean = false;

  public isShowingQuestionnaire: boolean = false;

  public isShowingQuestionnaireNews: boolean = false;

  public actionMap: string;

  public actionNews: string;

  public actionQuestionnaire: string;

  public actionQuestionnaireNews: string;

  public newsContent!: Delta;

  public questionnaireJson: any;

  public questionnaireNewsJson: any;

  public questionnaireCorrectAnswer: any;

  constructor(
    private modalController: ModalController,
    private apiService: ApiService,
    private translate: TranslateService,
    private localStorage: LocalStorageService,
  ) {
    this.actionMap = this.translate.instant('CHALLENGES.ACTION.SHOW_MAP');
    this.actionNews = this.translate.instant('CHALLENGES.ACTION.SHOW_NEWS');
    this.actionQuestionnaire = this.translate.instant('CHALLENGES.ACTION.SHOW_QUESTIONNAIRE');
    this.actionQuestionnaireNews = this.translate.instant('CHALLENGES.ACTION.SHOW_QUESTIONNAIRE');
  }

  /**
   * Call on change method when challenge object change
   */
  ngOnChanges() {
    this.resolveDidacticUnits();
    this.resolveSpecificIds();
  }

  resolveDidacticUnits() {
    this.apiService.getDidacticUnits(this.challenge.didacticUnitsIds).subscribe({
      error: (result) => {
        if (result.status === 200) {
          this.resolvedDidacticUnits = result.message.map(unit => unit.name).join(', ');
        }
      },
      next: error => console.error(error),
    });
  }

  resolveSpecificIds() {
    if (this.challenge.specificIds && this.challenge.specificIds.length > 0) {
      let entityId: string = '';
      if (this.localStorage.containsKey('entitySelected')) {
        entityId = this.localStorage.getFromLocalStorage<Entity>('entitySelected').id;
      }

      // TODO: what happens if entitySelected is not present in storage?
      this.apiService.getUsers([entityId], undefined, this.challenge.specificIds).subscribe({
        next: (result) => {
          if (result.status === 200) {
            const usersDescription = result.message.map(u => {
              if (u.roles.includes(UserRole.Teacher) || u.roles.includes(UserRole.Clinician)) {
                return u.email;
              } else {
                return u.displayId.toString();
              }
            });

            this.resolvedSpecificIds = usersDescription.join(', ');
          }
        },
        error: error => console.error(error),
      });
    }
  }

  showMap() {
    if (this.isShowingMap) {
      this.isShowingMap = false;
      this.actionMap = this.translate.instant('CHALLENGES.ACTION.SHOW_MAP');
    } else {
      this.isShowingMap = true;
      this.actionMap = this.translate.instant('CHALLENGES.ACTION.HIDE_MAP');

      setTimeout(() => { this.initMap(this); }, 0);
    }
  }

  showNews() {
    if (this.isShowingNews) {
      this.isShowingNews = false;
      this.actionNews = this.translate.instant('CHALLENGES.ACTION.SHOW_NEWS');
    } else {
      this.isShowingNews = true;
      this.actionNews = this.translate.instant('CHALLENGES.ACTION.HIDE_NEWS');
      this.newsContent = JSON.parse(this.challenge.news);
    }
  }

  showQuestionnaire() {
    if (this.isShowingQuestionnaire) {
      this.isShowingQuestionnaire = false;
      this.actionQuestionnaire = this.translate.instant('CHALLENGES.ACTION.SHOW_QUESTIONNAIRE');
    } else {
      this.isShowingQuestionnaire = true;
      this.actionQuestionnaire = this.translate.instant('CHALLENGES.ACTION.HIDE_QUESTIONNAIRE');
      this.questionnaireJson = JSON.parse(this.challenge.questionnaire as string);
      this.questionnaireCorrectAnswer = this.challenge.questionnaireCorrectAnswer ?
        JSON.parse(this.challenge.questionnaireCorrectAnswer) : null;
    }
  }

  showQuestionnaireNews() {
    if (this.isShowingQuestionnaireNews) {
      this.isShowingQuestionnaireNews = false;
      this.actionQuestionnaireNews = this.translate.instant('CHALLENGES.ACTION.SHOW_QUESTIONNAIRE');
    } else {
      this.isShowingQuestionnaireNews = true;
      this.actionQuestionnaireNews = this.translate.instant('CHALLENGES.ACTION.HIDE_QUESTIONNAIRE');
      this.questionnaireNewsJson = JSON.parse(this.challenge.questionnaire as string);
      this.questionnaireCorrectAnswer = this.challenge.questionnaireCorrectAnswer ?
        JSON.parse(this.challenge.questionnaireCorrectAnswer) : null;
    }
  }

  getChildRelation() {
    if (this.challenge.childRelation.includes('teacher') ||
      this.challenge.childRelation.includes('clinician')) {
      return this.translate.instant('ROL.TEACHERS_CLINICIANS');
    } else if (this.challenge.childRelation.includes('parent')) {
      return this.translate.instant('ROL.PARENTS');
    } else {
      return '';
    }
  }

  initMap(parent: ChallengePreviewComponent) {
    if (parent.map !== undefined) { parent.map.remove(); }

    const polygonLeaflet: L.LatLngExpression[][] = [];
    const challengePolygon = this.challenge.location.coordinates;

    challengePolygon.forEach((item: any) => {
      const vector: L.LatLngExpression[] = [];
      item.forEach((element: number[]) => {
        const value: L.LatLngExpression = {
          lng: element[0],
          lat: element[1],
        };
        vector.push(value);
      });
      polygonLeaflet.push(vector);
    });

    const polygon = L.polygon(polygonLeaflet);
    const centerPolygon = polygon.getBounds().getCenter();

    parent.map = new L.Map('map').setView([centerPolygon.lat, centerPolygon.lng], 13);
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; OpenStreetMap contributors',
    }).addTo(parent.map);

    const editableLayers = new L.FeatureGroup();
    parent.map.addLayer(editableLayers);

    polygon.setStyle({
      color: '#ff4b00',
    });
    polygon.addTo(parent.map);
  }

  async showModalUsersAssignTo() {
    const modal = await this.modalController.create({
      component: UsersAssignedToChallengePageComponent,
      componentProps: {
        assignedToIds: this.challenge.assignedToIds,
        programmingId: this.challenge.id,
      },
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });

    return modal.present();
  }

  getAssignedForTranslated(assignedFor: string[]) {
    return assignedFor.map(assigned => this.translate.instant(`ROL.${assigned.toUpperCase()}`)).join(', ');
  }
}
