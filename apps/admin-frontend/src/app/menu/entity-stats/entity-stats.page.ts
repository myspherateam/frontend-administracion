import { Component, OnInit } from '@angular/core';

import { ApiResponse, Entity, EntityStats } from '@paido/data-model';

import { ApiService } from '../../api/api-service';
import { LocalStorageService } from '../../services/storage-service';

@Component({
  selector: 'app-entity-stats',
  templateUrl: './entity-stats.page.html',
  styleUrls: ['./entity-stats.page.scss'],
})
export class EntityStatsPageComponent implements OnInit {

  public entity: Entity;

  public statTypeSelection: string;

  public entityStats?: EntityStats;

  constructor(
    private localStorage: LocalStorageService,
    private apiService: ApiService,
  ) {
    this.statTypeSelection = 'usersStats';
    this.entity = this.localStorage.getFromLocalStorage<Entity>('entitySelected');
  }

  ngOnInit() {
    if (this.entity) {
      this.apiService.getEntityStats(this.entity.id).subscribe({
        next: (result: ApiResponse<EntityStats>) => {
          if (result.status === 200) {
            this.entityStats = result.message;
          }
        },
      });
    }
  }

  /**
   * Get professionals quantity
   */
  getProfessionalsNumber() {
    return (this.entityStats?.numTeachers ? this.entityStats.numTeachers : 0) +
      (this.entityStats?.numClinicians ? this.entityStats.numClinicians : 0);
  }

  /**
   * Get users quantity
   */
  getUsersNumber() {
    return (this.entityStats?.numChildren ? this.entityStats.numChildren : 0) +
      (this.entityStats?.numParents ? this.entityStats.numParents : 0);
  }

  /**
   * Get total number of challenges assigned
   */
  getNumberAssignedChallenges(): number {
    const numAssignedChallenges = this.entityStats?.numAssignedChallenges;
    return (numAssignedChallenges) ? numAssignedChallenges.authority +
      numAssignedChallenges.gps +
      numAssignedChallenges.news +
      numAssignedChallenges.physical +
      numAssignedChallenges.questionnaire : 0;
  }

  /**
   * Get total number of challenges completed
   */
  getNumberCompletedChallenges(): number {
    const numCompletedChallenges = this.entityStats?.numCompletedChallenges;
    return (numCompletedChallenges) ? numCompletedChallenges.authority +
      numCompletedChallenges.gps +
      numCompletedChallenges.news +
      numCompletedChallenges.physical +
      numCompletedChallenges.questionnaire : 0;
  }

  /**
   * Get number of challenges not completed
   */
  getNumberOfNotCompletedChallenges(): number {
    return this.getNumberAssignedChallenges() - this.getNumberCompletedChallenges();
  }
}
