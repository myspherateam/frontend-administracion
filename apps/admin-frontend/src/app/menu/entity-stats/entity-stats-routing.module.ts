import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EntityStatsPageComponent } from './entity-stats.page';

const routes: Routes = [
  {
    path: '',
    component: EntityStatsPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntityStatsPageRoutingModule { }
