import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { EntityStatsPageComponent } from './entity-stats.page';
import { EntityStatsPageRoutingModule } from './entity-stats-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EntityStatsPageRoutingModule,
    TranslateModule,
  ],
  declarations: [EntityStatsPageComponent],
})
export class EntityStatsPageModule { }
