import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AddUserPageModule } from './add-user/add-user.module';
import { CommonsModule } from '../../menu/challenges/common/commons.module';
import { ManageUsersPageComponent } from './manage-users.page';
import { ManageUsersPageRoutingModule } from './manage-users-routing.module';
import { UserDetailsPageModule } from './user-details/user-details.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    ManageUsersPageRoutingModule,
    CommonsModule,
    ReactiveFormsModule,
    AddUserPageModule,
    UserDetailsPageModule,
  ],
  declarations: [ManageUsersPageComponent],
})
export class ManageUsersPageModule { }
