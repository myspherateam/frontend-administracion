import { Component, Input } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { finalize } from 'rxjs';
import { jsPDF } from 'jspdf';

import { ApiResponse, Entity, User, UserRole } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { LocalStorageService } from '../../../services/storage-service';
import { PresentatorService } from '../../../services/common/presentator.service';
import { Toast } from '../../../services/common/dto/toast';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPageComponent {

  @Input() public groupId!: string;

  public isEmailActive!: boolean;

  public roleViews: { value: UserRole, name: string }[] = [];

  public hideUsers!: boolean;

  public addUserForm!: UntypedFormGroup;

  public usersCreated: User[] = [];

  public creating: boolean = false;

  constructor(
    private modalController: ModalController,
    private localStorage: LocalStorageService,
    private formBuilder: UntypedFormBuilder,
    private apiService: ApiService,
    private analytics: AngularFireAnalytics,
    private presentatorService: PresentatorService,
    private translate: TranslateService,
  ) {
    this.checkUserRol();
  }

  /**
   * Create form group and validators
   */
  setupForm() {
    if (this.isEmailActive) {
      this.addUserForm = this.formBuilder.group({
        email: [null, Validators.compose([
          Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
          Validators.required])],
        roleSelected: [this.roleViews.length === 1 ? this.roleViews[0].value : null, Validators.required],
      });
    } else {
      this.addUserForm = this.formBuilder.group({
        externalId: [null],
        roleSelected: [this.roleViews.length === 1 ? this.roleViews[0].value : null, Validators.required],
        parentSelection: ['none'],
      });
    }
  }

  /**
   * Check user rol to setup form
   */
  checkUserRol() {
    const user: User = this.localStorage.getFromLocalStorage<User>('user');
    const roles: string[] = user.roles;

    if (roles.includes(UserRole.GlobalAdmin)) {
      // user is global admin and can create local admins
      this.roleViews.push(
        {
          value: UserRole.LocalAdmin,
          name: this.translate.instant('ROL.LOCAL_ADMIN'),
        },
      );

      this.hideUsers = true;
      this.isEmailActive = true;

    } else if (roles.includes(UserRole.LocalAdmin)) {
      // user is local admin and can create teacher or clinician
      this.roleViews.push(...
        [
          {
            value: UserRole.Teacher,
            name: this.translate.instant('ROL.TEACHER'),
          },
          {
            value: UserRole.Clinician,
            name: this.translate.instant('ROL.CLINICIAN'),
          },
        ],
      );

      this.hideUsers = true;
      this.isEmailActive = true;

    } else if (roles.includes(UserRole.Teacher) || roles.includes(UserRole.Clinician)) {
      this.roleViews.push(
        {
          value: UserRole.Child,
          name: this.translate.instant('ROL.CHILD'),
        },
      );

      this.hideUsers = false;
      this.isEmailActive = false;
    }

    this.setupForm();
  }

  /**
   * Close modal function
   */
  closeModal() {
    this.modalController.dismiss();
  }

  /**
   * Get user role literal
   * @param user user instance
   */
  getRole(user: User): string {
    let role = '';

    user.roles.forEach((value: string) => {
      switch (value) {
        case UserRole.Parent:
          role = this.translate.instant('ROL.PARENT');
          break;
        case UserRole.Teacher:
          role = this.translate.instant('ROL.TEACHER');
          break;
        case UserRole.Clinician:
          role = this.translate.instant('ROL.CLINICIAN');
          break;
        case UserRole.Child:
          role = this.translate.instant('ROL.CHILD');
          break;
      }
    });

    return role;
  }

  /**
   * Add user button function
   */
  addUser() {
    const form = this.addUserForm.value;
    const email = form.email;
    const role = form.roleSelected;
    const parentSelection = form.parentSelection;
    const externalId = form.externalId;

    const user: User = {} as User;
    if (email) { user.email = email; }
    if (role) { user.roles = [role]; }
    user.externalId = (externalId && externalId.trim().length) ? externalId.toUpperCase() : null;

    if (this.localStorage.containsKey('entitySelected')) {
      user.entityIds = [this.localStorage.getFromLocalStorage<Entity>('entitySelected').id];
    }

    user.groupIds = [this.groupId];

    switch (role) {
      case UserRole.Child:
        this.createAChild(user, parentSelection);
        break;
      default:
        this.createAnAdmin(user);
        break;
    }
  }

  /**
   * Create a LocalAdmin or a GlobalAdmin
   * @param user user instance
   */
  createAnAdmin(user: User) {
    this.creating = true;
    this.apiService.createUser(user)
      .pipe(
        finalize(() => { this.creating = false; }),
      )
      .subscribe({
        next: (result: ApiResponse<User>) => {
          if (result.status === 200) {
            this.analytics.logEvent('new_user', { id: result.message.id });
            this.presentatorService.showInfoAlert('USER.ADD_USER_SUCCESS', 'USER.ADD_USER_SUCCESS_EXTENDED').then(
              result2 => {
                if (result2) {
                  this.addUserForm.reset();
                  this.modalController.dismiss(true);
                }
              },
            );
          }
        },
        error: (error: HttpErrorResponse) => {
          console.error(error);
          if (error.status >= 400) {
            this.presentatorService.showToast(Toast.message('ERRORS.ERROR_WITH_VALUE', { val: error.error.message }));
          }
        },
      });
  }

  /**
   * Create a child
   * @param user child user
   * @param parentSelection selection value to create parents
   */
  createAChild(user: User, parentSelection: string) {
    this.apiService.createUser(user).subscribe({
      next: (result: ApiResponse<User>) => {
        if (result.status === 200) {
          const child = result.message;
          this.usersCreated = [];
          this.usersCreated.push(child);

          switch (parentSelection) {
            case 'one':
              this.createAParent(child);
              break;
            case 'two':
              this.createTwoParents(child);
              break;
            default:
              this.createPDF(this.usersCreated);
              this.analytics.logEvent('new_child_user', { id: result.message.id });
              break;
          }

          this.addUserForm.reset();
        }
      },
      error: (error: HttpErrorResponse) => {
        console.error(error);
        this.presentatorService.showToast(Toast.message('ERRORS.ERROR_WITH_VALUE', { val: error.error.message }));
      },
    });
  }

  /**
   * Create two parents with a child assigned
   * @param child the child created to assign to the parent
   */
  createTwoParents(child: User) {
    const newParentPromises: Promise<ApiResponse<User>>[] = [];
    for (let i = 0; i < 2; i++) {
      const parentUser = this.getParentUser(child);
      newParentPromises.push(this.apiService.createUser(parentUser).toPromise());
    }

    Promise.all(newParentPromises).then(results => {
      const parents = results.map((result) => result.message);
      this.usersCreated = this.usersCreated.concat(parents);
      this.createPDF(this.usersCreated);
      this.analytics.logEvent('new_two_parent_user');
    });
  }

  /**
   * Create a parent with a child assigned
   * @param child the child created to assign to the parent
   */
  createAParent(child: User) {
    const parentUser = this.getParentUser(child);
    this.apiService.createUser(parentUser).subscribe({
      next: (result: ApiResponse<User>) => {
        if (result.status === 200) {
          this.usersCreated.push(result.message);
          this.createPDF(this.usersCreated);
          this.analytics.logEvent('new_parent_user', { id: result.message.id });
        }
      },
      error: (error: HttpErrorResponse) => {
        console.error(error);
      },
    });
  }

  /**
   * Create a parent object
   * @param user the child created to assign to the parent
   */
  getParentUser(user: User): User {
    const parentUser = {} as User;
    parentUser.roles = ['Parent'];
    if (this.localStorage.containsKey('entitySelected')) {
      parentUser.entityIds = user.entityIds;
    }
    if (user.groupIds) { parentUser.groupIds = user.groupIds; }
    parentUser.caringForIds = [user.id];
    return parentUser;
  }

  /**
   * Create a pdf doc to give users created data
   */
  createPDF(newUsers: User[]) {
    const doc = new jsPDF({});

    doc.setFont(doc.getFont().fontName, 'normal', 'bold');
    doc.text(this.translate.instant('USER.NEW_USERS'), 25, 25);
    doc.setFontSize(14);

    let rowPixels = 50;
    newUsers.forEach((user) => {
      let rowMessage: string = '';
      if (user.roles.includes(UserRole.Child)) {
        // eslint-disable-next-line max-len
        rowMessage = `${this.translate.instant('ROL.CHILD')} ${user.displayId} - ${this.translate.instant('USER.FIELDS.USER_REG_CODE')}: ${user.registrationCode}`;
      } else if (user.roles.includes(UserRole.Parent)) {
        // eslint-disable-next-line max-len
        rowMessage = `${this.translate.instant('ROL.PARENT')} ${user.displayId} - ${this.translate.instant('USER.FIELDS.USER_REG_CODE')}: ${user.registrationCode} - ${this.translate.instant('USER.FIELDS.CARING_FOR_CHILD')}: ${user.caringForIds.length}`;
      } else {
        // TODO: add translation
        console.error(`Server data error. User: ${user.id}`);
      }

      doc.text(rowMessage, 25, rowPixels);
      rowPixels += 15;
    });

    doc.save(`nuevosUsuarios${Date.now()}.pdf`);
  }
}
