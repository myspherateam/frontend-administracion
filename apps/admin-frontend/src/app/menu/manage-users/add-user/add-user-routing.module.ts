import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AddUserPageComponent } from './add-user.page';

const routes: Routes = [
  {
    path: '',
    component: AddUserPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddUserPageRoutingModule { }
