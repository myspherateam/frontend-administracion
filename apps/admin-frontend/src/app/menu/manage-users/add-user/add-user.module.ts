import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AddUserPageComponent } from './add-user.page';
import { AddUserPageRoutingModule } from './add-user-routing.module';
import { CommonsModule } from '../../challenges/common/commons.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    AddUserPageRoutingModule,
    ReactiveFormsModule,
    CommonsModule,
  ],
  declarations: [AddUserPageComponent],
})
export class AddUserPageModule { }
