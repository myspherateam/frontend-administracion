import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, Entity, Group, User } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { LocalStorageService } from '../../../services/storage-service';
import { PresentatorService } from '../../../services/common/presentator.service';
import { Toast } from '../../../services/common/dto/toast';

@Component({
  selector: 'app-move-user-to-group',
  templateUrl: './move-user-to-group.component.html',
  styleUrls: ['./move-user-to-group.component.scss'],
})
export class MoveUserToGroupComponent implements OnInit {

  @Input()
  public user!: User;

  @Input() public username!: string;

  public groups: Group[] = [];

  public allGroups: Group[] = [];

  public groupSelected?: Group;

  constructor(
    private modalController: ModalController,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private presentatorService: PresentatorService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.getGroups();
  }

  closeModal() {
    this.modalController.dismiss(false);
  }

  getGroups() {
    let entityId: string;
    if (this.localStorage.containsKey('entitySelected')) {
      entityId = this.localStorage.getFromLocalStorage<Entity>('entitySelected').id;

      this.apiService.getGroups([entityId]).subscribe({
        next: (result: ApiResponse<Group[]>) => {
          if (result.status === 200) {
            this.groups = result.message;
            this.allGroups = this.groups;
          }
        },
        error: error => console.error(error),
      });
    }
  }

  assignUserToGroup() {
    if (this.groupSelected !== undefined) {
      // add groupId to user groups array
      this.user.groupIds.push(this.groupSelected.id);
      this.apiService.createUser(this.user).subscribe({
        next: (result: ApiResponse<User>) => {
          if (result.status === 200) {
            this.presentatorService.showToast(
              Toast.message(
                this.translateService.instant(
                  'USER.ASSIGN_USER_SUCCESS',
                  { user: this.username, group: this.groupSelected?.name },
                ),
              ),
            );
            this.modalController.dismiss(true);
          }
        },
      });
    }
  }

}
