import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { EditUserPageComponent } from './edit-user.page';
import { EditUserPageRoutingModule } from './edit-user-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    EditUserPageRoutingModule,
    IonicSelectableModule,
  ],
  declarations: [EditUserPageComponent],
})
export class EditUserPageModule { }
