import { Component, Input, OnInit } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import { ModalController } from '@ionic/angular';

import { ApiResponse, Entity, User, UserRole } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { LocalStorageService } from '../../../services/storage-service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.page.html',
  styleUrls: ['./edit-user.page.scss'],
})
export class EditUserPageComponent implements OnInit {

  @Input() public userId!: string;

  @Input() public groupId!: string;

  public user!: User;

  public users!: User[];

  public entityId!: string;

  public selectedUsers: User[] = [];

  constructor(
    private modalController: ModalController,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
  ) { }

  ngOnInit() {
    this.apiService.getUserDetail(this.userId).subscribe({
      next: (result: ApiResponse<User>) => {
        if (result.status === 200) {
          this.user = result.message;
          this.getListToAssign();
        }
      },
    });
  }

  getDisplayName(): string {
    if (this.user.roles.includes('Child') || this.user.roles.includes('Parent')) {
      return this.user.displayName ? this.user.displayName : String(this.user.displayId);
    } else {
      return this.user.email;
    }
  }

  getRole(): string {
    let role = '';

    this.user.roles.forEach((value: string) => {
      switch (value) {
        case UserRole.LocalAdmin:
          role = 'Administrador local';
          break;
        case UserRole.Parent:
          role = 'Padre/madre/tutor';
          break;
        case UserRole.Teacher:
          role = 'Profesor/a';
          break;
        case UserRole.Clinician:
          role = 'Médico/a';
          break;
        case UserRole.Child:
          role = ' Niño/a';
          break;
      }
    });

    return role;
  }

  getListToAssign() {
    if (this.localStorage.containsKey('entitySelected')) {
      this.entityId = this.localStorage.getFromLocalStorage<Entity>('entitySelected').id;
    }

    // get parents or children to assign
    this.users = [];
    const role: string = this.user.roles.includes('Child') ? UserRole.Parent : UserRole.Child;
    const fromGroup: string[] | undefined = this.groupId ? [this.groupId] : undefined;

    this.apiService.getUsers([this.entityId], fromGroup, undefined, [role]).subscribe({
      next: (result: ApiResponse<User[]>) => {
        if (result.status === 200) {
          this.users.push(...result.message);
          this.preselectAssignValues();
        }
      },
    });
  }

  preselectAssignValues() {
    this.selectedUsers = [];
    if (this.user.roles.includes('Child')) {
      this.users.forEach((user: User) => {
        if (this.user.parents && this.user.parents.includes(user.id)) {
          this.selectedUsers.push(user);
        }
      });
    } else {
      this.users.forEach((user: User) => {
        if (this.user.caringForIds && this.user.caringForIds.includes(user.id)) {
          this.selectedUsers.push(user);
        }
      });
    }
  }

  formatUsers(users: User[]) {
    return users.map(user => this.getItemTextField(user)).join(', ');
  }

  /**
   * Get the text field for the item of "caring for"/ "assign to" list
   */
  getItemTextField(user: User): string {
    if (user.roles.includes('Child') || user.roles.includes('Parent')) {
      return user.displayName ? user.displayName : String(user.displayId);
    } else {
      return user.email;
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

  saveUser() {
    if (this.user.roles.includes('Child')) {
      this.user.parents = this.selectedUsers.map(user => user.id);
    } else {
      this.user.caringForIds = this.selectedUsers.map(user => user.id);
    }

    this.user.externalId = (this.user.externalId && this.user.externalId.trim().length > 0) ?
      this.user.externalId.toUpperCase() :
      undefined;

    this.apiService.createUser(this.user).subscribe({
      next: (result: ApiResponse<User>) => {
        if (result.status === 200) {
          this.closeModal();
        }
      },
    });
  }

  /**
   * Filter users by displayId and displayName
   */
  filterUsers(users: User[], text: string) {
    return users.filter(user => {
      return user.displayName ?
        (user.displayName.toLowerCase().indexOf(text) !== -1) :
        (user.displayId.toString().toLowerCase().indexOf(text) !== -1);
    });
  }

  /**
   * Search method for ionic-selectable
   * @param event ionic-selectable search event
   */
  searchUsers(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    const text = event.text.trim().toLowerCase();
    event.component.startSearch();

    event.component.items = this.filterUsers(this.users, text);
    event.component.endSearch();
  }

}
