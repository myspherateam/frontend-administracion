import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EditUserPageComponent } from './edit-user.page';

const routes: Routes = [
  {
    path: '',
    component: EditUserPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditUserPageRoutingModule { }
