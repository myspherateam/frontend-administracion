import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, Entity, User, UserRole } from '@paido/data-model';

import { AddUserPageComponent } from './add-user/add-user.page';
import { ApiService } from '../../api/api-service';
import { LocalStorageService } from '../../services/storage-service';
import { PresentatorService } from '../../services/common/presentator.service';
import { Toast } from '../../services/common/dto/toast';
import { UserDetailsComponent } from './user-details/user-details.page';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.page.html',
  styleUrls: ['./manage-users.page.scss'],
})
export class ManageUsersPageComponent implements OnInit {

  public users: User[] = [];

  public allUsers: User[] = [];

  private entityId?: string;

  constructor(
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private modalController: ModalController,
    private presentatorService: PresentatorService,
    private translate: TranslateService,
  ) {
    if (this.localStorage.containsKey('entitySelected')) {
      this.entityId = this.localStorage.getFromLocalStorage<Entity>('entitySelected').id;
    }
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    const entityIds: string[] = [];
    if (this.entityId) { entityIds.push(this.entityId); }
    this.apiService.getUsers(entityIds).subscribe({
      next: (result: ApiResponse<User[]>) => {
        if (result.status === 200) {
          this.users = result.message;
          this.allUsers = this.users;
        }
      },
      error: error => {
        console.error(error);
        this.presentatorService.showToast(Toast.message('ERRORS.ERROR_WITH_VALUE', { val: error.error.message }));
      },
    });
  }

  getRole(user: User): string {
    let role = '';

    user.roles.forEach((value: string) => {
      switch (value) {
        case UserRole.LocalAdmin:
          role = this.translate.instant('ROL.LOCAL_ADMIN');
          break;
        case UserRole.Parent:
          role = this.translate.instant('ROL.PARENT');
          break;
        case UserRole.Teacher:
          role = this.translate.instant('ROL.TEACHER');
          break;
        case UserRole.Clinician:
          role = this.translate.instant('ROL.CLINICIAN');
          break;
        case UserRole.Child:
          role = this.translate.instant('ROL.CHILD');
          break;
      }
    });

    return role;
  }

  onSearch(event: Event) {
    const value: string = (event as CustomEvent).detail.value.trim();
    if (value && value !== '') {
      this.users = this.allUsers.filter(
        user => {
          const regexp = RegExp(value, 'i');
          return regexp.test('' + user.displayId) ||
            regexp.test(user.id) || regexp.test(user.email);
        });

    } else {
      this.users = this.allUsers;
    }
  }

  async addUser() {
    // go to add user form
    const modal = await this.modalController.create({
      component: AddUserPageComponent,
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });

    modal.onDidDismiss().then((createdUsers) => {
      if (createdUsers.data) {
        this.getUsers();
      }
    });

    return modal.present();
  }

  onUserSelected(user: User) {
    // show user detail
    this.showUserDetails(user.id);
  }

  resendEmail(user: User) {
    this.presentatorService.createYesNoAlert('USER.RESEND_REGISTRATION_EMAIL', 'ACTION.ARE_YOU_SURE').then(
      (value: boolean) => {
        if (value) {
          this.apiService.resendEmail(user.id).subscribe({
            next: (result: ApiResponse<boolean>) => {
              if (result.status === 200) {
                if (result.message) {
                  this.presentatorService.showToast(Toast.message('USER.RESEND_REGISTRATION_EMAIL_SUCCESS', { val: user.email }));
                } else {
                  this.presentatorService.showToast(Toast.message('ERRORS.RESEND_REGISTRATION_EMAIL_FAIL'));
                }
              }
            },
            error: error => console.error(error),
          });
        }
      },
    );
  }

  async showUserDetails(id: string) {
    // go to add user form
    const modal = await this.modalController.create({
      component: UserDetailsComponent,
      componentProps: {
        userId: id,
      },
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });

    return modal.present();
  }

}
