import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ManageUsersPageComponent } from './manage-users.page';

const routes: Routes = [
  {
    path: '',
    component: ManageUsersPageComponent,
  },
  {
    path: 'add-user',
    loadChildren: () => import('./add-user/add-user.module').then(m => m.AddUserPageModule),
  },
  {
    path: 'user-details',
    loadChildren: () => import('./user-details/user-details.module').then(m => m.UserDetailsPageModule),
  },
  {
    path: 'edit-user',
    loadChildren: () => import('./edit-user/edit-user.module').then(m => m.EditUserPageModule),
  },
  {
    path: 'user-stats',
    loadChildren: () => import('./user-stats/user-stats.module').then(m => m.UserStatsPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageUsersPageRoutingModule { }
