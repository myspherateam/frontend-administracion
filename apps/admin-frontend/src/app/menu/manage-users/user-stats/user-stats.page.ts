import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { ApiResponse, Group, User, UserStats } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';

@Component({
  selector: 'app-user-stats',
  templateUrl: './user-stats.page.html',
  styleUrls: ['./user-stats.page.scss'],
})
export class UserStatsPageComponent implements OnInit {

  @Input() public user!: User;

  @Input() public username!: string;

  @Input() public group!: Group;

  public statTypeSelection: string;

  public userStats!: UserStats;

  private rankingField = 'points';

  private rankingType = 'group';

  constructor(
    private modalController: ModalController,
    private apiService: ApiService,
  ) {
    this.statTypeSelection = 'usersStats';
  }

  ngOnInit() {
    if (this.showRankingInfo()) {
      this.apiService.getUserStats(this.user.id, this.rankingField, this.rankingType).subscribe({
        next: (result: ApiResponse<UserStats>) => {
          if (result.status === 200) {
            this.userStats = result.message;
          }
        },
      });
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

  /**
   * Get number of relatives
   * @param user user instance
   */
  getRelatives(user: User): number {
    if (user.parentsUsers) {
      return user.parentsUsers.length;
    } else {
      return user.childsUsers.length;
    }
  }

  /**
   * Get total number of challenges assigned
   */
  getNumberAssignedChallenges(): number {
    const numAssignedChallenges = this.user.numAssignedChallenges;
    if (numAssignedChallenges) {
      return numAssignedChallenges.authority +
        numAssignedChallenges.gps +
        numAssignedChallenges.news +
        numAssignedChallenges.physical +
        numAssignedChallenges.questionnaire;
    }
    return 0;
  }

  /**
   * Get total number of challenges completed
   */
  getNumberCompletedChallenges(): number {
    const numCompletedChallenges = this.user.numCompletedChallenges;
    if (numCompletedChallenges) {
      return numCompletedChallenges.authority +
        numCompletedChallenges.gps +
        numCompletedChallenges.news +
        numCompletedChallenges.physical +
        numCompletedChallenges.questionnaire;
    }
    return 0;
  }

  /**
   * Get number of challenges not completed
   */
  getNumberOfNotCompletedChallenges(): number {
    return this.getNumberAssignedChallenges() - this.getNumberCompletedChallenges();
  }

  /**
   * Show or hide ranking column if user is a parent or user has no group assigned
   */
  public showRankingInfo() {
    return !this.user.roles.includes('Parent') && this.group;
  }
}
