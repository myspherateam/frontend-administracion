import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { UserStatsPageComponent } from './user-stats.page';
import { UserStatsPageRoutingModule } from './user-stats-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserStatsPageRoutingModule,
    TranslateModule,
  ],
  declarations: [UserStatsPageComponent],
})
export class UserStatsPageModule { }
