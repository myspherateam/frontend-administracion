import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { UserStatsPageComponent } from './user-stats.page';

const routes: Routes = [
  {
    path: '',
    component: UserStatsPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserStatsPageRoutingModule { }
