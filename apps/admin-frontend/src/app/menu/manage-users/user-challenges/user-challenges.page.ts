import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { ChallengeCompletion, ChallengeLockdownTypes } from '@paido/data-model';

@Component({
  selector: 'app-user-challenges',
  templateUrl: './user-challenges.page.html',
  styleUrls: ['./user-challenges.page.scss'],
})
export class UserChallengesPageComponent implements OnInit {

  @Input() public challenges: ChallengeCompletion[] = [];

  public challengesFiltered!: ChallengeCompletion[];

  constructor(private modalController: ModalController) { }

  ngOnInit() {
    this.challengesFiltered = this.challenges;
  }

  closeModal() {
    this.modalController.dismiss();
  }

  onLockDownChanged(event: CustomEvent) {
    const lockDownMode = event.detail.value;

    this.challengesFiltered = this.challenges.filter(challenge => {
      return lockDownMode === ChallengeLockdownTypes.ALL ? true :
        (lockDownMode === ChallengeLockdownTypes.COMPATIBLE ? challenge.lockDown === true : challenge.lockDown === false);
    });
  }
}
