import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { UserChallengesPageComponent } from './user-challenges.page';
import { UserChallengesPageRoutingModule } from './user-challenges-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    UserChallengesPageRoutingModule,
  ],
  declarations: [UserChallengesPageComponent],
})
export class UserChallengesPageModule { }
