import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { UserDetailsComponent } from './user-details.page';

const routes: Routes = [
  {
    path: '',
    component: UserDetailsComponent,
  },
  {
    path: 'user-challenges',
    loadChildren: () => import('../user-challenges/user-challenges.module').then(m => m.UserChallengesPageModule),
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserDetailsPageRoutingModule { }
