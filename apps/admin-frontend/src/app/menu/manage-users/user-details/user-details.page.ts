import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, ChallengeCompletion, User, UserRole, getUserIdentifier, userIdsToString } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { EditUserPageComponent } from '../edit-user/edit-user.page';
import { UserChallengesPageComponent } from '../user-challenges/user-challenges.page';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.page.html',
  styleUrls: ['./user-details.page.scss'],
})
export class UserDetailsComponent implements OnInit {

  @Input() public userId!: string;

  @Input() public groupId!: string;

  public user!: User;

  public challenges: ChallengeCompletion[] = [];

  constructor(
    private apiService: ApiService,
    private modalController: ModalController,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.loadUserAndChallenges();
  }

  private loadUserAndChallenges() {
    this.apiService.getUserDetail(this.userId).subscribe({
      next: (result: ApiResponse<User>) => {
        if (result.status === 200) {
          this.user = result.message;
          if (this.user.roles.includes(UserRole.Child) || this.user.roles.includes(UserRole.Parent)) {
            this.getChallenges();
          }
        }
      },
    });
  }

  private getChallenges() {
    this.apiService.getUserChallenges(this.user.id).subscribe({
      next: (result: ApiResponse<ChallengeCompletion[]>) => {
        if (result.status === 200) {
          this.challenges = result.message;
        }
      },
    });
  }

  getDisplayName(): string {
    if (this.user.roles.includes('Child') || this.user.roles.includes('Parent')) {
      return getUserIdentifier(this.user);
    } else {
      return this.user.email;
    }
  }

  getRole(): string {
    let role = '';

    this.user.roles.forEach((value: string) => {
      switch (value) {
        case UserRole.LocalAdmin:
          role = this.translate.instant('ROL.LOCAL_ADMIN');
          break;
        case UserRole.Parent:
          role = this.translate.instant('ROL.PARENT');
          break;
        case UserRole.Teacher:
          role = this.translate.instant('ROL.TEACHER');
          break;
        case UserRole.Clinician:
          role = this.translate.instant('ROL.CLINICIAN');
          break;
        case UserRole.Child:
          role = this.translate.instant('ROL.CHILD');
          break;
      }
    });

    return role;
  }

  closeModal() {
    this.modalController.dismiss();
  }

  editUser() {
    this.showEdit();
  }

  async showEdit() {
    const modal = await this.modalController.create({
      component: EditUserPageComponent,
      componentProps: {
        userId: this.userId,
        groupId: this.groupId,
      },
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });

    modal.onDidDismiss().then(() => {
      this.loadUserAndChallenges();
    });

    return modal.present();
  }

  async showChallengesList() {
    const modal = await this.modalController.create({
      component: UserChallengesPageComponent,
      componentProps: {
        challenges: this.challenges,
      },
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });

    return modal.present();
  }

  /**
   * Get child's parents
   */
  public getParents(): string {
    return userIdsToString(this.user.parentsUsers);
  }

  /**
   * Get parent's children
   */
  public getChildren(): string {
    return userIdsToString(this.user.childsUsers);
  }

}
