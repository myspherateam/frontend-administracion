import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { EditUserPageModule } from '../edit-user/edit-user.module';
import { UserChallengesPageModule } from '../user-challenges/user-challenges.module';
import { UserDetailsComponent } from './user-details.page';
import { UserDetailsPageRoutingModule } from './user-details-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    UserDetailsPageRoutingModule,
    EditUserPageModule,
    UserChallengesPageModule,
  ],
  declarations: [UserDetailsComponent],
})
export class UserDetailsPageModule { }
