import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { User, UserRole } from '@paido/data-model';
import { startWith } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPageComponent implements OnInit {

  public menuOptions: { name: string, url: string, icon: string }[] = [];

  constructor(
    private router: Router,
    private translate: TranslateService,
    private activated: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.activated.data.subscribe({
      next: (data: { user: User }) => {
        this.translate.onLangChange.pipe(
          startWith(this.translate.getDefaultLang),
        ).subscribe(() => {
          this.createMenu(data.user?.roles || []);
        });
      },
    });
  }

  createMenu(roles: string[]) {
    if (roles.includes(UserRole.GlobalAdmin)) {
      this.menuOptions = [
        {
          name: this.translate.instant('MENU.MANAGE_USERS'),
          url: '/menu/manage-users',
          icon: 'person',
        },
        {
          name: this.translate.instant('MENU.STATS'),
          url: '/menu/stats',
          icon: 'stats-chart',
        },
      ];
    } else if (roles.includes(UserRole.LocalAdmin)) {
      this.menuOptions = [
        {
          name: this.translate.instant('MENU.MANAGE_USERS'),
          url: '/menu/manage-users',
          icon: 'person',
        },
      ];
    } else if (roles.includes(UserRole.Teacher) || roles.includes(UserRole.Clinician)) {
      this.menuOptions = [
        {
          name: this.translate.instant('MENU.CHALLENGES.MANAGE_CHALLENGES'),
          url: '/menu/challenges-menu',
          icon: 'list',
        },
        {
          name: this.translate.instant('MENU.MANAGE_GROUPS'),
          url: '/menu/manage-groups',
          icon: 'people',
        },
        {
          name: this.translate.instant('MENU.STATS'),
          url: '/menu/entity-stats',
          icon: 'stats-chart',
        },
      ];
    }
  }

  goToUrl(url: string) {
    this.router.navigate([url]);
  }
}
