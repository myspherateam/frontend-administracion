import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../services/guards/auth.guard';
import { EntityResolver } from './entity.resolver';
import { MenuPageComponent } from './menu.page';
import { RoleGuard } from '../services/guards/role.guard';
import { UserResolver } from './user.resolver';

const routes: Routes = [
  {
    path: '',
    component: MenuPageComponent,
    resolve: {
      user: UserResolver,
    },
  },
  {
    path: 'manage-groups',
    loadChildren: () => import('./manage-groups/manage-groups.module').then(m => m.ManageGroupsPageModule),
    canActivate: [AuthGuard, RoleGuard],
    resolve: {
      entity: EntityResolver,
    },
  },

  {
    path: 'manage-users',
    loadChildren: () => import('./manage-users/manage-users.module').then(m => m.ManageUsersPageModule),
    canActivate: [AuthGuard, RoleGuard],
  },
  {
    path: 'stats',
    loadChildren: () => import('./stats/stats.module').then(m => m.StatsPageModule),
    canActivate: [AuthGuard, RoleGuard],
  },
  {
    path: 'challenges-menu',
    loadChildren: () => import('./challenges/challenges-menu.module').then(m => m.ChallengesMenuPageModule),
    canActivate: [AuthGuard, RoleGuard],
  },
  {
    path: 'entity-stats',
    loadChildren: () => import('./entity-stats/entity-stats.module').then(m => m.EntityStatsPageModule),
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule { }
