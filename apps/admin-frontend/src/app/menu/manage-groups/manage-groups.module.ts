import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AddGroupModalPageModule } from './add-group-modal/add-group-modal.module';
import { ManageGroupsPageComponent } from './manage-groups.page';
import { ManageGroupsPageRoutingModule } from './manage-groups-routing.module';
import { ModifyGroupComponent } from './modify-group/modify-group.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TranslateModule.forChild(),
        ManageGroupsPageRoutingModule,
        AddGroupModalPageModule,
        ReactiveFormsModule,
    ],
    declarations: [
        ManageGroupsPageComponent,
        ModifyGroupComponent,
    ],
})
export class ManageGroupsPageModule { }
