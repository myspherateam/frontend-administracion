import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { finalize, firstValueFrom } from 'rxjs';
import { utils, writeFile } from 'xlsx';
import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, ChallengeCompletion, Entity, Group, User } from '@paido/data-model';

import { AddGroupModalPageComponent } from './add-group-modal/add-group-modal.page';
import { ApiService } from '../../api/api-service';
import { PresentatorService } from '../../services/common/presentator.service';
import { Toast } from '../../services/common/dto/toast';

type Callback = () => void;

@Component({
  selector: 'app-manage-groups',
  templateUrl: './manage-groups.page.html',
  styleUrls: ['./manage-groups.page.scss'],
})
export class ManageGroupsPageComponent implements OnInit {
  public groups: Group[] = [];

  public loadingGroups: boolean = false;

  public allGroups: Group[] = [];

  private entityId: string;

  private entityName: string;

  public buildingReportFor?: string;

  constructor(
    private modalController: ModalController,
    private apiService: ApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private alertController: AlertController,
    private presentatorService: PresentatorService,
    private translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe({
      next: (data: { entity: Entity }) => {
        this.entityId = data.entity.id;
        this.entityName = data.entity.name;
        this.getGroups();
      },
    });
  }

  getGroups() {
    this.loadingGroups = true;
    this.apiService.getGroups([this.entityId]).pipe(
      finalize(() => this.loadingGroups = false),
    )
      .subscribe({
        next: (result: ApiResponse<Group[]>) => {
          if (result.status === 200) {
            this.groups = result.message;
            this.allGroups = this.groups;
          }
        },
        error: (error) => console.error(error),
      });
  }

  onSearch(event: Event) {
    const value = (event as CustomEvent).detail.value;
    if (value) {
      if (value.trim !== '') {
        this.groups = this.allGroups.filter((term) => {
          return (
            term.name.toLowerCase().indexOf(value.trim().toLowerCase()) > -1
          );
        });
      }
    } else {
      this.groups = this.allGroups;
    }
  }

  async addGroup() {
    const modal = await this.modalController.create({
      component: AddGroupModalPageComponent,
      componentProps: {
        entityId: this.entityId,
        entityName: this.entityName,
      },
      backdropDismiss: false,
    });

    modal.onDidDismiss().then((createdGroups) => {
      if (createdGroups !== null) {
        this.groups = this.groups.concat(createdGroups.data);
      }
    });

    return modal.present();
  }

  showGroupDetail(group?: Group) {
    this.router.navigateByUrl('menu/manage-groups/group-details', {
      state: {
        groupId: group ? group.id : undefined,
        group: group ? group : undefined,
      },
    });
  }

  onRemoveClick(index: number) {
    this.presentRemoveGroupAlert(this.groups[index].name, () => {
      this.apiService.removeGroup(this.groups[index].id).subscribe({
        next: (result: ApiResponse<void>) => {
          if (result.status === 200) {
            this.groups.splice(index, 1);
            this.presentatorService.showToast(
              Toast.message(
                this.translateService.instant(
                  'GROUPS.DELETE_GROUP_MODAL.ACTION_DONE',
                ),
              ),
            );
          }
        },
        error: (result: HttpErrorResponse) => {
          if (
            result.error.message &&
            // TODO: add translation
            result.error.message === 'The group must be empty'
          ) {
            this.presentatorService.showToast(
              Toast.message(
                this.translateService.instant('GROUPS.GROUP_MUST_BE_EMPTY'),
              ),
            );
          }
        },
      });
    });
  }

  async presentRemoveGroupAlert(groupName: string, callback: Callback) {
    const alert = await this.alertController.create({
      header: this.translateService.instant('GROUPS.DELETE_GROUP_MODAL.TITLE', {
        val: groupName,
      }),
      message: this.translateService.instant(
        'GROUPS.DELETE_GROUP_MODAL.SUBTITLE',
      ),
      buttons: [
        {
          text: this.translateService.instant('ACTION.CANCEL'),
          role: 'cancel',
        },
        {
          text: this.translateService.instant('ACTION.CONFIRM'),
          handler: () => {
            callback();
          },
        },
      ],
    });

    await alert.present();
  }

  /**
   * Go to group stats page
   */
  onStatsClick(group: Group) {
    this.router.navigateByUrl('menu/manage-groups/group-stats', {
      state: {
        group,
      },
    });
  }

  /**
   * Method that generates an Excel report for a specific group
   * @param group 
   */
  async onExportClick(group: Group) {
    console.log('exporting excel');
    try {
      this.buildingReportFor = group.id;
      this.presentatorService.showToast(Toast.message('GROUPS.DOWNLOAD.STARTED', { group: group.name }, 'success'));
      const wb = utils.book_new();
      const children: User[] = (
        await firstValueFrom(this.apiService.getUsers([group.entityId], [group.id], undefined, ['Child']))).message;

      // Export children data
      const childrenData = children.map((child: User) => {
        console.log(child);

        return {
          Id: child.id,
          'Display Id': child.displayId,
          'Display Name': child.displayName,
          points: child.points,
          potentialPoints: child.potentialPoints,

          numPhysicalAssignedChallenges: child.numAssignedChallenges?.physical,
          numAuthorityAssignedChallenges: child.numAssignedChallenges?.authority,
          numGPSAssignedChallenges: child.numAssignedChallenges?.gps,
          numQuestinnaireAssignedChallenges:
            child.numAssignedChallenges?.questionnaire,
          numNewsAssignedChallenges: child.numAssignedChallenges?.news,

          numPhysicalCompletedChallenges: child.numCompletedChallenges?.physical,
          numAuthorityCompletedChallenges: child.numCompletedChallenges?.authority,
          numGPSCompletedChallenges: child.numCompletedChallenges?.gps,
          numQuestinnaireCompletedChallenges:
            child.numCompletedChallenges?.questionnaire,
          numNewsCompletedChallenges: child.numCompletedChallenges?.news,
        };
      });
      console.log(childrenData);

      const ws = utils.json_to_sheet(childrenData);
      utils.book_append_sheet(wb, ws, 'Children');

      // Obtain all challenges assigned to the children in group
      // each challenge group contains all challenges with the same definition
      const challengeGroups: { [key: string]: ChallengeCompletion[] } = {};

      // used a for loop to allow async operations
      for (const child of children) {
        const childChallenges: ChallengeCompletion[] = (await firstValueFrom(this.apiService.getUserChallenges(child.id))).message;
        childChallenges.forEach((challenge: ChallengeCompletion) => {
          if (challengeGroups[challenge.definitionId] === undefined) {
            challengeGroups[challenge.definitionId] = [];
          }
          challengeGroups[challenge.definitionId].push(challenge);
        });
      }

      for (const challengeGroup in challengeGroups) {
        const example = challengeGroups[challengeGroup][0];
        const type = example.type;

        const data = [];
        for (const challenge of challengeGroups[challengeGroup]) {
          let toPush: Record<string, unknown> = {
            Id: challenge.id,
            'User Id': challenge.forId,
            'Completed by child': challenge.completed,
            'Completion date child': challenge.completionDatetime,
            'Completed by parents':
              (challenge.completedByParents[0] &&
                challenge.completedByParents[0].completionDatetime) !== null,
            'Completion date parents':
              challenge.completedByParents[0] &&
              challenge.completedByParents[0].completionDatetime,
          };

          switch (type) {
            case 'physical':
              toPush = { ...toPush, Steps: challenge.userSteps };
              break;
            case 'authority':
              toPush = { ...toPush, 'Approved by': challenge.approvedById };
              break;
            case 'gps':
              break;
            case 'questionnaire': {
              const questionnaire = JSON.parse(challenge.questionnaire as string);
              let answers;
              if (challenge.questionnaireAnswer === undefined) {
                answers = {};
              } else {
                answers = JSON.parse(challenge.questionnaireAnswer);
              }

              for (const page of questionnaire.pages) {
                for (const element of page.elements) {
                  const name = element.name;
                  let answer = answers && answers[name];
                  if (answer && answer.join) {
                    answer = answer.join(', ');
                  }
                  toPush[name] = answer || '';
                }
              }
              break;
            }
            case 'news': {
              let questionnaire;
              if (challenge.questionnaire === undefined) {
                questionnaire = { pages: [] };
              } else {
                questionnaire = JSON.parse(challenge.questionnaire);
                try {
                  questionnaire = JSON.parse(questionnaire);
                } catch (error) {
                  // TODO: what do we do here?
                }
              }
              let answers;
              if (challenge.newsAnswer === undefined) {
                answers = {};
              } else {
                answers = JSON.parse(challenge.newsAnswer);
              }

              for (const page of questionnaire.pages) {
                for (const element of page.elements) {
                  const name = element.name;
                  let answer = answers && answers[name];
                  if (answer && answer.join) {
                    answer = answer.join(', ');
                  }
                  toPush[name] = answer || '';
                }
              }
              break;
            }
          }

          data.push(toPush);
        }
        const ws_data = utils.json_to_sheet(data);
        const origTitle = example.title
          .replace(/\\|\/|\?|\*|\[|\]/g, '')
          .substring(0, 25);
        let title = origTitle;
        let added = false;
        let i = 1;
        while (!added) {
          try {
            utils.book_append_sheet(wb, ws_data, title);
            added = true;
          } catch (error) {
            // TODO: what do we do here?
          }
          title = `${origTitle}-${i}`;
          i++;
        }
      }

      const date = new Date().toJSON().substring(0, 10);
      writeFile(wb, `Export-${group.name}-${date}.xlsx`);

    } catch (error) {
      console.log(error);
      // failed to create the excel report
      const toast: Toast = Toast.message('GROUPS.DOWNLOAD.FAILED', undefined, 'danger');

      this.presentatorService.showToast(toast);
    } finally {
      this.buildingReportFor = undefined;
    }
  }
}
