import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiResponse, Group, GroupStats } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';

@Component({
  selector: 'app-group-stats',
  templateUrl: './group-stats.page.html',
  styleUrls: ['./group-stats.page.scss'],
})
export class GroupStatsPageComponent implements OnInit {

  public group?: Group;

  public statTypeSelection: string;

  public groupStats?: GroupStats;

  constructor(
    private router: Router,
    private apiService: ApiService,
  ) {
    this.statTypeSelection = 'usersStats';
    if (this.router.getCurrentNavigation()?.extras.state !== undefined) {
      this.group = (this.router.getCurrentNavigation()?.extras.state as { group?: Group }).group;
    }
  }

  ngOnInit() {
    if (this.group) {
      this.apiService.getGroupStats(this.group.id).subscribe({
        next: (result: ApiResponse<GroupStats>) => {
          if (result.status === 200) {
            this.groupStats = result.message;
          }
        },
        error: (error: Error) => {
          console.error(error);
        },
      });
    }
  }

  /**
   * Get users quantity
   */
  getUsersNumber() {
    return (this.groupStats?.numChildren ? this.groupStats.numChildren : 0) +
      (this.groupStats?.numParents ? this.groupStats.numParents : 0);
  }

  /**
   * Get total number of challenges assigned
   */
  getNumberAssignedChallenges(): number {
    const numAssignedChallenges = this.groupStats?.numAssignedChallenges;
    return (numAssignedChallenges) ? numAssignedChallenges.authority +
      numAssignedChallenges.gps +
      numAssignedChallenges.news +
      numAssignedChallenges.physical +
      numAssignedChallenges.questionnaire : 0;
  }

  /**
   * Get total number of challenges completed
   */
  getNumberCompletedChallenges(): number {
    const numCompletedChallenges = this.groupStats?.numCompletedChallenges;
    return (numCompletedChallenges) ? numCompletedChallenges.authority +
      numCompletedChallenges.gps +
      numCompletedChallenges.news +
      numCompletedChallenges.physical +
      numCompletedChallenges.questionnaire : 0;
  }

  /**
   * Get number of challenges not completed
   */
  getNumberOfNotCompletedChallenges(): number {
    return this.getNumberAssignedChallenges() - this.getNumberCompletedChallenges();
  }

}
