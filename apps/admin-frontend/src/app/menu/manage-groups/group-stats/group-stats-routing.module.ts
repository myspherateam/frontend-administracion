import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { GroupStatsPageComponent } from './group-stats.page';

const routes: Routes = [
  {
    path: '',
    component: GroupStatsPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupStatsPageRoutingModule { }
