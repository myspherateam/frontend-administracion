import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { GroupStatsPageComponent } from './group-stats.page';
import { GroupStatsPageRoutingModule } from './group-stats-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupStatsPageRoutingModule,
    TranslateModule,
  ],
  declarations: [GroupStatsPageComponent],
})
export class GroupStatsPageModule { }
