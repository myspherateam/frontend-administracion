import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, Group } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { PresentatorService } from '../../../services/common/presentator.service';
import { Toast } from '../../../services/common/dto/toast';

@Component({
  selector: 'app-modify-group',
  templateUrl: './modify-group.component.html',
  styleUrls: ['./modify-group.component.scss'],
})
export class ModifyGroupComponent implements OnInit {

  @Input() public group!: Group;

  public modifyGroupForm: UntypedFormGroup;

  constructor(
    private modalController: ModalController,
    private formBuilder: UntypedFormBuilder,
    private presentatorService: PresentatorService,
    private apiService: ApiService,
    private translateService: TranslateService,
  ) {

  }

  ngOnInit(): void {
    this.modifyGroupForm = this.formBuilder.group({
      name: [this.group.name, Validators.required],
    });
  }

  modifyGroup() {
    const newName = this.modifyGroupForm.get('name')?.value;
    this.presentatorService.createYesNoAlert(
      this.translateService.instant('GROUPS.MODIFY_GROUP_MODAL.TITLE'),
      this.translateService.instant('GROUPS.MODIFY_GROUP_MODAL.SUBTITLE', { group: this.group.name, name: newName }),
    ).then((accepted: boolean) => {
      if (accepted) {
        this.group.name = newName;
        this.apiService.modifyGroup(this.group).subscribe({
          next: (result: ApiResponse<Group>) => {
            if (result.status === 200) {
              this.modalController.dismiss(this.group);
              this.presentatorService.showToast(
                Toast.message(
                  this.translateService.instant('GROUPS.MODIFY_GROUP_MODAL.ACTION_DONE'),
                ),
              );
            }
          },
        });
      }
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }

}
