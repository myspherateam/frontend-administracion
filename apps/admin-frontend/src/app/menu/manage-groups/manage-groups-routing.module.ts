import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ManageGroupsPageComponent } from './manage-groups.page';

const routes: Routes = [
  {
    path: '',
    component: ManageGroupsPageComponent,
  },
  {
    path: 'group-details',
    loadChildren: () => import('./group-details/group-details.module').then(m => m.GroupDetailsPageModule),
  },
  {
    path: 'group-stats',
    loadChildren: () => import('./group-stats/group-stats.module').then(m => m.GroupStatsPageModule),
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageGroupsPageRoutingModule { }
