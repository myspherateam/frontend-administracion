import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AddGroupModalPageComponent } from './add-group-modal.page';

const routes: Routes = [
  {
    path: '',
    component: AddGroupModalPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddGroupModalPageRoutingModule { }
