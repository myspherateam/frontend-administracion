import { Component, OnInit } from '@angular/core';
import { FormControl, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { AngularFireAnalytics } from '@angular/fire/compat/analytics';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, Group } from '@paido/data-model';

import { ApiService } from '../../../api/api-service';
import { PresentatorService } from '../../../services/common/presentator.service';
import { Toast } from '../../../services/common/dto/toast';

@Component({
  selector: 'app-add-group-modal',
  templateUrl: './add-group-modal.page.html',
  styleUrls: ['./add-group-modal.page.scss'],
})
export class AddGroupModalPageComponent implements OnInit {

  public addGroup!: UntypedFormGroup;

  public createdGroups: Group[] = [];

  public entityId?: string;

  public entityName: string;

  constructor(
    private modalController: ModalController,
    private formBuilder: UntypedFormBuilder,
    private presentatorService: PresentatorService,
    private apiService: ApiService,
    private analytics: AngularFireAnalytics,
    private traslate: TranslateService,
  ) { }

  ngOnInit(): void {
    this.addGroup = this.formBuilder.group({
      name: new FormControl<string>('', [Validators.required]),
    });

    // Validate that the trimmed name has at least 3 chars
    this.addGroup.get('name').valueChanges.subscribe({
      next: (value: string) => {
        if (value.trim().length < 3) this.nameField.setErrors({ min: true });
      },
    });
  }

  closeAddGroupModal() {
    this.modalController.dismiss(this.createdGroups);
  }

  get nameField() {
    return this.addGroup.get('name');
  }

  newGroup() {
    const groupName = this.addGroup.value.name.trim();
    if (groupName.length < 4) {
      this.presentatorService.showToast(Toast.message(this.traslate.instant('ERRORS.NAME_MUST_BE_VALID')));
      this.addGroup.setErrors({ 'invalid-name': true });
    } else {
      this.addGroup.setErrors({ 'invalid-name': false });
      const newGroup = {} as Group;
      newGroup.name = groupName;
      newGroup.entityId = this.entityId || '';

      this.apiService.createGroup(newGroup).subscribe({
        next: (result: ApiResponse<Group>) => {
          if (result.status === 200) {
            this.analytics.logEvent('new_group', { id: result.message.id });
            const group = result.message;
            this.createdGroups.push(group);
            this.presentatorService.showToast(Toast.message(this.traslate.instant('GROUPS.ADD_GROUP_SUCCESS', { val: groupName })));
            this.addGroup.get('name')?.setValue(null);
          }
        },
        error: error => console.error(error),
      });
    }
  }
}
