import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { AddGroupModalPageComponent } from './add-group-modal.page';
import { AddGroupModalPageRoutingModule } from './add-group-modal-routing.module';

@NgModule({
  imports: [
    AddGroupModalPageRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    IonicModule,
  ],
  declarations: [AddGroupModalPageComponent],
})
export class AddGroupModalPageModule { }
