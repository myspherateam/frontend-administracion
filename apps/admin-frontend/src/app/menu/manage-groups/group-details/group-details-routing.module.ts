import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EntityResolver } from '../../entity.resolver';
import { GroupDetailsPageComponent } from './group-details.page';

const routes: Routes = [
  {
    path: '',
    component: GroupDetailsPageComponent,
    resolve: {
      entity: EntityResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupDetailsPageRoutingModule { }
