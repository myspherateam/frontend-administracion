import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import {
  ApiResponse, ChallengeCompletion, Entity, Group, RegisterUserState, User,
  UserRole, getUserIdentifier, userIdsToString,
} from '@paido/data-model';

import { AddUserPageComponent } from '../../manage-users/add-user/add-user.page';
import { ApiService } from '../../../api/api-service';
import { EditUserPageComponent } from '../../manage-users/edit-user/edit-user.page';
import { ModifyGroupComponent } from '../modify-group/modify-group.component';
import { MoveUserToGroupComponent } from '../../manage-users/move-user-to-group/move-user-to-group.component';
import { PresentatorService } from '../../../services/common/presentator.service';
import { Toast } from '../../../services/common/dto/toast';
import { UserChallengesPageComponent } from '../../manage-users/user-challenges/user-challenges.page';
import { UserStatsPageComponent } from '../../manage-users/user-stats/user-stats.page';

interface GroupDetailsState {
  groupId?: string; group?: Group;
}

@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.page.html',
  styleUrls: ['./group-details.page.scss'],
})
export class GroupDetailsPageComponent implements OnInit {
  public users: User[] = [];

  public allUsers: User[] = [];

  private entityId!: string;

  private groupId?: string;

  public group?: Group;

  constructor(
    private apiService: ApiService,
    private router: Router,
    private modalController: ModalController,
    private presentatorService: PresentatorService,
    private translate: TranslateService,
    private activated: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activated.data.subscribe({
      next: (data: { entity: Entity }) => {
        if (data.entity?.id) {
          this.entityId = data.entity.id;
        }
        if (this.router.getCurrentNavigation()?.extras?.state) {
          const state: GroupDetailsState = this.router.getCurrentNavigation().extras.state as GroupDetailsState;
          this.groupId = state.groupId;
          this.group = state.group;
        }
        this.getUsers();
      },
    });
  }

  getUsers() {
    const entityIds: string[] = [];
    if (this.entityId) { entityIds.push(this.entityId); }
    const groupIds: string[] = [];
    if (this.groupId) { groupIds.push(this.groupId); }

    this.apiService.getUsers(entityIds, groupIds, undefined, [UserRole.Child, UserRole.Parent]).subscribe({
      next: (result: ApiResponse<User[]>) => {
        if (result.status === 200) {
          this.users = [];
          this.users.push(...result.message);
          this.allUsers = this.users;
        }
      },
      error: error => console.error(error),
    });
  }

  createRegistrationCode(user: User) {
    this.apiService.getNewRegistrationCodeFor(user).subscribe({
      next: (result: ApiResponse<User>) => {
        if (result.status === 200) {
          this.getUsers();
          const newRegistrationCode = result.message.registrationCode;
          this.presentatorService.showToast(Toast.message('USER.NEW_REG_CODE_SUCCESS', { val: newRegistrationCode }));
        }
      },
      error: error => console.error(error),
    });
  }

  /**
   * Get username
   */
  getUsername(user: User): string {
    return getUserIdentifier(user);
  }

  /**
   * Get user role
   */
  getUserRole(user: User): string {
    return user.roles.includes('Child') ? this.translate.instant('ROL.CHILD') :
      (user.roles.includes('Parent') ? this.translate.instant('ROL.PARENT') : this.translate.instant('USER'));
  }

  /**
   * Get user relatives
   */
  getRelatives(user: User): string {
    if (user.parentsUsers) {
      return this.getParents(user);
    } else {
      return this.getChildren(user);
    }
  }

  /**
   * If user is a child, get the parents
   */
  public getParents(user: User): string {
    return userIdsToString(user.parentsUsers);
  }

  /**
   * If user is a parent, get the children
   */
  public getChildren(user: User): string {
    return userIdsToString(user.childsUsers);
  }

  /**
   * Get user challenges to show them in a modal
   * @param user user selected
   */
  getUserChallenges(user: User) {
    this.apiService.getUserChallenges(user.id).subscribe({
      next: (result: ApiResponse<ChallengeCompletion[]>) => {
        if (result.status === 200) {
          const challenges = result.message;
          this.showChallengesList(challenges);
        }
      },
    });
  }

  /**
   * Show user challenges list modal
   * @param challenges user challenges
   */
  async showChallengesList(challenges: ChallengeCompletion[]) {
    const modal = await this.modalController.create({
      component: UserChallengesPageComponent,
      componentProps: {
        challenges,
      },
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });
    return modal.present();
  }

  /**
   * Get user register status
   */
  getRegisterStatus(user: User): string {
    switch (user.registerState) {
      case RegisterUserState.RegisteredFromParent:
        return this.translate.instant('USER.REGISTERED_ON_PARENT_MOBILE');
      case RegisterUserState.RegisteredDirectly:
        return this.translate.instant('USER.REGISTERED_ON_MOBILE');
      default:
        return this.translate.instant('USER.PENDING_REGISTRATION');
    }
  }

  onSearch(event: CustomEvent) {
    const value = event.detail.value;
    if (value) {
      if (value.trim !== '') {
        this.users = this.allUsers.filter(
          user => {
            const valueId = user.displayId + ' ' +
              (user.displayName ? user.displayName.toLowerCase() : '') + ' ' +
              (user.externalId ? user.externalId.toLowerCase() : '');
            return valueId.indexOf(value.trim().toLowerCase()) > -1;
          });
      }
    } else {
      this.users = this.allUsers;
    }
  }

  async addUser() {
    // go to add user form
    const modal = await this.modalController.create({
      component: AddUserPageComponent,
      componentProps: {
        groupId: this.groupId,
      },
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });

    modal.onDidDismiss().then(() => {
      this.getUsers();
    });

    return modal.present();
  }

  /**
   * Show a pop up with the group and an option to edit it
   */
  async modifyGroup() {
    const modal = await this.modalController.create({
      component: ModifyGroupComponent,
      componentProps: {
        group: this.group,
      },
      backdropDismiss: false,
      cssClass: 'modal-move-user',
    });
    modal.onDidDismiss().then(
      (result) => {
        if (result.data) {
          this.group = result.data;
        }
      },
    );
    return modal.present();
  }

  /**
   * Remove the user selected from the group
   * @param user object selected
   * @param index index of the object selected in users array
   */
  removeUserFromGroup(user: User, _index: number) {
    this.presentatorService.createYesNoAlert(
      this.translate.instant('GROUPS.DELETE_USER_FROM_GROUP_MODAL.TITLE', { val: getUserIdentifier(user) }),
      this.translate.instant('GROUPS.DELETE_USER_FROM_GROUP_MODAL.SUBTITLE'),
    ).then((accepted: boolean) => {
      if (accepted) {
        // remove group from user's groups array
        const groupIndex = user.groupIds.indexOf((this.group as Group).id);
        user.groupIds.splice(groupIndex, 1);
        this.apiService.createUser(user).subscribe({
          next: (result: ApiResponse<User>) => {
            if (result.status === 200) {
              this.getUsers();
              this.presentatorService.showToast(
                Toast.message(
                  this.translate.instant('GROUPS.DELETE_USER_FROM_GROUP_MODAL.ACTION_DONE'),
                ),
              );
            }
          },
        });
      }
    });
  }

  /**
   * Edit a user from the list
   * @param user selected user
   */
  async editUser(user: User) {
    const modal = await this.modalController.create({
      component: EditUserPageComponent,
      componentProps: {
        userId: user.id,
        groupId: this.groupId,
      },
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });

    modal.onDidDismiss().then(() => {
      this.getUsers();
    });

    return modal.present();
  }

  /**
   * Show modal to assign user to a group
   * @param user selected user
   */
  async assignUserToGroup(user: User) {
    const modal = await this.modalController.create({
      component: MoveUserToGroupComponent,
      componentProps: {
        user,
        username: this.getUsername(user),
      },
      backdropDismiss: false,
      cssClass: 'modal-move-user',
    });

    modal.onDidDismiss().then((result) => {
      const reload: boolean = result.data;
      if (reload) {
        this.getUsers();
      }
    });

    return modal.present();
  }

  /**
   * Go to user stats page
   */
  async onUserStatsClick(user: User) {
    const modal = await this.modalController.create({
      component: UserStatsPageComponent,
      componentProps: {
        user,
        username: this.getUsername(user),
        group: this.group,
      },
      backdropDismiss: false,
      cssClass: 'modal-fullscreen',
    });

    return modal.present();
  }
}
