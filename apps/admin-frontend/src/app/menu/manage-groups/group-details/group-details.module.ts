import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AddUserPageModule } from '../../manage-users/add-user/add-user.module';
import { GroupDetailsPageComponent } from './group-details.page';
import { GroupDetailsPageRoutingModule } from './group-details-routing.module';
import { MoveUserToGroupComponent } from '../../manage-users/move-user-to-group/move-user-to-group.component';
import { UserDetailsPageModule } from '../../manage-users/user-details/user-details.module';
import { UserStatsPageModule } from '../../manage-users/user-stats/user-stats.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        TranslateModule.forChild(),
        GroupDetailsPageRoutingModule,
        AddUserPageModule,
        UserDetailsPageModule,
        IonicSelectableModule,
        UserStatsPageModule,
    ],
    declarations: [
        GroupDetailsPageComponent,
        MoveUserToGroupComponent,
    ],
})
export class GroupDetailsPageModule { }
