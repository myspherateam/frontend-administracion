import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

import { User } from '@paido/data-model';

import { LocalStorageService } from '../services/storage-service';

@Injectable({
  providedIn: 'root',
})
/**
 * Resolver que inyecta en la ruta activada el objeto User guardado en localstorage
 */
export class UserResolver implements Resolve<User> {
  constructor(
    private storage: LocalStorageService,
  ) { }

  resolve(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Observable<User | undefined> {
    return of(this.storage.getFromLocalStorage<User>('user'));
  }
}
