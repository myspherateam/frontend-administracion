import { AngularFireAnalyticsModule, ScreenTrackingService } from '@angular/fire/compat/analytics';
import { HTTP_INTERCEPTORS, HttpBackend, HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { AngularFireModule } from '@angular/fire/compat';
import { BrowserModule } from '@angular/platform-browser';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
import { RouteReuseStrategy } from '@angular/router';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './services/guards/auth.guard';
import { AuthenticatorInterceptor } from './api/authenticator-interceptor';
import { EntityGuard } from './services/guards/entity.guard';
import { LocalStorageService } from './services/storage-service';
import { ModalInfoComponent } from './menu/challenges/common/modal-info/modal-info.component';
import { PresentatorService } from './services/common/presentator.service';
import { RoleGuard } from './services/guards/role.guard';
import { environment } from '../environments/environment';

@Injectable({ providedIn: 'root' })
export class CustomHttpClient extends HttpClient {
  constructor(handler: HttpBackend) {
    super(handler);
  }
}

export function jwtOptionsFactory(storage: StorageService) {
  return {
    tokenGetter: () => {
      return storage.get('access_token');
    },
  };
}

export function newTranslateLoader(http: CustomHttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent, ModalInfoComponent],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAnalyticsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [LOCAL_STORAGE],
      },
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (newTranslateLoader),
        deps: [CustomHttpClient],
      },
    }),
    Ionic4DatepickerModule,
  ],
  providers: [
    PresentatorService,
    ScreenTrackingService,
    LocalStorageService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: AuthenticatorInterceptor, multi: true },
    AuthGuard,
    RoleGuard,
    EntityGuard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
