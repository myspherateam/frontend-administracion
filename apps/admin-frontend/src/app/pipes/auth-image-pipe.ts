import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ApiService } from '../api/api-service';

@Pipe({
    name: 'authImage',
})
export class AuthImagePipe implements PipeTransform {

    constructor(
        private http: HttpClient,
        private auth: ApiService, // our service that provides us with the authorization token
    ) { }

    async transform(src: string): Promise<string> {
        const headers = this.auth.getAuthHeader();
        const imageBlob = await this.http.get(src, { headers, responseType: 'blob' }).toPromise();
        const reader = new FileReader();
        return new Promise((resolve, _reject) => {
            reader.onloadend = () => resolve(reader.result as string);
            reader.readAsDataURL(imageBlob);
        });
    }

}
