import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({ name: 'safeHtml' })
export class SafeHtml implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) { }

    transform(html: string | null) {
        return (html !== null) ? this.sanitizer.bypassSecurityTrustResourceUrl(html) : html;
    }
}
