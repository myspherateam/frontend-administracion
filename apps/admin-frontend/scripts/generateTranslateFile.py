from googletrans import Translator, constants
import html
import json

translator = Translator()


def translatePhrase(phrase):
    
    try:
        translation = translator.translate(phrase, src='es', dest='ca')
    except:
        translator = Translator()
        translation = translator.translate(phrase, src='es', dest='ca')
    return translation.text

def iterateJson(data):
    for attribute, value in data.items():
       
        if isinstance(value, str): 
            newValue = translatePhrase(value)
            print(value, newValue) # example usage
            data[attribute] = html.unescape(newValue)
        else:
            iterateJson(value)

# Opening JSON file
f = open('../../apps/admin-frontend/src/assets/i18n/es.json')
  
# returns JSON object as 
# a dictionary
data = json.load(f)
cloneData = data  
# Iterating through the json
# list
iterateJson(data)

# Closing file
f.close()

print("resulkt:")
print(data)
 
with open('generatedCat.json', 'w', encoding='utf8') as json_file:
    json.dump(data,  json_file, ensure_ascii=False, indent=4)

 
json_file.close()

