const path = require('path');
const fs = require('fs');
const pckJson = require('../../package.json');

const versionFilePath = path.join(__dirname + '/src/environments/version.ts');

const src = `export const version = '${pckJson.config['admin-frontend'].version}';\n`;

// ensure version module pulls value from package.json
fs.writeFile(versionFilePath, src, { flag: 'w' }, function (err) {
    if (err) {
        return console.error(err);
    }
});
