import { baseEnvironment } from './baseEnvironment';

const baseUrl: string = 'https://www.escalasalut.com';

export const environment = {
  ...baseEnvironment,
  production: true,
  baseUrl,
  authUrl: `${baseUrl}/auth`,
  challengesUrl: `${baseUrl}/challenges`,
  adminUrl: `${baseUrl}/administration`,
};
