const baseUrl: string = 'https://test.escalasalut.mysphera-innov.es';

export const baseEnvironment = {
  production: false,
  googleMapApiKey: 'AIzaSyD8na2rf6R0JcmfGhZRQwthGFpc4szI68U',
  baseUrl: baseUrl,
  authUrl: `${baseUrl}/auth`,
  challengesUrl: `${baseUrl}/challenges`,
  adminUrl: `${baseUrl}/administration`,
  keycloakRealm: 'paido',
};
