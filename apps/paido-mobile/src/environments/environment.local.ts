// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { baseEnvironment } from './baseEnvironment';

const baseUrl: string = 'http://192.168.1.40:4200';

export const environment = {
  ...baseEnvironment,
  baseUrl,
  authUrl: 'https://staging.escalasalut.mysphera-innov.es/auth',
  challengesUrl: 'http://192.168.1.40:8086',
  adminUrl: 'http://192.168.1.40:8085',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error';  // Included with Angular CLI.import { baseEnvironment } from './baseEnvironment';

