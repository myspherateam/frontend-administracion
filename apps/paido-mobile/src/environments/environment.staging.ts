import { baseEnvironment } from './baseEnvironment';

const baseUrl: string = 'https://staging.escalasalut.mysphera-innov.es';

export const environment = {
  ...baseEnvironment,
  production: false,
  baseUrl,
  authUrl: `${baseUrl}/auth`,
  challengesUrl: `${baseUrl}/challenges`,
  adminUrl: `${baseUrl}/administration`,
  keycloakRealm: 'paido',
};
