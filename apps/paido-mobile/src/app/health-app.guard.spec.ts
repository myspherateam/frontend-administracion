import { TestBed } from '@angular/core/testing';

import { HealthAppGuard } from './health-app.guard';

describe('HealthAppGuard', () => {
  let guard: HealthAppGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HealthAppGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
