import { Component, OnDestroy, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

import { AnimalMap, ApiResponse, ColorMap, User, displayNameComponents } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { LocalStorageService } from '../services/storage-service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends AnalyzableComponent implements OnInit, OnDestroy {
  protected screenName: string = 'profile';

  public user!: User;

  public localUser!: User;

  public displayName!: string;

  public displayNumber!: string;

  public points!: number;

  public animalName?: string;

  public animalColor?: string;

  public isAChild: boolean = false;

  constructor(
    private menuController: MenuController,
    private router: Router,
    private localStorage: LocalStorageService,
    private apiService: ApiService,
  ) {
    super();
    this.menuController.enable(false);
    this.localUser = (this.router.getCurrentNavigation()?.extras?.state as { user: User })?.user;
  }

  override async ngOnInit() {
    await super.ngOnInit();
    this.apiService.getMeMobile().subscribe({
      next: (result: ApiResponse<User[]>) => {
        if (result.status === 200) {
          const users = result.message;
          users.forEach((element: User) => {
            if (element.id === this.localUser.id) {
              this.user = element;
            }
          });

          if (this.user) {
            if (this.localStorage.containsKey('childDisplayId')) {
              // user is a parent that enter like a child
              this.getDisplayName(String(this.localStorage.getFromLocalStorage('childDisplayId')));
              this.isAChild = true;
            } else {
              // user is parent or unique child
              this.getDisplayName(this.user.displayName);

              if (this.user.roles.includes('Child')) {
                this.isAChild = true;
              } else {
                this.isAChild = false;
              }
            }
            this.points = this.user.points ? this.user.points : 0;
          }
        }
      },
      error: error => console.error(error),
    });
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
    this.menuController.enable(true);
  }

  getDisplayName(displayName: string) {

    const [animal, color, code] = displayNameComponents(displayName);
    this.animalName = AnimalMap.get(animal);
    this.animalColor = ColorMap.get(color);
    this.displayNumber = code;
  }

  /**
   * Get total number of challenges assigned
   */
  getNumberAssignedChallenges(): number {
    const numAssignedChallenges = this.user.numAssignedChallenges;
    if (numAssignedChallenges) {
      return numAssignedChallenges.authority +
        numAssignedChallenges.gps +
        numAssignedChallenges.news +
        numAssignedChallenges.physical +
        numAssignedChallenges.questionnaire;
    }
    return 0;
  }

  /**
   * Get total number of challenges completed
   */
  getNumberCompletedChallenges(): number {
    const numCompletedChallenges = this.user.numCompletedChallenges;
    if (numCompletedChallenges) {
      return numCompletedChallenges.authority +
        numCompletedChallenges.gps +
        numCompletedChallenges.news +
        numCompletedChallenges.physical +
        numCompletedChallenges.questionnaire;
    }
    return 0;
  }

  /**
   * Get number of challenges not completed
   */
  getNumberOfNotCompletedChallenges(): number {
    return this.getNumberAssignedChallenges() - this.getNumberCompletedChallenges();
  }

}
