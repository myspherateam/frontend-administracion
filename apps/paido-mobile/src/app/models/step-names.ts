import { TranslateService } from '@ngx-translate/core';

export class StepNames {
    /**
     * Step names
     */
    public names: Map<string, string>;

    /**
     * Constructor
     */
    constructor(private translateService: TranslateService) {
        this.names = new Map<string, string>([
            ['prepare', this.translateService.instant('HOME.STEP_PREPARE')],
            ['activate', this.translateService.instant('HOME.STEP_ACTIVATE')],
            ['avoid', this.translateService.instant('HOME.STEP_AVOID')],
            ['enjoy', this.translateService.instant('HOME.STEP_ENJOY')],
            ['keep', this.translateService.instant('HOME.STEP_KEEP')],
        ]);
    }

}
