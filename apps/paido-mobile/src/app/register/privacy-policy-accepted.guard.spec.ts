import { TestBed } from '@angular/core/testing';

import { PrivacyPolicyAcceptedGuard } from './privacy-policy-accepted.guard';

describe('PrivacyPolicyAcceptedGuard', () => {
  let guard: PrivacyPolicyAcceptedGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(PrivacyPolicyAcceptedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
