import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PolicyRejectedComponent } from './policy-rejected/policy-rejected.component';
import { PrivacyPolicyAcceptedGuard } from './privacy-policy-accepted.guard';
import { RegisterPage } from './register.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterPage,
    canActivate: [PrivacyPolicyAcceptedGuard],
  },
  {
    path: 'policy-rejected',
    component: PolicyRejectedComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterPageRoutingModule { }
