import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-policy-rejected',
  templateUrl: './policy-rejected.component.html',
  styleUrls: ['./policy-rejected.component.scss'],
})
export class PolicyRejectedComponent {

  private router: Router = inject(Router);

  retryPolicyAcceptance() {
    this.router.navigateByUrl('/register');
  }
}
