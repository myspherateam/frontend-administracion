import { CanActivate, Router } from '@angular/router';
import { Injectable, inject } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core';

import { LocalStorageService } from '../services/storage-service';
import { PrivacyPolicyModalComponent } from './privacy-policy-modal/privacy-policy-modal.component';

@Injectable({
  providedIn: 'root',
})
export class PrivacyPolicyAcceptedGuard implements CanActivate {
  private localStorage: LocalStorageService = inject(LocalStorageService);

  private modalController: ModalController = inject(ModalController);

  private router: Router = inject(Router);

  async canActivate(): Promise<boolean> {
    const privacyAccepted: boolean = this.localStorage.getFromLocalStorage<boolean>('privacyAccepted') || false;

    if (!privacyAccepted) {
      const modal = await this.modalController.create({
        component: PrivacyPolicyModalComponent,
        cssClass: 'privacy-modal',
        backdropDismiss: false,
      });
      modal.onDidDismiss().then(result => {
        if (result.data && result.data === 'accept') {
          this.localStorage.storeOnLocalStorage('privacyAccepted', true);
        }
      });
      await modal.present();
      const result: OverlayEventDetail<boolean> = await modal.onDidDismiss<boolean>();
      this.localStorage.storeOnLocalStorage<boolean>('privacyAccepted', !!result.data);
      if (!result.data) {
        this.router.navigateByUrl('/register/policy-rejected');
        return false;
      }
    }
    return true;
  }

}
