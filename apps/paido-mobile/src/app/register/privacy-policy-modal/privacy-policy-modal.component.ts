import { Component, ViewEncapsulation } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-privacy-policy-modal',
  templateUrl: './privacy-policy-modal.component.html',
  styleUrls: ['./privacy-policy-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PrivacyPolicyModalComponent {

  public privacyAccepted: boolean = false;

  constructor(
    private modalController: ModalController,
  ) { }

  /**
   * Cancel button. Dismiss dialog.
   */
  cancel() {
    this.modalController.dismiss(false, 'cancel');
  }

  /**
   * Accept button. Dismiss dialog passing identificator tag
   */
  accept() {
    this.modalController.dismiss(true);
  }
}
