import { AlertController, MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { LocalNotifications } from '@capacitor/local-notifications';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, Login, Register } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { LocalStorageService } from '../services/storage-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage extends AnalyzableComponent implements OnInit {

  protected readonly screenName = 'register';

  public registerCode!: string;

  public isAlreadyRegistered: boolean = false;

  public password: string = '';

  constructor(
    private router: Router,
    private menuController: MenuController,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private alertController: AlertController,
    private translateService: TranslateService,
  ) {
    super();
    this.menuController.enable(false);
  }

  /**
   * User press access button. Check if user has been registered.
   */
  register() {
    if (!this.isAlreadyRegistered) {
      // register user and check if is already registered
      this.registerUser();
    } else {
      this.loginRegisteredUser();
    }
  }

  /**
   * Register user
   */
  registerUser() {
    this.apiService.register(this.registerCode.trim()).subscribe({
      next: (result: ApiResponse<Register>) => {
        if (result.status === 200) {
          const register: Register = result.message;
          this.localStorage.storeOnLocalStorage<string>('username', register.username);
          this.localStorage.storeOnLocalStorage<string>('password', register.password);
          this.localStorage.storeOnLocalStorage<boolean>('isAlreadyRegistered', register.isAlreadyRegistered);
          this.isAlreadyRegistered = register.isAlreadyRegistered;

          if (!this.isAlreadyRegistered) {
            this.presentAlert();
          } else {
            // user is already registered alert user
            this.showAlert(
              this.translateService.instant('REGISTER.DIALOG_REGISTERED_TITLE'),
              this.translateService.instant('REGISTER.DIALOG_REGISTERED_SUBTITLE'),
            );
          }
        }
      },
      error: (errorHttp: HttpErrorResponse) => {
        if (errorHttp.status === 404) {
          // código no encontrado
          this.showAlert(this.translateService.instant('REGISTER.ERROR.NOT_FOUND'),
            `${this.translateService.instant('REGISTER.ERROR.NOT_FOUND_EXPLANATION', { code: this.registerCode })}`);
        } else {
          // error genérico
          this.showAlert(this.translateService.instant('ERRORS.ERROR'),
            this.translateService.instant('REGISTER.ERROR.UNKNOWN'));
        }
      },
    });
  }

  /**
   * Login a user that was registered. Pass the input password value
   */
  loginRegisteredUser() {
    // user is already registered login user
    const username: string = this.localStorage.getFromLocalStorage<string>('username') as string;
    this.apiService.login(username, this.password).subscribe({
      next: async (result: Login) => {
        this.localStorage.storeOnLocalStorage<string>('access_token', result.access_token);
        this.localStorage.storeOnLocalStorage<string>('refresh_token', result.refresh_token);
        this.localStorage.storeOnLocalStorage<string>('password', this.password);

        const notificationGrants = await LocalNotifications.checkPermissions();
        if (notificationGrants.display !== 'granted') {
          await LocalNotifications.requestPermissions();
        }

        this.router.navigateByUrl('/login');
      },
      error: (response: HttpErrorResponse) => {
        if (response.status === 401) {
          this.showAlert(
            this.translateService.instant('ERRORS.ERROR'),
            this.translateService.instant('ERRORS.INCORRECT_CREDENTIALS'),
          );
        } else {
          console.log('fallo en la página de registro al hacer el login');
          this.showAlert(this.translateService.instant('ERRORS.ERROR'), response.message);
        }
      },
    });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('REGISTER.DIALOG_CREATED_TITLE'),
      subHeader: this.translateService.instant('REGISTER.DIALOG_CREATED_SUBTITLE'),
      message: this.translateService.instant('REGISTER.DIALOG_CREATED_DESC'),
      backdropDismiss: false,
      buttons: [{
        text: this.translateService.instant('VERBS.ACCESS'),
        handler: () => {
          this.router.navigateByUrl('/login');
        },
      }],
    });

    await alert.present();
  }

  async showAlert(header: string, message: string) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: [{
        text: this.translateService.instant('VERBS.OK'),
      }],
    });

    await alert.present();
  }
}
