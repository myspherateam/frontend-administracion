import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthAppSetupComponent } from './health-app-setup.component';

describe('GoogleFitSetupComponent', () => {
  let component: HealthAppSetupComponent;
  let fixture: ComponentFixture<HealthAppSetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HealthAppSetupComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HealthAppSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
