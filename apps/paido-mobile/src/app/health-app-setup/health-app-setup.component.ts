import { App, AppState } from '@capacitor/app';
import { Component, NgZone, OnInit, inject } from '@angular/core';
import { User, UserRole } from '@paido/data-model';
import { Capacitor } from '@capacitor/core';
import { Health } from '@awesome-cordova-plugins/health/ngx';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { LocalStorageService } from '../services/storage-service';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-health-app-setup',
  templateUrl: './health-app-setup.component.html',
  styleUrls: ['./health-app-setup.component.scss'],
})
export class HealthAppSetupComponent extends AnalyzableComponent implements OnInit {
  protected readonly screenName: string = 'health-app-setup';

  private zone: NgZone = inject(NgZone);

  private menuConntroller: MenuController = inject(MenuController);

  private router: Router = inject(Router);

  // boolean flag that indicates whether the running device is a native os
  isNativePlatform: boolean;

  isAndroid: boolean;

  isIOs: boolean;

  isWeb: boolean;

  healthAppInstalled: boolean = false;

  healthDataAuthorized: boolean = false;

  private localStorage: LocalStorageService = inject(LocalStorageService);

  currentUser: User | undefined;

  constructor(private health: Health) {
    super();
    this.isNativePlatform = Capacitor.isNativePlatform();
    this.isAndroid = Capacitor.getPlatform() === 'android';
    this.isIOs = Capacitor.getPlatform() === 'ios';
    this.isWeb = Capacitor.getPlatform() === 'web'; 
    this.currentUser = this.localStorage.getFromLocalStorage<User>('user');
    
    App.addListener('appStateChange', async ({ isActive }: AppState) => {
      if (isActive) {
        this.zone.run(async () => {
          await this.ngOnInit();
        });
      }
    });
    this.menuConntroller.enable(false);
  }

  override async ngOnInit(): Promise<void> {
    super.ngOnInit();
    const isParent = this.currentUser && this.currentUser?.roles.includes(UserRole.Parent);
    
    if( !isParent){ //if is child, not to request the permissions because them can not install google fit
      this.healthAppInstalled = true;
      this.healthDataAuthorized = true;
      await this.continue();
    }else{
      await this.checkHealthAppInstalled();
      await this.checkHealthDataAuthorized();

    }
  }

  async installHealthApp() {
    if (Capacitor.isNativePlatform()) {
      await this.health.promptInstallFit();
    }
  }

  async checkHealthAppInstalled() {
    if (Capacitor.isNativePlatform()) {
      this.healthAppInstalled = await this.health.isAvailable();
    }
    return this.healthAppInstalled;
  }

  async checkHealthDataAuthorized() {
    if (Capacitor.isNativePlatform()) {
      if (Capacitor.getPlatform() === 'android') {
        this.healthDataAuthorized = await this.health.isAuthorized([{ read: ['steps'] }]);
      } else {
        this.healthDataAuthorized = await this.health.isAuthorized([{ write: ['steps'] }]);
      }
    }
    return this.healthDataAuthorized;
  }

  async askHealthDataAccessPermission() {
    if (Capacitor.isNativePlatform()) {
      try {
        if (Capacitor.getPlatform() === 'android') {
          await this.health.requestAuthorization([{ read: ['steps'] }]);
        } else {
          await this.health.requestAuthorization([{ read: ['steps'], write: ['steps'] }]);
          await this.checkHealthDataAuthorized();
        }
      } catch (error) {
        console.error(error);
      }
    }
  }

  async continue() {
    await this.router.navigateByUrl('/home');
  }
}
