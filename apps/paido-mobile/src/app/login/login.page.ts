import { AlertController, MenuController } from '@ionic/angular';
import { App, AppInfo } from '@capacitor/app';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { filter, firstValueFrom, from, map, mergeMap, of, switchMap, take, toArray } from 'rxjs';
import { Capacitor } from '@capacitor/core';
import { HttpErrorResponse } from '@angular/common/http';

import { AnimalMap, ApiResponse, ColorMap, Login, User, UserRole, displayNameComponents } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_EVENTS } from '../utils/constants';
import { getLocalizedChildUsername } from '../utils/username';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends AnalyzableComponent implements OnInit, OnDestroy {

  protected readonly screenName: string = 'login';

  public users!: User[];

  public role!: string;

  constructor(
    private router: Router,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private menuController: MenuController,
    private alertController: AlertController,
    private events: Events,
    public jwtHelper: JwtHelperService,
  ) { super(); }

  /**
   * OnInit implementation. Subscribe refresh token event to reload login
   */
  override async ngOnInit(): Promise<void> {
    await super.ngOnInit();
    this.menuController.enable(false);
    this.events.subscribe(PAIDO_EVENTS.REFRESH_TOKEN,
      () => {
        this.menuController.enable(false);
      },
    );
    const accessToken = this.localStorage.getFromLocalStorage<string>('access_token');
    const username = this.localStorage.getFromLocalStorage<string>('username');
    const password = this.localStorage.getFromLocalStorage<string>('password');
    if (!accessToken || this.jwtHelper.isTokenExpired(accessToken)) {
      // system does not have a token, but it has user and pass. This is token expired case.
      if (username && password) {
        this.apiService.login(username, password).subscribe({
          next:
            (result: Login) => {
              this.localStorage.storeOnLocalStorage('access_token', result.access_token);
              this.localStorage.storeOnLocalStorage('refresh_token', result.refresh_token);

              this.registerCurrentVersion();

              this.getMe();
            },
          error: (response: HttpErrorResponse) => {
          console.log('fallo en la página de login al hacer el login');
          console.log(JSON.stringify(response));

            if (response.error.error_description?.includes('Invalid user credentials')) {
              this.showAlertError();
            }
          },
        });
      }
    } else {
      this.getMe();
    }
  }

  /**
  * Unsubscribe refresh token event
  */
  override ngOnDestroy(): void {
    super.ngOnDestroy();
    this.events.destroy(PAIDO_EVENTS.REFRESH_TOKEN);
  }

  /**
   * Register current app version on server
   */
  async registerCurrentVersion() {
    if (Capacitor.isNativePlatform()) {

      const info: AppInfo = await App.getInfo();
      const currentPlatform: string = Capacitor.getPlatform();
      const currentVersion = info.version;
      try {
        await firstValueFrom(this.apiService.registerCurrentVersion(currentPlatform, currentVersion));
      } catch (error) {
        // TODO: send this error to firebase
      }
    }
  }

  /**
   * Get my user instance
   */
  private getMe() {
    this.apiService.getMeMobile()
      .pipe(
        filter((response: ApiResponse<User[]>) => response.status === 200),
        switchMap((response: ApiResponse<User[]>) => {
          // If user is child and does not have a display name, add centre/group info
          return from(response.message).pipe(
            mergeMap((user: User) => {
              if (user.roles.includes(UserRole.Child) && user.displayName === undefined) {
                return this.apiService.getUserGroupDetails(user.groupIds[0]).pipe(
                  take(1),
                  map((group: string) => {
                    user['groupName'] = group || '';
                    return user;
                  }),
                );
              } else {
                return of<User>(user);
              }
            }),
            toArray<User>(),
          );
        }),
        map((users: User[]) => {
          return users.sort((a, b) => {
            if (a.roles.includes(UserRole.Parent)) {
              return -1;
            } else if (b.roles.includes(UserRole.Parent)) {
              return 1;
            } else {
              return 0;
            }
          });
        }),
      ).subscribe({
        next:
          (result: User[]) => {
            this.users = result;
            this.saveUserAndRedirect(this.users.length > 1);
          },
        error: error => console.error(error),
      });
  }

  /**
   * Save user instance and redirect to next page.
   * This method is triggered when user opens de app from background or when user comes from register page
   * @param multi more than one user profile available.
   * Count with value 1 indicates that user enters with a child role.
   */
  private saveUserAndRedirect(multi: boolean) {    // CHILD
    (multi) ? this.saveParentData() : this.saveChildData();
    this.events.publish(PAIDO_EVENTS.ROLE_CHANGE, {});
  }

  private saveParentData() {
    // PARENT
    // save my user. I'm parent or a parent that enter as a child
    let userParent: User | null = null;
    const childId = this.localStorage.getFromLocalStorage<string>('childId');
    this.users.forEach((user: User) => {
      // check if parent entered as a child, if not save the parent user
      if (childId) {
        // parent entered as a child
        if (user.id === childId) {
          this.localStorage.storeOnLocalStorage<User>('user', user);
          // save user points
          this.localStorage.storeOnLocalStorage<number>('userPoints', user.points || 0);
        }
      } else if (user.roles.includes('Parent')) {
        // i'm a parent
        userParent = { ...user };
        this.localStorage.storeOnLocalStorage<User>('user', userParent);
        // save user points
        this.localStorage.storeOnLocalStorage<number>('userPoints', userParent.points || 0);
      }
    });

    this.apiService.hasOneUser.next({ hasOneUser: false });
    const isAlreadyRegistered = this.localStorage.getFromLocalStorage<boolean>('isAlreadyRegistered') || false;
    const isSignIn: boolean = this.localStorage.isSignIn();

    // check if the parent has been already registered or not. If not, keep in login page.
    if (!isAlreadyRegistered && userParent) {
      const navigationExtras: NavigationExtras = {
        state: {
          user: userParent,
        },
      };
      return this.router.navigateByUrl('/user-selection', navigationExtras);
    } else if (isAlreadyRegistered && isSignIn) {
      return this.router.navigateByUrl('/home');
    } else {
      // TODO: handle this case -> show popup alert
      return null;
    }
  }

  private saveChildData() {
    // save my user. I'm a child
    const child = this.users[0];
    this.localStorage.storeOnLocalStorage<User>('user', child);
    this.apiService.hasOneUser.next({ hasOneUser: true });

    // save user points
    this.localStorage.storeOnLocalStorage('userPoints', child.points || 0);

    // check if user comes from register page
    if (!child.displayName) {
      const navigationExtras: NavigationExtras = {
        state: {
          user: child,
        },
      };
      return this.router.navigateByUrl('/user-selection', navigationExtras);
    } else {
      return this.router.navigateByUrl('/home');
    }
  }

  /**
   * Alert that there is a session active in other device
   */
  private async showAlertError() {
    const alert = await this.alertController.create({
      header: this.translate.instant('ERRORS.ERROR'),
      message: this.translate.instant('ERRORS.USER_SESSION_ACTIVE'),
      buttons: [
        {
          text: this.translate.instant('VERBS.ACCEPT'),
          handler: () => {
            this.router.navigateByUrl('/register');
          },
        },
      ],
      backdropDismiss: false,
    });

    await alert.present();
  }

  /**
   * Gets the key to localize the user role
   * @param roles user roles
   */
  getRoleTranslationId(roles: string[]) {
    if (roles.includes('Parent')) {
      return this.translate.instant('LOGIN.ROLE_PARENT');
    } else if (roles.includes('Child')) {
      return this.translate.instant('LOGIN.ROLE_CHILD');
    }
  }

  /**
   * User click on a user button in login page
   * @param user user instance
   */
  goHome(user: User) {
    this.localStorage.storeOnLocalStorage<boolean>('signIn', true);
    // user is a parent that enter like a child
    if (this.users.length > 1 && user.roles.includes('Child')) {
      // save childId
      this.localStorage.storeOnLocalStorage<string>('childId', user.id);
      this.localStorage.storeOnLocalStorage<string>('childDisplayId', user.displayName ? user.displayName : user.displayId + '');
    } else {
      // user is a child or a parent
      if (this.localStorage.containsKey('childId')) {
        this.localStorage.remove('childId');
        this.localStorage.remove('childDisplayId');
      }
    }

    // save user points
    this.localStorage.storeOnLocalStorage<number>('userPoints', user.points ? user.points : 0);
    this.localStorage.storeOnLocalStorage<User>('user', user);

    if (this.localStorage.containsKey('access_token')) {
      // notify app page that user role has changed and needs to refresh menu list
      this.events.publish(PAIDO_EVENTS.ROLE_CHANGE, {});
      this.router.navigateByUrl('/home');
    }
  }

  getAnimal(displayName: string = ''): string | undefined {
    return AnimalMap.get(displayNameComponents(displayName)[0]);
  }

  getColor(displayName: string = ''): string | undefined {
    return ColorMap.get(displayNameComponents(displayName)[1]);
  }

  translateUsername(username: string) {
    return getLocalizedChildUsername(this.translate, username);
  }
}
