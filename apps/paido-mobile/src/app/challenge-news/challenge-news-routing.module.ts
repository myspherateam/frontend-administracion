import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ChallengeNewsComponent } from './challenge-news.component';

const routes: Routes = [
  {
    path: '',
    component: ChallengeNewsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChallengeNewsPageRoutingModule { }
