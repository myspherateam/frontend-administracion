import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { QuillModule } from 'ngx-quill';
import { TranslateModule } from '@ngx-translate/core';

import { ChallengeNewsComponent } from './challenge-news.component';
import { ChallengeNewsPageRoutingModule } from './challenge-news-routing.module';
import { EditorComponent } from './news/editor/editor.component';
import { ViewerComponent } from './news/viewer/viewer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChallengeNewsPageRoutingModule,
    TranslateModule,
    QuillModule.forRoot(),
  ],
  declarations: [
    ChallengeNewsComponent,
    EditorComponent,
    ViewerComponent,
  ],
})
export class ChallengeNewsModule { }
