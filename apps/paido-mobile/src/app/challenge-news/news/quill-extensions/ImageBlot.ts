import Quill from 'quill';

const BaseImage = Quill.import('formats/image');

const ATTRIBUTES = [
  'alt',
  'height',
  'width',
  'style',
];

const WHITE_STYLE = ['margin', 'display', 'float'];

export class ImageBlot extends BaseImage {
  static formats(domNode: any) {
    return ATTRIBUTES.reduce((_formats: Record<string, any>, attribute) => {
      if (domNode.hasAttribute(attribute)) {
        _formats[attribute] = domNode.getAttribute(attribute);
      }
      return _formats;
    }, {});
  }

  format(name: string, value: any) {
    if (ATTRIBUTES.indexOf(name) > -1) {
      const domNode = 'domNode';
      if (value) {
        if (name === 'style') {
          value = this.sanitize_style(value);
        }
        this[domNode].setAttribute(name, value);
      } else {
        this[domNode].removeAttribute(name);
      }
    } else {
      super.format(name, value);
    }
  }

  sanitize_style(style: string) {
    const styleArr = style.split(';');
    let allowStyle = '';
    styleArr.forEach((v: string, _i) => {
      if (WHITE_STYLE.indexOf(v.trim().split(':')[0]) !== -1) {
        allowStyle += v + ';';
      }
    });
    return allowStyle;
  }
}
