import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { QuillEditorComponent } from 'ngx-quill';

import { Delta } from '../interfaces/delta';
// Needed to actually run the code that injects the extensions into quill
import '../quill-extensions/Load';

@Component({
  selector: 'app-news-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
})
export class EditorComponent {

  @ViewChild('editor', { static: true }) editor!: QuillEditorComponent;

  @Input() styles: Record<string, string> = {};

  @Input() placeholder!: string;

  @Output() contentChange: EventEmitter<Delta> = new EventEmitter<Delta>();

  private contentValue!: Delta;

  /*
  modules: any = {};

   constructor() {
       this.modules = {
        imageResize: {},
        imageDrop: true,
      };
   }
  */

  @Input() get content(): Delta {
    return this.contentValue;
  }

  set content(content: Delta) {
    this.contentValue = content;
  }

  onType(event: Delta) {
    this.contentValue = event;
    this.contentChange.emit(this.contentValue);
  }
}
