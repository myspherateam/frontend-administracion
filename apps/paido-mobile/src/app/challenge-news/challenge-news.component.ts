import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, AnimationController, MenuController } from '@ionic/angular';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, ChallengeCompletion } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { Delta } from './news/interfaces/delta';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_EVENTS } from '../utils/constants';

@Component({
  selector: 'app-challenge-news',
  templateUrl: './challenge-news.component.html',
  styleUrls: ['./challenge-news.component.scss'],
})

export class ChallengeNewsComponent extends AnalyzableComponent implements OnInit, OnDestroy {

  protected readonly screenName: string = 'challenge-news';

  public challenge!: ChallengeCompletion;

  public newsContent!: Delta;

  public isOnTime!: boolean;

  constructor(
    private router: Router,
    public route: ActivatedRoute,
    private menuController: MenuController,
    private events: Events,
    public apiService: ApiService,
    public localStorage: LocalStorageService,
    public alertController: AlertController,
    private translateService: TranslateService,
    private animationController: AnimationController,
  ) {
    super();
    this.menuController.enable(false);
    this.challenge = (this.router.getCurrentNavigation()?.extras?.state as { challenge: ChallengeCompletion })?.challenge;
    this.newsContent = JSON.parse(this.challenge.news);
  }

  override async ngOnInit() {
    super.ngOnInit();
    this.events.subscribe(PAIDO_EVENTS.CHALLENGE_COMPLETED, () => {
      // nothing todo
    });
    this.isOnTime = this.challengeIsOnTime();
  }

  override  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.events.destroy(PAIDO_EVENTS.CHALLENGE_COMPLETED);
  }

  /**
   * Check if a challenge has started but hasn't finished yet
   */
  challengeIsOnTime(): boolean {
    const finishDatetime = this.getFormattedDateInMillis(
      this.challenge.periodFinishDatetime,
    );
    const startDatetime = this.getFormattedDateInMillis(
      this.challenge.periodStartDatetime,
    );
    const actualDatetime = new Date().getTime();
    return actualDatetime >= startDatetime && actualDatetime < finishDatetime;
  }

  getFormattedDateInMillis(dateString: string): number {
    return new Date(dateString).getTime();
  }

  /**
   * Open questionnaire to complete, if news has one.
   */
  openNewsTest() {
    this.router.navigateByUrl('/challenge-questionnaire', { state: { challenge: this.challenge } });
  }

  /**
   * Complete challenge if user read the news
   */
  completeChallenge() {
    const finishDatetime = new Date(this.challenge.periodFinishDatetime).getTime();
    const startDatetime = new Date(this.challenge.periodStartDatetime).getTime();
    const actualDatetime = new Date().getTime();

    if (actualDatetime >= startDatetime && actualDatetime < finishDatetime) {
      if (this.localStorage.containsKey('childId')) {
        const childId = this.localStorage.getFromLocalStorage<string>('childId');

        const lastAwardedPoints = this.challenge.awardedPoints;

        this.apiService.markAChallengeAsComplete(this.challenge, childId).subscribe({
          next: (result: ApiResponse<ChallengeCompletion>) => {
            if (result.status === 200) {
              const challengeCompletion: ChallengeCompletion = result.message;
              const pointsSaved = challengeCompletion.awardedPoints - lastAwardedPoints;

              this.showAlertDialog(
                this.translateService.instant('CHALLENGE_DETAIL.DONE'),
                this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_DONE') +
                pointsSaved +
                this.translateService.instant('CHALLENGE_DETAIL.POINTS'),
                this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_OVERCOME'),
                'completion-modal',
                true,
              );
            }
          },
          error: (error: HttpErrorResponse) => {
            console.error(error);
            if (error.error) {
              const apiError: ApiResponse<string> = error.error;
              if (apiError.message) {
                this.showAlertDialog(
                  this.translateService.instant('ERRORS.TRY_AGAIN'),
                  this.translateService.instant('ERRORS.CHALLENGE_FAILED_TITLE'),
                  this.translateService.instant('ERRORS.CHALLENGE_FAILED'),
                  undefined,
                  false,
                );
              }
            }
          },
        });
      }
    }
  }

  async showAlertDialog(header: string, subHeader: string, message: string, cssClass?: string, animated?: boolean) {
    const alert = await this.alertController.create({
      header,
      subHeader,
      message,
      buttons: [{
        text: this.translateService.instant('VERBS.ACCEPT'),
        handler: () => {
          // challenge was completed. Go back to challenges list
          if (!header.includes('Error')) {
            this.events.publish(PAIDO_EVENTS.CHALLENGE_COMPLETED, this.challenge);
            // this.navController.pop();
          }
        },
      }],
      cssClass,
      animated,
      enterAnimation: (baseEl: Element) => {
        return (this.animationController
          .create()
          .addElement(baseEl.querySelector('.alert-wrapper') as Element)
          .duration(1000)
          .keyframes([
            { offset: 0, transform: 'scale(1)', opacity: '1' },
            { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
            { offset: 1, transform: 'scale(1)', opacity: '1' },
          ]));
      },
    });
    await alert.present();
  }
}
