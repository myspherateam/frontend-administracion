import { CanActivate, Router } from '@angular/router';
import { Injectable, inject } from '@angular/core';

import { LocalStorageService } from './services/storage-service';

@Injectable({
  providedIn: 'root',
})
export class RegisteredDeviceGuard implements CanActivate {

  private localStorage: LocalStorageService = inject(LocalStorageService);

  private router: Router = inject(Router);

  /**
   * A device is registered if it contains either an access token or credentials stored
   * @returns
   */
  async canActivate(): Promise<boolean> {
    const token = this.localStorage.getFromLocalStorage<string>('access_token');
    const username = this.localStorage.getFromLocalStorage<string>('username');
    const password = this.localStorage.getFromLocalStorage<string>('password');

    const isRegistered = token !== undefined || (username !== undefined && password !== undefined);
    if (!isRegistered) {
      await this.router.navigateByUrl('/register');
    }
    return isRegistered;
  }
}
