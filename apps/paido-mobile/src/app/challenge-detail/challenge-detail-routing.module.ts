import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ChallengeDetailPage } from './challenge-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ChallengeDetailPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChallengeDetailRoutingModule { }
