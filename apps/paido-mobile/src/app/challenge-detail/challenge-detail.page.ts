import { AlertController, AnimationController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { Health } from '@awesome-cordova-plugins/health/ngx';
import { Router } from '@angular/router';

import { ChallengeCompletion, ChallengeType, User } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_EVENTS } from '../utils/constants';
import { StepNames } from '../models/step-names';

@Component({
  selector: 'app-challenge-detail',
  templateUrl: './challenge-detail.page.html',
  styleUrls: ['./challenge-detail.page.scss'],
})
export class ChallengeDetailPage extends AnalyzableComponent implements OnInit {

  protected readonly screenName = 'challenge-detail';

  public challengeId!: string;

  public challenge!: ChallengeCompletion;

  public challengeTagType: string = '';

  public readonly challengeType = ChallengeType;

  public user!: User;

  public isParent: boolean = false;

  public haveToAuthorize: boolean = true;

  public stepsDone: number = 0;

  public arePermissionsAuthorized: boolean = false;

  public challengeCategory!: string;

  public challengeCompleted: boolean;

  public isOnTime: boolean = false;

  public hasChallengeStarted: boolean = false;

  private stepNames: Map<string, string>;

  lastCheckDate!: Date;

  constructor(
    public router: Router,
    public alertController: AlertController,
    public challengesService: ApiService,
    private health: Health,
    private localStorage: LocalStorageService,
    private events: Events,
    private animationController: AnimationController,
  ) {
    super();
    this.challengeCompleted = false;

    this.challengeId = (this.router.getCurrentNavigation()?.extras?.state as { challenge: string })?.challenge;

    this.stepNames = new StepNames(this.translate).names;
  }

  override async ngOnInit() {
    super.ngOnInit();
    this.getChallengeDetail();
    this.arePermissionsAuthorized = await this.health.isAuthorized([{ read: ['steps'] }]);
  }

  getChallengeDetail() {
    const childId: string | undefined = this.localStorage.getFromLocalStorage<string>('childId');

    this.challengesService
      .getCompletionChallengeDetail(this.challengeId, childId)
      .subscribe({
        next:
          (result) => {
            if (result.status === 200) {
              this.challenge = result.message;
              this.user = this.localStorage.getFromLocalStorage<User>('user') as User;
              this.isParent =
                this.localStorage.getFromLocalStorage('childId') === undefined;
              this.haveToAuthorize = this.checkHaveToAuthorize();
              this.initDetail();
            }
          },
        error: (error) => {
          // TODO: add translation
          console.error('Error challenge detail: ' + error);
        },
      });
  }

  initDetail() {
    if (this.challenge) {
      this.isOnTime = this.challengeIsOnTime();
      switch (this.challenge.type) {

        case this.challengeType.physical:
          if (this.isOnTime) {
            this.getSteps();
          }
          break;
      }
      this.getChallengeTypeTag();
      this.challengeCategory = this.stepNames.get(this.challenge.category) as string;
      this.hasChallengeStarted = this.checkChallengeHasStarted();
    }
  }

  // get challenge title
  getChallengeTypeTag() {
    if (this.challenge) {
      switch (this.challenge.type) {
        case this.challengeType.authority:
          this.challengeTagType = this.translate.instant('CHALLENGES_LIST.AUTHORITY');
          break;
        case this.challengeType.gps:
          this.challengeTagType = this.translate.instant('CHALLENGES_LIST.GPS');
          break;
        case this.challengeType.news:
          this.challengeTagType = this.translate.instant('CHALLENGES_LIST.NEWS');
          break;
        case this.challengeType.physical:
          this.challengeTagType = this.translate.instant('CHALLENGES_LIST.PHYSICAL_ACTIVITY');
          break;
        case this.challengeType.questionnaire:
          this.challengeTagType = this.translate.instant('CHALLENGES_LIST.QUESTIONNAIRE');
          break;
      }
    }
  }

  /**
   * Get a tag for the user who is assigned to the challenge
   */
  getChallengeFor(challengeFor: string): string {
    return challengeFor === 'child' ?
      this.translate.instant('CHALLENGE_DETAIL.CHILDREN') :
      this.translate.instant('CHALLENGE_DETAIL.PARENTS');
  }

  /**
   * Check if a challenge has started but hasn't finished yet
   */
  challengeIsOnTime(): boolean {
    const finishDatetime = this.getFormattedDateInMillis(
      this.challenge.periodFinishDatetime,
    );
    const startDatetime = this.getFormattedDateInMillis(
      this.challenge.periodStartDatetime,
    );
    const actualDatetime = new Date().getTime();
    return actualDatetime >= startDatetime && actualDatetime < finishDatetime;
  }

  /**
   * Check if a challenge has started or it's starting in several days
   */
  checkChallengeHasStarted(): boolean {
    const startDatetime = this.getFormattedDateInMillis(
      this.challenge.periodStartDatetime,
    );
    const actualDatetime = new Date().getTime();
    return actualDatetime >= startDatetime;
  }

  getFormattedDateInMillis(dateString: string): number {
    return new Date(dateString).getTime();
  }

  markAChallengeAsCompleted(challengeComplete: ChallengeCompletion) {
    const finishDatetime = this.getFormattedDateInMillis(
      this.challenge.periodFinishDatetime,
    );
    const startDatetime = this.getFormattedDateInMillis(
      this.challenge.periodStartDatetime,
    );
    const actualDatetime = new Date().getTime();

    if (actualDatetime >= startDatetime && actualDatetime < finishDatetime) {

      const childId: string | undefined = this.localStorage.getFromLocalStorage('childId');

      const lastAwardedPoints = this.challenge.awardedPoints;

      this.challengesService
        .markAChallengeAsComplete(challengeComplete, childId)
        .subscribe({
          next:
            (result) => {
              if (result.status === 200) {
                const challengeCompletion: ChallengeCompletion = result.message;
                const pointsSaved = challengeCompletion.awardedPoints - lastAwardedPoints;

                this.showAlertDialog(
                  this.translate.instant('CHALLENGE_DETAIL.DONE'),
                  this.translate.instant('CHALLENGE_DETAIL.CHALLENGE_DONE') +
                  pointsSaved +
                  this.translate.instant('CHALLENGE_DETAIL.POINTS'),
                  this.translate.instant('CHALLENGE_DETAIL.CHALLENGE_OVERCOME'),
                );
              }
            },
          error: (error) => {
            console.error(error);
          },
        });
    } else {
      console.error('Could not retrieve the child id');
    }
  }

  async showAlertDialog(header: string, subHeader: string, message: string) {
    const alert = await this.alertController.create({
      header,
      subHeader,
      message,
      buttons: [
        {
          text: this.translate.instant('VERBS.ACCEPT'),
          handler: () => {
            // go back
            this.challengeCompleted = true;
            this.events.publish(PAIDO_EVENTS.CHALLENGE_COMPLETED, this.challenge);
          },
        },
      ],
      cssClass: 'completion-modal',
      enterAnimation: (baseEl: Element) => {
        return (this.animationController
          .create()
          .addElement(baseEl.querySelector('.alert-wrapper') as Element)
          .duration(1000)
          .keyframes([
            { offset: 0, transform: 'scale(1)', opacity: '1' },
            { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
            { offset: 1, transform: 'scale(1)', opacity: '1' },
          ]));
      },
    });

    await alert.present();
  }

  // launch questionnaire detail
  goToQuestionnaire() {
    this.router.navigateByUrl('/challenge-questionnaire', {
      state: { challenge: this.challenge },
    });
  }

  // launch news detail
  goToNews() {
    this.router.navigateByUrl('/challenge-news', {
      state: { challenge: this.challenge },
    });
  }

  /**
   * Challenge physical activity
   */

  async getSteps() {
    // check platform ready
    if (Capacitor.isNativePlatform()) {
      this.getHealthData();
    }
  }

  getHealthData() {
    const todayMidnight: Date = new Date();
    todayMidnight.setHours(0, 0, 0, 0);

    this.health
      .query({
        // data from today steps
        startDate: new Date(todayMidnight.getTime()),
        endDate: new Date(),
        dataType: 'steps',
        limit: 1000,
        filtered: true,
      })
      .then((data) => {
        this.lastCheckDate = new Date();
        if (data && data.length > 0) {
          this.stepsDone = data.reduce((prev, healthData) => {
            return prev + parseInt(healthData.value, 10);
          }, 0);
          this.checkPhysicalChallengeDone(this.stepsDone);
        }
      })
      .catch((error) => console.error('Data error: ' + error));
  }

  /**
   * Check if the user can complete a physical challenge.
   * Only if the user overcome the steps proposed, it's assigned
   * to the challenge and isn't a parent that enter like a child.
   * @param stepValue steps done by the user
   */
  checkPhysicalChallengeDone(stepValue: number) {
    if (this.challenge) {
      // check if there is a childId, so the user is a parent that enter like a child
      if (this.localStorage.containsKey('childId')) {
        const childId = this.localStorage.getFromLocalStorage('childId');

        if (
          stepValue >= this.challenge.steps && this.challenge.important && !childId
        ) {
          const challengeCompletion = {} as ChallengeCompletion;
          challengeCompletion.id = this.challenge.id;
          challengeCompletion.userSteps = stepValue;
          this.markAChallengeAsCompleted(challengeCompletion);
        }
      }
    }
  }

  /**
   * Refresh steps count
   */
  refreshSteps() {
    this.getSteps();
  }

  /*
  // TODO: recreate this method
  getCenterPointFromPolygon(): string {
     const bounds: LatLngBounds = {};
     for (const coordinates of this.polygon) {
       bounds.extend(coordinates);
     }
     return bounds.getCenter().lat + ',' + bounds.getCenter().lng;
   } */

  /**
   * Challenge authority
   */

  // check if user is a parent and has to authorize the challenge
  checkHaveToAuthorize(): boolean {
    let authorize = false;

    // check if challenge is authority type and user is the parent of the user is assigned to
    if (this.challenge && this.user) {
      if (
        this.challenge.type === this.challengeType.authority &&
        this.user.roles.includes('Parent') &&
        this.isParent
      ) {
        authorize = true;
      }
    }

    return authorize;
  }

  // mark authority challenge to done
  authorityChallengeDone(authorizeAChild: boolean) {
    const challengeCompletion = {} as ChallengeCompletion;
    challengeCompletion.id = this.challenge.id;
    challengeCompletion.approvedById = this.user.id;
    challengeCompletion.completedByParents = [{ parentId: this.user.id }];
    if (authorizeAChild) {
      challengeCompletion.forId = this.challenge.forId;
    }
    this.markAChallengeAsCompleted(challengeCompletion);
  }

  /**
   * Check if an authority challenge, assigned for a parent, has been completed at least by one parent
   */
  authorityIsCompletedByParents(): boolean {
    return (
      this.challenge.assignedFor &&
      this.challenge.assignedFor.includes('parent') &&
      this.challenge.completedByParents.length === 0
    );
  }

  /**
   * Check if an authority challenge, assigned for a child, has been completed by the child
   */
  authorityIsCompletedByChild(): boolean {
    return (
      this.challenge.assignedFor &&
      this.challenge.assignedFor.includes('child') &&
      !this.challenge.completed
    );
  }
}
