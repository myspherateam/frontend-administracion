import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LaunchNavigator } from '@awesome-cordova-plugins/launch-navigator/ngx';
import { TranslateModule } from '@ngx-translate/core';

import { ChallengeDetailPage } from './challenge-detail.page';
import { ChallengeDetailRoutingModule } from './challenge-detail-routing.module';
import { GpsChanllengeComponent } from './gps-challenge/gps-challenge.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChallengeDetailRoutingModule,
    TranslateModule,
  ],
  providers: [LaunchNavigator],
  declarations: [ChallengeDetailPage, GpsChanllengeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ChallengeDetailModule { }
