//import * as L from 'leaflet';
import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Geolocation, Position } from '@capacitor/geolocation';
import { Icon, LatLng, Map, Marker, Polygon, icon, map, marker, tileLayer } from 'leaflet';
import { LaunchNavigator, LaunchNavigatorOptions } from '@awesome-cordova-plugins/launch-navigator/ngx';
import { Capacitor } from '@capacitor/core';
import { Coordinate } from 'tsgeo/Coordinate';
import { TranslateService } from '@ngx-translate/core';

import { ChallengeCompletion } from '@paido/data-model';

import { polygonContains } from '../../utils/polygon';

@Component({
  selector: 'app-gps-challenge',
  templateUrl: './gps-challenge.component.html',
  styleUrls: ['./gps-challenge.component.scss'],
})
export class GpsChanllengeComponent implements AfterViewInit, OnInit, OnDestroy {
  @Input() challenge!: ChallengeCompletion;

  @Input() hasChallengeStarted: boolean = false;

  @Input() isOnTime: boolean = false;

  @Output() completed: EventEmitter<ChallengeCompletion> = new EventEmitter();

  // reference for the map object
  private map!: Map;

  // target area for the challenge
  private polygon!: Polygon;

  // language to use to translate dates
  locale: string;

  // how the marker is displayed in the map
  private userIcon!: Icon;

  // marker that signals the user location
  private userMarker?: Marker = undefined;

  // geolocation session handler
  watchId?: string = undefined;

  constructor(private launchNavigator: LaunchNavigator, private translate: TranslateService) {
    this.locale = this.translate.currentLang;
  }

  ngOnInit(): void {
    this.userIcon = icon({
      ...Icon.Default.prototype.options,
      iconUrl: 'assets/marker-icon.png',
      iconRetinaUrl: 'assets/marker-icon.png',
      shadowUrl: 'assets/marker-shadow.png',
    });
  }

  ngOnDestroy(): void {
    if (this.watchId) {
      this.stopTracking();
    }
  }

  async ngAfterViewInit(): Promise<void> {
    if (Capacitor.isNativePlatform()) {

      // GeoJson Polygon: first element is the polygon itself, next elements are the holes inside the polygon
      const holes: LatLng[][] = [];

      this.challenge.location.coordinates.forEach((values, index) => {
        const polygonValues: L.LatLng[] = [];

        values.forEach((element) => {
          // get the coordinates inside
          // pass the number points to ILatLng model
          const polygonPoint: L.LatLng = {
            lat: element[1],
            lng: element[0],
          } as L.LatLng;

          polygonValues.push(polygonPoint);
        });

        if (index > 0) {
          holes.push(polygonValues);
        } else {
          this.polygon = new Polygon(polygonValues);
        }
      });

      this.initLeafletMap(this.polygon);
    }
  }

  private initLeafletMap(poly: L.Polygon) {
    this.map = map('map');

    const tiles = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '',
    });

    tiles.addTo(this.map);
    poly.addTo(this.map);

    this.map.fitBounds(poly.getBounds());
  }

  // launch map
  async goToMap() {
    // destination direction
    if ((await this.checkGPSPermissions())) {
      const options: LaunchNavigatorOptions = {
        appSelection: {
          dialogHeaderText: this.translate.instant('CHALLENGE_DETAIL.SELECT_MAPS_APP'),
          cancelButtonText: this.translate.instant('VERBS.CANCEL'),
          rememberChoice: {
            enabled: false,
          },
        },
      };

      const center = this.polygon.getCenter();
      this.launchNavigator.navigate([center.lat, center.lng], options);
    }
  }

  async startTracking() {
    const gpsPermissions = await this.checkGPSPermissions();
    if (gpsPermissions) {
      this.watchId = await Geolocation.watchPosition({
        enableHighAccuracy: true,
      }, (_position: Position | null, _err?: Error) => {
        if (_position) {
          this.updatePositionOnMap(_position);
        }
      });
    }
  }

  async stopTracking() {
    await Geolocation.clearWatch({ id: this.watchId || '' });
    this.watchId = undefined;
  }

  async checkGPSPermissions() {
    let geolocPermissionsStatus = await Geolocation.checkPermissions();

    if (geolocPermissionsStatus.coarseLocation !== 'granted' || geolocPermissionsStatus.location !== 'granted') {
      geolocPermissionsStatus = await Geolocation.requestPermissions();
    }
    return geolocPermissionsStatus.coarseLocation === 'granted' && geolocPermissionsStatus.location === 'granted';
  }

  private updatePositionOnMap(myLocation: Position) {

    const isInPolygon: boolean = polygonContains(this.polygon, new Coordinate(myLocation.coords.latitude, myLocation.coords.longitude));

    this.map.setView([myLocation.coords.latitude, myLocation.coords.longitude], undefined);
    if (this.userMarker) {
      this.userMarker.setLatLng([myLocation.coords.latitude, myLocation.coords.longitude]);
    } else {
      this.userMarker = marker([myLocation.coords.latitude, myLocation.coords.longitude], { icon: this.userIcon }).addTo(this.map);
    }

    if (isInPolygon && this.challenge.important) {
      // challenge passed
      const challengeCompletion = {} as ChallengeCompletion;
      challengeCompletion.id = this.challenge.id;
      challengeCompletion.userLocation = {
        type: 'Point',
        coordinates: [myLocation.coords.longitude, myLocation.coords.latitude],
      };
      challengeCompletion.precision = myLocation.coords.accuracy;
      this.completed.emit(challengeCompletion);
      this.stopTracking();
    }
  }
}
