import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { RankingPage } from './ranking.page';

const routes: Routes = [
  {
    path: '',
    component: RankingPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RankingPageRoutingModule { }
