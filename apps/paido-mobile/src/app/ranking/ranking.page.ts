import { Component, OnInit } from '@angular/core';

import { AnimalMap, ApiResponse, ColorMap, Ranking, User } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { LocalStorageService } from '../services/storage-service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.page.html',
  styleUrls: ['./ranking.page.scss'],
})
export class RankingPage extends AnalyzableComponent implements OnInit {

  protected readonly screenName: string = 'rankings';

  private childId?: string;

  public rankingField: string = 'points';

  public rankingType: string = 'group';

  private user?: User;

  public animalName?: string;

  public animalColor?: string;

  public displayName?: string;

  public displayNumber?: string;

  public ranking?: Ranking;

  constructor(
    private apiService: ApiService,
    private localStorage: LocalStorageService,
  ) {
    super();
  }

  override  async ngOnInit() {
    super.ngOnInit();

    this.childId = this.localStorage.getFromLocalStorage<string>('childId');

    if (this.localStorage.containsKey('user')) {
      this.user = this.localStorage.getFromLocalStorage<User>('user') as User;
      if (this.localStorage.containsKey('childDisplayId')) {
        // user is a parent that enter like a child
        this.getDisplayName(String(this.localStorage.getFromLocalStorage('childDisplayId')));
      } else {
        // user is parent or unique child
        this.getDisplayName(this.user.displayName);
      }
    }
    this.getMyRanking();

  }

  /**
   * Get user display name and icon
   */
  getDisplayName(displayName: string) {
    this.displayName = '';
    this.displayNumber = '';

    if (displayName) {
      // user have displayName
      for (let i = 0; i < displayName.length; i++) {
        const char = displayName.charAt(i);
        if (char >= '0' && char <= '9') {
          this.displayNumber += char;
        } else {
          this.displayName += char;
        }
      }
      if (this.displayName) {
        this.getUserIcon(this.displayName);
      }
    } else {
      // user is parent
      this.displayName = String(this.user?.displayId);
    }
  }

  /**
   * Get animal and color of user icon to show it
   * @param displayName user displayName
   */
  getUserIcon(displayName: string) {
    const splitName = displayName.replace(/([a-z](?=[A-Z]))/g, '$1 ');
    this.animalName = AnimalMap.get(splitName.split(' ')[0].toString());
    this.animalColor = ColorMap.get(splitName.split(' ')[1].toString());
  }

  /**
   * Get ranking list
   */
  getMyRanking() {
    this.apiService.getRanking(this.childId as string, this.rankingField, this.rankingType).subscribe({
      next: (result: ApiResponse<Ranking>) => {
        if (result.status === 200) {
          this.ranking = result.message;
        }
      },
    });
  }

  getMockRanking() {
    this.ranking = {} as Ranking;
    this.ranking.myPosition = 2;
    this.ranking.myValue = 850;
    this.ranking.totalUsers = 130;
    this.ranking.topUsers = [
      {
        name: 'GalloRojo1209',
        value: 900,
      },
      {
        name: 'MambaAzul1028',
        value: 850,
      },
      {
        name: 'LeónMorado2010',
        value: 600,
      },
    ];
  }

  /**
   * Filter ranking list by type selected
   */
  filterByType(ev: Event) {
    // filter by challenge type
    this.rankingType = (ev as CustomEvent).detail.value;
    this.getMyRanking();
  }

  /**
   * Filter ranking list by value selected
   */
  filterByValue(ev: Event) {
    this.rankingField = (ev as CustomEvent).detail.value;
    this.getMyRanking();
  }
}
