import { AlertButton, AlertController } from '@ionic/angular';
import { App, AppInfo } from '@capacitor/app';
import { CanActivate, UrlTree } from '@angular/router';
import { Injectable, inject } from '@angular/core';
import { catchError, firstValueFrom, of } from 'rxjs';
import { Capacitor } from '@capacitor/core';
import { NativeMarket } from '@capgo/native-market';
import { TranslateService } from '@ngx-translate/core';

import { ApiService } from './api/api-service';

@Injectable({
  providedIn: 'root',
})
export class AppVersionGuard implements CanActivate {

  private translate: TranslateService = inject(TranslateService);

  private api: ApiService = inject(ApiService);

  private alertController: AlertController = inject(AlertController);

  async canActivate(): Promise<boolean | UrlTree> {
    const versionStatus = await this.getVersionStatus();
    if (versionStatus === 1) {
      return true;
    } else {
      this.showVersionAlert(versionStatus !== 1,
        this.translate.instant('VERSION.UPDATE_TITLE'),
        this.translate.instant((versionStatus === 0) ? 'VERSION.UPDATE_DESC' : 'VERSION.UPDATE_DESC_NOW'));

      return false;
    }
  }

  /**
   * Method that checks if the app version running needs update
   * @returns ```1``` when app is up.to-date. ```0``` when the app can be updated but the update can be postponed. ```-1```
   * when update is mandatory
   */
  private async getVersionStatus(): Promise<number> {
    if (Capacitor.isNativePlatform()) {
      const appInfo: AppInfo = await App.getInfo();
      return await firstValueFrom(this.api.checkCurrentVersion(Capacitor.getPlatform(), appInfo.version).pipe(
        catchError(() => {
          // for some reason, the server is nor reachable, so proceed to app
          return of(1);
        }),
      ));
    } else {
      return 1;
    }
  }

  /**
   * Show a dialog to alert user to update version
   * @param needUpdate false to update later, true for a mandatory update
   * @param title dialog title
   * @param subtitle dialog subtitle
   */
  private async showVersionAlert(needUpdate: boolean, title: string, subtitle: string) {
    const buttons: AlertButton[] = [
      { text: this.translate.instant('VERSION.LATER'), role: 'cancel' },
      {
        text: this.translate.instant('VERBS.ACCEPT'),
        handler: async () => {
          try {
            await NativeMarket.openStoreListing({ appId: 'com.mysphera.paido' });
            alert.dismiss(false);
          }
          catch (error) { console.error(error); }
        },
      },
    ];

    if (needUpdate) {
      // remove the "later" button
      buttons.shift();
    }

    const alert = await this.alertController.create({
      header: title,
      message: subtitle,
      backdropDismiss: false,
      buttons,
    });
    await alert.present();
  }
}
