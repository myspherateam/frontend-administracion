import { AlertController, MenuController, Platform } from '@ionic/angular';
import { Channel, ListChannelsResult, PermissionStatus, PushNotifications, Token } from '@capacitor/push-notifications';
import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { filter, firstValueFrom, map } from 'rxjs';
import { Capacitor } from '@capacitor/core';
import { Health } from '@awesome-cordova-plugins/health/ngx';

import {
  AnimalMap, ApiResponse, Badge, CategoriesCount, ChallengeCompletion, ColorMap, NotificationDevice, PendingCount, Step, User, displayNameComponents,
} from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_CHALLENGES_URL } from '../api/api-config';
import { PAIDO_EVENTS } from '../utils/constants';
import { StepNames } from '../models/step-names';
import { getLocalizedChildUsername } from '../utils/username';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage extends AnalyzableComponent implements OnInit, OnDestroy {

  protected screenName: string = 'home';

  public stepsText: Map<string, string>;

  public steps:Array<Step> = [
    {
        index: 4,
        name: "mantenlo!",
        key: "keep",
        width: 40,
        pendingCount: {
            count: 0,
            subcategories: {
                challenges: 0,
                news: 0,
                questionnaires: 0,
                activities: 0
            }
        }
    },
    {
        index: 3,
        name: "disfruta!",
        key: "enjoy",
        width: 52,
        pendingCount: {
            count: 0,
            subcategories: {
                challenges: 0,
                news: 0,
                questionnaires: 0,
                activities: 0
            }
        }
    },
    {
        index: 2,
        name: "evita!",
        key: "avoid",
        width: 64,
        pendingCount: {
            count: 0,
            subcategories: {
                challenges: 0,
                news: 0,
                questionnaires: 0,
                activities: 0
            }
        }
    },
    {
        index: 1,
        name: "actívate!",
        key: "activate",
        width: 76,
        pendingCount: {
            count: 0,
            subcategories: {
                challenges: 0,
                news: 0,
                questionnaires: 0,
                activities: 0
            }
        }
    },
    {
        index: 0,
        name: "prepárate!",
        key: "prepare",
        width: 88,
        pendingCount: {
            count: 0,
            subcategories: {
                challenges: 0,
                news: 0,
                questionnaires: 0,
                activities: 0
            }
        }
    }
]

  categoryCount: CategoriesCount = {
    count: 0, 
    subcategories:{
      challenges: 0,
      activities:0,
      news: 0,
      questionnaires:0,
    },
  };

  private pendingCountStep: CategoriesCount[] = Array(5).fill(this.categoryCount);

  public user!: User;

  public displayName!: string;

  public displayNumber!: string;

  public animal?: string;

  public color?: string;

  // color variables for circle progress component. White color by default.
  public outerStrokeColor = '#ffffff';

  public innerStrokeColor = '#ffffff';

  public progressPercent = 0;

  public progressLabel = "";

  private childId?: string;

  public pointProgress = 0;

  public userPoints: number = 0;

  public activeChallenges:ChallengeCompletion[] = [];

  public showBadgeProgress: boolean = false;

  public nextBadgeId: string = '';
  public isParent: boolean = false;

  public havePendingChallenges: boolean = false;
  public isInitialized: boolean = false;

  public relativesPendingCount: number = 0;

  public challengesUrl: string = inject(PAIDO_CHALLENGES_URL);

  constructor(
    public router: Router,
    private menuController: MenuController,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private health: Health,
    private platform: Platform,
    private alertController: AlertController,
    private events: Events,
  ) {
    super();
    this.stepsText = new StepNames(this.translate).names;
  }

  override async ngOnInit(): Promise<void> {
    super.ngOnInit();
    try{
      this.isInitialized = true;
      this.childId = this.localStorage.getFromLocalStorage<string>('childId');
      if (this.localStorage.containsKey('user')) {
        this.user = this.localStorage.getFromLocalStorage<User>('user') as User;
        if (this.localStorage.containsKey('childDisplayId')) {
          // user is a parent that entered like a child
          const name = this.localStorage.getFromLocalStorage<string>('childDisplayId') || '';
          const parsed = displayNameComponents(String(name));
          this.animal = AnimalMap.get(parsed[0]);
          this.color = ColorMap.get(parsed[1]);
          this.displayName = getLocalizedChildUsername(this.translate, name) || '';
        } else {
          // user is parent or unique child
          this.displayName = String(this.user.displayId);
          if(this.childId) this.isParent = true;
        }
      }
  
      this.apiService.hasOneUser.next({ hasOneUser: this.localStorage.getFromLocalStorage('hasOneUser') || false });
      await this.menuController.enable(true);

      this.getUserPoints();
      this.generateSteps();
      this.generateProgress();
      this.getRelativesPendingChallenges();
      this.getAllChallenges();
      this.subscribePushNotifications();
    }catch(e){
      this.isInitialized = false;
      console.log(JSON.stringify(e));
    }

  }

  ionViewDidEnter() {
    if(!this.isInitialized){
      this.ngOnInit();
    }
  }

  override ngOnDestroy(): void {
    this.isInitialized = false;
    super.ngOnDestroy();
  }

  buildStairs(){
    if(this.stepsText && this.pendingCountStep){
      const isLargeScreen = this.platform.is('ipad') || this.platform.is('tablet');
      let width = (isLargeScreen) ? 75 : 100;
      this.steps = Array.from(this.stepsText.entries()).map(([key, value]: string[], index: number) => {
        width -= (isLargeScreen) ? 15 : 12;
        return {
          index: index,
          name: value,
          key,
          width,
          pendingCount: this.pendingCountStep[index],
        } as Step;}).reverse();
    }
  }

  getUserPoints() {
    if (this.childId || this.user.roles.includes('Child')) {
      this.showBadgeProgress = true;
      this.apiService.getNextBadge('points', this.childId).subscribe({
        next: (result: ApiResponse<Badge>) => {
          try{
            if (result.status === 200) {
              const badge = result.message;
              const currentProgress = badge.currentProgress || 0;
              const nextMilestone = badge.nextMilestone || 0;
              this.nextBadgeId = badge.id;
  
              this.userPoints = currentProgress;
              if (currentProgress > 0 && nextMilestone > 0) {
                this.pointProgress = currentProgress / nextMilestone;
              }
            }
          }catch{
            this.showBadgeProgress = false;
          }
        },
      });
    } else {
      this.showBadgeProgress = false;
    }
  }

  async generateSteps() {
      
    // get pending count home steps
    let apiResponse: ApiResponse<PendingCount> | undefined = undefined;
    try{
      apiResponse = await firstValueFrom(this.apiService.getPendingChallengeProgramming(this.childId));
    }catch{
      apiResponse = undefined;
    }
    if (apiResponse&&apiResponse.status === 200) {
      const pendingCount: PendingCount = apiResponse.message;
      this.pendingCountStep = [
        pendingCount.prepare,
        pendingCount.activate,
        pendingCount.avoid,
        pendingCount.enjoy,
        pendingCount.keep,
      ];
      this.localStorage.storeOnLocalStorage('pendingCountStep', this.pendingCountStep);

    }else if(this.localStorage.containsKey('pendingCountStep')){
      this.pendingCountStep = this.localStorage.getFromLocalStorage<CategoriesCount[]>('pendingCountStep') as CategoriesCount[];
    }
    this.buildStairs();
    
  }

  onStepClick(step: Step) {
    this.router.navigateByUrl('/step-detail', { state: { step } });
  }

  goToProfile() {
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user,
      },
    };
    this.router.navigateByUrl('/profile', navigationExtras);
  }

  /**
   * Get user steps and total steps from next challenge to accomplish
   */
  private generateProgress() {
    if(this.isParent){
      this.getPhysicalSteps();
    }else{
      this.showChallengesProgress(this.activeChallenges);
    }
  }

  private getAllChallenges() {
    this.apiService.getCompletionChallenges(undefined, undefined, undefined, undefined, this.childId)
      .pipe(
        filter((response: ApiResponse<ChallengeCompletion[]>) => response.status === 200),
        map((response: ApiResponse<ChallengeCompletion[]>) => response.message),
      )
      .subscribe({
        next: (challenges: ChallengeCompletion[]) => {
          this.events.publish(PAIDO_EVENTS.CHALLENGE_LIST_RECEIVED, challenges);
        },
      });
  }

  /**
   * Check if cordova platform and health platform are ready to request data
   */
  private async getPhysicalSteps() {
    if (Capacitor.isNativePlatform()) {
      this.apiService.getNextPhysicalCompletion(this.childId).subscribe({
        next:
          (response: ApiResponse<ChallengeCompletion[]>) => {
            if (response.message) {
              const challenges: ChallengeCompletion[] = response.message;
              let totalSteps = 0;
              challenges.forEach((completion: ChallengeCompletion) => {
                totalSteps = completion.steps;
              });
              this.getHealthData(totalSteps);
            }
          },
      });
    }
  }

  /**
  * Get user steps from today
  */
  private getHealthData(totalSteps: number) {
    const todayMidnight: Date = new Date();
    todayMidnight.setHours(0, 0, 0, 0);

    this.health
      .query({
        startDate: new Date(todayMidnight.getTime()),
        endDate: new Date(),
        dataType: 'steps',
        limit: 1000,
      })
      .then((data) => {
        if (data && data.length > 0 && totalSteps > 0) {
          let stepValue = 0;
          for (const healthData of data) {
            stepValue += parseInt(healthData.value, 10);
          }
          // get percent from user steps and total challenge steps
          this.showStepsProgress(stepValue, totalSteps);
        } else {
          // there isn't steps data yet. Hide progress.
          this.hideStepsProgress();
        }
        
      })
      .catch((error) => console.error('Data error: ' + error));
  }

  /**
   * Hide steps progress component
   */
  private hideStepsProgress() {
    this.innerStrokeColor = '#ffffff';
    this.outerStrokeColor = '#ffffff';
  }

  /**
   * Get a percentage from user steps done
   * and show it in the progress component
   */
  private showStepsProgress(userSteps: number, totalSteps: number) {
    this.progressPercent = Math.round((userSteps * 100) / totalSteps);

    if (this.progressPercent < 100) {
      this.innerStrokeColor = '#d8e9eb';
      this.outerStrokeColor = '#64d2dc';
    } else {
      this.innerStrokeColor = '#bce8c8';
      this.outerStrokeColor = '#32a852';
    }
    this.progressLabel = `${userSteps}/${totalSteps} ${this.translate.instant('HOME.STEPS')}`
  }

private showChallengesProgress(challenges:ChallengeCompletion[]){
  if(challenges.length){
    const completed = challenges.filter(e=>e.completed).length
    this.progressPercent = Math.round((completed / challenges.length)*100);
    if (this.progressPercent < 100) {
      this.innerStrokeColor = '#d8e9eb';
      this.outerStrokeColor = '#64d2dc';
    } else {
      this.innerStrokeColor = '#bce8c8';
      this.outerStrokeColor = '#32a852';
    }
    this.progressLabel = `${completed}/${challenges.length} ${this.translate.instant('HOME.STEPS')}`
  }
}

  /**
   * Get pending challenges to complete by user's relatives
   */
  private getRelativesPendingChallenges() {
    this.apiService.getRelativesPendingChallenges(this.childId).subscribe({
      next:
        (result: ApiResponse<ChallengeCompletion[]>) => {
          if (result.status === 200) {
            const relativeChallenges = result.message;
            if (relativeChallenges.length > 0) {
              this.havePendingChallenges = true;
              this.relativesPendingCount = relativeChallenges.length;
            } else {
              this.havePendingChallenges = false;
            }
          }
        },
    });
  }

  /**
   * Show an alert to say to user that their family have pending challenges
   */
  async showPendingChallengesAlert() {
    const alert = await this.alertController.create({
      header: this.translate.instant('HOME.PENDING_CHALLENGES'),
      message: this.translate.instant('HOME.FAMILY_CHALLENGES'),
      buttons: [
        {
          text: this.translate.instant('VERBS.ACCEPT'),
        },
      ],
    });

    await alert.present();
  }

  private subscribePushNotifications() {
    if (!Capacitor.isNativePlatform()) {
      return;
    }
    const pushPermRequested = this.localStorage.getFromLocalStorage<boolean>('push.permission.requested') || false;
    if (!pushPermRequested){
      console.log('Evento registro')
      PushNotifications.requestPermissions().then((result: PermissionStatus) => {
        if (result.receive === 'granted') {
          PushNotifications.register();
        }
      });
    }
    if (this.user.roles.includes('Parent')) {
      const parentId = this.user.id;
      const childrenIds = this.user.caringForIds;

      PushNotifications.addListener('registration', (token: Token) => {
        console.log('Evento listener', token)
        childrenIds.forEach(childId=>this.registerOrRegenerateToken(token.value, childId))
      });
      
    }else{
      PushNotifications.addListener('registration', (token: Token) => {
        console.log('Evento listener 2', token)
        this.registerOrRegenerateToken(token.value, this.user.id)
      });
    }

  }

  private registerOrRegenerateToken(token:string, userId:string){
    const storedToken = this.localStorage.getFromLocalStorage<string>("notification.device.token")
    if(!storedToken){ 
      this.apiService.registerNotificationsDevice(token, userId).subscribe({
        next:(result:ApiResponse<NotificationDevice>)=>{
          console.log(result.status)
          if(result.status===200){
            this.localStorage.storeOnLocalStorage("notification.device.token",token)
          }
        }
      })
    }else if(storedToken!==token){
      this.apiService.updateNotificationsDevice(storedToken, token, userId).subscribe({
        next:(result:ApiResponse<NotificationDevice>)=>{
          if(result.status===200){
            this.localStorage.storeOnLocalStorage("notification.device.token",token)
          }
          else if(result.status===404){
            this.apiService.registerNotificationsDevice(token, userId).subscribe({
              next:(result:ApiResponse<NotificationDevice>)=>{
                console.log(result.status)
                if(result.status===200){
                  this.localStorage.storeOnLocalStorage("notification.device.token",token)
                }
              }
            })
          }
        }
      })
    }
  }
}
