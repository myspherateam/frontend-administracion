import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { HelpButtonComponent } from './help-button/help-button.component';
import { HomePage } from './home.page';
import { PipesModule } from '../pipes/pipes-module';
import { UserIconComponent } from '../user-icon/user-icon.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage,
      },
    ]),
    NgCircleProgressModule.forRoot({
      radius: 70,
      space: -8,
      outerStrokeWidth: 8,
      innerStrokeWidth: 8,
      animation: false,
      showUnits: false,
      showTitle: false,
      showSubtitle: false,
      imageSrc: 'assets/icono-paido.PNG',
      imageWidth: 80,
      imageHeight: 90,
      showImage: true,
      showBackground: false,
      maxPercent: 100,
      responsive: true,
    }),
    TranslateModule,
    PipesModule,
    UserIconComponent,
  ],
  declarations: [HomePage, HelpButtonComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class HomePageModule { }
