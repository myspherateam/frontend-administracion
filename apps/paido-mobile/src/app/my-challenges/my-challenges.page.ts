import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSelect, MenuController } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';

import { ApiResponse, ChallengeCompletion } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { LocalStorageService } from '../services/storage-service';

@Component({
  selector: 'app-my-challenges',
  templateUrl: './my-challenges.page.html',
  styleUrls: ['./my-challenges.page.scss'],
})
export class MyChallengesPage extends AnalyzableComponent implements OnInit {
  protected screenName: string = 'my-challenges';

  public challengeSubcategory: string = '';

  public challengeCategory: string = '';

  public isLoading: boolean = false;

  public challenges: ChallengeCompletion[] = [];

  public allChallenges: ChallengeCompletion[] = [];

  public type?: string = 'all';

  public finished = false;

  public completed = false;

  @ViewChild('challengeTypeFilter', { static: false }) challengeTypeSelect!: IonSelect;

  constructor(
    private localStorage: LocalStorageService,
    private apiService: ApiService,
    private router: Router,
    private menuController: MenuController,
  ) {
    super();
  }

  override async ngOnInit(): Promise<void> {
    super.ngOnInit();
    this.menuController.enable(true);
    this.getChallenges();
  }

  getChallenges(type?: string) {
    const userId: string | undefined = this.localStorage.getFromLocalStorage<string>('childId');
    //if (this.localStorage.containsKey('childId')) {
    // const childId: string | undefined = this.localStorage.getFromLocalStorage<string>('childId');
    this.apiService.getCompletionChallenges(
      undefined,
      undefined,
      type,
      this.finished,
      userId,
      this.completed,
    ).subscribe({
      next:
        (response: ApiResponse<ChallengeCompletion[]>) => {
          if (response.status === 200) {
            this.allChallenges = response.message;
            this.challenges = response.message;
            this.isLoading = false;
          }
        },
      error: error => console.error('Error challenges: ' + error),
    });
    // }
  }

  onChallengeClick(challenge: ChallengeCompletion) {
    // go to challenge detail
    const navigationExtras: NavigationExtras = {
      state: {
        challenge: challenge.id,
      },
    };

    this.router.navigateByUrl('/challenge-detail', navigationExtras);
  }

  onSearch(ev: Event) {
    this.isLoading = true;
    // filter list
    const value = (ev as CustomEvent).detail.value;

    if (value) {
      if (value.trim !== '') {
        this.challenges = this.allChallenges.filter(term => {
          return term.title.toLowerCase().indexOf(value.trim().toLowerCase()) > -1;
        });
      }
      this.isLoading = false;
    } else {
      this.challenges = this.allChallenges;
      this.isLoading = false;
    }
  }

  filterList(ev: Event) {
    // get filter list selected
    this.completed = false;
    this.finished = false;
    const listType = (ev as CustomEvent).detail.value;
    this.finished = listType === 'past' ? true : false;
    this.completed = listType === 'completed' ? true : false;

    // reload data with new filters
    this.getChallenges(this.type);
  }

  onFiltersClick() {
    this.challengeTypeSelect.open();
  }

  filterByType(ev: Event) {
    // filter by challenge type
    this.type = (ev as CustomEvent).detail.value;
    if (this.type === 'all') { this.type = undefined; }
    this.getChallenges(this.type);
  }
}
