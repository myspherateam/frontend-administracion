import { TestBed } from '@angular/core/testing';

import { AppVersionGuard } from './app-version.guard';

describe('AppVersionGuard', () => {
  let guard: AppVersionGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AppVersionGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
