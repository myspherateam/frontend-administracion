import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

import { ChallengesListComponent } from './challenges-list.component';
import { ChallengesListPageRoutingModule } from './challenges-list-routing.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChallengesListPageRoutingModule,
    TranslateModule,
  ],
  declarations: [ChallengesListComponent],
})
export class ChallengesListPageModule { }
