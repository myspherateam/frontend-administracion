import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IonSelect, MenuController } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';

import { ApiResponse, ChallengeCompletion, ChallengeType } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_EVENTS } from '../utils/constants';
import { getLocalizedChildUsername } from '../utils/username';

@Component({
  selector: 'app-challenges-list',
  templateUrl: './challenges-list.component.html',
  styleUrls: ['./challenges-list.component.scss'],
})
export class ChallengesListComponent extends AnalyzableComponent implements OnInit, OnDestroy {
  protected screenName: string = 'challenges-list';

  private readonly CHALLENGE = 'challenges';

  private readonly ACTIVITIES = 'activities';

  private readonly NEWS = 'news';

  private readonly QUESTIONNAIRE = 'questionnaires';

  public challenges: Array<ChallengeCompletion> = [];

  public allChallenges: Array<ChallengeCompletion> = [];

  public challengeSubcategory: string = '';

  public challengeCategory: string = '';

  public toolbarTitle: string = '';

  public isLoading: boolean = false;

  public type: string | null = 'all';

  public finished = false;

  public completed = false;

  @ViewChild('challengeTypeFilter', { static: false }) challengeTypeSelect!: IonSelect;

  public isParent: boolean;

  constructor(
    public router: Router,
    private challengeService: ApiService,
    private menuController: MenuController,
    private localStorage: LocalStorageService,
    private events: Events,
  ) {
    super();
    this.menuController.enable(false);
    const state = this.router.getCurrentNavigation()?.extras?.state as { subcategory: string, step: string };
    this.challengeSubcategory = state?.subcategory;
    this.challengeCategory = state?.step;

    this.isParent = this.localStorage.getFromLocalStorage('childId') === undefined;
  }

  override async ngOnInit() {
    await super.ngOnInit();
    this.setContentData();
    this.events.subscribe(PAIDO_EVENTS.CHALLENGE_COMPLETED, () => {
      this.getChallenges();
      this.events.publish(PAIDO_EVENTS.CHALLENGE_COUNT_CHANGED, {});
    });
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
    this.events.destroy(PAIDO_EVENTS.CHALLENGE_COMPLETED);
  }

  setContentData() {
    this.isLoading = true;
    switch (this.challengeSubcategory) {
      case this.CHALLENGE:
        this.toolbarTitle = this.translate.instant('STEP_DETAIL.CHALLENGES');
        break;
      case this.ACTIVITIES:
        this.toolbarTitle = this.translate.instant('STEP_DETAIL.ACTIVITIES');
        break;
      case this.NEWS:
        this.toolbarTitle = this.translate.instant('STEP_DETAIL.NEWS');
        break;
      case this.QUESTIONNAIRE:
        this.toolbarTitle = this.translate.instant('STEP_DETAIL.QUESTIONNAIRES');
        break;
    }
    this.getChallenges();
  }

  getChallenges(type?: string) {
    const childId: string | undefined = this.localStorage.getFromLocalStorage<string>('childId');

    this.challengeService.getCompletionChallenges(
      this.challengeCategory,
      this.challengeSubcategory,
      type,
      this.finished,
      childId,
      this.completed,
    ).subscribe({
      next: (response: ApiResponse<ChallengeCompletion[]>) => {
        if (response.status === 200) {
          this.allChallenges = response.message;
          this.challenges = response.message;
          this.isLoading = false;
        }
      },
      error: error => console.error('Error challenges: ' + error),
    });
  }

  onChallengeClick(challenge: ChallengeCompletion) {
    // go to challenge detail
    const navigationExtras: NavigationExtras = {
      state: {
        challenge: challenge.id,
      },
    };

    this.router.navigateByUrl('/challenge-detail', navigationExtras);
  }

  /**
   * Check if challenge start date is today
   */
  isStartDateToday(challenge: ChallengeCompletion): boolean {
    const challengeStartDate: Date = new Date(challenge.periodStartDatetime);
    const today: Date = new Date();
    return challengeStartDate.setHours(0, 0, 0, 0) === today.setHours(0, 0, 0, 0);
  }

  /**
   * FILTER METHODS
   */

  onSearch(ev: Event) {
    this.isLoading = true;
    // filter list
    const value = (ev as CustomEvent).detail.value;

    if (value) {
      if (value.trim !== '') {
        this.challenges = this.allChallenges.filter(term => {
          return term.title.toLowerCase().indexOf(value.trim().toLowerCase()) > -1;
        });
      }
      this.isLoading = false;
    } else {
      this.challenges = this.allChallenges;
      this.isLoading = false;
    }
  }

  filterList(ev: Event) {
    // get filter list selected
    this.completed = false;
    this.finished = false;
    const listType = (ev as CustomEvent).detail.value;
    this.finished = listType === 'past' ? true : false;
    this.completed = listType === 'completed' ? true : false;

    // reload data with new filters
    this.getChallenges(this.type as string);
  }

  onFiltersClick() {
    this.challengeTypeSelect.open();
  }

  filterByType(ev: Event) {
    // filter by challenge type
    this.type = (ev as CustomEvent).detail.value;
    if (this.type === 'all') { this.type = null; }
    this.getChallenges(this.type as string);
  }

  challengeIsOnTime(challenge: ChallengeCompletion): boolean {
    const finishDatetime = this.getFormattedDateInMillis(
      challenge.periodFinishDatetime,
    );
    const startDatetime = this.getFormattedDateInMillis(
      challenge.periodStartDatetime,
    );
    const actualDatetime = new Date().getTime();
    return actualDatetime >= startDatetime && actualDatetime < finishDatetime;
  }

  getFormattedDateInMillis(dateString: string): number {
    return new Date(dateString).getTime();
  }

  checkHaveToAuthorize(challenge: ChallengeCompletion): boolean {
    // check if challenge is authority type, user is the parent and challenge has started
    return challenge.type === ChallengeType.authority &&
      this.isParent &&
      this.challengeIsOnTime(challenge);
  }

  translateUsername(username: string) {
    return getLocalizedChildUsername(this.translate, username);
  }
}
