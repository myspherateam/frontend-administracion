import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ChallengesListComponent } from './challenges-list.component';

const routes: Routes = [
  {
    path: '',
    component: ChallengesListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChallengesListPageRoutingModule { }
