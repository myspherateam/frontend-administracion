import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { firstValueFrom } from 'rxjs';

import { ApiResponse, PendingCount, Step, SubcategoriesCount } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_EVENTS } from '../utils/constants';

@Component({
  selector: 'app-step-detail',
  templateUrl: './step-detail.page.html',
  styleUrls: ['./step-detail.page.scss'],
})
export class StepDetailPage extends AnalyzableComponent implements OnInit, OnDestroy {

  protected readonly screenName: string = 'step-detail';

  public title: string;

  public key: string;

  public count!: SubcategoriesCount;

  public keyPrepare = 'prepare';

  public keyActivate = 'activate';

  public keyAvoid = 'avoid';

  public keyEnjoy = 'enjoy';

  public keyKeep = 'keep';

  constructor(
    private router: Router,
    private menuController: MenuController,
    private apiService: ApiService,
    private events: Events,
    private localStorage: LocalStorageService,
  ) {
    super();
    this.title = (this.router.getCurrentNavigation()?.extras as { state: { step: Step } }).state.step.name;
    this.key = (this.router.getCurrentNavigation()?.extras as { state: { step: Step } }).state.step.key;
  }

  override async ngOnInit() {
    super.ngOnInit();
    this.menuController.enable(false);
    this.getCount();
    this.events.subscribe(PAIDO_EVENTS.CHALLENGE_COUNT_CHANGED, () => {
      this.getCount();
    });
  }

  override  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.events.destroy(PAIDO_EVENTS.CHALLENGE_COUNT_CHANGED);
    this.menuController.enable(true);
  }

  async getCount() {
    const childId: string | undefined = this.localStorage.getFromLocalStorage('childId');

    const apiResponse: ApiResponse<PendingCount> = await firstValueFrom(this.apiService.getPendingChallengeProgramming(childId));
    if (apiResponse.status === 200) {
      const pendingCount: PendingCount = apiResponse.message;
      switch (this.key) {
        case this.keyPrepare:
          this.count = pendingCount.prepare.subcategories;
          break;
        case this.keyActivate:
          this.count = pendingCount.activate.subcategories;
          break;
        case this.keyAvoid:
          this.count = pendingCount.avoid.subcategories;
          break;
        case this.keyEnjoy:
          this.count = pendingCount.enjoy.subcategories;
          break;
        case this.keyKeep:
          this.count = pendingCount.keep.subcategories;
          break;
      }
    }
  }

  openChallengesList(subcategory: string) {
    const navigationExtras: NavigationExtras = {
      state: {
        subcategory,
        step: this.key,
      },
    };
    this.router.navigateByUrl('/challenges-list', navigationExtras);
  }
}
