import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { IonicModule } from '@ionic/angular';

import { StepDetailPageRoutingModule } from './step-detail-routing.module';

import { StepDetailPage } from './step-detail.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StepDetailPageRoutingModule,
    TranslateModule,
  ],
  declarations: [StepDetailPage],
})
export class StepDetailPageModule { }
