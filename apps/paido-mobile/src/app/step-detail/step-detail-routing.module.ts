import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { StepDetailPage } from './step-detail.page';

const routes: Routes = [
  {
    path: '',
    component: StepDetailPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StepDetailPageRoutingModule { }
