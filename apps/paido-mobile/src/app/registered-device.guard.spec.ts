import { TestBed } from '@angular/core/testing';

import { RegisteredDeviceGuard } from './registered-device.guard';

describe('RegisteredDeviceGuard', () => {
  let guard: RegisteredDeviceGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RegisteredDeviceGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
