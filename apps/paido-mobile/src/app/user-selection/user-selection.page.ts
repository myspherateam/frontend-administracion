import { Component, OnInit } from '@angular/core';
import { MenuController, PickerColumn, PickerController } from '@ionic/angular';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { firstValueFrom, map } from 'rxjs';
import { Capacitor } from '@capacitor/core';
import { FirebaseCrashlytics } from '@capacitor-community/firebase-crashlytics';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AnimalMap, ApiResponse, ColorMap, User, UserRole } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { LocalStorageService } from '../services/storage-service';

@Component({
  selector: 'app-user-selection',
  templateUrl: './user-selection.page.html',
  styleUrls: ['./user-selection.page.scss'],
})

export class UserSelectionPage extends AnalyzableComponent implements OnInit {

  protected screenName: string = 'user-selection';

  public name: string = '';

  //public nameColumns: any[][] = [];

  private pickerColumns: PickerColumn[];

  public user?: User;

  public get animalColor(): string { return ColorMap.get(this.selection['color']) || ''; }

  public get animalName(): string { return this.selection['animal']?.toLowerCase(); }

  public get userDisplayId(): string { return this.user?.displayId + '' || ''; }

  public selection: Record<string, string> = {};

  public passwordGroup: UntypedFormGroup;

  public registerPassed: boolean = false;

  public usernameAvailable: boolean = false;

  public hasToHidePassword: boolean = true;

  constructor(
    public pickerController: PickerController,
    private router: Router,
    private menuController: MenuController,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private translateService: TranslateService,
  ) {
    super();
    this.menuController.enable(false);
    this.passwordGroup = new UntypedFormGroup(
      {
        passwordControl: new UntypedFormControl(
          null,
          Validators.compose(
            [
              Validators.required,
              Validators.minLength(8),
            ],
          ),
        ),
      },
    );
    this.user = (this.router.getCurrentNavigation()?.extras?.state as { user: User })?.user;
    this.hasToHidePassword = (this.user?.roles.includes(UserRole.Child) || false) &&
      this.localStorage.getFromLocalStorage<boolean>('hasOneUser') === false;

    this.pickerColumns = [
      {
        name: 'animals',
        options: Array.from(AnimalMap.keys()).map((key: string) => {
          return { text: this.translateService.instant(`ANIMALS.${key.toUpperCase()}`), value: key };
        }),
      },
      {
        name: 'colors',
        options: Array.from(ColorMap.keys()).map((key: string) => {
          return { text: this.translateService.instant(`COLORS.${key.toUpperCase()}`), value: key };
        }),
      },
    ];
  }

  async openPicker() {
    const picker = await this.pickerController.create({
      buttons: [
        {
          text: this.translateService.instant('VERBS.CANCEL'),
          role: 'cancel',
        },
        {
          text: this.translateService.instant('VERBS.CONFIRM'),
          handler: (value) => {

            this.selection['animal'] = value.animals.value;
            this.selection['color'] = value.colors.value;

            this.name = `${this.selection['animal']}${this.selection['color']}`;
            this.assertUsernameAvailable(this.name);
          },
        },
      ],
      columns: this.pickerColumns,
    });
    await picker.present();
  }

  /**
   * Localizes the username
   * @returns
   */
  public localizedName(): string | undefined {
    return this.translateService.instant('DISPLAY_NAME', {
      animal: this.translateService.instant(`ANIMALS.${this.selection['animal'].toUpperCase()}`),
      color: this.translateService.instant(`COLORS.${this.selection['color'].toUpperCase()}`),
    });
  }

  private async assertUsernameAvailable(username: string) {
    this.usernameAvailable = await firstValueFrom(this.apiService.checkAvailableDisplayName(username).pipe(map(x => x.message)));
    this.checkRegisterData();
  }

  /**
   * Check if all required data is valid
   */
  checkRegisterData() {
    if (this.user?.roles.includes('Parent')) {
      this.registerPassed = this.passwordGroup.valid;
    } else {
      this.registerPassed = !!this.name && this.usernameAvailable && (this.passwordGroup.valid || this.hasToHidePassword);
    }
  }

  onSelectionDone() {
    const password: string = this.passwordGroup.get('passwordControl')?.value;

    (this.user as User).displayName = this.name;

    (this.user as User).password = password;

    this.apiService.createUser(this.user as User).subscribe({
      next: (userResult: ApiResponse<User>) => {
        if (userResult.status === 200) {
          const user = userResult.message;
          if (!this.hasToHidePassword) {
            this.localStorage.storeOnLocalStorage('password', password);
          }
          this.localStorage.storeOnLocalStorage('user', user);
          this.localStorage.storeOnLocalStorage('isAlreadyRegistered', true);
          this.localStorage.storeOnLocalStorage('childDisplayId', user.displayName);
          this.apiService.hasOneUser.next({ hasOneUser: this.localStorage.getFromLocalStorage('hasOneUser') || false });
          if (user.roles.includes('Parent')) {
            this.router.navigateByUrl('/login');
          } else {
            this.router.navigateByUrl('/home');
          }
        }
      },
      error: async (error) => {
        console.error(error);
        if (Capacitor.isNativePlatform()) {
          const stacktrace = await StackTrace.fromError(error);
          FirebaseCrashlytics.recordException({
            stacktrace,
            message: 'Error registering username for child',
          });
        }
      },
    });
  }
}
