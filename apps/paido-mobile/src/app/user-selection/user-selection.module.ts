import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { UserSelectionPage } from './user-selection.page';
import { UserSelectionPageRoutingModule } from './user-selection-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserSelectionPageRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
  ],
  declarations: [UserSelectionPage],
})
export class UserSelectionPageModule { }
