import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ModalInfoComponent } from './modal-info/modal-info.component';
import { MyInsigniasPage } from './my-insignias.page';
import { MyInsigniasPageRoutingModule } from './my-insignias-routing.module';
import { NgModule } from '@angular/core';
import { PipesModule } from '../pipes/pipes-module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyInsigniasPageRoutingModule,
    TranslateModule,
    PipesModule,
  ],
  declarations: [MyInsigniasPage, ModalInfoComponent],
})
export class MyInsigniasPageModule { }
