import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-info',
  templateUrl: './modal-info.component.html',
  styleUrls: ['./modal-info.component.scss'],
})
export class ModalInfoComponent {

  @Input() public header!: string;

  @Input() public message!: string;

  constructor(
    private modalController: ModalController,
  ) { }

  closeModal() {
    this.modalController.dismiss();
  }
}
