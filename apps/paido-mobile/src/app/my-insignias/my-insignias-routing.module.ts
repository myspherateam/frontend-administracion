import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { MyInsigniasPage } from './my-insignias.page';

const routes: Routes = [
  {
    path: '',
    component: MyInsigniasPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyInsigniasPageRoutingModule { }
