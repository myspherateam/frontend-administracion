import { Component, OnInit, inject } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MenuController } from '@ionic/angular';

import { ApiResponse, Badge, User, UserBadges } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_CHALLENGES_URL } from '../api/api-config'; 
import { PresentatorService } from '../services/presentator.service';

@Component({
  selector: 'app-my-insignias',
  templateUrl: './my-insignias.page.html',
  styleUrls: ['./my-insignias.page.scss'],
})
export class MyInsigniasPage extends AnalyzableComponent implements OnInit {
  protected screenName: string = 'my-insignias';

  public badges: Badge[] = [];

  public futureBadges: Badge[] = [];

  public challengesUrl: string = inject(PAIDO_CHALLENGES_URL);

  constructor(
    private menuController: MenuController,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private presentatorService: PresentatorService,
  ) {
    super();
  }

  override async ngOnInit(): Promise<void> {
    super.ngOnInit();
    this.menuController.enable(true);
    this.getMyBadges();
  }

  showHelp() {
    this.presentatorService.showHelpAlert(
      // TODO: add translation
      this.translate.instant('BADGES.HELP_INFO_BADGES_TITLE'),
      this.translate.instant('BADGES.HELP_INFO_BADGES_HTML'),
    );
  }

  getMyBadges() {

    const user = this.localStorage.getFromLocalStorage<User>('user') as User;
    let userId = user.id;
    const childId: string | undefined = this.localStorage.getFromLocalStorage<string>('childId');
    if (childId) { userId = childId; }

    this.apiService.getUserBadges(userId, childId).subscribe({
      next: (result: ApiResponse<UserBadges>) => {
        if (result.status === 200) {
          this.badges = result.message.badges || [];
          this.futureBadges = result.message.futureBadges || [];
        }
      },
      error: (error: HttpErrorResponse) => console.error(error),
    });
  }
}
