import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

@Component({
  selector: 'app-user-icon',
  standalone: true,
  imports: [CommonModule, IonicModule],
  templateUrl: './user-icon.component.html',
  styleUrls: ['./user-icon.component.scss'],
})
export class UserIconComponent implements OnInit {
  @Input() animal?: string;

  @Input() color?: string = 'black';

  @Input() size?: number;

  /**
   * If true, the icon will show a shadow
   */
  @Input() shadow: boolean = true;

  @HostBinding('style.border-color') borderColor?: string;

  @HostBinding('style.width') width?: string;

  @HostBinding('style.height') height?: string;

  @HostBinding('style.filter') filter?: string;

  ngOnInit(): void {
    this.color = this.color ?? 'black';
    this.borderColor = this.color ?? 'black';
    if (this.color) {
      this.width = `${this.size}px`;
      this.height = `${this.size}px`;
    }
    if (this.shadow) {
      this.filter = `drop-shadow(2px 5px 2px ${this.color})`;
    } else { this.filter = undefined; }
  }
}
