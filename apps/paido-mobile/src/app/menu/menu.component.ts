import { Component, EventEmitter, OnDestroy, OnInit, Output, inject } from '@angular/core';
import { Browser } from '@capacitor/browser';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { User } from '@paido/data-model';

import { ApiService } from '../api/api-service';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_EVENTS } from '../utils/constants';
import { RouterModule } from '@angular/router';
import { environment } from '../../environments/environment';

interface MenuItem {
  title: string;
  url?: string;
  icon: string;
  forParent: boolean;
  forChild: boolean;
}

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [CommonModule, IonicModule, TranslateModule, RouterModule],
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, OnDestroy {

  public appPages!: MenuItem[];

  public allOptions: MenuItem[] = [
    {
      title: 'HOME.STAIR',
      url: '/home',
      icon: 'home',
      forParent: true,
      forChild: true,
    },
    {
      title: 'HOME_MENU.MY_INSIGNIAS',
      url: '/my-insignias',
      icon: 'ribbon',
      forParent: true,
      forChild: true,
    },
    {
      title: 'HOME_MENU.MY_CHALLENGES',
      url: '/my-challenges',
      icon: 'clipboard',
      forParent: true,
      forChild: true,
    },
    {
      title: 'HOME_MENU.RANKING',
      url: '/ranking',
      icon: 'podium',
      forParent: false,
      forChild: true,
    },
    {
      title: 'HOME_MENU.PRIVACY_POLICY',
      icon: 'document-text',
      forParent: true,
      forChild: true,
    },
    {
      title: 'HOME_MENU.SETTINGS',
      url: '/settings',
      icon: 'settings',
      forParent: true,
      forChild: true,
    },
    {
      title: 'HOME_MENU.LOG_OUT',
      icon: 'log-out',
      forParent: true,
      forChild: true,
    },
  ];

  private hasOneUser: boolean = true;

  // public isAParent: boolean = false;

  private localStorage = inject(LocalStorageService);

  private apiService = inject(ApiService);

  private events: Events = inject(Events);

  private baseUrl: string = environment.baseUrl;

  /**
   * Event emitted when the user clicks on the logout menu item
   */
  @Output()
  public logout: EventEmitter<void> = new EventEmitter();

  ngOnInit(): void {
    this.apiService.hasOneUser.subscribe({
      next: (result) => {
        if (result) {
          this.localStorage.storeOnLocalStorage('hasOneUser', result.hasOneUser);
        }
        this.hasOneUser = this.localStorage.getFromLocalStorage<boolean>('hasOneUser') || false;
      },
    });

    this.checkMenu();

    // subscribe to role change event from login page
    this.events.subscribe(PAIDO_EVENTS.ROLE_CHANGE, () => {
      this.checkMenu();
    });
  }

  ngOnDestroy(): void {
    this.events.destroy(PAIDO_EVENTS.ROLE_CHANGE);
  }

  onMenuItemClick(page: MenuItem) {
    switch (page.title) {
      case 'HOME_MENU.LOG_OUT':
        this.logout.emit();
        break;
      case 'HOME_MENU.PRIVACY_POLICY':
        Browser.open({ url: `${this.baseUrl}/assets/privacy.pdf`, windowName: '_system' });
        break;
    }
  }

  /**
   * Check if user role has changed to show other menu list
   */
  private checkMenu() {
    const isAParent = this.checkIsParent();

    this.appPages = this.allOptions
      // keep these options that apply to each profile
      .filter((option) => (isAParent) ? option.forParent : option.forChild)
      // if just a single user, remove the logout option
      .filter((option) => option.title.includes('LOG_OUT') ? !this.hasOneUser : true);
  }

  /**
 * Check if user is a parent or a child
 */
  checkIsParent() {
    return this.localStorage.containsKey('user') && this.localStorage.getFromLocalStorage<User>('user')?.roles.includes('Parent');
  }
}
