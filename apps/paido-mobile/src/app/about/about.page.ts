import { App, AppInfo } from '@capacitor/app';
import { Component, OnInit } from '@angular/core';
import { Capacitor } from '@capacitor/core';

import { AnalyzableComponent } from '../utils/analyzable-compoment';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage extends AnalyzableComponent implements OnInit {

  protected screenName = 'about';

  public versionNumber!: string;

  override  async ngOnInit() {

    if (Capacitor.isNativePlatform()) {
      App.getInfo().then((info: AppInfo) => {
        this.versionNumber = `${info.version}.${info.build}`;
      });
    } else {
      this.versionNumber = this.translate.instant('ABOUT.NOT_AVAILABLE');
    }
  }
}
