import { Component, OnInit } from '@angular/core';
import { StatusBar, Style } from '@capacitor/status-bar';
import { AlertController } from '@ionic/angular';
import { App } from '@capacitor/app';
import { Capacitor } from '@capacitor/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@capacitor/splash-screen';
import { TranslateService } from '@ngx-translate/core';

import { LocalNotificationService } from './services/local-notification.service';
import { LocalStorageService } from './services/storage-service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: [
    '../theme/variables.scss',
    'app.component.scss'],
})
export class AppComponent implements OnInit {

  constructor(
    //private platform: Platform,
    private router: Router,
    private localStorage: LocalStorageService,
    private alertController: AlertController,
    private translateService: TranslateService,
    private localNotification: LocalNotificationService,
  ) { }

  ngOnInit() {
    // checks the param that displays the diferent menus depending if the user has multiple access
    this.translateService.use(this.translateService.getBrowserLang() || '');
    this.translateService.setDefaultLang('es');
    if (Capacitor.isNativePlatform()) {
      StatusBar.setStyle({ style: Style.Light });
      SplashScreen.hide();
      StatusBar.show();
      StatusBar.setStyle({ style: Style.Light });

      App.addListener('backButton', () => {
        const currentUrl = this.router.url;
        if (
          currentUrl.includes('/home') ||
          currentUrl.includes('/login') ||
          currentUrl.includes('/register') ||
          currentUrl.includes('/user-selection')
        ) {
          App.exitApp();
        }
      });
    } else {
      // TBI
    }

    return this.router.navigateByUrl('/login');
  }

  async showAlertLogout() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('CHANGE_USER_DIALOG.HEADER'),
      message: this.translateService.instant('CHANGE_USER_DIALOG.MESSAGE'),
      buttons: [
        {
          text: this.translateService.instant('VERBS.CANCEL'),
        },
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            this.logout();
          },
        },
      ],
    });

    await alert.present();
  }

  logout() {
    if (this.localStorage) {
      this.localStorage.storeOnLocalStorage('signIn', false);
      // replace url forces a new state and therefore, destroys previous component
      this.router.navigateByUrl('/login', { replaceUrl: true });
    }
  }
}
