import { AlertController, MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AnalyzableComponent } from '../utils/analyzable-compoment';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage extends AnalyzableComponent implements OnInit {
  protected screenName: string = 'help';

  public question: string = '';

  public email: string = '';

  constructor(
    private router: Router,
    public alertController: AlertController,
    private menuController: MenuController,
    private translateService: TranslateService,
  ) {
    super();
    this.menuController.enable(false);
  }

  sendHelp() {
    this.presentAlert();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('HELP.DIALOG_HEADER'),
      subHeader: this.translateService.instant('HELP.DIALOG_SUBHEADER'),
      message: this.translateService.instant('HELP.DIALOG_MESSAGE'),
      buttons: [{
        text: this.translateService.instant('VERBS.ACCEPT'),
        handler: () => {
          this.router.navigateByUrl('home');
        },
      }],
    });

    alert.present();
  }
}
