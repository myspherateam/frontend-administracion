import { ErrorHandler, Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { FirebaseCrashlytics } from '@capacitor-community/firebase-crashlytics';
import { HttpErrorResponse } from '@angular/common/http';
import { fromError } from 'stacktrace-js';

@Injectable({
  providedIn: 'root',
})
export class GlobalErrorHandlerService implements ErrorHandler {

  async handleError(error: Error | HttpErrorResponse): Promise<void> {
    console.log('*************  Global error handler  ***********************');
    console.error(error);
    console.log('************************************************************');
    if (Capacitor.isNativePlatform() && Capacitor.isPluginAvailable('FirebaseCrashlytics')) {
      const trace = await fromError(error);
      FirebaseCrashlytics.recordException({
        message: error.name || error.message || 'Generic error',
        stacktrace: trace,
      });
    }
  }
}
