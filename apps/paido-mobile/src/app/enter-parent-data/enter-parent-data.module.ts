import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

import { EnterParentDataPage } from './enter-parent-data.page';
import { EnterParentDataPageRoutingModule } from './enter-parent-data-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnterParentDataPageRoutingModule,
  ],
  declarations: [EnterParentDataPage],
})
export class EnterParentDataPageModule { }
