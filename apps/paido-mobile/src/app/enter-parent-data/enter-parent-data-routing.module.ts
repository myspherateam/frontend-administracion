import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EnterParentDataPage } from './enter-parent-data.page';

const routes: Routes = [
  {
    path: '',
    component: EnterParentDataPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnterParentDataPageRoutingModule { }
