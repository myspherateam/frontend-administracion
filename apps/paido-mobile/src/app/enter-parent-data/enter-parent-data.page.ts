import { AlertController, MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { LocalStorageService } from '../services/storage-service';

@Component({
  selector: 'app-enter-parent-data',
  templateUrl: './enter-parent-data.page.html',
  styleUrls: ['./enter-parent-data.page.scss'],
})
export class EnterParentDataPage extends AnalyzableComponent implements OnInit {
  protected screenName: string = 'enter-parent-data';

  public birthDate!: string;

  public selectedSex!: string;

  public maxDatetime: string;

  constructor(
    private router: Router,
    public alertController: AlertController,
    private menuController: MenuController,
    private localStorage: LocalStorageService,
  ) {
    super();
    this.menuController.enable(false);
    this.maxDatetime = new Date().toISOString();
  }

  onDataAdded() {
    this.selectedSex = '';
    this.birthDate = '';
    // show register confirmed
    this.presentAlert();
  }

  async presentAlert(): Promise<void> {
    this.localStorage.storeOnLocalStorage('signIn', true);
    const alert = await this.alertController.create({
      header: 'Creado',
      subHeader: 'Registro realizado correctamente',
      message: 'El usuario ha sido creado. Pulse Acceder para entrar en la aplicación.',
      buttons: [{
        text: 'Acceder',
        handler: () => {
          this.router.navigateByUrl('/login');
        },
      }],
    });

    return alert.present();
  }
}
