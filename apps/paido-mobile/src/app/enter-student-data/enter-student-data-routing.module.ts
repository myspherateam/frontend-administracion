import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EnterStudentDataPage } from './enter-student-data.page';

const routes: Routes = [
  {
    path: '',
    component: EnterStudentDataPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnterStudentDataPageRoutingModule { }
