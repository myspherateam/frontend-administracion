import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

import { AnalyzableComponent } from '../utils/analyzable-compoment';

@Component({
  selector: 'app-enter-student-data',
  templateUrl: './enter-student-data.page.html',
  styleUrls: ['./enter-student-data.page.scss'],
})
export class EnterStudentDataPage extends AnalyzableComponent implements OnInit {
  protected screenName: string = 'enter-student-data';

  public birthDate!: string;

  public selectedSex!: string;

  public maxDatetime: string;

  constructor(
    private router: Router,
    private menuController: MenuController,
  ) {
    super();
    this.menuController.enable(false);
    this.maxDatetime = new Date().toISOString();
  }

  onDataAdded() {
    this.selectedSex = '';
    this.birthDate = '';
    this.router.navigateByUrl('enter-parent-data');
  }
}
