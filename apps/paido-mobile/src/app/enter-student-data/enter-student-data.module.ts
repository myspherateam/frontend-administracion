import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

import { EnterStudentDataPage } from './enter-student-data.page';
import { EnterStudentDataPageRoutingModule } from './enter-student-data-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnterStudentDataPageRoutingModule,
  ],
  declarations: [EnterStudentDataPage],
})
export class EnterStudentDataPageModule { }
