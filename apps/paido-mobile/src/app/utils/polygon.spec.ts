import { Coordinate } from 'tsgeo/Coordinate';
import { Polygon } from 'leaflet';
import { polygonContains } from './polygon';

const poly: Polygon = new Polygon([
  [38.996056, -1.850246],
  [38.99689, -1.851265],
  [38.997174, -1.851625],
  [38.997032, -1.851963],
  [38.996419, -1.851888],
  [38.996086, -1.851679],
  [38.995877, -1.850992],
  [38.995393, -1.850118],
  [38.994726, -1.849319],
  [38.994075, -1.84844],
  [38.994238, -1.848161],
  [38.994388, -1.848225],
  [38.995226, -1.849255],
]);
describe('Polygon utils', () => {

  describe('polygonContains', () => {
    it('should return true when point inside the polygon', () => {
      const point: Coordinate = new Coordinate(38.996034922746375, -1.8507966906306919);
      const inside = polygonContains(poly, point);
      expect(inside).toBeTruthy();

    });

    it('should return false when the point is outside the polygon', () => {
      const point: Coordinate = new Coordinate(38.996823415486546, -1.8493548473172474);
      const inside = polygonContains(poly, point);
      expect(inside).toBeFalsy();

    });
  });
});
