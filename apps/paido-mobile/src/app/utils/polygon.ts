import { Coordinate } from 'tsgeo/Coordinate';
import { LatLng } from 'leaflet';
import { Polygon } from 'tsgeo/Polygon';

export function polygonContains(layer: L.Polygon, point: Coordinate): boolean {
  const poly = new Polygon();
  (layer.getLatLngs() as LatLng[][])[0]?.forEach((vertix: L.LatLng) => {
    poly.addPoint(new Coordinate(vertix.lat, vertix.lng));
  });
  return poly.contains(point);
}
