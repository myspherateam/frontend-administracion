import { TranslateService } from '@ngx-translate/core';
import { displayNameComponents } from '@paido/data-model';

export function getLocalizedChildUsername(translate: TranslateService, username: string): string | undefined {

  if (!username) return;
  const parts = displayNameComponents(username);
  const animal = parts[0] ? translate.instant('ANIMALS.' + parts[0].toUpperCase()) : '';
  const color = parts[1] ? translate.instant('COLORS.' + parts[1].toUpperCase()) : '';
  return translate.instant('DISPLAY_NAME', { animal, color }) + parts[2];
}
