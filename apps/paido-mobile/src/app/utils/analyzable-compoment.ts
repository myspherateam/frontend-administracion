import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { Subject, takeUntil } from 'rxjs';
import { Capacitor } from '@capacitor/core';
import { FirebaseAnalytics } from '@capacitor-community/firebase-analytics';

@Component({ template: '' })
export abstract class AnalyzableComponent implements OnInit, OnDestroy {

    protected abstract readonly screenName: string;

    protected readonly translate: TranslateService = inject(TranslateService);

    protected locale: string = this.translate.currentLang;

    protected finalize$: Subject<void> = new Subject();;

    async ngOnInit(): Promise<void> {
        if (Capacitor.isNativePlatform()) {
            await FirebaseAnalytics.logEvent({ name: 'select_content', params: { content_type: 'page_view', item_name: this.screenName } });
            await FirebaseAnalytics.setScreenName({ screenName: this.screenName });
        }
        this.translate.onLangChange.pipe(
            takeUntil(this.finalize$),
        ).subscribe({
            next: (evt: LangChangeEvent) => {
                this.locale = evt.lang;
            },
        });
    }

    ngOnDestroy(): void {
        this.finalize$.next();
        this.finalize$.complete();
    }
}
