export class PAIDO_EVENTS {
    /**
     * Event fired when a challenge is completed. As payload, teh event carries the Challenge object
     */
    public static readonly CHALLENGE_COMPLETED = 'challenge:completed';

    /**
     * Event fired when the number of challenges changes, either because new challenges were added or they were completed
     * This event does not carry payload
     */
    public static readonly CHALLENGE_COUNT_CHANGED = 'count_changed:completed';

    public static readonly CHALLENGE_LIST_RECEIVED = 'challenge:list';

    /**
     * Event fired when the stored refresh token is expired
     */
    public static readonly REFRESH_TOKEN = 'refresh_token:expired';

    /**
     * Event fired when the actual user id changes in the app.
     * This event does not carry payload.
     */
    public static readonly ROLE_CHANGE = 'role:changed';
}
