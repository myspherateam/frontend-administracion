import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { ModalInfoComponent } from '../my-insignias/modal-info/modal-info.component';
import { Toast } from './dto/toast';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class PresentatorService {

  constructor(
    private alertController: AlertController,
    private toastController: ToastController,
    private modalController: ModalController,
    private translate: TranslateService,
  ) { }

  /**
   * Information alert without user interaction
   */
  async showInfoAlert(titleKey: string, messageKey: string): Promise<boolean> {
    let result: boolean = false;
    const alert = await this.alertController.create({
      header: this.translate.instant(titleKey),
      message: this.translate.instant(messageKey),
      buttons: [{
        text: 'OK',
        handler: () => {
          result = true;
        },
      }],
      backdropDismiss: false, // If false, wont make background clicking or escape key press dismiss the alert
    });

    await alert.present();
    await alert.onDidDismiss<boolean>();
    return result;
  }

  /**
   * Accept / Cancel alert. Returns a Promise.
   */
  async createYesNoAlert(titleKey: string, messageKey: string): Promise<boolean> {
    let result: boolean = false;
    const alert: HTMLIonAlertElement = await this.alertController.create({
      header: this.translate.instant(titleKey),
      message: this.translate.instant(messageKey),
      buttons: [{
        text: this.translate.instant('ACTION.CANCEL'),
        handler: () => {
          result = false;
        },
      }, {
        text: this.translate.instant('ACTION.ACCEPT'),
        handler: () => {
          result = true;
        },
      }],
      backdropDismiss: false,
    });

    await alert.present();
    await alert.onDidDismiss<boolean>();
    return result;

  }

  /**
   * Shows a Toast with a determined lifetime in seconds.
   */
  async showToast(toastDTO: Toast) {
    const toast: HTMLIonToastElement = await this.toastController.create({
      message: toastDTO.messageValues ?
        this.translate.instant(toastDTO.messageKey, toastDTO.messageValues) :
        this.translate.instant(toastDTO.messageKey),
      duration: toastDTO.durationInSeconds * 1000,
      color: toastDTO.color,
      position: toastDTO.position,

      buttons: toastDTO.closeButtonMode === 'buttonClosesToast' ?
        [{ side: 'end', icon: 'close', text: this.translate.instant('ACTION.CLOSE'), handler: () => toast.dismiss() }] : undefined,
    });
    toast.present();
  }

  /**
   * Show helper info alert
   */
  async showHelpAlert(titleKey: string, messageKey: string, titleValues?: object, messageValues?: object) {
    const alert = await this.modalController.create({
      component: ModalInfoComponent,
      componentProps: {
        header: titleValues ? this.translate.instant(titleKey, titleValues) : this.translate.instant(titleKey),
        message: messageValues ? this.translate.instant(messageKey, messageValues) : this.translate.instant(messageKey),
      },
    });

    alert.present();
  }
}
