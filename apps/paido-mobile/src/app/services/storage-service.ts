import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LocalStorageService {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService, private translate: TranslateService) { }

    storeOnLocalStorage<T>(key: string, value: T): void {
        this.storage.set(key, value);
    }

    getFromLocalStorage<T>(key: string): T | undefined {
        return this.storage.get(key);
    }

    containsKey(key: string) {
        return this.storage.has(key);
    }

    logOut() {
        if (this.storage.has('access_token')) {
            this.storage.remove('access_token');
        }
        if (this.storage.has('refresh_token')) {
            this.storage.remove('refresh_token');
        }
    }

    isSignIn() {
        return this.storage.get('signIn') || false;
    }

    signOut() {
        this.storage.clear();
    }

    remove(key: string) {
        this.storage.remove(key);
    }

    getChildDisplayName(): string | undefined {
        return this.getFromLocalStorage<string>('childDisplayId') ?? undefined;
    }
}
