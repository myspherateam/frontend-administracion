type Colors = 'primary' | 'secondary' | 'tertiary' | 'success' | 'warning' | 'danger' | 'light' | 'medium' | 'dark';
type Position = 'bottom' | 'middle' | 'top';
type CloseButtonTypes = 'textButtonClosesToast' | 'buttonClosesToast';

export class Toast {
    private _messageKey: string = '';

    private _messageValues?: Record<string, unknown>;

    private _durationInSeconds = 5;

    /**
     * If provided, it takes priority over default app css (global.scss).
     * If null defaults to CSS rules.
     */
    private _color: Colors = 'primary';

    /**
     * If 'textButtonClosesToast' a text button will dismiss,
     * if 'buttonClosesToast' the typical button (X) will,
     * else (null) no action to dismiss (default)
     */
    private _closeButtonMode: CloseButtonTypes = 'buttonClosesToast';

    private _position: Position = 'top';

    /**
     * Creates a basic Toast DTO (Text and default CSS)
     */
    static message(
        messageKey: string, messageValues?: Record<string, unknown>, color: Colors = 'secondary', position: Position = 'bottom'): Toast {
        const t = new Toast();
        t._messageKey = messageKey;
        t._messageValues = messageValues;
        t._color = color;
        t._position = position;
        return t;
    }

    get messageKey() { return this._messageKey; }

    get messageValues() { return this._messageValues; }

    get durationInSeconds() { return this._durationInSeconds; }

    get color() { return this._color; }

    set color(color: Colors) {
        this._color = color;
    }

    get closeButtonMode() { return this._closeButtonMode; }

    set closeButtonMode(mode: CloseButtonTypes) { this._closeButtonMode = mode; }

    get position() { return this._position; }

    set position(pos: Position) { this._position = pos; }
}
