import { Injectable, inject } from '@angular/core';
import {
  LocalNotificationSchema, LocalNotifications, PendingLocalNotificationSchema, PendingResult, PermissionStatus,
} from '@capacitor/local-notifications';
import { TranslateService } from '@ngx-translate/core';
import { formatDate } from '@angular/common';

import { ChallengeCompletion, User } from '@paido/data-model';

import { Events } from './events-service';
import { LocalStorageService } from './storage-service';
import { PAIDO_EVENTS } from '../utils/constants';
import { getLocalizedChildUsername } from '../utils/username';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class LocalNotificationService {

  private events: Events = inject(Events);

  private translate: TranslateService = inject(TranslateService);

  private storage: LocalStorageService = inject(LocalStorageService);
  private platform: Platform = inject(Platform);

  constructor() 
  {
    LocalNotifications.checkPermissions().then((permissionStatus: PermissionStatus) => {
      if (permissionStatus.display === 'granted') {
        this.initialize();
      }else{
        LocalNotifications.requestPermissions().then((status)=>{
          if(status.display === 'granted'){
            this.initialize();
          }
        });
      }
    });
  }

  private initialize() {

    this.platform.ready().then(()=>{
      this.events.subscribe(PAIDO_EVENTS.CHALLENGE_LIST_RECEIVED,  (challenges: ChallengeCompletion[]) => {
        const isChild: boolean = this.isAChild();
        const userId: string = (this.storage.getFromLocalStorage<User>('user') as User).id;
        this.clearNotificationsFor(userId);
        challenges.forEach((challenge: ChallengeCompletion) => {
          const challenegForChild: boolean = challenge.assignedFor.includes('child');
          const challegeForParent: boolean = challenge.assignedFor.includes('parent');
          if (!challenge.active && !challenge.completed) {
            if (!isChild && challegeForParent) {
              // we are in the parent's mobile and the challenge is for a parent
              this.scheduleChallengeStart(challenge, userId, false);
            }
            if (challenegForChild) {
              // either parents mobile or child's mobile, but challenge for child
              this.scheduleChallengeStart(challenge, challenge.forId, true);
            }
          }
          if (challenge.active && !challenge.completed) {
            if (!isChild && challegeForParent) {
              this.schedulleChallengeEndReminder(challenge, userId, false);
            }
            if (challenegForChild) {
              this.schedulleChallengeEndReminder(challenge, challenge.forId, true);
            }
          }
        });
      });
  
      this.events.subscribe(PAIDO_EVENTS.CHALLENGE_COMPLETED, (challenge: ChallengeCompletion) => {
        // when a challenge is completed, cancel all pending notifications
        this.cancelChallengeNotifications(challenge);
        // TODO: differentiate between parent/child to remove only the challenge for the user who completed it
      });
    })
  }

  private maxIntDate():number{
    //this generate id taking last 9 digits from date
    const date = Date.now().toString();
    return Number.parseInt(date.slice(-9));
  }

  private getPendingNotifications(){
    return this.platform.ready().then(async ()=>{
      const pending = (await LocalNotifications.getPending())
      return pending.notifications
    })
  }
  
  /**
   * Cancels notifications for a specific user id
   * @param id
   */
  private async clearNotificationsFor(id: string) {
    const cancel = (await this.getPendingNotifications()).filter((notif) => notif.extra.for === id)

    if (cancel.length) {
      await LocalNotifications.cancel({ notifications: cancel });
    }

}

  /**
   * Creates a notification to inform the user the day a challenge becomes active
   * @param challenge
   * @param when
   */
  private async scheduleChallengeStart(challenge: ChallengeCompletion, forId: string, isChild: boolean) {
    const pending = await this.getPendingNotifications();
    this.platform.ready().then(async ()=>{
      if (Date.parse(challenge.periodStartDatetime) > Date.now() &&
        !pending.some((notification: PendingLocalNotificationSchema) => {
          return notification.extra?.id === challenge.id &&
            notification.extra?.type === 'start' &&
            notification.extra?.for === forId;
        })) {

        // Schedule at 8AM, local time
        const scheduleAt: Date = new Date(Date.parse(challenge.periodStartDatetime));
        scheduleAt.setHours(8);
        scheduleAt.setMinutes(0);
        scheduleAt.setSeconds(0);
        scheduleAt.setDate(scheduleAt.getDate() - 1);

        try {
          let textBody: string = '';
          let title: string = '';
          let username = getLocalizedChildUsername(this.translate, challenge.displayName);
          username = username !== 'undefined' ? username : challenge.displayName;
          if (isChild) {
            textBody = this.translate.instant('CHALLENGE_NOTIFICATION.CHILD.END_BODY', {
              name: challenge.title,
              date: formatDate(challenge.periodFinishDatetime, 'longDate', this.translate.currentLang),
            });
            title = this.translate.instant('CHALLENGE_NOTIFICATION.CHILD.END_TITLE', {
              name: username,
            });
          } else {
            textBody = this.translate.instant('CHALLENGE_NOTIFICATION.PARENT.NEW_BODY', {
              title: challenge.title,
              name: username,
              date: formatDate(challenge.periodFinishDatetime, 'longDate', this.translate.currentLang),
            });
            title = this.translate.instant('CHALLENGE_NOTIFICATION.PARENT.NEW_TITLE', {
              name: username,
            });
          }

          await LocalNotifications.schedule({
            notifications: [{
              group: forId,
              threadIdentifier: forId,
              title: title,
              summaryText: textBody,
              largeBody: textBody,
              body: textBody,
              id: this.maxIntDate(),
              schedule: {
                at:  scheduleAt,
                allowWhileIdle: true,
              },
              extra: {
                id: challenge.id,
                type: 'start',
                for: forId,
              },
            }],
          });
        } catch (error) {
          console.error(`Failed schedulling start notification for challenge ${challenge.id}`);
        }
      }
    })

  }

  /**
   * Creates a challenge reminder when the challenge is not completed and 1/2 days before it finishes
   * @param challenge
   * @param when
   */
  private async schedulleChallengeEndReminder(challenge: ChallengeCompletion, forId: string, isChild: boolean) {
    let username = getLocalizedChildUsername(this.translate, challenge.displayName);
    username = username !== 'undefined' ? username : challenge.displayName;
    const pending = await this.getPendingNotifications();
    this.platform.ready().then(async ()=>{
      if (!pending.some((notification: LocalNotificationSchema) => {
        return notification.extra?.id === challenge.id && notification.extra?.type === 'end' && notification.extra?.for === forId;
      })) {
        //const scheduleAt: Date = new Date();
        const scheduleAt: Date = new Date(Date.parse(challenge.periodFinishDatetime));
        scheduleAt.setHours(8);
        scheduleAt.setMinutes(0);
        scheduleAt.setSeconds(0);
        // only schedule reminders if at least 1 day of margin to complete
        if (Date.parse(challenge.periodStartDatetime) < ( scheduleAt).getTime()) {
          let textBody: string = '';
          let title: string = '';
        
          if (isChild) {
            textBody = this.translate.instant('CHALLENGE_NOTIFICATION.CHILD.END_BODY', {
              name: username,
              title: challenge.title,
              points: challenge.points.onlyChild,
              date: formatDate(challenge.periodFinishDatetime, 'longDate', this.translate.currentLang),
            });
            title = this.translate.instant('CHALLENGE_NOTIFICATION.CHILD.END_TITLE', {
              name: username,
            });
          } else {
            textBody = this.translate.instant('CHALLENGE_NOTIFICATION.PARENT.END_BODY', {
              name: username,
              title: challenge.title,
              points: challenge.points.onlyParent,
              date: formatDate(challenge.periodFinishDatetime, 'longDate', this.translate.currentLang),
            });
            title = this.translate.instant('CHALLENGE_NOTIFICATION.PARENT.END_TITLE');
          }
          await LocalNotifications.schedule({
            notifications: [{
              group: challenge.forId,
              threadIdentifier: challenge.forId,
              title: title,
              body: textBody,
              largeBody: textBody,
              summaryText: textBody,
              id: this.maxIntDate(),
              schedule: {
                at: scheduleAt,
                allowWhileIdle: true,
              },
              extra: {
                id: challenge.id,
                type: 'end',
                for: forId,
              },
            }],
          });
        }
      }
    })
  }

  /**
   * Removes all pending notifications related to a specific challenge and user id
   * @param challenge
   */
  private async cancelChallengeNotifications(challenge: ChallengeCompletion) {
    const pending = (await this.getPendingNotifications()).filter((n: PendingLocalNotificationSchema) => n.extra?.id === challenge.id && n.extra?.for === challenge.forId)
    if (pending.length) {
      await LocalNotifications.cancel({ notifications: pending });
    }
  }

  /**
   * Funtion that checks if a logged user is a child or a parent
   * @returns
   */
  private isAChild(): boolean {
    return this.storage.containsKey('childDisplayId');
  }
}
