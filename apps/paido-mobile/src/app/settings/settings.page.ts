import { AlertController, MenuController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { LocalStorageService } from '../services/storage-service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage extends AnalyzableComponent implements OnInit {
  protected screenName: string = 'settings';

  constructor(
    private router: Router,
    private localStorage: LocalStorageService,
    private menuController: MenuController,
    private alertController: AlertController,
    private translateService: TranslateService,
  ) {
    super();
  }

  override async ngOnInit() {
    super.ngOnInit();
    this.menuController.enable(true);
  }

  async showAlertSignOut() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('SETTINGS.SIGN_OUT'),
      message: this.translateService.instant('SETTINGS.SIGN_OUT_DIALOG_MESSAGE'),
      buttons: [
        {
          text: this.translateService.instant('VERBS.CANCEL'),
        },
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            this.signOut();
          },
        },
      ],
    });

    await alert.present();
  }

  signOut() {
    this.localStorage.signOut();
    this.router.navigateByUrl('/register');
  }

  showAbout() {
    this.router.navigateByUrl('/about');
  }
}
