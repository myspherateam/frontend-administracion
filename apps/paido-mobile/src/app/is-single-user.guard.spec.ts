import { TestBed } from '@angular/core/testing';

import { IsSingleUserGuard } from './is-single-user.guard';

describe('IsSingleUserGuard', () => {
  let guard: IsSingleUserGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(IsSingleUserGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
