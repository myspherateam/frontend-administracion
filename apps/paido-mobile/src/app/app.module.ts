import { ErrorHandler, LOCALE_ID, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { BrowserModule } from '@angular/platform-browser';
import { Health } from '@awesome-cordova-plugins/health/ngx';
import { LaunchNavigator } from '@awesome-cordova-plugins/launch-navigator/ngx';
import { LocationAccuracy } from '@awesome-cordova-plugins/location-accuracy/ngx';
import { RouteReuseStrategy } from '@angular/router';
import localeCa from '@angular/common/locales/ca-ES-valencia';
import localeEn from '@angular/common/locales/en';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';

import { HttpLoaderFactory, TranslatorHttpClient } from './api/http-client-translator';
import { PAIDO_ADMIN_URL, PAIDO_AUTH_URL, PAIDO_CHALLENGES_URL } from './api/api-config';
import { ApiService } from './api/api-service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticatorInterceptor } from './api/authenticator-interceptor';
import { GlobalErrorHandlerService } from './global-error-handler.service';
import { HealthAppSetupComponent } from './health-app-setup/health-app-setup.component';
import { LocalNotificationService } from './services/local-notification.service';
import { LocalStorageService } from './services/storage-service';
import { MenuComponent } from './menu/menu.component';
import { environment } from '../environments/environment';

registerLocaleData(localeEs, 'es');
registerLocaleData(localeEn, 'en');
registerLocaleData(localeCa, 'ca');

export function jwtOptionsFactory(storage: LocalStorageService) {
  return {
    tokenGetter: () => {
      return storage.getFromLocalStorage<string>('access_token');
    },
  };
}

@NgModule({
  declarations: [AppComponent, HealthAppSetupComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [LocalStorageService],
      },
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [TranslatorHttpClient],
      },
    }),
    MenuComponent,
  ],
  providers: [
    ApiService,
    LaunchNavigator,
    Geolocation,
    LocationAccuracy,
    Health,
    LocalStorageService,
    LocalNotificationService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LOCALE_ID, useValue: 'es' },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticatorInterceptor,
      multi: true,
    },
    { provide: PAIDO_AUTH_URL, useValue: environment.authUrl },
    { provide: PAIDO_ADMIN_URL, useValue: environment.adminUrl },
    { provide: PAIDO_CHALLENGES_URL, useValue: environment.challengesUrl },
    { provide: ErrorHandler, useClass: GlobalErrorHandlerService },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(translate: TranslateService) {
    translate.setDefaultLang('es');
  }
}
