import { CanActivate, CanLoad, Router, UrlTree } from '@angular/router';
import { Injectable, inject } from '@angular/core';
import { User, UserRole } from '@paido/data-model';
import { Capacitor } from '@capacitor/core';
import { Health } from '@awesome-cordova-plugins/health/ngx';
import { LocalStorageService } from './services/storage-service';

@Injectable({
  providedIn: 'root',
})
export class HealthAppGuard implements CanActivate, CanLoad {

  private localStorage: LocalStorageService = inject(LocalStorageService);

  private health: Health = inject(Health);

  private router: Router = inject(Router);

  async canActivate(): Promise<boolean | UrlTree> {
    if (Capacitor.isNativePlatform()) {
      const available = await this.health.isAvailable();
      const stepsAuthorized = await this.health.isAuthorized([{ read: ['steps'] }]);
      const user: User | undefined = this.localStorage.getFromLocalStorage<User>('user');
      const isParent = user && user?.roles.includes(UserRole.Parent);
      if (isParent && (!available || !stepsAuthorized) ) {
        this.router.navigateByUrl('/setup-health-app');
        return false;
      }
    }

    return true;
  }

  async canLoad(): Promise<boolean> {
    if (Capacitor.isNativePlatform()) {
      const available = await this.health.isAvailable();
      const stepsAuthorized = await this.health.isAuthorized([{ read: ['steps'] }]);
      return available && stepsAuthorized;
    }
    return true;
  }
}
