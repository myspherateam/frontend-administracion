import { CanActivate, Router } from '@angular/router';
import { Injectable, inject } from '@angular/core';

import { User, UserRole } from '@paido/data-model';

import { LocalStorageService } from './services/storage-service';

@Injectable({
  providedIn: 'root',
})
/**
 * This guard intercepts the access to '/home' and checks, when the user role is child, that the user is registerted
 * When not registered, the '/select-user' is activated
 */
export class IsChildRegisteredGuard implements CanActivate {

  private localStorage: LocalStorageService = inject(LocalStorageService);

  private router: Router = inject(Router);

  canActivate(): boolean {

    const user: User | undefined = this.localStorage.getFromLocalStorage<User>('user');
    if (user && user?.roles.includes(UserRole.Child)) {
      const hasUserName: boolean = user.displayName !== undefined;
      if (!hasUserName) {
        this.router.navigateByUrl('/user-selection', { state: { user } });
        return false;
      }
    }
    return true;
  }
}
