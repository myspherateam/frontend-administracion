import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppVersionGuard } from './app-version.guard';
import { HealthAppGuard } from './health-app.guard';
import { HealthAppSetupComponent } from './health-app-setup/health-app-setup.component';
import { IsChildRegisteredGuard } from './is-child-registered.guard';
import { RegisteredDeviceGuard } from './registered-device.guard';
import { UserAlreadyLoggedinGuard } from './user-already-loggedin.guard';
import { environment } from '../environments/environment';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    canActivate: [AppVersionGuard],
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
    canActivate: [IsChildRegisteredGuard, HealthAppGuard],
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then(m => m.RegisterPageModule),
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule),
    runGuardsAndResolvers: 'always',
    canActivate: [RegisteredDeviceGuard, UserAlreadyLoggedinGuard],

  },
  {
    path: 'setup-health-app',
    component: HealthAppSetupComponent,
  },
  {
    path: 'user-selection',
    loadChildren: () => import('./user-selection/user-selection.module').then(m => m.UserSelectionPageModule),
  },
  {
    path: 'enter-student-data',
    loadChildren: () => import('./enter-student-data/enter-student-data.module').then(m => m.EnterStudentDataPageModule),
  },
  {
    path: 'enter-parent-data',
    loadChildren: () => import('./enter-parent-data/enter-parent-data.module').then(m => m.EnterParentDataPageModule),
  },
  {
    path: 'step-detail',
    loadChildren: () => import('./step-detail/step-detail.module').then(m => m.StepDetailPageModule),
  },
  {
    path: 'challenge-news',
    loadChildren: () => import('./challenge-news/challenge-news.module').then(m => m.ChallengeNewsModule),
  },
  {
    path: 'my-insignias',
    loadChildren: () => import('./my-insignias/my-insignias.module').then(m => m.MyInsigniasPageModule),
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule),
  },
  {
    path: 'help',
    loadChildren: () => import('./help/help.module').then(m => m.HelpPageModule),
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule),
  },
  {
    path: 'challenges-list',
    loadChildren: () => import('./challenges-list/challenges-list.module').then(m => m.ChallengesListPageModule),
  },
  {
    path: 'challenge-detail',
    loadChildren: () => import('./challenge-detail/challenge-detail.module').then(m => m.ChallengeDetailModule),
  },
  {
    path: 'challenge-questionnaire',
    loadChildren: () => import('./challenge-questionnaire/challenge-questionnaire.module')
      .then(m => m.ChallengeQuestionnairePageModule),
  },
  {
    path: 'my-challenges',
    loadChildren: () => import('./my-challenges/my-challenges.module').then(m => m.MyChallengesPageModule),
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then(m => m.AboutPageModule),
  },
  {
    path: 'ranking',
    loadChildren: () => import('./ranking/ranking.module').then(m => m.RankingPageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload',
      enableTracing: !environment.production,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
