import { CanActivate, Router } from '@angular/router';
import { Injectable, inject } from '@angular/core';

import { LocalStorageService } from './services/storage-service';

@Injectable({
  providedIn: 'root',
})
export class UserAlreadyLoggedinGuard implements CanActivate {

  private readonly localStorage: LocalStorageService = inject(LocalStorageService);

  private readonly router: Router = inject(Router);

  async canActivate(): Promise<boolean> {
    const signedIn = this.localStorage.getFromLocalStorage<boolean>('signIn');
    if (signedIn) {
      return this.router.navigateByUrl('/home');
    }
    return true;
  }
}
