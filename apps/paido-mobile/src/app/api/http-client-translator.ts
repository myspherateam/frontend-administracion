import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@Injectable({ providedIn: 'root' })
export class TranslatorHttpClient extends HttpClient {
  constructor(handler: HttpBackend) {
    super(handler);
  }
}

export function HttpLoaderFactory(httpClient: TranslatorHttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}
