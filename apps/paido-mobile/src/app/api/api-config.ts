import { InjectionToken } from '@angular/core';

export const PAIDO_AUTH_URL: InjectionToken<string> = new InjectionToken<string>('PAIDO_AUTH_URL');
export const PAIDO_ADMIN_URL: InjectionToken<string> = new InjectionToken<string>('PAIDO_ADMIN_URL');
export const PAIDO_CHALLENGES_URL: InjectionToken<string> = new InjectionToken<string>('PAIDO_CHALLENGES_URL');
