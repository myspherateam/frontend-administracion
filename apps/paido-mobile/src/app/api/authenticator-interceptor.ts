import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { NEVER, Observable, firstValueFrom, from, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { mergeMap } from 'rxjs/operators';

import { ApiService } from './api-service';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_EVENTS } from '../utils/constants';

@Injectable({
    providedIn: 'root',
})
export class AuthenticatorInterceptor implements HttpInterceptor {

    constructor(
        public apiService: ApiService,
        public storageService: LocalStorageService,
        public jwtHelper: JwtHelperService,
        public router: Router,
        public events: Events,
    ) {
        this.jwtHelper = new JwtHelperService();
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        const token: string | undefined = this.storageService.getFromLocalStorage<string>('access_token');
        const refresh_token: string | undefined = this.storageService.getFromLocalStorage<string>('refresh_token');
        const username: string | undefined = this.storageService.getFromLocalStorage<string>('username');
        const password: string | undefined = this.storageService.getFromLocalStorage<string>('password');
        const promise: Promise<Observable<HttpEvent<unknown>>> = new Promise(async (resolve) => {
            if (!request.url.includes('token')) {//si no está pidiendo el token

                if(token && !this.jwtHelper.isTokenExpired(token)){
                        //todo ok
                        request = request.clone({
                            setHeaders: { Authorization: `Bearer ${token}` },
                        });
                 
                }else if(refresh_token && !this.jwtHelper.isTokenExpired(refresh_token) ){
                    //refresh
                    // if token expires
                    const result = await firstValueFrom(this.apiService.refreshToken())
                        .catch((error: HttpErrorResponse) => {
                            console.log(JSON.stringify(error));
                            this.storageService.logOut();
                            this.router.navigateByUrl('/login');
                            this.events.publish(PAIDO_EVENTS.REFRESH_TOKEN, {});
                            resolve(NEVER);
                        });
                        
                    if (result) {  
                        this.storageService.storeOnLocalStorage('access_token', result.access_token);
                        this.storageService.storeOnLocalStorage('refresh_token', result.refresh_token);

                        request = request.clone({
                            setHeaders: { Authorization: `Bearer ${result.access_token}` },
                        });
                    }else{
                  
                            console.log('login failed'); 
                            this.storageService.signOut();
                            this.router.navigateByUrl('/login');
  
                         }
                   
                }else{
                    //login
                    // reset token and send to login
                    if( username && password){
                        const result = await firstValueFrom(this.apiService.login(username, password))
                       .catch((error)=>{
                           console.log('fallo en el autheticator al hacer el login');
                           console.log(JSON.stringify(error));

                           this.storageService.signOut();
                           this.router.navigateByUrl('/login');

                       });
                       if(result){
                   
                               this.storageService.storeOnLocalStorage('access_token', result.access_token);
                               this.storageService.storeOnLocalStorage('refresh_token', result.refresh_token);
                               request = request.clone({
                                   setHeaders: { Authorization: `Bearer ${result.access_token}` },
                               });
                       }else{
                  
                          console.log('login failed'); 
                          this.storageService.signOut();
                          this.router.navigateByUrl('/login');

                       }
                        
                   }else{
                   
                       this.storageService.signOut();
                       this.router.navigateByUrl('/login');

                   } 
                }
                  
            }
            resolve(next.handle(request));
        });

        return from(promise)
            .pipe(
                mergeMap(it => it), 
            );
    }
}
