import { BehaviorSubject, catchError, map, mergeMap, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

import {
    ApiResponse, Badge, ChallengeCompletion, ChallengeProgramming, Entity, Group, Login, PendingCount, Ranking, Register,
    User, UserBadges, NotificationDevice, UpdateNotificationDevice
} from '@paido/data-model';

import { PAIDO_ADMIN_URL, PAIDO_AUTH_URL, PAIDO_CHALLENGES_URL } from './api-config';
import { LocalStorageService } from '../services/storage-service';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class ApiService {

    public accessToken!: string;

    public hasOneUser = new BehaviorSubject<{ hasOneUser: boolean } | null>(null);

    private authUrl: string = inject(PAIDO_AUTH_URL);

    private adminUrl: string = inject(PAIDO_ADMIN_URL);

    private challengeUrl: string = inject(PAIDO_CHALLENGES_URL);

    constructor(
        private http: HttpClient,
        private localStorage: LocalStorageService,
    ) { }

    // API CALLS

    register(registrationCode: string): Observable<ApiResponse<Register>> {
        const params = new HttpParams().set('registrationCode', registrationCode);
        const options = { params };
        return this.http.get<ApiResponse<Register>>(this.adminUrl + '/users/credentials', options);
    }

    login(username: string, password: string): Observable<Login> {
        const httpParams = new HttpParams()
            .set('username', username)
            .set('password', password)
            .set('grant_type', 'password')
            .set('client_id', 'frontend');
        return this.http.post<Login>(
            this.authUrl + `/realms/${environment.keycloakRealm}/protocol/openid-connect/token`,
            httpParams,
        );
    }

    refreshToken(): Observable<Login> {
        const refreshToken = this.localStorage.getFromLocalStorage<string>('refresh_token') as string;
        const httpParams = new HttpParams()
            .set('grant_type', 'refresh_token')
            .set('client_id', 'frontend')
            .set('refresh_token', refreshToken);
        return this.http.post<Login>(
            this.authUrl + `/realms/${environment.keycloakRealm}/protocol/openid-connect/token`,
            httpParams,
        );
    }

    getAuthHeader(): HttpHeaders {
        this.accessToken = this.localStorage.getFromLocalStorage('access_token') as string;
        return new HttpHeaders({ Authorization: 'Bearer ' + this.accessToken });
    }

    // ADMINISTRATION
    getMeMobile(): Observable<ApiResponse<User[]>> {
        return this.http.get<ApiResponse<User[]>>(this.adminUrl + '/me/mobile');
    }

    checkAvailableDisplayName(displayName: string): Observable<ApiResponse<boolean>> {
        const httpParams = new HttpParams()
            .set('displayName', displayName);
        return this.http.get<ApiResponse<boolean>>(this.adminUrl + '/users/available', { params: httpParams });
    }

    /**
     * Auxiliary method that returns the display name of a group inside an entity
     * @param groupId
     * @returns
     */
    getUserGroupDetails(groupId: string): Observable<string> {
        return this.http.get<ApiResponse<Group>>(this.adminUrl + '/groups/' + groupId)
            .pipe(
                map(x => x.message),
                mergeMap((group: Group) => {
                    return this.http.get<ApiResponse<Entity>>(this.adminUrl + '/entities/' + group.entityId).pipe(
                        map(x => x.message),
                        map((entity: Entity) => `${entity.name} - ${group.name}`),
                    );
                }),
                catchError(() => of('')),
            );
    }

    createUser(body: User): Observable<ApiResponse<User>> {
        return this.http.post<ApiResponse<User>>(this.adminUrl + '/users/mobile', body);
    }

    checkCurrentVersion(platform: string, version: string): Observable<number> {
        const queryParams = new HttpParams()
            .set('platform', platform)
            .set('currentVersion', version);
        return this.http.get<ApiResponse<number>>(
            this.adminUrl + '/version/check',
            { params: queryParams },
        ).pipe(
            map(response => response.message),
        );
    }

    registerCurrentVersion(platform: string, version: string): Observable<ApiResponse<User>> {
        const body = {
            platform,
            currentVersion: version,
        };
        return this.http.post<ApiResponse<User>>(this.adminUrl + '/version/register', body);
    }

    /**
     * Get user ranking
     */
    getRanking(childId?: string, field?: string, rankingType?: string): Observable<ApiResponse<Ranking>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        if (field) { queryParams = queryParams.set('field', field); }
        if (rankingType) { queryParams = queryParams.set('rankingType', rankingType); }
        return this.http.get<ApiResponse<Ranking>>(`${this.adminUrl}/me/ranking`, { params: queryParams });
    }

    // CHALLENGES
    /*
     *   filters:
     *   category(prepare, activate, avoid, enjoy, keep)
     *   subcategory(challenges, news, questionnaires, activities)
     *   type(physical, authority, gps, questionnaire, news)
     *   finished(true, false)
     */
    getProgrammingChallenges(
        category?: string,
        subcategory?: string,
        type?: string,
        finished: boolean = false,
    ): Observable<ApiResponse<ChallengeProgramming[]>> {
        let queryParams = new HttpParams()
            .set('finished', String(finished));
        if (category) { queryParams = queryParams.set('category', category); }
        if (subcategory) { queryParams = queryParams.set('subcategory', subcategory); }
        if (type) { queryParams = queryParams.set('type', type); }

        const options = {
            params: queryParams,
        };

        return this.http.get<ApiResponse<ChallengeProgramming[]>>(this.challengeUrl + '/challengeProgramming', options);
    }

    getProgrammingChallengeDetail(programmingId: string): Observable<ApiResponse<ChallengeProgramming>> {
        return this.http.get<ApiResponse<ChallengeProgramming>>(this.challengeUrl + '/challengeProgramming/' + programmingId);
    }

    markAChallengeAsComplete(body: ChallengeCompletion, childId?: string): Observable<ApiResponse<ChallengeCompletion>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.post<ApiResponse<ChallengeCompletion>>(
            this.challengeUrl + '/challengeCompletion', body, { params: queryParams },
        );
    }

    getPendingChallengeProgramming(childId?: string): Observable<ApiResponse<PendingCount>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<PendingCount>>(
            this.challengeUrl + '/challengeCompletion/count', { params: queryParams },
        );
    }

    getCompletionChallenges(
        category?: string,
        subcategory?: string,
        type?: string,
        finished: boolean = false,
        childId?: string,
        completed?: boolean,
    ): Observable<ApiResponse<ChallengeCompletion[]>> {
        let queryParams = new HttpParams()
            .set('finished', String(finished));
        if (category) { queryParams = queryParams.set('category', category); }
        if (subcategory) { queryParams = queryParams.set('subcategory', subcategory); }
        if (type && type !== 'all') { queryParams = queryParams.set('type', type); }
        if (childId) { queryParams = queryParams.set('childId', childId); }
        if (completed) { queryParams = queryParams.set('completed', String(completed)); }

        const options = {
            params: queryParams,
        };

        return this.http.get<ApiResponse<ChallengeCompletion[]>>(this.challengeUrl + '/challengeCompletion', options);
    }

    getCompletionChallengeDetail(completionId: string, childId?: string): Observable<ApiResponse<ChallengeCompletion>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<ChallengeCompletion>>(
            this.challengeUrl + '/challengeCompletion/' + completionId, { params: queryParams },
        );
    }

    /**
     * Get next physical challenge to accomplish
     */
    getNextPhysicalCompletion(childId?: string): Observable<ApiResponse<ChallengeCompletion[]>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<ChallengeCompletion[]>>(
            this.challengeUrl + '/challengeCompletion/nextPhysical', { params: queryParams },
        );
    }

    /**
     * Get user badges
     */
    getUserBadges(userId: string, childId?: string): Observable<ApiResponse<UserBadges>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<UserBadges>>(this.challengeUrl + `/badge/forId/${userId}`, { params: queryParams });
    }

    getNextBadge(type: string, childId?: string): Observable<ApiResponse<Badge>> {
        let queryParams = new HttpParams().set('type', type);
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<Badge>>(this.challengeUrl + '/badge/nextBadge', { params: queryParams });
    }

    /**
     * Get relatives pending challenges
     */
    getRelativesPendingChallenges(childId?: string): Observable<ApiResponse<ChallengeCompletion[]>> {
        let queryParams = new HttpParams().set('inverted', 'true');
        queryParams = queryParams.set('started', 'true');
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<ChallengeCompletion[]>>(
            `${this.challengeUrl}/challengeCompletion`,
            { params: queryParams },
        );
    }

    registerNotificationsDevice(deviceToken:string, userId:string,childId?:string):Observable<ApiResponse<NotificationDevice>>{
        let queryParams = new HttpParams()
        if (childId) { 
            queryParams = queryParams.set('childId', childId); 
        }
        return this.http.post<ApiResponse<NotificationDevice>>(`${this.challengeUrl}/notifications/device`, {token:deviceToken, userId:childId??userId})
    }    
    
    updateNotificationsDevice(oldDeviceToken:string, newDeviceToken:string,userId:string,childId?:string):Observable<ApiResponse<UpdateNotificationDevice>>{
        let queryParams = new HttpParams()
        if (childId) { 
            queryParams = queryParams.set('childId', childId); 
        }
        return this.http.put<ApiResponse<UpdateNotificationDevice>>(`${this.challengeUrl}/notifications/device`, {token:newDeviceToken, userId:childId??userId, oldDeviceToken:oldDeviceToken})
    }
}
