import * as Survey from 'survey-angular';
import { Component, EventEmitter, Input, OnInit, Output, inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import theme from './theme.json';

// We are creating the theme object 'paido' here.
Survey.StylesManager.ThemeColors['paido'] = theme;
Survey.StylesManager.applyTheme('paido');

@Component({
  selector: 'app-survey',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss'],
})
export class QuestionnaireComponent implements OnInit {
  private translate: TranslateService = inject(TranslateService);

  @Output() results: EventEmitter<any> = new EventEmitter<any>();

  @Input() json!: Record<string, unknown>;

  @Input() surveyMode!: string;

  ngOnInit() {
    const surveyModel = new Survey.Model(this.json);

    surveyModel.onComplete.add((result: any, _options: any) => {
      this.results.emit(result.data);
    });

    // 'edit' for editable survey. 'display' for read-only survey
    surveyModel.mode = this.surveyMode;
    surveyModel.locale = this.translate.currentLang;
    const defaultColor = 'default';
    const defaultThemeStyles = Survey.StylesManager.ThemeColors[defaultColor];
    defaultThemeStyles['$body-container-background-color'] = '#ffffff';
    defaultThemeStyles['$text-color'] = '#000';
    Survey.StylesManager.applyTheme();
    Survey.SurveyNG.render('surveyElement', { model: surveyModel });
  }
}
