import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { QuestionnaireComponent } from './questionnaire/questionnaire.component';

@NgModule({
  declarations: [QuestionnaireComponent],
  imports: [
    CommonModule,
  ],
  exports: [QuestionnaireComponent],
})
export class SurveyModule { }
