import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { ChallengeQuestionnairePage } from './challenge-questionnaire.page';
import { ChallengeQuestionnairePageRoutingModule } from './challenge-questionnaire-routing.module';
import { SurveyModule } from './survey/survey.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChallengeQuestionnairePageRoutingModule,
    SurveyModule,
    TranslateModule,
  ],
  declarations: [ChallengeQuestionnairePage],
})
export class ChallengeQuestionnairePageModule { }
