import { AlertController, AnimationController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { filter, map } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ApiResponse, ChallengeCompletion } from '@paido/data-model';

import { AnalyzableComponent } from '../utils/analyzable-compoment';
import { ApiService } from '../api/api-service';
import { Events } from '../services/events-service';
import { LocalStorageService } from '../services/storage-service';
import { PAIDO_EVENTS } from '../utils/constants';

@Component({
  selector: 'app-challenge-questionnaire',
  templateUrl: './challenge-questionnaire.page.html',
  styleUrls: ['./challenge-questionnaire.page.scss'],
})
export class ChallengeQuestionnairePage extends AnalyzableComponent implements OnInit {
  protected screenName: string = 'challenge-questionnaire';

  questionnaireJson!: Record<string, unknown>;

  private challenge!: ChallengeCompletion;

  challengeCompleted: boolean = false;

  constructor(
    public router: Router,
    public apiService: ApiService,
    public localStorage: LocalStorageService,
    public alertController: AlertController,
    private events: Events,
    private translateService: TranslateService,
    private animationController: AnimationController,
  ) {
    super();
    this.challenge = (this.router.getCurrentNavigation()?.extras as { state: { challenge: ChallengeCompletion } }).state.challenge;
    if (this.challenge.questionnaire) {
      this.questionnaireJson = JSON.parse(this.challenge.questionnaire);
    }
  }

  onQuestionnaireResult(event: unknown) {
    this.challengeCompleted = true;

    if (this.challenge.news) {
      this.challenge.newsAnswer = JSON.stringify(event);
    } else {
      this.challenge.questionnaireAnswer = JSON.stringify(event);
    }
  }

  markChallengeAsCompleted(_event: Event) {
    const finishDatetime = new Date(this.challenge.periodFinishDatetime).getTime();
    const startDatetime = new Date(this.challenge.periodStartDatetime).getTime();
    const actualDatetime = new Date().getTime();

    if (actualDatetime >= startDatetime && actualDatetime < finishDatetime) {

      const childId: string | undefined = this.localStorage.getFromLocalStorage<string>('childId');

      const lastAwardedPoints = this.challenge.awardedPoints;

      this.apiService.markAChallengeAsComplete(this.challenge, childId).pipe(
        filter((result: ApiResponse<ChallengeCompletion>) => result.status === 200),
        map((result: ApiResponse<ChallengeCompletion>) => result.message),
      )
        .subscribe({
          next: (challengeCompletion: ChallengeCompletion) => {
            const pointsSaved = challengeCompletion.awardedPoints - lastAwardedPoints;

            this.showAlertDialog(
              this.challenge,
              this.translateService.instant('CHALLENGE_DETAIL.DONE'),
              this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_DONE') +
              pointsSaved +
              this.translateService.instant('CHALLENGE_DETAIL.POINTS'),
              this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_OVERCOME'),
              'completion-modal',
              true,
            );
          },
          error: (error: HttpErrorResponse) => {
            if (error.error) {
              const apiError: ApiResponse<string> = error.error;
              if (apiError.message) {
                const failedQuestions: string[] = JSON.parse(apiError.message);
                const failedMessage = `
              <ul>
              ${failedQuestions.map((x) => '<li>' + x).join('\n')}
               </ul>
               `;
                this.showAlertDialog(
                  this.challenge,
                  this.translateService.instant('ERRORS.TRY_AGAIN'),
                  this.translateService.instant('ERRORS.CHALLENGE_FAILED'),
                  this.translateService.instant('ERRORS.QUESTIONS_FAILED') + '<br/>' + failedMessage,
                  undefined,
                  false,
                  true,
                );
              }
            }
          },
        });

    } else {
      // TODO: inform the challenge is out of date
    }
  }

  async showAlertDialog(challenge: ChallengeCompletion, header: string,
    subHeader: string, message: string, cssClass?: string, animated?: boolean,
    isError: boolean = false) {
    const alert = await this.alertController.create({
      header,
      subHeader,
      message,
      buttons: [{
        text: this.translateService.instant('VERBS.ACCEPT'),
        handler: () => {
          // challenge was completed. Go back to challenges list
          if (!isError) {
            this.events.publish(PAIDO_EVENTS.CHALLENGE_COMPLETED, challenge);
            this.router.navigateByUrl('/home');
          }
        },
      }],
      cssClass,
      animated,
      enterAnimation: (baseEl: Element) => {
        return (this.animationController
          .create()
          .addElement(baseEl.querySelector('.alert-wrapper') as Element)
          .duration(1000)
          .keyframes([
            { offset: 0, transform: 'scale(1)', opacity: '1' },
            { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
            { offset: 1, transform: 'scale(1)', opacity: '1' },
          ]));
      },
    });
    await alert.present();
  }
}
