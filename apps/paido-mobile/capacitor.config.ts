import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.mysphera.paido',
  appName: 'Esc@la Salut',
  webDir: '../../dist/apps/paido-mobile',
  bundledWebRuntime: false,
  plugins: {
    LocalNotifications: {
      smallIcon: 'ic_launcher_round',
    },
    PushNotifications: {
      presentationOptions: ["badge", "sound", "alert"],
    },
  },
  // server: {
  //    url: 'http://192.168.1.131:4200',
  //    cleartext: true,
  //  }
};

export default config;
